using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowerUpgradeMaterialCostTemplate : BaseGUiElement
    {
        [Header("Materials")]
        [SerializeField] private UpgradeCraftItemTemplate wood;
        [SerializeField] private UpgradeCraftItemTemplate clay;
        [SerializeField] private UpgradeCraftItemTemplate iron;

        public void SetTemplate(TowerStock stock)
        {
            wood.SetText(stock.wood.ToString());
            clay.SetText(stock.clay.ToString());
            iron.SetText(stock.iron.ToString());
        }
    }
}
