using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoreUI
{
    public class DifficultySelector : MonoBehaviour
    {
        private MainController controller;

        [Header("Difficulty buttons")]
        [SerializeField]
        private DifficultyButton easyButton;

        [SerializeField]
        private DifficultyButton mediumButton;

        [SerializeField]
        private DifficultyButton hardButton;

        private DifficultyButton currentButton;

        [Space(10)]
        [SerializeField]
        private Button nextButton;
        [SerializeField] private CanvasGroup hoverNextCanvasGroup;


        public void Init(MainController controller)
        {
            this.controller = controller;

            easyButton.EnableButton(true);
            mediumButton.EnableButton(true);
            hardButton.EnableButton(true);

            easyButton.Button.onClick.AddListener(() => { OnButtonClicked(easyButton, 0); });
            mediumButton.Button.onClick.AddListener(() => { OnButtonClicked(mediumButton, 1); });
            hardButton.Button.onClick.AddListener(() => { OnButtonClicked(hardButton, 2); });

            int levelDifficulty = controller.Storage.ReferenceStorage.Settings.difficultyLevel;
            if (levelDifficulty >= 0) //If selected
            {
                DifficultyButton button;
                switch(levelDifficulty)
                {
                    case 0:
                        button = easyButton;
                        break;
                    case 1:
                        button = mediumButton;
                        break;
                    case 2:
                        button = hardButton;
                        break;
                    default:
                        button = mediumButton;
                        break;
                }

                OnButtonClicked(button, levelDifficulty);
            }
        }

        private void OnButtonClicked(DifficultyButton button, int difficulty)
        {
            currentButton?.EnableButton(true);
            currentButton = button;
            currentButton?.EnableButton(false);

            controller.Storage.ReferenceStorage.Settings.difficultyLevel = difficulty;
            nextButton.interactable = true;
            hoverNextCanvasGroup.alpha = 1f;
        }

        public void Deinit()
        {
            easyButton.Button.onClick.RemoveAllListeners();
            mediumButton.Button.onClick.RemoveAllListeners();
            hardButton.Button.onClick.RemoveAllListeners();

            nextButton.interactable = false;
        }
    }
}
