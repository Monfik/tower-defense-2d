using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class GraphicsOutliner
    {
        [SerializeField] private Material outlineMaterial;
        private List<SpriteMaterialPair> pairs;

        private Color colorA;
        private Color colorB;
        private float speed;

        private float currentTime = 0f;

        public void OutlineGraphics(List<SpriteRenderer> renderers, Color colorA, Color colorB, float speed)
        {
            pairs = new List<SpriteMaterialPair>();

            foreach(SpriteRenderer rend in renderers)
            {
                pairs.Add(new SpriteMaterialPair(rend, rend.material));
                rend.material = outlineMaterial;
            }

            this.colorA = colorA;
            this.colorB = colorB;
            this.speed = speed;

            currentTime = 0f;
        }

        public void UpdateOutline()
        {
            currentTime += Time.deltaTime * speed;
            float sin = Mathf.Sin(currentTime);
            float t = Mathf.InverseLerp(-1, 1, sin);
            Color lerpedColor = Color.Lerp(colorA, colorB, t);

            foreach(SpriteMaterialPair pair in pairs)
            {
                pair.renderer.material.SetColor("_Color", lerpedColor);
                
            }

            if (currentTime >= 900f)
                currentTime -= 900f;
        }

        public void FinishOutline()
        {
            foreach(SpriteMaterialPair pair in pairs)
            {
                pair.renderer.material = pair.material;
            }

            pairs.Clear();
        }
    }

    public class SpriteMaterialPair
    {
        public SpriteRenderer renderer;
        public Material material;

        public SpriteMaterialPair(SpriteRenderer renderer, Material material)
        {
            this.renderer = renderer;
            this.material = material;
        }
    }
}
