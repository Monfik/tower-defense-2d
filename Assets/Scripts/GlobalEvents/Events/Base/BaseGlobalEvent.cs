using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public abstract class BaseGlobalEvent : BaseInitable, IGlobalEvent
    {
        [Space]
        public Sprite icon;
        protected UIUpdatableIcon updatableIcon;
        public string eventDescription;
        protected UIUpdatableManager iconsManager;
        protected UnityAction<IGlobalEvent> OnEventFinished;

        [SerializeField] protected string eventName;
        public string EventName => eventName;

        public virtual void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnEventFinished)
        {
            this.OnEventFinished = OnEventFinished;
            this.storage = storage;

            iconsManager = storage.ReferenceStorage.UIIconsManager;
            updatableIcon = iconsManager.CreateIcon(icon, eventDescription, false);
        }

        public virtual void UpdateEvent()
        {

        }

        public virtual void StartToFinishEvent()
        {

        }

        public virtual void FinishEvent()
        {
            iconsManager.ReturnIcon(updatableIcon);
            OnEventFinished?.Invoke(this);
            Destroy(gameObject);
        }
    }
}
