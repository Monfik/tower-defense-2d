using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public abstract class BaseEfficientAttackReducer : MonoBehaviour
    {
        [SerializeField] protected Tower tower;
        public RangeIntInterval range;
        public abstract void Reduce(float efficient);
        public abstract void Restore();
    }

    [System.Serializable]
    public struct RangeIntInterval
    {
        public int start;
        public int end;
    }
    
    [System.Serializable]
    public struct RangeFloat
    {
        public float a;
        public float b;

        public RangeFloat(float a, float b)
        {
            this.a = a;
            this.b = b;
        }

        public float GetRandom(float strength = 1f)
        {
            return Random.Range(a * strength, b * strength);
        }
    }

    [System.Serializable]
    public struct RangeInt
    {
        public int a;
        public int b;

        public RangeInt(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
    }
}
