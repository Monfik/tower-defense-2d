using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITransparable
{
    public void MakeTransparable(float alpha); // 0 - 1
}
