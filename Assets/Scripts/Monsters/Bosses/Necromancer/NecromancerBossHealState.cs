using Data;
using DG.Tweening;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class NecromancerBossHealState : BaseBossState<NecromancerBoss>
    {
        [SerializeField] private float healDuration = 1.5f;
        [SerializeField] private Towers.RangeInt healValue;
        [SerializeField] private Towers.RangeFloat healValuePercentage;

        [SerializeField] private float healInterval = 10f;
        private float nextHealTime;
        [SerializeField] private RangeFloat healIntervalNoise;

        public override void PreInit(NecromancerBoss boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            nextHealTime = Time.time + healInterval + Random.Range(healIntervalNoise.a, healIntervalNoise.b);
        }

        public override void Init(NecromancerBoss boss, DataStorage storage)
        {
            base.Init(boss, storage);
            
            StartHeal();
        }

        public override void Deinit()
        {
            
        }

        public override void UpdateState()
        {
            
        }

        public override bool IsReadyForWork()
        {
            return Time.time >= nextHealTime && boss.Data.hp != boss.HealthBar.MaxValue;
        }

        private void StartHeal()
        {
            boss.Animator.Play("Heal");
            DOVirtual.DelayedCall(healDuration, HealCallback, false);
            DOVirtual.DelayedCall(healDuration / 2, SpeedUpHealAnimation, false);
        }

        private void SpeedUpHealAnimation()
        {
            boss.Animator.SetFloat("HealSpeed", 1.8f);
        }

        private void HealCallback()
        {
            int healToValueAbs = Random.Range(healValue.a, healValue.b);
            int healToValueFabs = Mathf.RoundToInt(boss.HealthBar.MaxValue * Random.Range(healValuePercentage.a, healValuePercentage.b));
            int valueToHeal = healToValueAbs + healToValueFabs;
            Heal(valueToHeal);

            nextHealTime = Time.time + healInterval + Random.Range(healIntervalNoise.a, healIntervalNoise.b);
        }

        private void Heal(int value)
        {
            boss.Heal(value);
            boss.SetDefaultMovementState();
        }
    }
}
