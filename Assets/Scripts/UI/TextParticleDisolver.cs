using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class TextParticleDisolver : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> particles;

        public void Play(Texture2D texture)
        {
            foreach(ParticleSystem system in particles)
            {
                var shape = system.shape;
                shape.texture = texture;
                system.Play();
            }
        }

        [ContextMenu("Play")]
        public void Play()
        {
            particles.ForEach(c => c.Play());
        }
    }
}
