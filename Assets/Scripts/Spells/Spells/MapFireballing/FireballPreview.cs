using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class FireballPreview : MonoBehaviour
    {
        [SerializeField] private LineRenderer lineRenderer;
        [SerializeField] private SpriteRenderer fire;
        [SerializeField] private SpriteRenderer targetX;

        public void UpdatePosition(Vector2 position)
        {
            transform.position = position;
            lineRenderer.SetPosition(0, position);
            position.y = 100f;
            lineRenderer.SetPosition(1, position);
        }
    }
}
