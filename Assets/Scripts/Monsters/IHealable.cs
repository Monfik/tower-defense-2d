using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;

public interface IHealable
{
    public void Heal(int value);
}
