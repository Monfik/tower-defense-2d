using Core;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TowerDefense.Spells
{
    [System.Serializable]
    public class NinjaGhostTravelState : IBaseState<NinjaGhost>
    {
        protected NinjaGhost controller;
        private NinjaGhostData data;
        private PathCreator path;

        private int currentDir; //-1 = backward, 1 = forward
        private float traveledDistanceInDirection = 0;

        [Header("Curves")]
        [SerializeField] private AnimationCurve switchDirectionRisk;

        [Space(10)]
        [SerializeField] private float checkDirectionSwitchTimeInterval = 0.2f;

        private Tween CheckDirTween;

        public void Preinit(NinjaGhost controller)
        {
            currentDir = Random.Range(0, 1) == 0 ? -1 : 1; //random dir
        }

        public void Init(NinjaGhost controller)
        {
            this.controller = controller;
            data = controller.NinjaData;
            path = controller.CurrentPath;

            controller.Collider.enabled = true;

            controller.Animator.SetTrigger("Walk");
            CheckDirTween = DOVirtual.DelayedCall(checkDirectionSwitchTimeInterval, CheckForSwitchDirection, false);
        }

        public void UpdateState()
        {
            Walk();
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(NinjaGhost controller)
        {
            CheckDirTween?.Kill();
        }

        private void Walk()
        {
            Vector3 oldPosition = controller.transform.position;
            float pathDistance = path.path.GetClosestDistanceAlongPath(oldPosition);
            pathDistance += currentDir * data.movementSpeed * Time.deltaTime;
            Vector3 newPosition = path.path.GetPointAtDistance(pathDistance);
            controller.transform.position = newPosition;

            controller.GhostRenderer.transform.rotation = Quaternion.Euler(0f, newPosition.x < oldPosition.x ? 180f : 0f, 0f);

            float traveledDistance = Vector3.Distance(oldPosition, newPosition);
            traveledDistanceInDirection += traveledDistance;
        }

        private int RandomDirection(float distanceToEndPath)
        {
            float pathT = distanceToEndPath / path.path.length;
            float percentagesToSwitch = switchDirectionRisk.Evaluate(pathT) * 100;

            for (int i = 0; i < 10; i++)
            {
                float temp = Random.Range(0, 100);
            }

            float result = Random.Range(0f, 100f);
            if (result <= percentagesToSwitch)
            {
                traveledDistanceInDirection = 0f;
                return currentDir * -1;
            }
            else
                return currentDir;

        }

        private void CheckForSwitchDirection()
        {
            float distanceToEndPath;
            float pathDistance = path.path.GetClosestDistanceAlongPath(controller.transform.position);
            if (currentDir == 1)
            {
                distanceToEndPath = path.path.length - pathDistance;
            }
            else
            {
                distanceToEndPath = pathDistance;
            }

            currentDir = RandomDirection(distanceToEndPath);
            CheckDirTween = DOVirtual.DelayedCall(checkDirectionSwitchTimeInterval, CheckForSwitchDirection, false);
        }

        private bool CanAttack()
        {
            return false;
        }
    }
}
