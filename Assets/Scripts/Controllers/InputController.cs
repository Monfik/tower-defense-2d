using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Data;
using Utils;

namespace CustomInput
{
    public class InputController : MonoBehaviour
    {
        [SerializeField]
        private List<InputPair> inputs;
        public List<InputPair> Inputs => inputs;

        [SerializeField]
        private bool pointerOverUi;
        public bool PointerOverUI => pointerOverUi;

        public void Init(DataStorage storage)
        {
            
        }

        public void AddNewInput(int id, KeyCode key, UnityAction PressCallback, UnityAction HoldCallback, UnityAction ReleaseCallback, UnityAction DoubleClickCallback = null)
        {
            var existingPair = inputs.Find(c => c.id == id);

            if (existingPair != null)
            {
                existingPair.OnButtonPressed += PressCallback;
                existingPair.OnButtonHold += HoldCallback;
                existingPair.OnButtonReleased += ReleaseCallback;
                existingPair.OnDoublePress += DoubleClickCallback;

            }
            else
            {
                InputPair pair = new InputPair(id, key, PressCallback, HoldCallback, ReleaseCallback, DoubleClickCallback);
                inputs.Add(pair);
            }
        }

        private void Update()
        {
            for (int i = 0; i < inputs.Count; i++)
            {
                if (inputs[i] != null)
                    inputs[i].CheckInput();
            }
        }

        public void RemoveInput(int id)
        {
            InputPair pair = inputs.Find(c => c.id == id);
            if (pair != null)
            {
                inputs.Remove(pair);
            }
        }
    }

    [System.Serializable]
    public class InputPair
    {
        public int id;
        public KeyCode keyCode;
        public UnityAction OnButtonPressed;
        public UnityAction OnDoublePress;
        public UnityAction OnButtonHold;
        public UnityAction OnButtonReleased;

        //testing
        //[System.Serializable]
        public class CustEven : UnityEvent { };
        public CustEven Press;
        public CustEven Hold;
        public CustEven Release;

        private float lastPressTime;

        public InputPair(int id, KeyCode key, UnityAction PressCallback, UnityAction HoldCallback, UnityAction ReleaseCallback, UnityAction DoublePressCallback = null)
        {
            this.id = id;
            this.keyCode = key;
            this.OnButtonPressed = PressCallback;
            this.OnButtonHold = HoldCallback;
            this.OnButtonReleased = ReleaseCallback;
            this.OnDoublePress = DoublePressCallback;
        }

        public void CheckInput()
        {
            if (Input.GetKeyDown(keyCode))
            {
                if(OnDoublePress != null)
                {
                    if (Time.unscaledTime - lastPressTime < Values.InputValues.DOUBLE_CLICK_THRESHOLD)
                    {
                        OnDoublePress?.Invoke();
                        lastPressTime = Time.unscaledTime;
                        return;
                    }
                }

                OnButtonPressed?.Invoke();
                Press?.Invoke(); //test
                lastPressTime = Time.unscaledTime;
            }

            if (Input.GetKeyUp(keyCode))
            {
                OnButtonReleased?.Invoke();
                Release?.Invoke(); //Test
            }

            if (Input.GetKey(keyCode))
            {
                OnButtonHold?.Invoke();
                Hold?.Invoke(); //Test
            }
        }

        public void Clear()
        {
            OnButtonPressed = null;
            OnButtonHold = null;
            OnButtonReleased = null;
        }
    }
}