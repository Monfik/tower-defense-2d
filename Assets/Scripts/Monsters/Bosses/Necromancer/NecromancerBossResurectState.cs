using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Creatures
{
    public class NecromancerBossResurectState : BaseBossState<NecromancerBoss>
    {
        public Resurecter resurecter;   

        public override void PreInit(NecromancerBoss boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            resurecter.Preinit(storage, boss);
    }

        public override void Init(NecromancerBoss boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Resurect");
            storage.ReferenceStorage.Postponer.InvokePostponeAction(() => { resurecter.Resurect(FinishResurect); });       
        }

        public override void Deinit()
        {
            
        }

        public void FinishResurect()
        {
            boss.SetDefaultMovementState();
        }

        public override void UpdateState()
        {
            
        }

        public override bool IsReadyForWork()
        {
            return resurecter.CanResurect();
        }
    }

    [System.Serializable]
    public class Resurecter
    {
        [SerializeField] private List<Monster> diedSpawnedMonsters = new List<Monster>();
        [SerializeField] private List<ResurectHandler> currentResurectHandlers = new List<ResurectHandler>();

        [SerializeField]
        private int maxResurects = 2;
        public int MaxResurects => maxResurects;

        [SerializeField] private int currentResurects = 0;

        [SerializeField] private float resurectDuration = 3f;

        [SerializeField] private float resurectDistanceThreshold = 10f;

        public float nextPathResurectLength;
        private Monster owner;
        private DataStorage storage;

        public bool CanResurect()
        {
            //when distance reached, resurect limits didnt reached and some monsters died
            return nextPathResurectLength <= owner.DistanceTravelled && currentResurects < maxResurects && diedSpawnedMonsters.Count != 0;
        }

        public void Preinit(DataStorage storage, Monster owner)
        {
            storage.EventsStorage.CoreEvnts.OnMonsterDied += OnMonsterDied;
            diedSpawnedMonsters = new List<Monster>();
            currentResurects = 0;
            nextPathResurectLength = owner.DistanceTravelled + resurectDistanceThreshold;
            this.owner = owner;
            this.storage = storage;
        }

        private void OnMonsterDied(Monster monster)
        {
            if (diedSpawnedMonsters.Count >= 3)
                return;

            MonsterData resurectData = new MonsterData(monster.Data);
            resurectData.hp = monster.HealthBar.MaxValue;
            Monster resurectedDeactivated = storage.ReferenceStorage.WaveSpawner.SpawnMonster(monster, monster.Path, resurectData, monster.transform.parent.transform, false, false, monster.DistanceTravelled);
            resurectedDeactivated.gameObject.SetActive(false);
            resurectedDeactivated.gameObject.layer = 0;
            resurectedDeactivated.SetSpeed(0f);
            resurectedDeactivated.MakeTransparable(0f);
            diedSpawnedMonsters.Add(resurectedDeactivated);
        }

        public void Resurect(UnityAction Resurected)
        {         
            float resurectAnimationDuration = owner.Animator.GetCurrentAnimatorStateInfo(0).length;
            float animatorSpeed = resurectAnimationDuration / resurectDuration;
            owner.Animator.SetFloat("ResurectionSpeed", animatorSpeed);
            currentResurects++;
            nextPathResurectLength = owner.DistanceTravelled + resurectDistanceThreshold;

            ResurectMonsters(diedSpawnedMonsters);
            diedSpawnedMonsters.Clear();

            Resurected += FinalizeResurect;

            DOVirtual.Float(0f, 1f, resurectDuration, OnResurectUpdate).OnComplete(() => Resurected?.Invoke());
        }

        private void ResurectMonsters(List<Monster> monsters)
        {
            foreach(Monster monster in monsters)
            {
                ResurectHandler newHandler = new ResurectHandler(monster);
                currentResurectHandlers.Add(newHandler);
                monster.gameObject.SetActive(true);
                monster.SetSpeed(0f);
            }
        }

        private void FinalizeResurect()
        {
            foreach (ResurectHandler handler in currentResurectHandlers)
            {
                Monster resurectedMonster = handler.monster;
                resurectedMonster.MakeTransparable(1f);
                resurectedMonster.gameObject.layer = Values.LayerMasks.MonsterLayer;
                resurectedMonster.SetSpeed(resurectedMonster.Data.speed.currentValue);
                storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(resurectedMonster);
            }

            currentResurectHandlers.Clear();
        }

        private void OnResurectUpdate(float value)
        {
            foreach (ResurectHandler handler in currentResurectHandlers)
            {
                handler.monster.MakeTransparable(value);
            }
        }
    }

    [System.Serializable]
    public class ResurectHandler
    {
        public Monster monster;

        public ResurectHandler(Monster monster)
        {
            this.monster = monster;
        }
    }
}
