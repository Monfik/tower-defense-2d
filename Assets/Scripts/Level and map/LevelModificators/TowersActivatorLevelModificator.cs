using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace LevelLogic
{
    public class TowersActivatorLevelModificator : BaseLevelModificator
    {
        [SerializeField] private List<Tower> towersToActivate;
        public List<Tower> TowersToActivate => towersToActivate;

        public override void StartLevel(Level level)
        {
            foreach(Tower tower in towersToActivate)
            {
                ActivateTower(tower);
            }
        }

        public override void FinishLevel(Level level)
        {
            foreach(Tower tower in towersToActivate)
            {
                storage.ReferenceStorage.TowersController.CurrentTowers.Remove(tower);
            }          
        }

        private void ActivateTower(Tower tower)
        {
            tower.Preinit();
            storage.EventsStorage.CoreEvnts.CallOnTowerBuilded(tower);
        }

    }
}
