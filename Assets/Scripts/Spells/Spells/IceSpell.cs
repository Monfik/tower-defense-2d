using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class IceSpell : BaseSpell
    {
        [SerializeField] private IceEffectData iceData;
        [SerializeField] private IceMapEffect icePrefab;
        [SerializeField] private SpriteRenderer icePreview;
        private PathSnapper snapper;

        [Header("Colors")]
        [SerializeField] private Color possibleCastColor;
        [SerializeField] private Color unpossibleCastColor;

        public override void StartCast()
        {
            base.StartCast();
            snapper = storage.ReferenceStorage.PathSnapper;
            icePreview.gameObject.SetActive(true);
            Values.InputValues.POINTER_ON_ACTION = true;
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);

            Vector2 snappedPosition;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);

            if (snapped)
            {
                icePreview.transform.position = snappedPosition;
                icePreview.material.SetColor("_Color", possibleCastColor);
                if (Input.GetKeyDown(KeyCode.Mouse0) && !Values.InputValues.POINTER_OVER_UI)
                {
                    SpawnIce(snappedPosition);
                }
            }
            else
            {
                icePreview.material.SetColor("_Color", unpossibleCastColor);
                icePreview.transform.position = mousePosition;
            }
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Slow", iceData.slowData.slowValue.ToString()));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Ice duration", iceData.iceDuration.ToString() + "s"));

            return data;
        }

        private void SpawnIce(Vector2 position)
        {
            Values.InputValues.POINTER_ON_ACTION = false;
            IceMapEffect ice = Instantiate(icePrefab);
            ice.InitIce(iceData, position);
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(ice);
            PayMana();
            OnSpellCasted?.Invoke(this);
            FinishSpell();
        }
    }
}
