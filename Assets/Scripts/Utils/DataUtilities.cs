using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace Utils
{
    public class DataUtilities 
    {
        public static string GetColorStringByStaticsData(TowerStaticsData data)
        {
            string color = "<color=grey>";

            if (data.currentValue == data.coreValue)
                color = "<color=#cfcebe>";
            else if (data.currentValue < data.coreValue)
                color = "<color=red>";
            else if (data.currentValue > data.coreValue)
                color = "<color=green>";

            return color;
        }

        public static string GetColorStringByStaticsData(TowerStaticsDataFloat data)
        {
            string color = "<color=grey>";

            if (data.currentValue == data.coreValue)
                color = "<color=#cfcebe>";
            else if (data.currentValue < data.coreValue)
                color = "<color=red>";
            else if (data.currentValue > data.coreValue)
                color = "<color=green>";

            return color;
        }

        //public static string ComposeUpgradeGUITextBasedOnBasic(TowerStaticsData basic, TowerStaticsData upgrade)
        //{
        //    upgrade.currentValue += basic.buffValue;
        //    string color = GetColorStringByStaticsData(basic);
        //    string label = "<b> " + color + upgrade.currentValue.ToString() + "</color></b>";
        //    return label;
        //}
    }
}
