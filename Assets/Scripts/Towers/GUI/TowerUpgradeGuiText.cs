using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Towers
{
    public class TowerUpgradeGuiText : BaseGUiElement
    {
        [SerializeField]
        private RectTransform rect;
        public RectTransform Rect => rect;
        [SerializeField]
        private TMP_Text text;
        public TMP_Text Text => text;

        [SerializeField]
        private Image image;
        public Image Image => image;

        [SerializeField]
        private TMP_Text upgradeText;
        public TMP_Text UpgradeText => upgradeText;


        public void Deinit()
        {
            image.gameObject.SetActive(false);
            upgradeText.gameObject.SetActive(false);
        }
    }
}
