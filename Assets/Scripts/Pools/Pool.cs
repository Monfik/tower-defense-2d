using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    [System.Serializable]
    public class Pool<T> where T:MonoBehaviour
    {
        [SerializeField] private Transform poolTransform;
        [SerializeField] private List<T> elements;

        [SerializeField]
        private GameObject poolPrefab;
        public GameObject PoolPrefab => poolPrefab;

        private Queue<T> availableObjects = new Queue<T>();
        public Queue<T> AvailableObject => availableObjects;

        public T GetElement()
        {
            T element = GetPoolObject();
            MonoBehaviour Mono = element as MonoBehaviour;
            GameObject elementGO = Mono.gameObject;
            elementGO.SetActive(true);
            elementGO.transform.localScale = Vector3.one;
            elements.Add(element);
            return element;
        }

        public void Return(T element)
        {
            MonoBehaviour elementGO = element as MonoBehaviour;
            elementGO.gameObject.SetActive(false);
            elementGO.transform.SetParent(poolTransform);
            elements.Remove(element);
            availableObjects.Enqueue(element);
        }

        public void Clear()
        {
            for (int i = elements.Count - 1; i >= 0; i--)
            {
                Return(elements[i]);
            }

            elements.Clear();
        }

        protected virtual void AddObjectToPool()
        {
            var newObj = GameObject.Instantiate(poolPrefab);
            newObj.SetActive(false);
            availableObjects.Enqueue(newObj.GetComponent<T>());
        }

        public T GetPoolObject()
        {
            if (availableObjects.Count == 0)
                AddObjectToPool();

            var poolObject = availableObjects.Dequeue();
            return poolObject;
        }

        public void ReturnToPool(T ob)
        {
            availableObjects.Enqueue(ob);
        }

        public void Destroy()
        {
            while(availableObjects.Count != 0)
            {
                T element = availableObjects.Dequeue();
                GameObject.Destroy(element);
            }
        }
    }
}
