using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Fractions
{
    [CreateAssetMenu(fileName = "Fraction", menuName = "Fractions/Paladin", order = 1)]
    public class Paladin : Fraction
    {

    }
}
