using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Feature.Notifications
{
    public class InfoMarkGenerator : MonoBehaviour
    {
        [SerializeField] private List<BaseInfoMark> marks;
        [SerializeField] private InfoMarkPool markerStaticPool;
        [SerializeField] private RectTransform markersParent;

        [Header("Mark timers")]
        [SerializeField] private float extraTimeForEachChar = 0.1f;
        [SerializeField] private float minTime = 0.5f;

        private Camera mainCamera;

        public void Init(DataStorage storage)
        {
            mainCamera = storage.ReferenceStorage.MainCamera;
        }

        public void Notify(string message, Vector3 startPosition)
        {
            InfoMarkStatic mark = CreateStaticMarker(message, startPosition);
            marks.Add(mark);
        }

        private InfoMarkStatic CreateStaticMarker(string message, Vector3 startPosition)
        {
            InfoMarkStatic marker = markerStaticPool.GetPoolObject() as InfoMarkStatic;
            marker.transform.SetParent(null);
            marker.transform.position = startPosition;
            marker.transform.SetParent(markersParent);
            float markExistTime = minTime + message.Length * extraTimeForEachChar;
            marker.Display(message, startPosition, markExistTime, mainCamera, OnStaticMarkFinished);
            return marker;
        }

        public void UpdateMarks()
        {
            for (int i = 0; i < marks.Count; i++)
            {
                marks[i].UpdateMark(mainCamera);
            }
        }

        public void OnStaticMarkFinished(BaseInfoMark mark)
        {
            mark.Clear();
            marks.Remove(mark);
            markerStaticPool.ReturnToPool(mark);
        }

        public void Deinit()
        {
            for (int i = marks.Count - 1; i >= 0; i--)
            {
                OnStaticMarkFinished(marks[i]);
            }
        }
    }
}
