using System.Collections;
using System.Collections.Generic;
using TowerDefense;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Spells;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class FreezeSpell : BaseSpell
    {
        [SerializeField] private float duration;
        [SerializeField] private FreezeEffect freezeEffectPrefab;

        public override void StartCast()
        {
            base.StartCast();
            FreezeEnemys();
            OnSpellCasted?.Invoke(this);
            OnSpellFinished?.Invoke(this);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Freeze duration", duration.ToString() + "s"));

            return data;
        }

        private void FreezeEnemys()
        {
            foreach (Monster monster in storage.ReferenceStorage.WaveController.AllMonsters)
            {
                FreezeEffect slow = Instantiate(freezeEffectPrefab);
                slow.duration = duration;
                monster.Effects.AddEffect(slow);
            }

            PayMana();
        }

        public override void UpdateSpell()
        {
            
        }
    }
}
