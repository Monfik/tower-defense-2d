using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    public class CastleDestroyer : MonoBehaviour
    {
        [SerializeField] private Castle castle;
        [SerializeField] private Canvas canvas;

        [SerializeField] private List<Animator> animators;

        public void Destroy()
        {
            DisplayEffects(true);
        }

        public void SetDefault()
        {
            DisplayEffects(false);
        }

        private void DisplayEffects(bool display)
        {
            canvas.gameObject.SetActive(!display);

            foreach (Animator animator in animators)
            {
                animator.enabled = display;
                animator.gameObject.SetActive(display);
            }
        }
    }
}
