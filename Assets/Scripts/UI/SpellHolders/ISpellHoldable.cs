using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;

namespace TowerDefense.Spells
{
    public interface ISpellHoldable
    {
        void DisableByMana();
        void EnableByMana();
        void RefreshToDefaultState();
        void SetupTooltipDescription(DescriptionData data);
    }
}
