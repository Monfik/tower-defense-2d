using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    public abstract class RespawnPlaceAnimator : MonoBehaviour
    {
        public abstract void Animate();
    }
}
