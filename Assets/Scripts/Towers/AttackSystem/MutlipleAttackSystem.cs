using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class MutlipleAttackSystem : BaseAttackSystem
    {
        [SerializeField] private List<BaseAttackSystem> attackSystems;

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.Init(attackable, storage);
            }
        }

        public override void UpdateAttackSystem()
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.UpdateAttackSystem();
            }
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            UnityAction BuffAction = null;

            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                UnityAction Buff = attackSystem.BuffDamageByMultiplier(multiplier);
                BuffAction += Buff;
            }

            return BuffAction;
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.ComposeBasicInfo(infoPanel);
            }
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.ComposeUpgradeInfo(infoPanel);
            }
        }

        public override void OnTowerDemolished()
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.OnTowerDemolished();
            }
        }

        public override void Attack()
        {

        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            foreach (BaseAttackSystem attackSystem in attackSystems)
            {
                attackSystem.SetupDamageMultiplier(multiplier);
            }
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            data = base.CreateTooltipData(data);

            foreach (BaseAttackSystem system in attackSystems)
            {
                data = system.CreateTooltipData(data);
            }

            return data;
        }
    }
}
