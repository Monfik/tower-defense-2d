using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class LightingSpell : BaseSpell
    {
        [SerializeField] private SpriteRenderer lightPreview;
        [SerializeField] private Highlight lightHighlight;
        [SerializeField] private Lightning lightningPrefab;
        [SerializeField] private LightningShotData shotData;

        [Header("Colors")]
        [SerializeField] private Color possibleCastColor;
        [SerializeField] private Color unpossibleCastColor;
        private PathSnapper snapper;

        public override void StartCast()
        {
            base.StartCast();
            lightPreview.gameObject.SetActive(true);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            snapper = storage.ReferenceStorage.PathSnapper;
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);

            Vector2 snappedPosition;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);

            if (snapped)
            {
                lightPreview.transform.position = snappedPosition;
                lightPreview.material.SetColor("_Color", possibleCastColor);
                if (Input.GetKeyDown(KeyCode.Mouse0) && !Values.InputValues.POINTER_OVER_UI)
                {
                    SpawnLightning(snappedPosition);
                }
            }
            else
            {
                lightPreview.material.SetColor("_Color", unpossibleCastColor);
                lightPreview.transform.position = mousePosition;
            }       
        }

        private void SpawnLightning(Vector2 position)
        {
            Lightning lightning = Instantiate(lightningPrefab);         
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(lightning);
            lightning.Shot(shotData, position);
            OnSpellCasted?.Invoke(this);
            FinishSpell();
            PayMana();
        }

        public override void CancelSpell()
        {
            base.CancelSpell();
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=0>Lighting", $"{shotData.damage}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Lighting reduction", $"{shotData.lightningReduction}"));

            return data;
        }

        public override void FinishSpell()
        {
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            base.FinishSpell();
        }
    }

    [System.Serializable]
    public class LightningShotData
    {
        public int damage;
        public int lightningReduction;
        public float delay;
        public float hitRadius;
    }
}
