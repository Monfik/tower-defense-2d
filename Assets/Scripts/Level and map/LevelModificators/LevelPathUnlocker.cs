using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Enviroment;
using UnityEngine;

public class LevelPathUnlocker : BaseLevelModificator
{
    [SerializeField] List<TreeMapEntity> trees;
    private int currentCount;

    [SerializeField] private List<PathCreator> pathToUnlock;
    //[SerializeField] private List<PathCreator> pathToLock;

    private Level level;

    public override void FinishLevel(Level level)
    {
        currentCount = 0;

        foreach (TreeMapEntity tree in trees)
        {
            tree.OnBroken -= OnTreeBroken;
        }

        LockPath();
    }

    public override void StartLevel(Level level)
    {
        this.level = level;

        currentCount = 0;

        foreach (TreeMapEntity tree in trees)
        {
            tree.OnBroken += OnTreeBroken;
        }
    }

    private void OnTreeBroken(TreeMapEntity entity)
    {
        currentCount++;

        if(currentCount >= trees.Count)
        {
            UnlockPath();
        }
    }

    private void UnlockPath()
    {
        foreach(PathCreator path in pathToUnlock)
        {
            level.Map.Paths.Add(path);
            level.Map.MiddlePaths.Add(path);
        }
    }

    private void LockPath()
    {
        foreach (PathCreator path in pathToUnlock)
        {
            level.Map.Paths.Remove(path);
            level.Map.MiddlePaths.Remove(path);
        }
    }
}
