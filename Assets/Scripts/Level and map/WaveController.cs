using TowerDefense.Creatures;
using Data;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    public class WaveController : MonoBehaviour
    {
        private DataStorage storage;

        private List<Monster> monsters = new List<Monster>();
        public List<Monster> Monsters => monsters;

        public List<Monster> AllMonsters
        {
            get
            {
                List<Monster> monss = new List<Monster>();
                foreach (Wave wave in currentWaves)
                {
                    monss.AddRange(wave.Monsters);
                }

                return monss;
            }
        }

        [SerializeField]
        private Wave wavePrefab;
        [SerializeField] private Wave lastWave;

        [SerializeField]
        private List<Wave> currentWaves;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.EventsStorage.CoreEvnts.OnMonsterSpawned += OnMonsterSpawned;
            storage.EventsStorage.CoreEvnts.OnMonsterDied += OnMonsterDied;
        }

        private void Update()
        {
#if DEBUG_MODE
          if (Input.GetKeyDown(KeyCode.K))
                KillEveryone();
#endif
        }

        public void KillEveryone()
        {
            foreach(Monster monster in AllMonsters)
            {
                monster.TakeDamage(100000, IDestructable.DamageType.Magic);
            }
        }

        public void LoadWave(WaveData waveData, int waveIndex)
        {
            Wave spawnedWave = Instantiate(wavePrefab, storage.ReferenceStorage.LevelManager.CurrentLevel.transform); //spawn wave
            spawnedWave.name = "Wave " + waveIndex.ToString();
            spawnedWave.Init(waveIndex, waveData);
            currentWaves.Add(spawnedWave);
            lastWave = spawnedWave;
            storage.ReferenceStorage.WaveSpawner.Spawn(waveData, spawnedWave);
        }

        private void OnMonsterSpawned(Monster monster)
        {
            monsters.Add(monster);
            lastWave.Monsters.Add(monster);
        }

        public void UpdateController()
        {
            for (int i = 0; i < monsters.Count; i++)
            {
                monsters[i].UpdateMonster();
            }
        }

        public Wave FindWaveByMonster(Monster monster)
        {
            foreach (Wave wave in currentWaves)
            {
                if (wave.Monsters.Exists(c => c == monster))
                    return wave;
            }

            return null;
        }

        public void RemoveMonsterByFinishedPath(Monster monster)
        {
            int goldWorth = monster.Data.goldWorth / 2; //It shouldnt be here... Move it to LevelController later
            storage.ReferenceStorage.Wallet.Add(goldWorth);
            RemoveMonster(monster);
            Destroy(monster.gameObject);
        }

        public void RemoveMonster(Monster monster)
        {
            monster.transform.SetParent(this.transform);
            monsters.Remove(monster);

            Wave waveToDestroyMonster = null;

            foreach (Wave wave in currentWaves)
            {
                bool exist = wave.Monsters.Exists(c => c == monster);

                if (exist)
                {
                    waveToDestroyMonster = wave;
                    break;
                }
            }

            waveToDestroyMonster.Monsters.Remove(monster);

            if (waveToDestroyMonster.IsFinished())
            {
                storage.EventsStorage.CoreEvnts.OnWaveFinished?.Invoke(waveToDestroyMonster);
                currentWaves.Remove(waveToDestroyMonster);
                Destroy(waveToDestroyMonster.gameObject);
            }

            if (currentWaves.Count == 0)
            {
                storage.EventsStorage.CoreEvnts.CallOnCurrentWavesFinished();
            }
        }

        private void DestroyWaves()
        {
            foreach(Wave wave in currentWaves)
            {
                foreach(Monster monster in wave.Monsters)
                {
                    Destroy(monster.gameObject);
                }

                Destroy(wave.gameObject);
            }

            monsters.Clear();
            currentWaves.Clear();
        }

        private void OnMonsterDied(Monster monster)
        {
            int goldWorth = monster.Data.goldWorth; //It shouldnt be here... Move it to LevelController later
            storage.ReferenceStorage.Wallet.Add(goldWorth);
            RemoveMonster(monster);
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnMonsterSpawned -= OnMonsterSpawned;
            storage.EventsStorage.CoreEvnts.OnMonsterDied -= OnMonsterDied;
            DestroyWaves();
        }
    }
}
