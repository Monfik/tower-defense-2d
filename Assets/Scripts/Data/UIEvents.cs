﻿using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameEvents
{
    public class UIEvents
    {
        public UnityAction<PointerEventData> OnDrag;
        public void CallOnDrag(PointerEventData eventData)
        {
            OnDrag?.Invoke(eventData);
        }

        public UnityAction<PointerEventData> OnScroll;
        public void CallOnScroll(PointerEventData eventData)
        {
            OnScroll?.Invoke(eventData);
        }

        public UnityAction<PointerEventData> OnClick;
        public void CallOnClick(PointerEventData eventData)
        {
            OnClick?.Invoke(eventData);
        }
    }
}