using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class WaveData
    {
        [SerializeField]
        private List<MonsterReleaseData> releasersData;
        public List<MonsterReleaseData> RealeasersData => releasersData;

        public float duration = 20f;

        public WaveData(WaveData data)
        {
            releasersData = new List<MonsterReleaseData>(data.RealeasersData);
            duration = data.duration;
        }

        public WaveData()
        {; }
    }
}
