using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class FreezeEffect : BaseMonsterEffect
    {
        private int oldMinSpeedValue;
        [SerializeField] private Material freezeMaterial;
        private Material originalMaterial;

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);

            oldMinSpeedValue = monster.Data.speed.minValue;
            monster.Data.speed.minValue = 0;
            monster.Data.speed.AddValue(-10000);
            monster.SetSpeed(0f);

            originalMaterial = monster.Renderer.material;
            monster.Renderer.material = freezeMaterial;
        }

        public override void EndEffect()
        {
            base.EndEffect();
            monster.Data.speed.minValue = oldMinSpeedValue;
            monster.Data.speed.AddValue(10000);

            monster.SetSpeed(monster.Data.speed.currentValue);
            monster.Renderer.material = originalMaterial;
        }
    }
}
