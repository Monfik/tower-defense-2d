using Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class StackableSpellHolder : SpellHolder
    {
        [SerializeField] private TMP_Text stacksNumber;

        [SerializeField] protected BaseStackableSpell stackableSpell;

        [Header("Colors")]
        [SerializeField] protected Color disabledSpellColor;
        [SerializeField] protected List<ColorWisePair> artworkColorPairs = new List<ColorWisePair>();

        public override void Init(SpellsController controller, DataStorage storage)
        {
            base.Init(controller, storage);

            if(stackableSpell.StartStacks < stackableSpell.MaxStacks)
            {
                controller.UpdatableSpellHolders.Add(this);
                loadBarsController.Reset(true);
            }

            currentColorWise.bool2 = stackableSpell.StartStacks > 0;
            SetupState(currentColorWise, unlockedButtonWiseValue);
        }

        public override void Use()
        {
            stackableSpell.currentStackAmount--;
            if(stackableSpell.currentStackAmount == 0)
            {
                DisableSpell();
            }
            else if(stackableSpell.currentStackAmount == stackableSpell.MaxStacks - 1)
            {
                controller.UpdatableSpellHolders.Add(this);
                loadBarsController.Reset(true);
            }

            stacksNumber.text = stackableSpell.currentStackAmount.ToString();
        }

        protected override void CastSpell()
        {
            if (Values.InputValues.BUILDING_POINTER)
                return;

            BaseSpell castingSpell = controller.CastSpell(stackableSpell.Spell);
            castingSpell.Init(storage, this, OnSpellCastStarted, OnSpellCasted, OnSpellFInished, OnSpellCanceled);
            castingSpell.StartCast();
        }

        public override void EnableByCooldown()
        {
            stackableSpell.currentStackAmount++;
            ResetButton(true);
            currentColorWise.bool2 = true;
            SetupState(currentColorWise, unlockedButtonWiseValue);

            if (stackableSpell.currentStackAmount == stackableSpell.MaxStacks)
            {
                controller.UpdatableSpellHolders.Remove(this);
                loadBarsController.Reset(false);
                fillImage.fillAmount = 1f;
            }

            stacksNumber.text = stackableSpell.currentStackAmount.ToString();
        }


        private void DisableSpell()
        {
            spellArtwork.color = disabledSpellColor;
            button.interactable = false;
            currentColorWise.bool2 = false;
        }

        protected override void CheckForDisableByMana(int mana)
        {
            if (mana < stackableSpell.ManaCost)
            {
                DisableByMana();
            }
        }

        protected override void CheckForEnableByMana(int mana)
        {
            if (mana >= stackableSpell.ManaCost)
            {
                EnableByMana();
            }
        }

        protected override bool CheckManaAvailability()
        {
            int currentMana = storage.ReferenceStorage.ManaController.CurrentMana;
            return currentMana >= stackableSpell.ManaCost;
        }

        public override void SetupState(Boolwise3 current, Boolwise3 unlock)
        {
            base.SetupState(current, unlock);

            ColorWisePair artworkColor = artworkColorPairs.Find(c => c.wise == current);
            if (artworkColor != null)
            {
                spellArtwork.color = artworkColor.color;
            }
        }
    }
}
