using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class BlueprintState : IBaseState<MainGameState>
    {
        private DataStorage storage;
        public DataStorage Storage => storage;

        public void Init(MainGameState controller)
        {
            storage = controller.Storage;
            storage.ReferenceStorage.BlueprintController.ShowBlueprint();
        }

        public void UpdateState()
        {
            
        }

        public void Deinit(MainGameState controller)
        {
            storage.ReferenceStorage.BlueprintController.HideBlueprint();
        }

        public void FixedUpdateState()
        {
            
        }
    }
}
