using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Tips
{
    public class TipPanel : MonoBehaviour
    {
        [SerializeField] private RectTransform rect;
        public RectTransform Rect => rect;

        [SerializeField] private RectTransform textRect;
        public RectTransform TextRect => textRect;

        [Space(15)]
        [Header("Tip description datas")]
        [SerializeField] private TMP_Text mainTitle;
        [SerializeField] private TMP_Text title;
        [SerializeField] private TMP_Text text;
        public TMP_Text Text => text;
        private List<SpecifiedTipDataRepresentation> specifiedTipRepresentations = new List<SpecifiedTipDataRepresentation>();
        [SerializeField] private SpecifiedTipDataRepresentation specifiedDataRepresentationPrefab;
        [SerializeField] private TMP_Text footer;

        [Space(10)]
        [Header("Tip UI Refs")]
        [SerializeField] private RectTransform mainTitleRect;
        [SerializeField] private RectTransform titleRect;
        [SerializeField] private RectTransform specifiedDatasRect;
        [SerializeField] private RectTransform footerRect;
        [SerializeField] private RectTransform parentRootRect;
        public RectTransform ParentRootRect => parentRootRect;
        [SerializeField] private VerticalLayoutGroup layoutGroup;
        [SerializeField] private ContentSizeFitter sizeFitter;
        [SerializeField] private Image tooltipIcone;
        public Image TooltipIcone => tooltipIcone;

        [Space(10)]
        [SerializeField] private List<GameObject> simpleDescExcludePanels;

        public void SetPosition(Vector2 screenPosition)
        {
            transform.position = screenPosition;
        }

        public void SetTooltip(string text)
        {
            //layoutGroup.enabled = false;
            //sizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            ClearTips();
            this.text.alignment = TextAlignmentOptions.Center;
            this.text.text = text;
            textRect.gameObject.SetActive(true);
            simpleDescExcludePanels.ForEach(c => c.SetActive(false));
        }

        public void SetTooltip(DescriptionData descriptionData)
        {
            this.text.alignment = TextAlignmentOptions.Left;
            textRect.gameObject.SetActive(!string.IsNullOrEmpty(descriptionData.description));
            text.text = descriptionData.description;

            mainTitleRect.gameObject.SetActive(!string.IsNullOrEmpty(descriptionData.title1));
            mainTitle.text = descriptionData.title1;

            titleRect.gameObject.SetActive(!string.IsNullOrEmpty(descriptionData.title2));
            title.text = descriptionData.title2;

            footerRect.gameObject.SetActive(!string.IsNullOrEmpty(descriptionData.footer));
            footer.text = descriptionData.footer;

            ClearTips();
            CreateSpecifiedDatas(descriptionData.specifiedDatas);
            LayoutRebuilder.ForceRebuildLayoutImmediate(parentRootRect);
        }

        private void CreateSpecifiedDatas(List<DescriptionSingleSpecifiedData> datas)
        {
            foreach (DescriptionSingleSpecifiedData data in datas)
            {
                SpecifiedTipDataRepresentation representation = Instantiate(specifiedDataRepresentationPrefab, specifiedDatasRect);
                representation.Setup(data);
                representation.gameObject.SetActive(true);
                specifiedTipRepresentations.Add(representation);
            }
        }

        private void ClearTips()
        {
            for (int i = specifiedTipRepresentations.Count - 1; i >= 0; i--)
            {
                Destroy(specifiedTipRepresentations[i].gameObject);
            }

            specifiedTipRepresentations.Clear();
        }
    }
}
