using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace LevelLogic
{
    public class LevelController : BaseInitable
    {
        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath += MonsterFinishedPath;
            storage.EventsStorage.CoreEvnts.OnBaseDestroyed += OnBaseDestroyed;
        }


        private void MonsterFinishedPath(Monster monster)
        {
            Castle castle = storage.ReferenceStorage.LevelManager.CurrentLevel.Castle;
            if (!castle.Invulnerable)
            {
                int castleHp = castle.TakeHit(monster.Data.towerDmg);
                if (castleHp <= 0)
                    castle.Destroy();
                else
                    storage.ReferenceStorage.WaveController.RemoveMonsterByFinishedPath(monster);
            }
        }

        private void OnBaseDestroyed()
        {
            LevelFinisher lostLevelFinished = new LevelFinisher(storage.ReferenceStorage.LevelManager.CurrentLevel, OnLostLevelsFinishersFinished);
            storage.EventsStorage.CoreEvnts.CallBaseDestroyed(lostLevelFinished);
            storage.ReferenceStorage.levelFinishers.finishables.ForEach(c => lostLevelFinished.AddFinisher(c));
            lostLevelFinished.StartFinishing();
        }

        private void OnLostLevelsFinishersFinished(Level level)
        {
            storage.ReferenceStorage.LevelManager.OrderRepeatLevel();
        }


        public override void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath -= MonsterFinishedPath;
            storage.EventsStorage.CoreEvnts.OnBaseDestroyed -= OnBaseDestroyed;
        }
    }
}