using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View;

namespace CoreUI
{
    public class MainMenuView : BaseView
    {
        [SerializeField] private ViewModelTransitionManager viewManager; 

        [SerializeField]
        private DifficultySelector difficultySelector;

        [SerializeField]
        private FractionSelector fractionSelector;

        public override void Init(MainController controller)
        {
            base.Init(controller);
            difficultySelector.Init(controller);
            fractionSelector.Init(controller);
            viewManager.ShowMenu();
        }

        public void Deinit()
        {
            difficultySelector.Deinit();
            fractionSelector.Deinit();
        }
    }
}
