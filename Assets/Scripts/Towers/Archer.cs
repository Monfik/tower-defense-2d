using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class Archer : MonoBehaviour
    {
        [SerializeField]
        new private SpriteRenderer renderer;
        public SpriteRenderer Renderer => renderer;
    }
}
