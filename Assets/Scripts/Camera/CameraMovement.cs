using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using Core;
using UnityEngine.EventSystems;
using LevelLogic;
using UnityEngine.Events;

namespace CameraHandle
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField]
        new private Camera camera;
        public Camera Cam => camera;

        [Header("Movement By Drag")]
        [SerializeField] private float moveSpeedByDrag = 1f;

        [Header("Movement By Mouse")]
        [SerializeField] private float moveSpeedByMouse = 1f;
        [Range(0, 1)] [SerializeField] private float threshold = 0.1f;

        [Header("Zoom")]
        [SerializeField] private float zoomSpeed = 1f;
        [SerializeField] private float maxZoom = 4f;
        [SerializeField] private float minZoom;
        [SerializeField] private float moveByZoomSpeed = 1f;

        [Space(10)]
        [Header("Default values")]
        [SerializeField] private Vector3 startPosition;
        [SerializeField] private float startZoom = 10f;

        [SerializeField]
        private RectTransform bottomPanelUIPanel;

        private CameraOverrider overrider;
        private bool processUpdate = true;
        private Vector2 mapSize;
        private DataStorage storage;
        
        private UnityAction CurrentAction;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.EventsStorage.UIEvents.OnDrag += OnDrag;
            storage.EventsStorage.UIEvents.OnScroll += OnZoom;
            storage.EventsStorage.CoreEvnts.OnLevelLoaded += OnLevelLoaded;
        }

        public void UpdateCamera()
        {
            CurrentAction?.Invoke();
        }

        private void UpdatePositionByMouse()
        {
            Vector2 mousePosition = Input.mousePosition;
            Vector2 screenSize = new Vector2(Screen.width, Screen.height);
            float factorX = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0, screenSize.x, mousePosition.x));
            float factorY = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0, screenSize.y, mousePosition.y));

            float resultX = 0f;
            float resultY = 0f;
            
            if(factorX <= -1 + threshold)
            {
                resultX = -Mathf.InverseLerp(-1, -1 + threshold, -factorX);
            }
            else if(factorX >= 1 - threshold)
            {
                resultX = Mathf.InverseLerp(1 - threshold, 1, factorX);
            }

            if (factorY <= -1 + threshold)
            {
                resultY = -Mathf.InverseLerp(-1, -1 + threshold, -factorY);
            }
            else if (factorY >= 1 - threshold)
            {
                resultY = Mathf.InverseLerp(1 - threshold, 1, factorY);
            }

            Vector2 movement = new Vector2(resultX, resultY);

            MoveCamera(movement * moveSpeedByMouse);
        }

        private void OnDrag(PointerEventData eventData)
        {
            if (!processUpdate)
                return;

            MoveCamera(-eventData.delta * moveSpeedByDrag);
        }

        private void OnZoom(PointerEventData eventData)
        {
            if (!processUpdate)
                return;

            Zoom(-eventData.scrollDelta);
        }

        public void MoveCamera(Vector2 delta)
        {
            delta *= Time.unscaledDeltaTime;
            Vector3 topLeft = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MapLimits.leftUpperCorner.transform.position;
            Vector3 bottomRight = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MapLimits.rightBottomCorner.transform.position;
            Vector3 bottomScreenPosition = camera.WorldToScreenPoint(bottomRight);
            Vector3 topScreenPosition = camera.WorldToScreenPoint(topLeft);

            float screenOffsetTopY = Screen.height - topScreenPosition.y;
            float screenOffsetBottomY = bottomScreenPosition.y - bottomPanelUIPanel.rect.height;
            if (screenOffsetTopY > 0)
            {
                Vector3 worldTopPoint = camera.ScreenToWorldPoint(new Vector3(0f, Screen.height));
                float yDiff = worldTopPoint.y - topLeft.y;
                delta.y -= yDiff;
            }
            else if (screenOffsetBottomY > 0)
            {
                Vector3 worldBottomPoint = camera.ScreenToWorldPoint(new Vector3(0f, bottomPanelUIPanel.rect.height));
                float yDiff = bottomRight.y - worldBottomPoint.y;
                delta.y += yDiff;
            }

            float screenOffsetRightX = Screen.width - bottomScreenPosition.x;
            float screenOffsetLeftX = topScreenPosition.x;
            if (screenOffsetRightX > 0)
            {
                Vector3 worldRightPoint = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0f));
                float xDiff = worldRightPoint.x - bottomRight.x;
                delta.x -= xDiff;
            }
            else if (screenOffsetLeftX > 0)
            {
                Vector3 worldLeftPoint = camera.ScreenToWorldPoint(new Vector3(0f, 0f));
                float xDiff = topLeft.x - worldLeftPoint.x;
                delta.x += xDiff;
            }

            Vector3 position = new Vector3(camera.transform.position.x + delta.x, camera.transform.position.y + delta.y, -1f);
            camera.transform.position = position;
        }

        public void Zoom(Vector2 scrollDelta)
        {
            scrollDelta *= zoomSpeed * Time.unscaledDeltaTime;
            camera.orthographicSize += scrollDelta.y;
            camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, maxZoom, minZoom);

            Vector2 mousePosition = Input.mousePosition;
            Vector2 screenSize = new Vector2(Screen.width, Screen.height);
            float factorX = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0, screenSize.x, mousePosition.x));
            float factorY = Mathf.Lerp(-1, 1, Mathf.InverseLerp(0, screenSize.y, mousePosition.y));
            Vector2 movement = new Vector2(factorX, factorY);
            
            MoveCamera(movement * moveByZoomSpeed);
        }

        public void ZoomToValue(float value)
        {
            value = Mathf.Clamp(value, maxZoom, minZoom);
            camera.orthographicSize = value;
        }

        private void OnLevelLoaded(LevelData levelData)
        {
            CurrentAction = UpdatePositionByMouse;
            processUpdate = true;

            mapSize = levelData.map.GetSize();
            float maxWidth = 1/((2f * camera.aspect) / mapSize.x);
            float maxHeight = 1 / ((camera.aspect) / mapSize.y);
            float maxOrthographicSize = maxWidth <= maxHeight ? maxWidth : maxHeight;
            minZoom = maxOrthographicSize;

            SetupDefaultPosition();
        }

        public void MoveTo(CameraOverrideData overrideData, UnityAction CameraOverided, bool processAfterFinished = true)
        {
            CameraOverided += () => { ReturnToDefaultMode(processAfterFinished); };
            overrider = new CameraOverrider(overrideData, this, CameraOverided);
            processUpdate = false;
            CurrentAction = UpdateOverider;
        }

        public void MoveTo(CameraOverrider overrider)
        {
            this.overrider = overrider;
            processUpdate = false;
            CurrentAction = UpdateOverider;
        }

        private void UpdateOverider()
        {
            overrider?.UpdateOverrider();
        }

        private void SetupDefaultPosition()
        {
            camera.transform.position = startPosition;
            camera.orthographicSize = Mathf.Clamp(startZoom, maxZoom, minZoom);
        }

        private void ReturnToDefaultMode()
        {
            CurrentAction = UpdatePositionByMouse;
            processUpdate = true;
        }

        private void ReturnToDefaultMode(bool processEnabled)
        {
            CurrentAction = UpdatePositionByMouse;
            processUpdate = processEnabled;
        }

        public void Deinit()
        {
            storage.EventsStorage.UIEvents.OnDrag -= OnDrag;
            storage.EventsStorage.UIEvents.OnScroll -= OnZoom;
            storage.EventsStorage.CoreEvnts.OnLevelLoaded -= OnLevelLoaded;
        }

        public CameraOverrideData overideDataTest;

        [ContextMenu("Test overide")]
        public void TestOverrider()
        {
            MoveTo(overideDataTest, null);
        }
    }

    public class CameraOverrider
    {
        protected CameraOverrideData overideData;
        protected Vector3 startPosition;
        protected Vector3 targetPosition;

        protected float startZoomValue;

        protected CameraMovement camera;
        protected UnityAction OverrideFinished;
        protected float startTime;
        protected float currentTime;

        public CameraOverrider(CameraOverrideData overideData, CameraMovement camera, UnityAction OverrideFinished)
        {
            this.overideData = overideData;
            targetPosition = new Vector3(overideData.targetPosition.x, overideData.targetPosition.y, camera.transform.position.z);
            this.camera = camera;
            this.OverrideFinished = OverrideFinished;

            startPosition = camera.transform.position;
            startZoomValue = camera.Cam.orthographicSize;
            startTime = Time.unscaledTime;
        }

        public virtual void UpdateOverrider()
        {
            currentTime = Time.unscaledTime - startTime;

            if (CheckIfTimeElapsed(currentTime))
            {
                OverrideFinished?.Invoke();
                return;
            }

            UpdatePosition();
            UpdateZoom();
        }

        protected virtual bool CheckIfTimeElapsed(float time)
        {
            return time >= overideData.duration;
        }

        protected void UpdatePosition()
        {
            float timerT = Mathf.InverseLerp(0f, overideData.duration, currentTime);
            float curveValue = overideData.positionCurve.Evaluate(timerT);
            Vector3 position = Vector3.Lerp(startPosition, targetPosition, curveValue);
            Vector3 movementDelta = position - camera.transform.position;

            camera.MoveCamera(movementDelta);
        }

        protected void UpdateZoom()
        {
            float timerT = Mathf.InverseLerp(0f, overideData.duration, currentTime);
            float curveValue = overideData.zoomCurve.Evaluate(timerT);
            float zoomValue = Mathf.Lerp(startZoomValue, overideData.targetZoom, curveValue);

            camera.ZoomToValue(zoomValue);
        }
    }

    public class FixedCameraOverrider : CameraOverrider
    {
        public FixedCameraOverrider(CameraOverrideData overideData, CameraMovement camera, UnityAction OverrideFinished) : 
            base(overideData, camera, OverrideFinished) { }

        public override void UpdateOverrider()
        {
            currentTime = Time.unscaledTime - startTime;

            if (CheckIfTimeElapsed(currentTime))
            {
                return;
            }

            UpdatePosition();
            UpdateZoom();
        }
    }

    [System.Serializable]
    public class CameraOverrideData
    {
        public Vector2 targetPosition;
        public float targetZoom;
        public float duration;
        public AnimationCurve positionCurve;
        public AnimationCurve zoomCurve;
    }
}