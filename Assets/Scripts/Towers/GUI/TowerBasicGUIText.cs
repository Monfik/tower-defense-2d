using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Towers
{
    public class TowerBasicGUIText : BaseGUiElement
    {
        [SerializeField]
        private RectTransform rect;
        public RectTransform Rect => rect;

        [SerializeField] 
        private TMP_Text text;
        public TMP_Text Text => text;


        public void Deinit()
        {
            
        }
    }
}
