using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace LevelLogic
{
    public class RunesActivator : BaseLevelModificator
    {
        [SerializeField] private List<DruidMapActionRune> runes;

        public override void FinishLevel(Level level)
        {
            
        }

        public override void StartLevel(Level level)
        {
            runes.ForEach(c => c.ActiveItself());
        }
    }
}
