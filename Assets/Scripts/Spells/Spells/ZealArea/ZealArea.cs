using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Spells;
using UnityEngine;

namespace TowerDefense.Effects
{
    public class ZealArea : MapEffect
    {
        [SerializeField] private SpriteRenderer area;
        [SerializeField] new private CapsuleCollider2D collider;

        [SerializeField]
        private MonsterBuffEffect additionalDmgMultiplier;
        public MonsterBuffEffect AdditionalDmgMultiplier => additionalDmgMultiplier;

        [SerializeField] private List<DebuffMonsterPair> monsterDebuffs;  

        [Header("Animations")]
        [SerializeField] private ZealAreaAnimationCreator animationCreator;

        public void StartAreaEffect()
        {
            animationCreator.gameObject.SetActive(true);
            collider.enabled = false;
            animationCreator.AnimationFinished += OnAnimationFinished;
        }

        private void AddZealEffect(Monster monster)
        {
            DebuffMonsterPair pair = monsterDebuffs.Find(c => c.monster == monster);
            if(pair != null)
            {
                pair.buffEffect.ResetTimer();
            }
            else
            {
                MonsterBuffEffect debuff = Instantiate(additionalDmgMultiplier);
                monsterDebuffs.Add(new DebuffMonsterPair(monster, debuff));
                monster.Effects.AddEffect(debuff);
            }
        }

        private void RemoveZealEffect(Monster monster)
        {
            DebuffMonsterPair pair = monsterDebuffs.Find(c => c.monster == monster);
            if(pair != null)
            {
                monster.Effects.FinishEffect(pair.buffEffect);
                monsterDebuffs.Remove(pair);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            MonsterLegsCollider monsterLegs = collision.gameObject.GetComponent<MonsterLegsCollider>();

            if (monsterLegs != null)
            {
                AddZealEffect(monsterLegs.Owner);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            MonsterLegsCollider monsterLegs = collision.gameObject.GetComponent<MonsterLegsCollider>();

            if (monsterLegs != null)
            {
                RemoveZealEffect(monsterLegs.Owner);
            }
        }

        public void OnAnimationFinished()
        {
            animationCreator.gameObject.SetActive(false);
            area.gameObject.SetActive(true);
            collider.enabled = true;
            currentTime = 0f;
        }
    }

    [System.Serializable]
    public class DebuffMonsterPair
    {
        public Monster monster;
        public MonsterBuffEffect buffEffect;

        public DebuffMonsterPair(Monster monster, MonsterBuffEffect buffEffect)
        {
            this.monster = monster;
            this.buffEffect = buffEffect;
        }
    }
}
