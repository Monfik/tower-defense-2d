using Data;
using GlobalEvents;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Affectors
{
    public class TowerNightSusceptibleAffector : BaseTowerAffector, INightSusceptible
    {
        [SerializeField] private BrightnessCalculator calc;
        private float lastRangeDebuff;

        public override void Init(Tower target, DataStorage storage)
        {
            base.Init(target, storage);
            lastRangeDebuff = 0;
            storage.ReferenceStorage.Brightness.AddSusceptible(this);
            target.Builder.OnFinishDemolish += RemoveFromNightSusceptibles;
        }

        private void RemoveFromNightSusceptibles()
        {
            storage.ReferenceStorage.Brightness.RemoveSusceptible(this);
        }

        public void Affect(float value)
        {
            target.range.data.BuffByValue(-lastRangeDebuff);
            float brightStrength = calc.GetBrightnessValue(value);
            float rangeCore = target.range.data.coreValue;
            float rangeDebuff = -(1 - brightStrength) * rangeCore;
            target.range.data.BuffByValue(rangeDebuff);
            target.UpdateRange();
            target.GuiManager.guiElementableActionController.CallActionIfExists(target.range);
            lastRangeDebuff = rangeDebuff;
        }
    }
    
    [System.Serializable]
    public class BrightnessCalculator
    {
        public float threshhold;
        [Range(0f, 1f)] public float nightStrength;

        public float GetBrightnessValue(float currentValue) //0 means total dark 1 means full bright
        {
            if (threshhold < currentValue)
                return 1f;

            float normalizedValue = (threshhold - currentValue) * nightStrength;
            normalizedValue = 1 - Mathf.Clamp(normalizedValue, 0f, 1f);
            return normalizedValue;
        }
    }
}
