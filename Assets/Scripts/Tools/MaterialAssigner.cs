using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

public class MaterialAssigner : MonoBehaviour
{
    public Material material;

    [ContextMenu("Assign")]
    public void Assign()
    {
        Tower tower = GetComponent<Tower>();

        foreach(SpriteRenderer renderer in tower.GraphicsSystem.GraphicsData.visualSprites)
        {
            renderer.sharedMaterial = material;
            renderer.sharedMaterial.SetTexture("_Emission", renderer.sprite.texture);
            renderer.sharedMaterial.SetColor("_EmissionColor", Color.white);
            renderer.sharedMaterial.SetFloat("_Intensity", 0);
        }

        foreach(GraphicsData graphicsData in tower.GraphicsSystem.UpgradeData)
        {
            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.sharedMaterial = material;
                renderer.sharedMaterial.SetTexture("_Emission", renderer.sprite.texture);
                renderer.sharedMaterial.SetColor("_EmissionColor", Color.white);
                renderer.sharedMaterial.SetFloat("_Intensity", 0);
            }
        }
    }
}
