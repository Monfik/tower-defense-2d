using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Enviroment;
using UnityEngine;
using Utils;

namespace TowerDefense.Controllers
{
    [System.Serializable]
    public class WindEntitiesManager
    {
        [Header("Wind values")]
        [SerializeField] private AnimationCurve strengthWindEntitiesCountCurve;
        [SerializeField] private float areaForOneEntity;
        [SerializeField] private int maxEntitiesCount;
        [SerializeField] [Range(0f, 0.5f)] private float thresholdToReverseDirection;
        [SerializeField] private AnimationCurve alphaWindCurve;

        [Header("Properties values")]
        [SerializeField] private float randomMaxDelay = 2f;
        [SerializeField] private float minScale = 0.5f;
        [SerializeField] private float maxScale = 1f;
        [SerializeField] private float noiseStrengthFactor = 0.2f;

        [Header("Wind References")]
        [SerializeField] private Pool<WindEntity> pool;
        [SerializeField] private List<WindEntity> currentWinds;

        private MapLimitsData mapLimits;
        private float currentStrength;

        public void Init(DataStorage storage)
        {
            storage.EventsStorage.CoreEvnts.OnLevelStarted += LevelStarted;
            
        }

        private void LevelStarted(Level level)
        {
            mapLimits = level.Map.MapLimits;
            float area = mapLimits.GetArea();
            maxEntitiesCount = Mathf.RoundToInt(area / areaForOneEntity);
        }

        public void HandleWindEntities(float strength)
        {
            currentStrength = strength;
            HandleWindAlpha(currentStrength);
            int properCount = GetProperWindsCount(strength);
            if (currentWinds.Count >= properCount)
                return;

            int countLeft = properCount - currentWinds.Count;
            for (int i = 0; i < countLeft; i++)
            {
                SpawnEntity();
            }
        }

        private void HandleWindAlpha(float strength)
        {
            float alpha = alphaWindCurve.Evaluate(strength);
            alpha = Mathf.Clamp01(alpha);

            foreach(WindEntity wind in currentWinds)
            {
                Material trailsMaterial = wind.ParticleRend.trailMaterial;
                Color trailsColor = trailsMaterial.color;
                trailsColor.a = alpha;
                trailsMaterial.color = trailsColor;
            }
        }

        private void SpawnEntity()
        {
            WindEntity newWindEntity = pool.GetElement();          
            newWindEntity.OnParticleStopped += OnParticleFinished;
            SetupEntity(newWindEntity);
            currentWinds.Add(newWindEntity);
        }

        private void SetupEntity(WindEntity entity)
        {
            Vector2 randomPosition = mapLimits.GetRandomPositionOnMap();
            entity.transform.position = randomPosition;
            var main = entity.Particle.main;
            main.startDelay = Random.Range(0f, randomMaxDelay);
           
            float scaleFactor = Mathf.Lerp(minScale, maxScale, currentStrength);
            float noiseStrength = scaleFactor * noiseStrengthFactor;
            float noiseValue = Random.Range(scaleFactor - noiseStrength, scaleFactor + noiseStrength);
            scaleFactor = scaleFactor + noiseValue;
            Vector3 windScale = Vector3.one * scaleFactor;
            float scaleX;
            float inversedMapPositionX = Mathf.InverseLerp(mapLimits.leftUpperCorner.position.x, mapLimits.rightBottomCorner.position.x, randomPosition.x);
            if (inversedMapPositionX < thresholdToReverseDirection)
                scaleX = -windScale.x;
            else if (inversedMapPositionX > 1f - thresholdToReverseDirection)
                scaleX = windScale.x;
            else
                scaleX = Random.Range(0, 2) == 0 ? -windScale.x : windScale.x;

            windScale.x = scaleX;
            entity.transform.localScale = windScale;
        }

        private void OnParticleFinished(WindEntity entity)
        {
            entity.OnParticleStopped -= OnParticleFinished;
            currentWinds.Remove(entity);
            pool.Return(entity);
            HandleWindEntities(currentStrength);
        }

        private int GetProperWindsCount(float strength)
        {
            if (strength == 0f)
                return 0;

            int count = Mathf.RoundToInt(strengthWindEntitiesCountCurve.Evaluate(strength) * maxEntitiesCount);
            count = Mathf.Clamp(count, 0, maxEntitiesCount);
            return count;
        }

        public void Deinit(DataStorage storage)
        {
            storage.EventsStorage.CoreEvnts.OnLevelStarted -= LevelStarted;
        }
    }
}
