using Data;
using GlobalEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace TowerDefense.Controllers
{
    public class BrightnessController : BaseInitable
    {
        [SerializeField] private float currentBrightness = 0f;
        [SerializeField] private float startBrightness = 1f;
        private float realBrightness;
        private ColorAdjustments adjustments;

        [Header("Limits")]
        [SerializeField] private float minBrightnessValue = -5f;
        [SerializeField] private float maxBrightnessValue = 3f;

        private List<INightSusceptible> susceptibles;
        public List<INightSusceptible> Susceptibles => susceptibles;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            susceptibles = new List<INightSusceptible>();
            storage.ReferenceStorage.PostprocessVolume.profile.TryGet<ColorAdjustments>(out adjustments);
            startBrightness = realBrightness = adjustments.postExposure.value;
        }

        public void AddValue(float value)
        {
            currentBrightness += value;
            realBrightness = startBrightness + currentBrightness;
            adjustments.postExposure.Override(realBrightness);
            PerformCallbacks();
        }

        private void PerformCallbacks()
        {
            susceptibles.ForEach(c => c.Affect(currentBrightness));
        }

        public float GetBrightnessValue()
        {
            return currentBrightness;
        }

        public void AddSusceptible(INightSusceptible susceptible)
        {
            susceptibles.Add(susceptible);
            susceptible.Affect(currentBrightness);
        }

        public void RemoveSusceptible(INightSusceptible susceptible)
        {
            susceptibles.Remove(susceptible);
        }
    }
}
