using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace TowerDefense.Building
{
    public class PreviewController : MonoBehaviour
    {
        private DataStorage storage;
        [SerializeField]
        private Tower preview;

        private Vector2 startDragScreenPos;
        [SerializeField] private float dragTreshold = 25;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.EventsStorage.CoreEvnts.OnTowerPicked += OnTowerPicked;
        }

        private void OnTowerPicked(Tower tower)
        {
            CancelPreview();
            tower.gameObject.layer = Values.LayerMasks.TowerUnaccessableLayer;
            preview = CreatePreview(tower);
            Values.InputValues.BUILDING_POINTER = true;

            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.PREVIEW_ACCEPT_ID, Values.InputValues.DEFAULT_ACCEPT_KEY, BeginDrag, null, OnAccept);
            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.PREVIEW_REJECT_ID, Values.InputValues.DEFAULT_REJECT_KEY, BeginDrag, null, OnReject);
        }

        private void BeginDrag()
        {
            startDragScreenPos = Input.mousePosition;
        }

        private Tower CreatePreview(Tower tower)
        {
            Tower preview = Instantiate(tower);
            preview.gameObject.SetActive(true);
            preview.EnableRangeVisualizer(true);
            preview.Preinit();
            

            return preview;
        }

        public void UpdatePreview()
        {
            if (preview == null)
                return;

            Vector3 VScreen = new Vector3();
            VScreen.x = Input.mousePosition.x;
            VScreen.y = Input.mousePosition.y;
            VScreen.z = Camera.main.transform.position.z;
            Vector3 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(VScreen);
            mousePosition.z = 0f;
            preview.transform.position = mousePosition;

            if (CanBuildOnArea())
            {
                preview.HighlightNormalBuildColor();
            }
            else
            {
                preview.HighlightBlockBuildColor();
            }     
        }
        
        private void OnAccept()
        {
            if (Values.InputValues.POINTER_OVER_UI)
                return;

            if (Vector2.Distance(startDragScreenPos, (Vector2)Input.mousePosition) > dragTreshold)
                return;

            bool canBuild = CanBuildOnArea();

            if(canBuild)
            {
                if(AffordTower(preview))
                {
                    storage.ReferenceStorage.MarkGenerator.Notify("Builded", preview.transform.position);
                    Build(preview);
                }
                else
                {
                    storage.ReferenceStorage.MarkGenerator.Notify("You have no gold", preview.transform.position);
                }
            }
        }

        private bool AffordTower(Tower tower)
        {
            TowerStock stock = tower.Builder.Cost.stock;
            float towerCost = stock.wood * Values.materialCosts.wood + stock.iron * Values.materialCosts.iron + stock.clay * Values.materialCosts.clay;
            int towerCostGold = Mathf.RoundToInt(towerCost);

            return Values.gold >= towerCostGold;
        }

        private void Build(Tower preview)
        {
            preview.gameObject.layer = Values.LayerMasks.TowerLayer;
            Values.InputValues.BUILDING_POINTER = false;
            preview.RangeRenderer.gameObject.SetActive(false);
            storage.EventsStorage.CoreEvnts.CallOnTowerBuilded(preview);
            this.preview = null;
            UnassignInput();
        }

        private bool CanBuildOnArea()
        {
            Collider2D[] colliders = new Collider2D[100];           
            int colliderCount = Physics2D.OverlapCollider(preview.Collider, new ContactFilter2D(), colliders);
            //Debug.Log("Collider count = " + colliderCount);
            return colliderCount == 0;
        }

        private void OnReject()
        {
            if (Vector2.Distance(startDragScreenPos, (Vector2)Input.mousePosition) > dragTreshold)
                return;

            CancelPreview();
        }

        private void CancelPreview()
        {
            if (preview == null)
                return;

            Values.InputValues.BUILDING_POINTER = false;
            storage.EventsStorage.CoreEvnts.CallOnTowerPreviewDeclined(preview);
            Destroy(preview.gameObject);
            preview = null;
            UnassignInput();
        }

        private void UnassignInput()
        {
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.PREVIEW_ACCEPT_ID);
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.PREVIEW_REJECT_ID);
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnTowerPicked -= OnTowerPicked;
            CancelPreview();
        }
    }
}
