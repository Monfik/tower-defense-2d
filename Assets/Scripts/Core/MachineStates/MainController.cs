﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class MainController : MachineState<MainController>
    {
        [SerializeField] private bool MenuOnStart = false;
        [SerializeField]
        private DataStorage storage;
        public DataStorage Storage => storage;

        [SerializeField]
        private MainGameState mainGameState;

        private void Start()
        {
            storage.EventsStorage.CoreEvnts.OnMainGameStateEnter += OnMainGameStateEnter;

            InitReferences();
            if (MenuOnStart)
                ChangeState(new MainMenuState()); //change it after
            else
                ChangeState(mainGameState);
        }

        public void InitReferences()
        {
            storage.ReferenceStorage.WaveSpawner.Init(storage);
            storage.ReferenceStorage.LevelManager.Init(storage);
        }

        private void Update()
        {
            UpdateMachine();
        }

        private void OnMainGameStateEnter()
        {
            ChangeState(mainGameState);
        }

        public void BackToMainMenu()
        {
            storage.ReferenceStorage.PauseController.DisablePauseInput();
            storage.ReferenceStorage.TransitionLoader.LoadByEvent(() => ChangeState(new MainMenuState()));
        }

        #region BaseImplementation
        public override void ChangeState(IBaseState<MainController> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {
            currentState?.UpdateState();
        }
        #endregion

    }

}