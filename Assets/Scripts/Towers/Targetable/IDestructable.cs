using UnityEngine;

namespace TowerDefense.Creatures
{
    public interface IDestructable
    {
        enum DamageType { Physics, Magic };
        void TakeDamage(int dmg, DamageType damageType);
        MonsterData GetData();
    }
}
