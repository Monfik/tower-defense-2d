using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class SpellTargetMarker : MonoBehaviour
    {
        [SerializeField] private List<SpriteRenderer> renderers;
        [SerializeField] private bool markerEnabled;
        public bool MarkerEnabled => markerEnabled;

        public void SetMark(Color color, bool targetable)
        {
            foreach(SpriteRenderer rend in renderers)
            {
                rend.color = color;
            }
            this.markerEnabled = targetable;
        }
    }
}
