using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Effects
{
    public class DamageNotification : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text notificationTMP;

        public TMP_Text NotificationTMP => notificationTMP;

        [SerializeField]
        private Animator animator;

        private void Start()
        {
            AnimatorClipInfo[] clipInfos = animator.GetCurrentAnimatorClipInfo(0);
            Destroy(gameObject, clipInfos[0].clip.length);
        }
    }

}