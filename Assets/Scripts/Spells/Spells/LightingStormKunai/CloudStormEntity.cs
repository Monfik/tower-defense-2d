using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TowerDefense.Spells
{
    public class CloudStormEntity : MonoBehaviour
    {
        [SerializeField] new private SpriteRenderer renderer;

        [Header("Scale")]
        [Range(0.8f, 1.2f)]
        [SerializeField] private float scalingValueA;
        [Range(0.8f, 1.2f)]
        [SerializeField] private float scalingValueB;
        private Vector3 startScale;
        [SerializeField] private float scaleTransitionTime = 0.4f;

        [Header("Alpha color")]
        [SerializeField] private float randomAlphaA;
        [SerializeField] private float randomAlphaB;
 
        [SerializeField] private float alphaTransitionTime = 0.4f;

        [Header("Dark color")]
        [Range(0f, 255f)] [SerializeField] private float colorA;
        [Range(0f, 255f)] [SerializeField] private float colorB;


        [Space(15)]
        [SerializeField] private float animateTimeInterval = 0.8f;
        private Tween TweenToKill;

        public void StartStorm()
        {
            gameObject.SetActive(true);
            startScale = transform.localScale;

            Color targetColor = renderer.color;
            targetColor.a = 0f;
            renderer.color = targetColor;

            Animate();
        }

        private void AnimateRandomColor()
        {
            float randomAlphaTarget = Random.Range(randomAlphaA, randomAlphaB) / 255f;
            float randomColorValue = Random.Range(colorA, colorB);
            Color targetColor = new Color(randomColorValue, randomColorValue, randomColorValue, randomAlphaTarget);
            renderer.DOColor(targetColor, alphaTransitionTime);
        }

        private void AnimateRandomScale()
        {
            float randomScale = Random.Range(scalingValueA, scalingValueB);
            Vector3 randomedScale = startScale * randomScale;
            transform.DOScale(randomedScale, scaleTransitionTime).SetEase(Ease.InBounce);
        }

        private void Animate()
        {
            AnimateRandomColor();
            AnimateRandomScale();
            TweenToKill =  DOVirtual.DelayedCall(animateTimeInterval, Animate, false);
        }

        public void DoColor(Color color, float time)
        {
            DOTween.Kill(renderer);
            renderer.DOColor(color, time);
        }

        public void KillAllTweens()
        {
            DOTween.Kill(transform);
            DOTween.Kill(renderer);
            TweenToKill?.Kill();
        }
    }
}
