using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public abstract class BaseUsableSpell : BaseSpell
    {

        public virtual void OnSpellRejected()
        {

        }
    }
}
