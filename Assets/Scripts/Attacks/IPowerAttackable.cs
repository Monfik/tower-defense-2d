using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace TowerDefense.Power
{
    public interface IPowerAttackable
    {
        void Attack(IDestructable dmgTakable);
        void Attack(IDestructable dmgTakable, float dmgFactor);
    }
}
