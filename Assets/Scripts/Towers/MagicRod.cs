using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicRod : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer rod;
    public SpriteRenderer Renderer => rod;
}
