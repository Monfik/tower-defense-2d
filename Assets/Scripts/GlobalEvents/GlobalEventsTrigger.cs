using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.GlobalEvents
{
    public class GlobalEventsTrigger : MonoBehaviour
    {
        [SerializeField] private GlobalEventsController controller;

        [Header("Events")]
        public BaseGlobalEvent night;
        public BaseGlobalEvent wind;  

        public void TriggerEvent(BaseGlobalEvent ev)
        {
            controller.BeginLevelEvent(ev);
        }
    }
}
