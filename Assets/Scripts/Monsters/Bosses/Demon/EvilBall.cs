using Data;
using DG.Tweening;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Animations;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Creatures
{
    public class EvilBall : Monster
    {
        [SerializeField] private DemonAttackTarget target;
        [SerializeField] private float rotatingSpeed;
        [SerializeField] private float reachDistance = 0.3f;
        public UnityAction<EvilBall> TargetReached;

        private Vector2 lastDirection;

        public void SetupTarget(DemonAttackTarget target)
        {
            this.target = target;
        }

        public override void UpdateMonster()
        {
            Vector3 direction = target.GetCenter() - transform.position;
            Vector3 normalizedDirection = direction.normalized;
            Vector3 movementDelta = normalizedDirection * data.speed.currentValue * Time.deltaTime;
            transform.position = transform.position + movementDelta;
            lastDirection = direction;
            float distance = Vector3.Distance(target.GetCenter(), transform.position);
            if(distance <= reachDistance)
            {
                ReachTarget();
            }
        }

        private void ReachTarget()
        {
            target.Activate();
            TargetReached?.Invoke(this);
            Die();
        }

        private void Update()
        {
            if(dead)
            {
                Vector3 normalizedDirection = lastDirection.normalized;
                Vector3 movementDelta = normalizedDirection * data.speed.currentValue * Time.deltaTime;
                transform.position = transform.position + movementDelta;
            }
        }

        public override void Die()
        {
            dead = true;
            storage.EventsStorage.CoreEvnts.CallOnMonsterDied(this);
            SelectableUnavailable?.Invoke(this);
            SpriteAlphaAnimation alphaAnimation = gameObject.AddComponent<SpriteAlphaAnimation>();
            alphaAnimation.Init(renderer.color.a, 0f, 0.35f, this, () => Destroy(gameObject));
        }
    }
}
