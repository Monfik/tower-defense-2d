using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using UnityEngine;

namespace TowerDefense.GlobalEvents
{
    public interface IWindSusceptible
    {
        void Affect(WindForceData windData);
    }
}
