using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;

namespace TowerDefense.Updatables
{
    public class UIUpdatableManager : MonoBehaviour
    {
        [SerializeField] private UpdatableIconPool icons;
        [SerializeField] private Transform contentIn;
        [SerializeField] private Transform contentOut;

        public UIUpdatableIcon CreateIcon(Sprite sprite, string tooltipDescription, bool enableBars = true)
        {
            UIUpdatableIcon icon = icons.GetPoolObject();
            icon.Artwork.sprite = sprite;
            icon.Fill.sprite = sprite;
            icon.Bars.Enable(enableBars);
            icon.UpdateIcon(0f);
            icon.transform.SetParent(contentIn);
            icon.Tip.SetDescription(DescriptionData.CreateDescription(tooltipDescription));
            icon.gameObject.SetActive(true);
            return icon;
        }

        public void ReturnIcon(UIUpdatableIcon icon)
        {
            icon.transform.SetParent(contentOut);
            icon.gameObject.SetActive(false);
            icons.ReturnToPool(icon);
        }
    }
}
