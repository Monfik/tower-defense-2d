using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace LevelLogic
{
    public class WaveNumberVisualizer : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;

        [SerializeField]
        private TMP_Text text;

        public void ShowWave(int waveNumber)
        {
            gameObject.SetActive(true);
            animator.enabled = true;
            text.text = waveNumber.ToString();
            animator.SetTrigger("Play");
        }

        public void ResetVisualizer()
        {
            gameObject.SetActive(false);
            animator.enabled = false;
        }
    }
}
