using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class AttackableTowerEntity : MonoBehaviour
    {
        [SerializeField]
        new private SpriteRenderer renderer;
        public SpriteRenderer Renderer => renderer;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private Transform attackPoint;
        public Transform AttackPoint {
            get => attackPoint == null? this.transform : attackPoint; }
         

        public void HighlightRedRenderer()
        {
            renderer.color = Color.red;
        }

        public void HighlightNormalRenderer()
        {
            renderer.color = Color.white;
        }

        public void PlayAttackAnimation()
        {
            if(animator != null)
                animator.Play("Attack");
        }
    }
}