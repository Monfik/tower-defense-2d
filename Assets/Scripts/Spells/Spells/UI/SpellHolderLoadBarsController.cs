using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class SpellHolderLoadBarsController : MonoBehaviour
    {
        [SerializeField] private GameObject barsGO;
        [SerializeField] private GameObject directBarParent;

        public void Enable(bool enabled)
        {
            barsGO.SetActive(enabled);
        }

        public void UpdateValue(float value)
        {
            float angle = Mathf.Lerp(0, -360, value);
            Vector3 directBarRotation = new Vector3(0, 0, angle);
            directBarParent.transform.rotation = Quaternion.Euler(directBarRotation);
        }

        public void Reset(bool enabled)
        {
            Enable(enabled);
            UpdateValue(0f);
        }
    }
}
