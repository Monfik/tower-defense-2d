using Data;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class AttackSupportTower : Tower
    {
        [SerializeField]
        private float rangeBuff;

        [SerializeField]
        private float attackSpeedBuff;

        [SerializeField]
        private List<AttackableTower> buffedTowers;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            SearchTowersToBuff();
            BuffTowers();
        }

        protected override void AssignEvents(EventsStorage events)
        {

        }

        private void SearchTowersToBuff()
        {
            foreach(Tower tower in storage.ReferenceStorage.TowersController.CurrentTowers)
            {
                if(Vector2.Distance(tower.transform.position, transform.position) <= realRange)
                {
                    AttackableTower attackable = tower as AttackableTower;

                    if(attackable != null)
                    {
                        buffedTowers.Add(attackable);
                    }
                }
            }
        }

        private void BuffTowers()
        {
            foreach(AttackableTower tower in buffedTowers)
            {
                tower.BuffAttackSpeed(attackSpeedBuff);
                tower.BuffRange(rangeBuff);
            }
        }
    }
}
