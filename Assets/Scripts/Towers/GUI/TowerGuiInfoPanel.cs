using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class TowerGuiInfoPanel : MonoBehaviour
    {
        [SerializeField] 
        private RectTransform rect;
        public RectTransform Rect => rect;

        [SerializeField] private TMP_Text title;
        [SerializeField] private TMP_Text level;
        [SerializeField] private RectTransform content;

        [SerializeField] private float minWidth;
        [SerializeField] private float minHeight;

        [SerializeField] private float additionalHeight = 0.2f;
        [SerializeField] private float additionalWidth = 0.2f;

        [Header("Elements")]
        [SerializeField]
        private TowerGuiElements<TowerBasicGUIText> basicTextes;
        public TowerGuiElements<TowerBasicGUIText> BasicTextes => basicTextes;

        [SerializeField]
        private TowerGuiElements<TowerUpgradeGuiText> upgradeTextes;
        public TowerGuiElements<TowerUpgradeGuiText> UpgradeTextes => upgradeTextes;

        [SerializeField]
        private TowerGuiElements<TowerUpgradeMaterialCostTemplate> upgradeCost;
        public TowerGuiElements<TowerUpgradeMaterialCostTemplate> UpgradeCost => upgradeCost;

        public void Open(Tower tower)
        {
            //UnityAction Resize = () =>
            //{
            //    Vector2 newSize = CalculateSize();
            //    rect.sizeDelta = newSize;
            //};

            //StartCoroutine(PostResize(Resize));

            title.text = tower.TowerName;
            level.text = "Level " + tower.Builder.TowerLevel;
        }

        public void AdjustPanelSize()
        {
            StartCoroutine(AdjustSize());
        }

        private IEnumerator AdjustSize()
        {
            yield return new WaitForEndOfFrame();

            float height = content.rect.height;
            if (height < minHeight)
                height = minHeight;

            Vector2 panelSize = new Vector2(rect.sizeDelta.x, height + -additionalHeight * 2);
            rect.sizeDelta = panelSize;

            content.anchoredPosition = new Vector2(0.2f, -0.2f);
        }

        private void ReturnToPool()
        {
            basicTextes.Clear();
            upgradeTextes.Clear();
            upgradeCost.Clear();
        }

        public void Close()
        {
            ReturnToPool();
        }

        IEnumerator PostResize(UnityAction Resize)
        {
            yield return null;

            Resize?.Invoke();
        }
    }
}
