using Core;
using DG.Tweening;
using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Power;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class NinjaGhost : MachineState<NinjaGhost>
    {
        [Header("Refs")]
        [SerializeField] 
        private SpriteRenderer ghostRenderer;
        public SpriteRenderer GhostRenderer => ghostRenderer;

        [SerializeField]
        private Animator animator;
        public Animator Animator => animator;

        [SerializeField] new private BoxCollider2D collider;
        public BoxCollider2D Collider => this.collider;

        [Space(10)]
        private Map map;
        public Map Map => map;

        private PathCreator currentPath;
        public PathCreator CurrentPath => currentPath;

        [SerializeField] private NinjaGhostData ninjaData;
        public NinjaGhostData NinjaData => ninjaData;

        [Header("States")]
        [SerializeField] private NinjaGhostAttackState attackState = new NinjaGhostAttackState();
        [SerializeField] private NinjaGhostTravelState travelState = new NinjaGhostTravelState();
        

        public void Init(PathCreator path, Map map, NinjaGhostData data, Vector2 position)
        {     
            currentPath = path;
            this.map = map;
            ninjaData = data;
            transform.position = position;

            travelState.Preinit(this);
            StartWalk();
        }

        public void UpdateGhost()
        {
            UpdateMachine();
        }

        public override void ChangeState(IBaseState<NinjaGhost> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState.Init(this);
        }

        public override void UpdateMachine()
        {
            currentState?.UpdateState();
        }

        public void StartWalk()
        {
            ChangeState(travelState);
        }

        public void StartAttack()
        {
            ChangeState(attackState);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            ITargetable targetable = collision.gameObject.GetComponent<ITargetable>();
            if(targetable != null)
            {
                attackState.SetTarget(targetable);
                StartAttack();
            }  
        }

        private void OnDestroy()
        {
            currentState?.Deinit(this);
        }
    }

    [System.Serializable]
    public class NinjaGhostData
    {
        public float movementSpeed;
        public PhysicalAttackData dmg;
        public float attackInterval = 3f;
        public float attackSpeedFactor = 1f;
    }
}
