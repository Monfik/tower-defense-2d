using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Tips;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class MonsterBuffEffect : BaseMonsterEffect
    {
        [SerializeField] private BuffData buffData;
        public BuffData BuffData => buffData;

        [SerializeField] private List<BuffData> upgrades;

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);
            monster.Data.speed.AddValue(buffData.speed);
            monster.Data.armor.AddValue(buffData.armorBuff);
            monster.Data.lightingResist.AddValue(buffData.lightingBuff);
            monster.Data.fireResist.AddValue(buffData.fireBuff);
            monster.Data.iceResist.AddValue(buffData.iceBuff);
            monster.Data.poisonResist.AddValue(buffData.poisonBuff);
            monster.Data.waterResist.AddValue(buffData.waterBuff);
            monster.Data.damageMultiplier += buffData.damageMultiplier;
            monster.SetSpeed(monster.Data.speed.currentValue);
        }

        public override void EndEffect()
        {
            monster.Data.speed.AddValue(-buffData.speed);
            monster.Data.armor.AddValue(-buffData.armorBuff);
            monster.Data.lightingResist.AddValue(-buffData.lightingBuff);
            monster.Data.fireResist.AddValue(-buffData.fireBuff);
            monster.Data.iceResist.AddValue(-buffData.iceBuff);
            monster.Data.poisonResist.AddValue(-buffData.poisonBuff);
            monster.Data.waterResist.AddValue(-buffData.waterBuff);
            monster.Data.damageMultiplier -= buffData.damageMultiplier;
            monster.SetSpeed(monster.Data.speed.currentValue);
            base.EndEffect();
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            string str =
                (buffData.speed != 0 ? buffData.speed < 0 ? "Speed slow: " + (-buffData.speed).ToString() : "Speed buff: " + buffData.speed.ToString() : "") + "\n" +
                (buffData.armorBuff != 0 ? "Armor buff: " + buffData.armorBuff.ToString() : "") + "\n" +
                (buffData.lightingBuff != 0 ? "Lighting buff: " + buffData.lightingBuff.ToString() : "") + "\n" +
                (buffData.fireBuff != 0 ? "Fire buff: " + buffData.fireBuff.ToString() : "") + "\n" +
                (buffData.iceBuff != 0 ? "Ice buff: " + buffData.iceBuff.ToString() : "") + "\n" +
                (buffData.poisonBuff != 0 ? "Poison buff: " + buffData.poisonBuff.ToString() : "") + "\n" +
                (buffData.damageMultiplier != 0 ? "Damage multiplier: " + buffData.damageMultiplier.ToString() : "") + "\n";

            return data;
        }
    }

    [System.Serializable]
    public class BuffData
    {
        public int speed;
        public int armorBuff;
        public int lightingBuff;
        public int fireBuff;
        public int iceBuff;
        public int poisonBuff;
        public int waterBuff;
        public float damageMultiplier;
    }
}
