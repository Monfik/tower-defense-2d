using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class TowerRangeBuff : BaseTowerEffect
    {
        private float additionalRange;
        public float rangeMultiplier;

        public TowerRangeBuff(TowerRangeBuff copy) :base(copy)
        {
            this.rangeMultiplier = copy.rangeMultiplier;
        }

        public override void StartEffect()
        {
            additionalRange = tower.range.data.coreValue * rangeMultiplier - tower.range.data.coreValue;
            tower.range.data.BuffByValue(additionalRange);
            tower.UpdateRange();
            tower.GuiManager.guiElementableActionController.CallActionIfExists(tower.range);
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();

        }

        public override void FinishEffect()
        {
            tower.range.data.BuffByValue(-additionalRange);
            tower.UpdateRange();
            tower.GuiManager.guiElementableActionController.CallActionIfExists(tower.range);
            base.FinishEffect();
        }
    }
}
