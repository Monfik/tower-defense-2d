using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using Tools;

namespace TowerDefense.Creatures
{
    public class BossBehaviour<T> : Monster
    {
        [Space(15)]
        [Header("State Machine")]
        [SerializeField] protected BossStateMachine<T> stateMachine;

        public override void Init(DataStorage storage, VertexPath path, MonsterData data, float distanceTravelled = 0)
        {
            base.Init(storage, path, data, distanceTravelled);
            OnMonsterDied += m => SetupEmptyState();
        }

        private void SetupEmptyState()
        {
            stateMachine.currentState?.Deinit();
            stateMachine.currentState = null;

            Timer timer = gameObject.GetComponent<Timer>();
            if(timer != null)
                Destroy(timer);
        }

        public override void UpdateMonster()
        {
            stateMachine.UpdateMachine();
            effects.UpdateEffects();
        }

        public virtual void ChangeState(BaseBossState<T> newState)
        {
            stateMachine.currentState?.Deinit();
            stateMachine.currentState = newState;
        }
    }
}
