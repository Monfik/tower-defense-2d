using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Spells;
using UnityEngine;
using Utils;

namespace TowerDefense.Effects
{
    public class PoisonRain : MapEffect
    {
        private PoisonRainData poisonRainData;
        private float currentRainTime = 0f;
        private float hitIntervalTime = 0f;
        private float intervalMultiplier = 0f;
        private Vector2 position;
        [SerializeField] private Animator animator;
        private bool finishingRain = false;

        [SerializeField] private PoisonEffect poisonEffectPrefab;

        private List<PoisonMonsterPair> pairs = new List<PoisonMonsterPair>();

        public void Rain(PoisonRainData poisonRainData, Vector2 position)
        {
            this.poisonRainData = poisonRainData;
            transform.position = position;
            this.position = position;
            animator.enabled = true;
            gameObject.SetActive(true);

            if (poisonRainData.duration <= poisonRainData.poisonHitInterval)
                Debug.LogError("Poison Rain duration is less than hit interval !");
        }

        public override void UpdateEffect()
        {
            currentRainTime += Time.deltaTime;
            hitIntervalTime += Time.deltaTime * intervalMultiplier;

            if(hitIntervalTime >= poisonRainData.poisonHitInterval)
            {
                TickRain(position, poisonRainData.hitRadius);
                hitIntervalTime = 0f;
            }

            if (finishingRain)
                return;

            if(currentRainTime >= poisonRainData.duration)
            {
                FinishRain();
            }
        }

        public void StartTicking()
        {
            intervalMultiplier = 1f;
        }

        public void FinishRain()
        {
            intervalMultiplier = 0f;
            animator.Play("FinishRain");
            finishingRain = true;
        }

        private void TickRain(Vector2 position, float radius)
        {
            int layerMask = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D[] collidedMonsters = Physics2D.OverlapCircleAll(position, radius, layerMask);

            foreach (Collider2D collider in collidedMonsters)
            {
                Monster monster = collider.GetComponent<Monster>();

                if (monster != null)
                {
                    AddPoison(monster);
                }
            }
        }

        private void AddPoison(Monster monster)
        {
            //Check For Existing Poison, if exists reset its timer
            PoisonMonsterPair poisonPair = pairs.Find(c => c.monster == monster);
            if(poisonPair != null) //exists
            {
                poisonPair.poison.ResetTimer();
                return;
            }
            else //not exists
            {
                PoisonEffect poison = Instantiate(poisonEffectPrefab);
                poison.Init(poisonRainData.poisonData);
                poison.transform.SetParent(monster.transform);
                poison.transform.position = Vector3.zero;
                poison.gameObject.SetActive(true);
                poison.duration = poisonRainData.poisonData.duration;
                monster.Effects.AddEffect(poison);
                pairs.Add(new PoisonMonsterPair(monster, poison));
            }
        }

        public override void FinishEffect()
        {
            animator.enabled = false;
            base.FinishEffect();
        }
    }

    public class PoisonMonsterPair
    {
        public Monster monster;
        public PoisonEffect poison;

        public PoisonMonsterPair(Monster monster, PoisonEffect poison)
        {
            this.monster = monster;
            this.poison = poison;
        }
    }
}
