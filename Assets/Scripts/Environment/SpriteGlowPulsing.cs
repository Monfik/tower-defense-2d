using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Enviroment
{
    public class SpriteGlowPulsing : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer sprite;
        private float currentTime;

        [Header("Values")]
        [SerializeField] [Range(0,10)] private float minIntensity;
        [SerializeField] [Range(0, 10)] private float maxIntensity;
        [SerializeField] [Range(0f, 10f)] private float speed;

        private void Update()
        {
            currentTime += Time.deltaTime * speed;
            float sin = Mathf.Sin(currentTime);
            float inverseT = Mathf.InverseLerp(-1, 1, sin);
            float intensity = Mathf.Lerp(minIntensity, maxIntensity, inverseT);
            sprite.material.SetFloat("_Intensity", intensity);
        }

        private void Reset()
        {
            sprite = GetComponent<SpriteRenderer>();
        }
    }
}
