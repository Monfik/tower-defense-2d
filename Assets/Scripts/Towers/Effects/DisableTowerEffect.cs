using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class DisableTowerEffect : BaseTowerEffect
    {
        public DisableTowerEffect(DisableTowerEffect effect) : base(effect)
        {
            
        }

        public override void StartEffect()
        {
            AttackableTower attackable = tower as AttackableTower;
            if(attackable == null)
            {
                Debug.LogError("The towers arent attackable!");
                return;
            }

            attackable.AttackSystem.SwitchToIdleState();
        }

        public override void FinishEffect()
        {
            base.FinishEffect();
            AttackableTower attackable = tower as AttackableTower;
            if (attackable == null)
            {
                Debug.LogError("The towers arent attackable!");
                return;
            }

            attackable.AttackSystem.SwitchToNormalState();
        }
    }
}
