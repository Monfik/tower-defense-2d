using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowerRepairer
    {
        private float efficient = 100;
        public float Efficient => efficient;

        private float fixMultiplier = 0.3f;
        public float FixMultiplier => fixMultiplier;

        private TowerBuilder builder;

        public TowerRepairer(TowerBuilder builder)
        {
            this.builder = builder;
        }

        public void Damage(float value)
        {
            efficient -= value;

            if(efficient <= 0)
            {
                efficient = 0f;
            }

            builder.OnTowerEfficientChanged?.Invoke(efficient);
        }

        public void Fix(float value)
        {
            efficient += value;
            if (efficient > 100)
                efficient = 100;

            builder.OnTowerEfficientChanged?.Invoke(efficient);
        }
    }
}
