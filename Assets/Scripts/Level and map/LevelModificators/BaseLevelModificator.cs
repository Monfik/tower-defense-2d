using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    public abstract class BaseLevelModificator : BaseInitable
    {
        public abstract void StartLevel(Level level);
        public abstract void FinishLevel(Level level);
    }
}
