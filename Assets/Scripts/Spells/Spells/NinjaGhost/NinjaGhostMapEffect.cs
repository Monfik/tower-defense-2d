using System.Collections;
using System.Collections.Generic;
using TowerDefense.Spells;
using UnityEngine;

namespace TowerDefense.Effects
{
    public class NinjaGhostMapEffect : MapEffect
    {
        [SerializeField] 
        private NinjaGhost ninjaGhost;
        public NinjaGhost NinjaGhost => ninjaGhost;

        public override void UpdateEffect()
        {
            base.UpdateEffect();
            ninjaGhost.UpdateGhost();
        }
    }
}
