﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class BasePool<T> : MonoBehaviour
{
    [SerializeField]
    private GameObject poolPrefab;
    public GameObject PoolPrefab => poolPrefab;

    [SerializeField]
    private int initialPoolSize;
    public int InitialPoolSize => initialPoolSize;

    private Queue<T> availableObjects = new Queue<T>();
    public Queue<T> AvailableObject => availableObjects;

    protected virtual void Awake()
    {
        InitializePool();
    }

    protected virtual void InitializePool()
    {
        for (int i = 0; i < initialPoolSize; i++)
        {
            AddObjectToPool();
        }
    }

    protected virtual void AddObjectToPool()
    {
        var newObj = Instantiate(poolPrefab, transform);
        newObj.SetActive(false);
        availableObjects.Enqueue(newObj.GetComponent<T>());
    }

    public virtual T GetPoolObject()
    {
        if (availableObjects.Count == 0)
            AddObjectToPool();

        var poolObject = availableObjects.Dequeue();
        return poolObject;
    }

    public virtual void ReturnToPool(T ob)
    {
        availableObjects.Enqueue(ob);
    }
}
