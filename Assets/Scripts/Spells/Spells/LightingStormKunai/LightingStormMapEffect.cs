using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefense.Creatures;
using TowerDefense.Power;
using TowerDefense.Spells;
using TowerDefense.Towers;
using UnityEngine;
using Utils;
using DG.Tweening;

namespace TowerDefense.Effects
{
    public class LightingStormMapEffect : MapEffect
    {
        private ExplosiveLightingStormData stormData;
        [SerializeField] private CircleCollider2D circleCollider2D;

        private float currentHitTime = 0f;

        private bool dissapearing;

        [Header("Lightings")]
        [SerializeField] private List<LightingStormEntity> lightnings;
        [Header("Clouds")]
        [SerializeField] private List<CloudStormEntity> clouds;

        [SerializeField] private SpriteAnimationv2 boomAnimation;
        [SerializeField] private SpriteRenderer floorMarker;

        [SerializeField] private float rescaleTime = 1f;
        [SerializeField] private float entitiesStartDelayTime = 0.5f;
        [SerializeField] private float dissapearingTime = 0.5f;

        public void Init(ExplosiveLightingStormData stormData)
        {
            this.stormData = stormData;
            duration = stormData.stormDuration;
            Boom(stormData.position, circleCollider2D.radius);
        }

        private void Boom(Vector2 position, float radius)
        {        
            boomAnimation.PlayAnimation(v => { boomAnimation.gameObject.SetActive(false); });
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, rescaleTime).SetEase(Ease.InBounce);
            DOVirtual.DelayedCall(entitiesStartDelayTime, StartEntityAnimating, false);

            int monsterLayers = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D[] hits = Physics2D.OverlapCircleAll(position, radius, monsterLayers);
            foreach (Collider2D hit in hits)
            {
                IDestructable monster = hit.GetComponent<IDestructable>();
                if (monster != null)
                {
                    PhysicalAttack physicalAttack = new PhysicalAttack(stormData.boomDmg);
                    Hit(physicalAttack, monster);
                }
            }
        }

        private void StartEntityAnimating()
        {
            StartLighting();
            StartClouding();
        }

        private void StartLighting()
        {
            float maxLeft = transform.position.x - circleCollider2D.radius / 2;
            float maxRight = transform.position.x + circleCollider2D.radius / 2;

            foreach (LightingStormEntity stormEntity in lightnings)
            {
                stormEntity.StartStorm(maxLeft, maxRight);
            }
        }

        private void StartClouding()
        {
            foreach(CloudStormEntity cloudEntity in clouds)
            {
                cloudEntity.StartStorm();
            }
        }

        public override void UpdateEffect()
        {
            if (dissapearing)
                return;

            base.UpdateEffect();
            CountHitTime();
        }

        private void CountHitTime()
        {
            currentHitTime += Time.deltaTime;

            if(currentHitTime >= stormData.lightingDamageTimeInterval)
            {
                Hit(circleCollider2D.bounds.center, circleCollider2D.radius);
                currentHitTime = 0f;
            }
        }

        private void Hit(Vector2 position, float radius)
        {
            int monsterLayers = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D[] hits = Physics2D.OverlapCircleAll(position, radius, monsterLayers);
            foreach(Collider2D hit in hits)
            {
                IDestructable monster = hit.GetComponent<IDestructable>();
                if(monster != null)
                {
                    MagicAttack magicAttack = new MagicAttack(stormData.lightingDamagePerSecond);
                    Hit(magicAttack, monster); 
                }
            }
        }

        private void Hit(IPowerAttackable attackable, IDestructable dmgTakable)
        {
            float dmgFactor = stormData.lightingDamageTimeInterval;
            attackable.Attack(dmgTakable, dmgFactor);
        }

        public override void FinishEffect()
        {
            dissapearing = true;

            Color alpha = Color.white;
            alpha.a = 0;

            foreach (LightingStormEntity entity in lightnings)
            {
                entity.DoColor(alpha, dissapearingTime);
            }
            foreach(CloudStormEntity entity in clouds)
            {
                entity.DoColor(alpha, dissapearingTime);
            }
            floorMarker.DOColor(alpha, dissapearingTime);

            StartCoroutine(Finish(dissapearingTime));
        }

        private IEnumerator Finish(float time)
        {
            yield return new WaitForSeconds(time);
            floorMarker.DOKill();
            foreach (LightingStormEntity entity in lightnings)
            {
                entity.KillAllTweens();
            }
            foreach (CloudStormEntity entity in clouds)
            {
                entity.KillAllTweens();
            }
            int killed = DOTween.Kill(this);
            base.FinishEffect();
        }
    }
}

