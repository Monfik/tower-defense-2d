using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class DirectionalAttackLine : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer attackDirectionLine;
        public LineRenderer AttackDirectionLine => attackDirectionLine;

        [SerializeField] private Vector2 direction = new Vector2(1f,0f);
        public Vector2 Direction => direction;
    }
}
