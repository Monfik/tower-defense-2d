using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class TowerStaticsData
    {
        public int coreValue;
        //[HideInInspector]
        public int currentValue;
        //[HideInInspector]
        public int buffValue;
        public float multiplier = 1f;
        public int minValue;

        public TowerStaticsData(TowerStaticsData data)
        {
            coreValue = data.coreValue;
            currentValue = data.currentValue;
            buffValue = data.buffValue;
            minValue = data.minValue;
            multiplier = data.multiplier;
        }

        public TowerStaticsData(int value)
        {
            coreValue = value;
            currentValue = value;
            buffValue = 0;
            minValue = 0;
            multiplier = 1f;
        }

        public void BuffByValue(int value)
        {
            buffValue += value;
            currentValue = coreValue + buffValue;

            if (currentValue < minValue)
                currentValue = minValue;

            currentValue = Mathf.RoundToInt(multiplier * currentValue);
        }

        public void SetupMultiplier(float multiplier)
        {
            this.multiplier = multiplier;
            if (multiplier <= 0f)
                multiplier = 0f;

            currentValue = coreValue + buffValue;

            if (currentValue < minValue)
                currentValue = minValue;

            currentValue = Mathf.RoundToInt(multiplier * currentValue);
        }

        public void Reset()
        {
            currentValue = coreValue;
            buffValue = 0;
        }

        public string GetGUILabel(int multiplier = 1)
        {
            string color = DataUtilities.GetColorStringByStaticsData(this);
            string label = "<b> " + color + (currentValue * multiplier).ToString() + "</color></b>";
            return label;
        }

        public TowerStaticsData BuffByData(TowerStaticsData towerData)
        {
            TowerStaticsData newData = new TowerStaticsData(this);
            newData.BuffByValue(towerData.buffValue);
            return newData;
        }
    }

    [System.Serializable]
    public class TowerDamageData
    {
        public int minDmg;
        public int maxDmg;
        public int minResistance;
        public int maxResistance;
    }

    [System.Serializable]
    public class TowerStaticsDataFloat
    {
        public float coreValue;
        [HideInInspector]
        public float currentValue;
        [HideInInspector]
        public float buffValue;
        public float minValue;

        public TowerStaticsDataFloat(TowerStaticsDataFloat data)
        {
            coreValue = data.coreValue;
            currentValue = data.currentValue;
            buffValue = data.buffValue;
            minValue = data.minValue;
        }

        public TowerStaticsDataFloat(float value)
        {
            coreValue = value;
            currentValue = value;
            buffValue = 0;
            minValue = 0;
        }

        public virtual void BuffByValue(float value)
        {
            buffValue += value;
            currentValue = coreValue + buffValue;

            if (currentValue < minValue)
                currentValue = minValue;
        }

        public void Reset()
        {
            currentValue = coreValue;
            buffValue = default;
        }

        public string GetGUILabel()
        {
            string color = DataUtilities.GetColorStringByStaticsData(this);
            string label = "<b> " + color + currentValue.ToString("N2") + "</color></b>";
            return label;
        }

        public TowerStaticsDataFloat BuffByData(TowerStaticsDataFloat towerData)
        {
            TowerStaticsDataFloat newData = new TowerStaticsDataFloat(this);
            newData.BuffByValue(towerData.buffValue);
            return newData;
        }
    }
}