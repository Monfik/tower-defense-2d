using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class TowerUpgradeData
    {
        public TowerStock stock;
        public float time;
    }

    [System.Serializable]
    public class TowerStock
    {
        public int wood;
        public int clay;
        public int iron;
    }
}
