using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Controllers
{
    public class Selector : BaseInitable
    {
        private ISelectable currentSelected;

        [Header("Mouse handler")]
        [SerializeField] private float mouseSelectThreshold = 10f;
        private Vector2 startDragScreenPosition;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.SELECTOR_MOUSE_CLICKED, Values.InputValues.DEFAULT_ACCEPT_KEY, OnMouseClicked, null, OnMouseReleased);
        }

        private void OnMouseClicked()
        {
            startDragScreenPosition = Input.mousePosition;
        }

        private void OnMouseReleased()
        {
            if (Values.InputValues.POINTER_ON_ACTION || Values.InputValues.POINTER_OVER_UI)
                return;

            float dragLength = Vector2.Distance(startDragScreenPosition, Input.mousePosition);

            if (dragLength > mouseSelectThreshold)
                return;

            Camera camera = storage.ReferenceStorage.MainCamera;
            Collider2D overlaped = Physics2D.OverlapPoint(camera.ScreenToWorldPoint(Input.mousePosition));
            if(overlaped != null)
            {
                ISelectable selectable = overlaped.GetComponent<ISelectable>();
                if(selectable != null)
                {
                    Select(selectable);
                }
                else
                {
                    UnselectCurrent();
                }
            }
            else if(currentSelected != null)
            {
                UnselectCurrent();
            }
        }

        public void Select(ISelectable selectable)
        {
            currentSelected?.OnDeselected();
            currentSelected = selectable;
            selectable.OnSelected();
            selectable.SelectableUnavailable += OnSelectableUnavilable;
        }

        private void UnselectCurrent()
        {
            if(currentSelected != null)
            {
                currentSelected.OnDeselected();
                currentSelected.SelectableUnavailable -= OnSelectableUnavilable;
                currentSelected = null;
            }
        }

        private void OnSelectableUnavilable(ISelectable selectable)
        {
            UnselectCurrent();
        }

        public override void Deinit()
        {
            base.Deinit();
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.SELECTOR_MOUSE_CLICKED);
        }
    }
}
