using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Invokers
{
    public class BaseCommandMono : MonoBehaviour, ICommandable
    {
        protected UnityAction OnCommandFinished;
        protected UnityAction OnCommandFailed;
        protected DataStorage storage;

        public virtual void InitCommand(DataStorage storage, UnityAction OnCommandFinished, UnityAction OnCommandFailed)
        {
            this.storage = storage;
            this.OnCommandFinished = OnCommandFinished;
            this.OnCommandFailed = OnCommandFailed;
        }

        public virtual void UpdateCommand()
        {
            
        }

        public virtual void FinishCommand()
        {
            
        }
    }
}
