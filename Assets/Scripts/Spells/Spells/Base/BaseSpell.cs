using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public abstract class BaseSpell : MonoBehaviour
    {
        [Header("Values")]
        [SerializeField]
        protected float cooldown = 10f;
        public float Cooldown => cooldown;

        [SerializeField]
        protected int manaCost;
        public int ManaCost => manaCost;

        [SerializeField] protected TooltipSpellDescriptionData tooltipData;

        protected ISpellHoldable spellHoldable;

        protected DataStorage storage;
        protected UnityAction<BaseSpell> OnSpellStartCasting;
        protected UnityAction<BaseSpell> OnSpellCasted;
        protected UnityAction<BaseSpell> OnSpellCanceled;
        protected UnityAction<BaseSpell> OnSpellFinished;

        public virtual void Init(DataStorage storage, ISpellHoldable holder,
            UnityAction<BaseSpell> OnSpellStartCasting,
            UnityAction<BaseSpell> OnSpellCasted,
            UnityAction<BaseSpell> OnSpellFinished,
            UnityAction<BaseSpell> OnSpellCanceled)
        {
            this.storage = storage;
            spellHoldable = holder;
            this.OnSpellStartCasting = OnSpellStartCasting;
            this.OnSpellCasted = OnSpellCasted;
            this.OnSpellCanceled = OnSpellCanceled;
            this.OnSpellFinished = OnSpellFinished;
        }

        public virtual TooltipSpellDescriptionData CreateTooltipData()
        {
            tooltipData.cost = "Mana cost: " + manaCost;
            tooltipData.cooldown = "Cooldown: " + GetCooldown() + "s";
            return tooltipData;
        }

        public virtual void Unlock(DataStorage storage, ISpellHoldable holder)
        {
            this.storage = storage;
            spellHoldable = holder;
        }

        public virtual bool HasManaForCast(int mana)
        {
            return mana >= manaCost;
        }

        public virtual void StartCast()
        {
            OnSpellStartCasting?.Invoke(this);
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic += CancelSpell;
        }

        public virtual void PayMana()
        {
            storage.ReferenceStorage.ManaController.Substract(manaCost);
        }

        public abstract void UpdateSpell();
        public virtual void FinishSpell()
        {
            OnSpellFinished?.Invoke(this);
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
        }

        public virtual void CancelSpell()
        {
            OnSpellCanceled?.Invoke(this);
        }

        public virtual float GetCooldown()
        {
            return cooldown;
        }

        public virtual void SetDefaultState()
        {

        }
    }
}
