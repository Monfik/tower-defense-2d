using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Power;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Updatables
{
    public class AdditionalElementPowerUpdatable : BaseDisplayableUpdatable //Non usable updatable :)
    {
        public MagicAttackData data;
        public List<BuffedTowerPowerPair> pairs;

        public override void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            base.StartUpdate(storage, OnUpdatableFinished);

            foreach (Tower tower in storage.ReferenceStorage.TowersController.CurrentTowers)
            {
                AttackableTower attackable = tower as AttackableTower;
                if (attackable != null)
                {
                    BaseAttackSystem attackSystem = attackable.AttackSystem;
                    MagicDamageAttackSystem magicAttackSystem = attackSystem as MagicDamageAttackSystem;

                    if(magicAttackSystem != null)
                    {
                        MagicDamageTowerBuff additionalDamageBuff = new MagicDamageTowerBuff(magicAttackSystem, data, true);
                        tower.Effects.AddEffect(additionalDamageBuff);
                        pairs.Add(new BuffedTowerPowerPair(tower, magicAttackSystem, additionalDamageBuff));
                    }
                }
            }
        }

        public override void Finish()
        {
            foreach(BuffedTowerPowerPair pair in pairs)
            {
                pair.buff.FinishEffect();
            }
            base.Finish();
        }
    }

    public class BuffedTowerPowerPair
    {
        public Tower tower;
        public MagicDamageAttackSystem attackSystem;
        public MagicDamageTowerBuff buff;

        public BuffedTowerPowerPair(Tower tower, MagicDamageAttackSystem attackSystem, MagicDamageTowerBuff buff)
        {
            this.tower = tower;
            this.attackSystem = attackSystem;
            this.buff = buff;
        }
    }
}
