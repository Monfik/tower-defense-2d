using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    [System.Serializable]
    public class TreeSpawnSkillState : BaseBossState<TreeBossBehaviour>
    {
        public override void Init(TreeBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.SetTrigger("Spawn");
        }

        public void Spawn()
        {
            boss.Spawner.SpawnNear(boss, OnMonstersSpawned);
        }

        public override void Deinit()
        {
            
        }

        public override void UpdateState()
        {
            
        }

        private void OnMonstersSpawned()
        {
            boss.SetDefaultMovementState();
        }

        public override bool IsReadyForWork()
        {
            return boss.Spawner.CanSpawn(boss.DistanceTravelled);           
        }
    }
}
