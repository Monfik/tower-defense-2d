using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Spells;
using UnityEngine;

namespace TowerDefense.Fractions
{
    
    public abstract class Fraction : ScriptableObject
    {
        [SerializeField] private int spellSetIndex;
        [SerializeField] private int manaHandlerId;
        [SerializeField] protected Sprite avatar;

        private DataStorage storage;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.ReferenceStorage.ManaController.Init(storage, manaHandlerId);
            storage.ReferenceStorage.Avatar.sprite = avatar;
        }

        public virtual void SetSpellsSet(SpellsController spellController)
        {
            spellController.SetSpellsSet(spellSetIndex);
        }

        //public virtual void InitMana(ManaController manaController)
        //{
        //    manaController.Init(startManaValue);
        //}

        public void Deinit()
        {

        }
    }


}
