using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class BaseDisplayableGlobalEvent : BaseGlobalEvent
    {
        [SerializeField] private Texture2D textureToDisplay;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnUpdatableFinished)
        {
            base.BeginEvent(storage, OnUpdatableFinished);
            storage.UIStorage.TextDisolver.Play(textureToDisplay);
        }
    }

    public class EventTitleDisplayData
    {
        //public float fontSize = 10f;
        //public Color fontColor = Color.white;
        //public float maxFont
    }
}
