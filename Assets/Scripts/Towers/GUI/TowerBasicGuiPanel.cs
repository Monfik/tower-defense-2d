using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Towers
{
    public class TowerBasicGuiPanel : BaseTowerGuiPanel
    {
        [SerializeField]
        private TowerGuiInfoPanel infoPanel;
        public TowerGuiInfoPanel InfoPanel => infoPanel;

        [SerializeField]
        private TowerBasicToolsPanel towerTools;
        public TowerBasicToolsPanel TowerTools => towerTools;

        public override void Open(Tower tower, TowerSelector gui, DataStorage storage)
        {
            base.Open(tower, gui, storage);
            tower.GuiManager.ComposeBasicGUI(infoPanel);
            towerTools.Open(tower, storage);
            infoPanel.Open(tower);

            towerTools.UpgradeHoverHandler.OnPointerEntered += OnUpgradeHoveringStart;
            towerTools.UpgradeHoverHandler.OnPointerExited += OnUpgradeHoveringStop;
        }

        public override void Close()
        {
            towerTools.UpgradeHoverHandler.OnPointerEntered -= OnUpgradeHoveringStart;
            towerTools.UpgradeHoverHandler.OnPointerExited -= OnUpgradeHoveringStop;
            currentTower.GuiManager.OnBasicGuiExit?.Invoke();
            base.Close();
        }

        public void OnUpgradeHoveringStart()
        {
            infoPanel.Close();
            currentTower.GuiManager.ComposeUpgradeGUI(infoPanel);
        }

        public void OnUpgradeHoveringStop()
        {
            infoPanel.Close();
            currentTower.GuiManager.ComposeBasicGUI(infoPanel);
        }

        private void OnDisable()
        {
            storage.ReferenceStorage.Postponer.InvokePostponeAction(() => { Values.InputValues.POINTER_OVER_UI = false; });
        }
    }
}
