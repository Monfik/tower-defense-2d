using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefense.Enviroment;
using UnityEngine;

namespace LevelLogic
{
    public class WindSusceptibleTreeRespawner : BaseLevelModificator
    {
        [SerializeField] List<TreeMapEntity> trees;

        public override void FinishLevel(Level level)
        {
            foreach (TreeMapEntity tree in trees)
            {
                level.Storage.ReferenceStorage.WindController.RemoveSusceptible(tree);
            }
        }

        public override void StartLevel(Level level)
        {
            foreach(TreeMapEntity tree in trees)
            {
                tree.ResetEntity();
                level.Storage.ReferenceStorage.WindController.AddSusceptible(tree);
            }
        }

        [ContextMenu("Collect trees")]
        private void CollectTrees()
        {
            trees = GameObject.FindObjectsOfType<TreeMapEntity>().ToList();
        }
    }
}
