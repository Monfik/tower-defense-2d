using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class SandDebuffEffect : BaseTowerEffect
    {
        [SerializeField] private List<SandEffectTier> tiers;
        [SerializeField] private SpriteRenderer sandRend;

        [SerializeField] private float sandStrengthRemovePerSecond = 0.1f;
        private AttackableTower attackableTower;
        [SerializeField] [Range(0f, 1f)] private float currentCeiling = 0f; //0-1
        [SerializeField] private SandEffectTier currentTier;

        public SandDebuffEffect(SandDebuffEffect effect) : base(effect)
        {
            tiers = effect.tiers;
            sandStrengthRemovePerSecond = effect.sandStrengthRemovePerSecond;
        }

        public override void StartEffect()
        {
            attackableTower = tower as AttackableTower;
            GameObject sandTierRenderer = new GameObject();
            sandTierRenderer.transform.SetParent(attackableTower.transform);
            sandTierRenderer.transform.localPosition = new Vector3(tower.BoxCollider.offset.x, -tower.BoxCollider.size.y / 2, 0f);
            sandRend = sandTierRenderer.AddComponent<SpriteRenderer>();
            sandRend.sortingOrder = Values.LayerMasks.TowerLayer + 1;
            
            RefreshEffect();
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();
            currentCeiling -= Time.deltaTime * sandStrengthRemovePerSecond;
            if (currentCeiling <= 0f)
            {
                FinishEffect();
                return;
            }

            RefreshEffect();
        }

        public override void FinishEffect()
        {
            RefreshEffect();
            GameObject.Destroy(sandRend.gameObject);
            base.FinishEffect();
        }

        public void RefreshEffect()
        {
            SandEffectTier properTier = FindTierBasedOnCeiling(currentCeiling);
            if (properTier != currentTier)
                SetupTier(properTier);
        }

        public void AddSand(float sandStrength)
        {
            currentCeiling += sandStrength;
            currentCeiling = Mathf.Clamp(currentCeiling, 0f, 1f);

            RefreshEffect();
        }

        public void SubtractSand(float sandStrength)
        {
            currentCeiling -= sandStrength;
            currentCeiling = Mathf.Clamp(currentCeiling, 0f, 1f);

            RefreshEffect();
        }

        public void SetupTier(SandEffectTier tier)
        {
            DebuffTower(tier);
            SetupRenderer(tier);
        }

        private void DebuffTower(SandEffectTier tier)
        {
            if (currentTier != null)
            {
                TowerStaticsData currentAttackSpeedData = attackableTower.AttackSystem.AttackSpeed.data;
                currentAttackSpeedData.SetupMultiplier(currentAttackSpeedData.multiplier + currentTier.attackSpeedDebuffMultiplier);
                tower.GuiManager.guiElementableActionController.CallActionIfExists(attackableTower.AttackSystem.AttackSpeed);
            }

            currentTier = tier;
            TowerStaticsData AttackSpeedData = attackableTower.AttackSystem.AttackSpeed.data;
            AttackSpeedData.SetupMultiplier(AttackSpeedData.multiplier - currentTier.attackSpeedDebuffMultiplier);
            tower.GuiManager.guiElementableActionController.CallActionIfExists(attackableTower.AttackSystem.AttackSpeed);
        }

        private void SetupRenderer(SandEffectTier tier)
        {
            if (tier.sprite == null && tier.ceiling != 0f)
            {
                Debug.LogError("Tier sprite is null");
                return;
            }

            sandRend.sprite = tier.sprite;
        }

        private SandEffectTier FindTierBasedOnCeiling(float ceiling)
        {
            for (int i = tiers.Count - 1; i >= 0; i--)
            {
                if (tiers[i].ceiling <= ceiling)
                    return tiers[i];
            }

            return tiers[0];
        }
    }

    [System.Serializable]
    public class SandEffectTier
    {
        public Sprite sprite;
        public int tier;
        [Range(0f, 1f)] public float attackSpeedDebuffMultiplier; //0-1;
        [Range(0f, 1f)] public float ceiling; //0-1
    }
}
