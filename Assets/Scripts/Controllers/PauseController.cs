using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Controllers
{
    public class PauseController : BaseInitable
    {
        [SerializeField] private bool paused = false;
        [SerializeField] private PausePanel pausePanel;

        [SerializeField] private Button pauseButton;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            storage.EventsStorage.CoreEvnts.OnLevelStartedBasic += EnablePauseInput;
            storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic += DisablePauseInput;
            storage.EventsStorage.CoreEvnts.OnBaseDestroyed += DisablePauseInput;
            paused = false;
        }

        public void UpdateController()
        {
            //if (Input.GetKeyDown(KeyCode.Escape))
            //{
            //    TriggerPause();
            //}
        }

        private void TriggerPause()
        {
            if (!paused)
                RunPause();
            else
                StopPause();
        }

        private void RunPause()
        {
            pausePanel.Enter(StopPause, storage);
            paused = true;
            storage.EventsStorage.CoreEvnts.CallOnGamePaused();
        }

        private void StopPause()
        {
            pausePanel.ClosePanel();
            paused = false;
            storage.EventsStorage.CoreEvnts.CallOnGameUnPaused();
        }

        public void EnablePauseInput()
        {
            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.PAUSE_TRIGGER_ID, Values.InputValues.PAUSE_TRIGGER_KEY, TriggerPause, null, null);
            pauseButton.onClick.AddListener(TriggerPause);
        }

        public void DisablePauseInput()
        {
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.PAUSE_TRIGGER_ID);
            pauseButton.onClick.RemoveListener(TriggerPause);
        }

        public override void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnLevelStartedBasic -= EnablePauseInput;
            storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic -= DisablePauseInput;
            storage.EventsStorage.CoreEvnts.OnBaseDestroyed -= DisablePauseInput;

            DisablePauseInput();
            paused = false;
        }
    }
}
