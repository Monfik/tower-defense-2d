using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense;
using UnityEngine;

public class PathSelectButtonPool: BasePool<PathSelectButton>
{
    public override PathSelectButton GetPoolObject()
    {
        var ob = base.GetPoolObject();
        ob.gameObject.SetActive(true);
        return ob;
    }

    public override void ReturnToPool(PathSelectButton ob)
    {
        ob.gameObject.SetActive(false);
        ob.transform.SetParent(transform);
        base.ReturnToPool(ob);
    }
}
