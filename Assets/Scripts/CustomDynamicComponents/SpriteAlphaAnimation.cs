using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Animations
{
    public class SpriteAlphaAnimation : MonoBehaviour
    {
        private ITransparable rend;
        public void Init(float from, float to, float duration, ITransparable sprite, TweenCallback OnFinishCallback = null)
        {
            rend = sprite;
            
            DOVirtual.Float(from, to, duration, OnAlphaUpdate).OnComplete(OnFinishCallback).OnComplete(DestroyItself).SetId("SpriteAlphaAnimation");
        }

        private void OnAlphaUpdate(float alpha)
        {
            rend.MakeTransparable(alpha);
        }

        private void DestroyItself()
        {
            Destroy(this);
        }

        private void OnDestroy()
        {
            int killed = DOTween.Kill("SpriteAlphaAnimation");
        }
    }
}
