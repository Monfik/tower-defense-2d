using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    [System.Serializable]
    public class StatsData 
    {
        public int hp;
        public float speed;
        public float physicsResist;
        public float magicResist;
    }
}
