using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class SpellHolderOptionsHolder : MonoBehaviour
    {
        [SerializeField] private List<SpellHolderConfigurableOption> options;
        public List<SpellHolderConfigurableOption> Options => options;

        [SerializeField] private Image image;
        [SerializeField] private CanvasGroup canvasGroup;

        [Header("Times")]
        [SerializeField] private float displayTime;
        [SerializeField] private float hideTime;

        [Space]
        [SerializeField] private Color selectedColor;
        [SerializeField] private Color unselectedColor;

        [SerializeField] private SpellHolderConfigurableOption currentOption;
        private Tween deactiveTween;

        public void Init()
        {
            foreach(SpellHolderConfigurableOption option in options)
            {
                option.Init(OnOptionSelected);
            }
        }

        public void Display()
        {
            deactiveTween?.Kill();
            DOVirtual.Float(0, 1, displayTime, CanvasGroupAlphaChanged);
            gameObject.SetActive(true);
        }

        private void CanvasGroupAlphaChanged(float f)
        {
            canvasGroup.alpha = f;
        }

        public void Hide()
        {
            DOVirtual.Float(1, 0, displayTime, CanvasGroupAlphaChanged);
            deactiveTween = DOVirtual.DelayedCall(hideTime, Deactive, false);
        }

        private void Deactive()
        {
            gameObject.SetActive(false);
            deactiveTween = null;
        }

        private void OnOptionSelected(SpellHolderConfigurableOption option)
        {
            currentOption?.MakeDeselected(unselectedColor);
            currentOption = option;
            currentOption.MakeSelected(selectedColor);
        }
    }
}
