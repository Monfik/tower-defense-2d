using CameraHandle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LevelLogic
{
    public class FocusBaseFinisher : BaseInitable, ILevelFinishable
    {
        private UnityAction<ILevelFinishable> OnFinished;

        [SerializeField] private CameraOverrideData overrideData;

        public void StartFinish(UnityAction<ILevelFinishable> OnFinished, Level level)
        {
            this.OnFinished = OnFinished;
            overrideData.targetPosition = storage.ReferenceStorage.LevelManager.CurrentLevel.Castle.transform.position;
            storage.ReferenceStorage.CameraMovement.MoveTo(overrideData, Finish, false);
        }

        public void UpdateFinisher()
        {
            
        }

        private void Finish()
        {
            OnFinished?.Invoke(this);
        }
    }
}
