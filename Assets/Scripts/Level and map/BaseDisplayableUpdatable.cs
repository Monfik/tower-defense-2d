using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Updatables
{
    public class BaseDisplayableUpdatable : BaseUpdatableMono
    {
        public Sprite icon;
        public string description;
        protected UIUpdatableIcon updatableIcon;
        protected UIUpdatableManager iconsManager;

        public override void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            base.StartUpdate(storage, OnUpdatableFinished);

            iconsManager = storage.ReferenceStorage.UIIconsManager;
            updatableIcon = iconsManager.CreateIcon(icon, description);
        }

        public override void UpdateUpdatable()
        {
            float t = currentTime / duration;
            updatableIcon.UpdateIcon(t);

            base.UpdateUpdatable();   
        }

        public override void Finish()
        {
            iconsManager.ReturnIcon(updatableIcon);
            base.Finish();
        }
    }
}
