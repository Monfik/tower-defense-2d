using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense
{
    public interface ISelectable
    {
        void OnSelected();
        void OnDeselected();
        UnityAction<ISelectable> SelectableUnavailable { get; set; }
    }
}
