using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class TreeBossBehaviour : BossBehaviour<TreeBossBehaviour>
    {
        [Header("States")]
        [SerializeField] private TreeDefaultMovementState defaultMovementState;

        [Header("Values")]
        [SerializeField] private TreeMonsterSpawner spawner;
        public TreeMonsterSpawner Spawner => spawner;

        [Header("Refs")]
        [SerializeField] private TreeBossBuffer buffer;
        public TreeBossBuffer Buffer => buffer;

        public override void Init(DataStorage storage, VertexPath path, MonsterData data, float distanceTravelled = 0f)
        {
            base.Init(storage, path, data);
            spawner.Init(storage, this);
            buffer.Init();
            defaultMovementState.AvailableStates.ForEach(c => c.PreInit(this, storage));
            ChangeState(defaultMovementState);
        }

        public override void ChangeState(BaseBossState<TreeBossBehaviour> newState)
        {
            base.ChangeState(newState);
            newState.Init(this, storage);
        }

        public void SetDefaultMovementState()
        {
            ChangeState(defaultMovementState);
        }
    }
}
