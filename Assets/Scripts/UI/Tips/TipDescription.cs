using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TowerDefense.Tips
{
    public class TipDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] float hoverTime = 1f;

        [Header("Data")]
        [SerializeField] private RectTransform target;
        [SerializeField] private DescriptionData description;
        public DescriptionData Description => description;
        [SerializeField] private Vector2 extraOffset;
        [SerializeField] private Sprite tooltipIcone;
        private Tween hoveringTween;
        private bool tipOpened = false;

        private void Reset()
        {
            target = gameObject.GetComponent<RectTransform>();
            extraOffset = new Vector2(10f, 10f);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            hoveringTween?.Kill();
            hoveringTween = DOVirtual.DelayedCall(hoverTime, DisplayTip);
        }

        private void DisplayTip()
        {
            TipManager.Display(target, description, extraOffset, tooltipIcone);
            tipOpened = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            hoveringTween?.Kill();
            TipManager.Hide();
            tipOpened = false;
        }

        public void SetDescription(DescriptionData data)
        {
            description = data;

            if (tipOpened)
                TipManager.UpdateTip(target, description, extraOffset, tooltipIcone);
        }

        private void OnDestroy()
        {
            hoveringTween?.Kill();
            TipManager.Hide();
            tipOpened = false;
        }
    }

    [System.Serializable]
    public class DescriptionData
    {
        public bool simpleText;
        [TextArea] public string title1;
        [TextArea] public string title2;
        [TextArea] public string description;
        [TextArea] public string footer;
        public List<DescriptionSingleSpecifiedData> specifiedDatas;

        public DescriptionData()
        {
            specifiedDatas = new List<DescriptionSingleSpecifiedData>();
        }

        public static DescriptionData CreateDescription(string desc)
        {
            DescriptionData data = new DescriptionData();
            data.description = desc;
            data.simpleText = true;

            return data;
        }

        public static DescriptionData CreateDescription(string title1, string title2, string description, string footer, List<DescriptionSingleSpecifiedData> specifiedDatas)
        {
            DescriptionData data = new DescriptionData();
            data.title1 = title1;
            data.title2 = title2;
            data.description = description;
            data.footer = footer;
            data.specifiedDatas = specifiedDatas;
            data.simpleText = false;
            return data;
        }
    }

    [System.Serializable]
    public class DescriptionSingleSpecifiedData
    {
        public string description;
        public string value;

        public DescriptionSingleSpecifiedData(string description, string value)
        {
            this.description = description;
            this.value = value;
        }
    }
}
