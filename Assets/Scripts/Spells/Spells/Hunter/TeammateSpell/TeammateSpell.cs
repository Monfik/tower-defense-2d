using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class TeammateSpell : DisplayableSpell
    {
        [SerializeField] private ConfigurableSpellHolder configurableHolder;

        [SerializeField] private BaseTeammate mate1;
        [SerializeField] private BaseTeammate mate2;
        [SerializeField] private BaseTeammate mate3;

        public BaseTeammate currentMate;

        public override void Unlock(DataStorage storage, ISpellHoldable holder)
        {
            base.Unlock(storage, holder);

            List<SpellHolderConfigurableOption> options = configurableHolder.Holder.Options;
            options[0].AddListener(() => { PickMate(mate1); });
            options[1].AddListener(() => { PickMate(mate2); });
            options[2].AddListener(() => { PickMate(mate3); });

            options[0].Select(); //default fire          
        }

        private void PickMate(BaseTeammate mate)
        {
            currentMate = mate;
            icon = mate.Avatar.sprite;
        }

        public override void StartCast()
        {
            base.StartCast();
            BaseTeammate teammate = Instantiate(currentMate, currentMate.transform.parent);
            teammate.gameObject.SetActive(true);
            teammate.Init(OnTeammateFinished, storage);
            currentMate = teammate;
            OnSpellCasted?.Invoke(this);
            PayMana();
        }

        private void OnTeammateFinished(BaseTeammate teammate)
        {
            Destroy(teammate.gameObject);
            FinishSpell();
        }

        public override void UpdateSpell()
        {
            base.UpdateSpell();
            currentMate?.UpdateMate();
        }

        public override void FinishSpell()
        {
            currentMate.gameObject.SetActive(false);

            if(currentMate != null)
            {
                Destroy(currentMate.gameObject);
            }

            base.FinishSpell();
        }

        public override void SetDefaultState()
        {
            base.SetDefaultState();

        }
    }
}
