using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class ExtensionMethods
    {
        public static Vector2 GetRandomPositionOnMap(this MapLimitsData limits)
        {
            Vector2 upperLeft = limits.leftUpperCorner.position;
            Vector2 lowerRight = limits.rightBottomCorner.position;
            float randomX = Random.Range(upperLeft.x, lowerRight.x);
            float randomY = Random.Range(upperLeft.y, lowerRight.y);
            return new Vector2(randomX, randomY);
        }

        public static Vector2 GetMapLength(this MapLimitsData limits)
        {
            Vector2 upperLeft = limits.leftUpperCorner.position;
            Vector2 lowerRight = limits.rightBottomCorner.position;
            float lengthX = lowerRight.x - upperLeft.x;
            float lengthY = upperLeft.y - lowerRight.y;
            return new Vector2(lengthX, lengthY);
        }

        public static float GetArea(this MapLimitsData limits)
        {
            Vector2 upperLeft = limits.leftUpperCorner.position;
            Vector2 lowerRight = limits.rightBottomCorner.position;
            float rangeX = lowerRight.x - upperLeft.x;
            float rangeY = upperLeft.y - lowerRight.y;
            return rangeX * rangeY;
        }

        public static void Shuffle<T>(this List<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void SetSpriteColorAlpha(this SpriteRenderer renderer, float alpha)
        {
            Color color = renderer.color;
            color.a = alpha;
            renderer.color = color;
        }
    }
}
