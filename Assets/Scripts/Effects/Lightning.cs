using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Spells;
using UnityEngine;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class Lightning : MapEffect
    {
        [SerializeField] private Animator animator;
        private LightningShotData shotData;
        private Vector2 shotPosition;

        public void Shot(LightningShotData shotData, Vector2 shotPosition)
        {
            animator.enabled = true;
            this.shotData = shotData;
            this.shotPosition = shotPosition;
            transform.position = shotPosition;
            gameObject.SetActive(true);
        }

        public void ShotExecuted()
        {
            int layerMask = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D[] collidedMonsters = Physics2D.OverlapCircleAll(shotPosition, shotData.hitRadius, layerMask);

            foreach (Collider2D collider in collidedMonsters)
            {
                Monster monster = collider.GetComponent<Monster>();

                if (monster != null)
                {
                    DealDamage(monster);
                }
            }
        }

        private void DealDamage(Monster monster)
        {        
            int lightningReduction = monster.Data.GetDataByType(MagicType.Lighting).currentValue - shotData.lightningReduction;
            if (lightningReduction < 0)
                lightningReduction = 0;
            int damage = DamageCalculator.CalculateDamage(shotData.damage, lightningReduction);
            monster.TakeDamage(damage, DamageType.Magic);
        }
    }
}
