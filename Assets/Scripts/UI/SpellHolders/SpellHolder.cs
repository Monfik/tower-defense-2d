using Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Spells
{
    public class SpellHolder : MonoBehaviour, ISpellHoldable
    {
        [SerializeField] protected Image spellArtwork;
        [SerializeField] protected Image fillImage;
        [SerializeField] protected Button button;
        [SerializeField] protected Image overlay;
        [SerializeField] protected SpellHolderLoadBarsController loadBarsController;
        [SerializeField] protected TipDescription tipDesc;
        [SerializeField] protected TMP_Text cost;
        [SerializeField] protected Image hoverImage;

        [SerializeField]
        protected BaseSpell spell;
        public BaseSpell Spell => spell;

        [Header("Colors")]
        [SerializeField] protected List<ColorWisePair> fillColorPairs = new List<ColorWisePair>();
        [SerializeField] protected Boolwise3 currentColorWise = new Boolwise3(false, false, false);      //1 - mana, 2 - cooldown
        protected Boolwise3 unlockedButtonWiseValue = new Boolwise3(true, true, false);

        protected float fillProgress;
        protected float currentTime = 0f;

        protected SpellsController controller;
        protected DataStorage storage;
        protected UnityAction<int> CurrentManaCheck;

        public virtual void Init(SpellsController controller, DataStorage storage)
        {
            this.controller = controller;
            this.storage = storage;
            SetupTooltipDescription(TooltipSpellDescriptionData.CreateDescription(spell.CreateTooltipData()));
            SetupCost(spell.ManaCost);
        }

        protected virtual void SetupCost(int cost)
        {
            this.cost.text = cost.ToString();
        }

        public virtual void Use()
        {
            ResetButton(false);
            loadBarsController.Reset(true);
            controller.UpdatableSpellHolders.Add(this);
            currentColorWise.bool2 = false;
            HandleStateByMana();
        }

        public virtual void UpdateHolder()
        {
            currentTime += Time.deltaTime;
            fillProgress = currentTime / spell.GetCooldown();
            fillImage.fillAmount = fillProgress;
            loadBarsController.UpdateValue(fillProgress);

            if (fillProgress >= 1f)
            {
                EnableByCooldown();
            }
        }

        public virtual void EnableByCooldown()
        {
            currentColorWise.bool2 = true;
            controller.UpdatableSpellHolders.Remove(this);
            loadBarsController.Reset(false);
            fillImage.fillAmount = 1f;
            //currentColorWise.bool2 = true; check if this line is needed
            SetupState(currentColorWise, unlockedButtonWiseValue);
        }

        public virtual void SetActiveByMana(bool active)
        {
            currentColorWise.bool1 = active;
            SetupState(currentColorWise, unlockedButtonWiseValue);
        }

        public virtual void SetupState(Boolwise3 current, Boolwise3 unlock)
        {
            button.interactable = current == unlock;
            hoverImage.color = button.interactable ? new Color(255, 255, 255, 60f / 255f) : new Color(255, 255, 255, 10f / 255f);
            ColorWisePair fillColor = fillColorPairs.Find(c => c.wise == current);
            if(fillColor != null)
            {
                fillImage.color = fillColor.color;
            }
        }

        protected void ResetButton(bool interactable)
        {
            fillImage.fillAmount = 0f;
            fillProgress = 0f;
            currentTime = 0f;
            button.interactable = interactable;
        }

        public void Unlock()
        {
            button.interactable = true;
            button.onClick.AddListener(CastSpell);
            spell.Unlock(storage, this);
            storage.EventsStorage.PlayerEv.OnManaChanged += OnManaChanged;
            currentColorWise.bool2 = true;
            HandleStateByMana();
        }

        public void HandleStateByMana()
        {
            bool manaAfford = CheckManaAvailability();
            if (manaAfford)
                CurrentManaCheck = CheckForDisableByMana;
            else
                CurrentManaCheck = CheckForEnableByMana;

            SetActiveByMana(manaAfford);
        }

        public void Lock()
        {
            button.interactable = false;
            button.onClick.RemoveAllListeners();
        }

        protected virtual void CastSpell()
        {
            if (Values.InputValues.BUILDING_POINTER)
                return;

            BaseSpell castingSpell = controller.CastSpell(spell);
            castingSpell.Init(storage, this, OnSpellCastStarted, OnSpellCasted, OnSpellFInished, OnSpellCanceled);
            castingSpell.StartCast();
            //storage.EventsStorage.PlayerEv.OnManaChanged -= OnManaChanged;
        }

        protected void OnSpellCastStarted(BaseSpell spell)
        {
            //Debug.Log("spell start casted " + spell.name);
        }

        protected virtual void OnSpellCasted(BaseSpell spell)
        {
            controller.OnSpellCasted(spell);
            Use();
        }

        protected virtual void OnSpellFInished(BaseSpell spell)
        {
            controller.OnSpellFinished(spell);
        }

        protected void OnSpellCanceled(BaseSpell spell)
        {
            controller.OnSpellCanceled(spell);
        }

        protected virtual bool CheckManaAvailability()
        {
            int currentMana = storage.ReferenceStorage.ManaController.CurrentMana;
            return currentMana >= spell.ManaCost;
        }

        protected virtual void OnManaChanged(int mana)
        {
            CurrentManaCheck?.Invoke(mana);
        }

        protected virtual void CheckForDisableByMana(int mana)
        {
            if (mana < spell.ManaCost)
            {
                DisableByMana();
            }
        }

        protected virtual void CheckForEnableByMana(int mana)
        {
            if (mana >= spell.ManaCost)
            {
                EnableByMana();
            }
        }

        public virtual void DisableByMana()
        {
            SetActiveByMana(false);
            CurrentManaCheck = CheckForEnableByMana;
        }

        public virtual void EnableByMana()
        {
            SetActiveByMana(true);
            CurrentManaCheck = CheckForDisableByMana;
        }

        public void RefreshToDefaultState()
        {
            HandleStateByMana();
            EnableByCooldown();
        }

        public void SetupTooltipDescription(DescriptionData data)
        {
            data.title1 = tipDesc.Description.title1;
            data.title2 = tipDesc.Description.title2;
            data.description = tipDesc.Description.description;
            tipDesc.SetDescription(data);
        }
    }

    [System.Serializable]
    public struct Boolwise3
    {
        public bool bool1;
        public bool bool2;
        public bool bool3;
        
        public Boolwise3(bool bool1, bool bool2, bool bool3)
        {
            this.bool1 = bool1;
            this.bool2 = bool2;
            this.bool3 = bool3;
        }

        public static bool operator ==(Boolwise3 a, Boolwise3 b)
        {
            return (a.bool1 == b.bool1) && (a.bool2 == b.bool2) && (a.bool3 == b.bool3);
        }

        public static bool operator !=(Boolwise3 a, Boolwise3 b)
        {
            return (a.bool1 != b.bool1) || (a.bool2 != b.bool2) || (a.bool3 != b.bool3);
        }
    }

    [System.Serializable]
    public class ColorWisePair
    {
        public Color color;
        public Boolwise3 wise;
    }

    [System.Serializable]
    public class TooltipSpellDescriptionData
    {
        //title and description setup from Inspector please
        public string title;
        public string description;
        public string title2;
        public string cooldown;
        public string cost;
        public List<DescriptionSingleSpecifiedData> specifiedDatas;

        public static string CreateDamageDescription(MagicType type, int damage, int reduction)
        {
            string dmg = type.ToString() + ": " + damage + ", <reduction> " + reduction;
            return dmg;
        }

        public static string CreateDamageDescription(MagicType type, int damageFrom, int damageTo, int reduction)
        {
            string dmg = type.ToString() + ": " + damageFrom + " - " + damageTo + ", <reduction> " + reduction;
            return dmg;
        }

        public static DescriptionData CreateDescription(TooltipSpellDescriptionData data)
        {
            //string description =
            //    "<size=25><color=blue>" + data.title + "</color></size>\n\n\n" +
            //    data.description + "\n\n" +
            //    data.details + "\n\n" +
            //    data.cooldown + "\n\n" +
            //    data.cost;

            DescriptionData descriptionData = new DescriptionData();
            descriptionData.title1 = data.title;
            descriptionData.title2 = data.title2;
            descriptionData.description = data.description;
            descriptionData.footer = data.cooldown;
            descriptionData.specifiedDatas = data.specifiedDatas;
            return descriptionData;
        }
    }
}
