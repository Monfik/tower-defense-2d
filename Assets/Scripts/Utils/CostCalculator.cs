using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace Utils
{
    public static class CostCalculator
    {
        public static float CalculateCost(TowerStock stock)
        {
            MaterialCostsData costData = Values.materialCosts;
            float cost = (stock.wood * costData.wood + stock.clay * costData.clay + stock.iron * costData.iron);
            return cost;
        }
    }
}
