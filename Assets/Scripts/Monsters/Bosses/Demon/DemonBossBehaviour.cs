using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class DemonBossBehaviour : BossBehaviour<DemonBossBehaviour>
    {
        [Header("States")]
        [SerializeField] private DemonBossDefaultState defaultMovementState;
        public DemonBossDefaultState DefaultMovementState => defaultMovementState;

        public override void Init(DataStorage storage, VertexPath path, MonsterData data, float distanceTravelled = 0f)
        {
            base.Init(storage, path, data);

            for(int i= defaultMovementState.AvailableStates.Count - 1; i >= 0; i--)
            {
                defaultMovementState.AvailableStates[i].PreInit(this, storage);
            }

            ChangeState(defaultMovementState);
        }

        public override void ChangeState(BaseBossState<DemonBossBehaviour> newState)
        {
            base.ChangeState(newState);
            newState.Init(this, storage);
        }

        public void SetDefaultMovementState()
        {
            ChangeState(defaultMovementState);
        }
    }
}
