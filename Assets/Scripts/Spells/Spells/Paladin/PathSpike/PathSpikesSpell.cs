using Data;
using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Power;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Spells
{
    public class PathSpikesSpell : BaseSpell
    {
        [Space(10)]
        [SerializeField] private SpikesData data;
        [SerializeField] private PathSpiker spiker;
        [SerializeField] private PathSelector pathSelector;

        public override void StartCast()
        {
            base.StartCast();
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            pathSelector.Show(storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MiddlePaths, OnPathSelected);
        }

        public override void UpdateSpell()
        {         
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            pathSelector.UpdateSelector();
        }

        public void SpikePath(PathCreator path, SpikesData spikesData)
        {
            spiker.Init(spikesData, path);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(spiker);
            OnSpellCasted?.Invoke(this);
            PayMana();
        }

        private void OnPathSelected(PathCreator path)
        {
            SpikePath(path, data);
            FinishSpell();
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            pathSelector.Hide();
            base.FinishSpell();
        }

        public override void CancelSpell()
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            pathSelector.Hide();
            base.CancelSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData tooltipData = base.CreateTooltipData();

            tooltipData.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=5>Physical", $"{data.attackData.minDmg} - {data.attackData.maxDmg}"));
            tooltipData.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Armor reduction", $"{data.attackData.armorReduction}"));
            tooltipData.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Speed", $"{data.speed}/s"));

            return tooltipData;
        }
    }

    [System.Serializable]
    public class SpikesData
    {
        public float speed;
        public float spikeSpawnInterval;
        public PhysicalAttackData attackData;
    }
}
