using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Updatables;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class FireballSpellCaster : BaseUpdatableMono
    {      
        [SerializeField] private float interval = 1f;
        [SerializeField] private float minDistanceToRemoveMark = 1f;
        private float currentIntervalTime = 0f;
        private int currentFireballIndex = 0;
        private int fireballCount;
        private int fireballHittedCount = 0;
        [SerializeField] private Fireball fireballPrefab;
        private bool spawningFireballs = true;

        private List<MarkFireballDataPair> markFireballPairs = new List<MarkFireballDataPair>();

        private List<MarkerFireballDataPair> markDataPairs = new List<MarkerFireballDataPair>();
        public List<MarkerFireballDataPair> MarkDataPairs => markDataPairs;


        public void Cast()
        {
            fireballCount = markDataPairs.Count;
        }

        public override void UpdateUpdatable()
        {
            if(spawningFireballs)
            {
                currentIntervalTime += Time.deltaTime;
                if (currentIntervalTime >= interval)
                {
                    SpawnNextFireball();
                    currentIntervalTime = 0f;
                }
            }

            UpdateMarks();
        }

        private void SpawnFireball(MarkerFireballDataPair pair)
        {
            Vector2 position = pair.marker.transform.position;
            Fireball fireball = Instantiate(fireballPrefab);
            FireballData fireballData = pair.fireballData;
            fireballData.targetPosition = position;
            position.y = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MapLimits.leftUpperCorner.transform.position.y + 2f;
            fireballData.startPosition = position;
            fireball.InitFireball(fireballData);
            fireball.gameObject.SetActive(true);
            fireball.InitData(storage, 0f);
            fireball.AssignEvents(OnFireballHitted);
            storage.EventsStorage.CoreEvnts.CallOnMapEffectAppeared(fireball);
            markFireballPairs.Add(new MarkFireballDataPair(pair, fireball));
        }

        private void SpawnNextFireball()
        {
            SpawnFireball(markDataPairs[currentFireballIndex]);
            currentFireballIndex++;

            if (currentFireballIndex >= fireballCount)
                FinishFireballing();
        }

        private void FinishFireballing()
        {
            spawningFireballs = false;
        }

        private void UpdateMarks()
        {
            for(int i=markFireballPairs.Count-1; i>=0; i--)
            {
                if (markFireballPairs[i].GetDistanceMarkToFireball() <= minDistanceToRemoveMark)
                {
                    Destroy(markFireballPairs[i].markerDataPair.marker.gameObject);
                    markFireballPairs.Remove(markFireballPairs[i]);
                    continue;
                }
            }
        }

        private void OnFireballHitted(MapEffect fireball)
        {
            fireballHittedCount++;

            if (fireballHittedCount >= fireballCount)
                Finish();
        }
    }

    public class MarkFireballDataPair
    {
        public MarkerFireballDataPair markerDataPair;
        public Fireball fireball;

        public MarkFireballDataPair(MarkerFireballDataPair markerDataPair, Fireball fireball)
        {
            this.markerDataPair = markerDataPair;
            this.fireball = fireball;
        }

        public float GetDistanceMarkToFireball()
        {
            return Vector2.Distance(markerDataPair.marker.transform.position, fireball.transform.position);
        }
    }
}
