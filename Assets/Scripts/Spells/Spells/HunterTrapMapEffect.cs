using System.Collections;
using System.Collections.Generic;
using Tools;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using TowerDefense.Creatures;
using Utils;

namespace TowerDefense.Effects
{
    public class HunterTrapMapEffect : MapEffect
    {
        [SerializeField] private HunterMapTrapData trapData;
        public HunterMapTrapData TrapData => trapData;
        [SerializeField] private SpriteRenderer trap;
        [SerializeField] private BoxCollider2D boxCollider2D;

        [Header("Animations")]
        [SerializeField] private SpriteAnimation openTrapAnimation;
        private UnityAction CurrentAction;

        [SerializeField] private HunterTrapEffect trapEffectPrefab;
        public HunterTrapEffect TrapEffectPrefab => trapEffectPrefab;

        public void PlaceTrap(Vector2 position)
        {
            duration = trapData.layingTime;
            transform.position = position;
            openTrapAnimation.PlayAnimation(OnTrapOpened);
            boxCollider2D.enabled = false;
        }

        public override void UpdateEffect()
        {
            CurrentAction?.Invoke();
        }

        private void CalculateTime()
        {
            base.UpdateEffect();
        }

        public void OnTrapOpened(SpriteAnimation anim)
        {
            CurrentAction = CalculateTime;
            boxCollider2D.enabled = true;
        }

        public override void FinishEffect()
        {
            CurrentAction = null;
            Color color = Color.white;
            color.a = 0;
            trap.DOColor(color, 1f).OnComplete(TrapDissapeared).SetEase(Ease.InFlash);
            boxCollider2D.enabled = false;
        }

        public void TrapDissapeared()
        {
            EffectExpired?.Invoke(this);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            MonsterLegsCollider trappedMonster = collision.gameObject.GetComponent<MonsterLegsCollider>();
            if(trappedMonster != null)
            {
                AddTrapEffect(trappedMonster.Owner);
                int roadMask = 1 << Values.LayerMasks.MonsterLegsLayer;
                Collider2D[] hits = Physics2D.OverlapBoxAll(boxCollider2D.bounds.center, boxCollider2D.size, 0f, roadMask);

                foreach(Collider2D coll in hits)
                {
                    MonsterLegsCollider monsterLegs = coll.gameObject.GetComponent<MonsterLegsCollider>();
                    if(monsterLegs != null && trappedMonster != monsterLegs)
                    {
                        Monster monster = monsterLegs.Owner;
                        AddTrapEffect(monster);
                    }
                }
            }

            FinishEffect();
        }

        private void AddTrapEffect(Monster monster)
        {
            HunterTrapEffect effect = Instantiate(trapEffectPrefab);
            effect.InitByData(trapData);
            monster.Effects.AddEffect(effect);
        }
    }

    [System.Serializable]
    public class HunterMapTrapData : HunterTrapEffectData
    {
        public float layingTime;
    }
}
