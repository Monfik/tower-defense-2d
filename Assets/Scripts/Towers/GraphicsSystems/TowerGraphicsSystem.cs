using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class TowerGraphicsSystem : MonoBehaviour, IUpgradable
    {
        [SerializeField]
        private GraphicsData graphicsData;
        public GraphicsData GraphicsData => graphicsData;

        [SerializeField]
        private SpriteRenderer buildRenderer;
        public SpriteRenderer BuildRenderer => buildRenderer;

        [SerializeField] 
        private List<GraphicsData> upgradeData;
        public List<GraphicsData> UpgradeData => upgradeData;

        private HighlightAnimator highlighter = new HighlightAnimator();
        public HighlightAnimator Highliter => highlighter;

        private Tower tower;

        public void Init(Tower tower)
        {
            this.tower = tower;
            tower.Builder.OnStartUpgrading += StartUpgrade;
            tower.Builder.OnFinishedUpgrading += Upgrade;
            tower.Builder.OnStartBuilding += StartBuild;
            tower.Builder.OnFinishedBuild += FinishBuild;
            tower.Builder.OnStartDemolishing += StartDemolish;
            tower.Builder.OnFinishDemolish += FinishDemolish;
            tower.Builder.OnStartFixing += StartFixing;
            tower.Builder.OnFinishedFix += FinishFix;
        }

        private void StartBuild()
        {
            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(false);
            }

            buildRenderer.gameObject.SetActive(true);
            buildRenderer.sprite = graphicsData.buildSprite;
            buildRenderer.material.SetTexture("_BuildTexture", buildRenderer.sprite.texture);
        }

        private void FinishBuild()
        {
            if(buildRenderer != null)
                buildRenderer.gameObject.SetActive(false);

            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(true);
            }
        }

        public void StartUpgrade(int level)
        {        
            foreach(SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(false);
            }
            buildRenderer.gameObject.SetActive(true);
            buildRenderer.sprite = upgradeData[level - 2].buildSprite;
            buildRenderer.material.SetTexture("_BuildTexture", buildRenderer.sprite.texture);
        }

        public void Upgrade(int level)
        {
            buildRenderer.gameObject.SetActive(false);

            graphicsData = upgradeData[level - 2];

            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(true);
            }

            //foreach (SpriteRenderer renderer in data.spritesToEnable)
            //{
            //    renderer.gameObject.SetActive(true);
            //}

            //foreach (SpriteRenderer renderer in data.spritesToDisable)
            //{
            //    renderer.gameObject.SetActive(false);
            //}

            //foreach (ReplaceSpriteData replacer in data.spriteReplacers)
            //{
            //    replacer.renderer.sprite = replacer.sprite;
            //}
        }

        public void UpdateSystem(float t)
        {
            buildRenderer.material.SetFloat("_Progress", t);
        }

        private void StartDemolish()
        {
            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(false);
            }

            buildRenderer.gameObject.SetActive(true);
            buildRenderer.sprite = graphicsData.buildSprite;
            buildRenderer.material.SetTexture("_BuildTexture", buildRenderer.sprite.texture);
        }

        private void StartFixing()
        {
            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(false);
            }

            buildRenderer.gameObject.SetActive(true);
            buildRenderer.sprite = graphicsData.buildSprite;
            buildRenderer.material.SetTexture("_BuildTexture", buildRenderer.sprite.texture);
        }

        private void FinishDemolish()
        {
            
        }

        private void FinishFix()
        {
            buildRenderer.gameObject.SetActive(false);

            foreach (SpriteRenderer renderer in graphicsData.visualSprites)
            {
                renderer.gameObject.SetActive(true);
            }
        }

        public void Highlight(HighlightData data)
        {
            List<Renderer> highlights = new List<Renderer>(graphicsData.visualSprites);
            highlights.Add(buildRenderer);
            highlighter.Highlight(data, highlights);

            tower.Builder.OnFinishedUpgrading += ChangeHighlightSprites;
        }

        private void ChangeHighlightSprites(int level)
        {
            List<Renderer> highlights = new List<Renderer>(graphicsData.visualSprites);
            highlights.Add(buildRenderer);
            highlighter.Highlight(highlights);
        }

        public void FinishHighlight()
        {
            highlighter.FinishHighlight();
            tower.Builder.OnFinishedUpgrading -= ChangeHighlightSprites;
        }

        public GraphicsData GetLevelGraphicsData(int level)
        {
            if(level - 2 >= upgradeData.Count)
            {
                Debug.LogError("Given level (" + level + ") is out of upgradeData count (" + upgradeData.Count + ")");
                return null;
            }

            return upgradeData[level - 2];
        }
    }
}
