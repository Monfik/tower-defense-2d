using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace Utils
{
    [ExecuteInEditMode]
    public class Highlight : MonoBehaviour
    {
        [SerializeField] private float glowSpeed;
        [SerializeField] private HighlightData highlightData;
        private float currentTime = 0f;
        private SpriteRenderer rend;
        [Range(0, 1)]
        public float tt;

        private void Awake()
        {
            rend = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            currentTime += Time.deltaTime * glowSpeed;
            float sin = Mathf.Sin(currentTime);
            float t = Mathf.InverseLerp(-1, 1, sin);
            rend.sharedMaterial.SetFloat("_Intensity", Mathf.Lerp(highlightData.startIntensity, highlightData.endIntensity, tt));
            rend.sharedMaterial.SetColor("_EmissionColor", Color.Lerp(highlightData.startColor, highlightData.endColor, tt));
        }

        private void OnEnable()
        {
            currentTime = 0f;
        }
    }
}
