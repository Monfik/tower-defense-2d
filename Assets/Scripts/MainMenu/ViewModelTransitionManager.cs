using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace View
{
    public class ViewModelTransitionManager : MonoBehaviour
    {
        [Header("Time")]
        [SerializeField]
        private float transitionTime;

        private float currentTime = 0f;

        [Header("Target Positions")]
        [SerializeField]
        private Vector2 hidePosition;

        [SerializeField]
        private Vector2 targetPosition;


        [Header("Animation Curves")]
        [SerializeField]
        private AnimationCurve introInterpolationCurve;

        [SerializeField]
        private AnimationCurve outroInterpolationCurve;

        private bool IsTransitioning = false;

        [Space(15)]
        [SerializeField]
        private BaseMainMenuView startView;

        private BaseMainMenuView outroView;
        private BaseMainMenuView currentView;

        public void ShowMenu()
        {
            DisplayMenuInstant(startView);
        }

        public void ChangeView(BaseMainMenuView view)
        {
            outroView = currentView;
            currentView = view;
            currentView.gameObject.SetActive(true);
            IsTransitioning = true;
        }

        private void DisplayMenuInstant(BaseMainMenuView view)
        {
            if(currentView != null && currentView != view)
            {
                currentView.gameObject.SetActive(false);
                currentView.Rect.anchoredPosition = new Vector2(hidePosition.x, hidePosition.y);
                currentView.Canvas.alpha = 0f;
            }

            currentView = view;
            currentView.gameObject.SetActive(true);
            currentView.Rect.anchoredPosition = new Vector2(targetPosition.x, targetPosition.y);
            currentView.Canvas.alpha = 1f;

            outroView = currentView;
        }


        private void Update()
        {
            if (IsTransitioning)
            {
                currentTime += Time.deltaTime;
                float t = currentTime / transitionTime;

                if (currentView)
                {
                    float evaluateAnimValue = introInterpolationCurve.Evaluate(t);
                    Vector2 currentIntroPosition = Vector2.Lerp(hidePosition, targetPosition, evaluateAnimValue);
                    currentView.Rect.anchoredPosition = new Vector3(currentIntroPosition.x, currentIntroPosition.y, currentView.transform.position.z);
                    currentView.Canvas.alpha = evaluateAnimValue;
                }

                if (outroView)
                {
                    float evaluateAnimValue = outroInterpolationCurve.Evaluate(t);
                    Vector2 currentOutroPosition = Vector2.Lerp(targetPosition, hidePosition, introInterpolationCurve.Evaluate(t));
                    outroView.Rect.anchoredPosition = new Vector3(currentOutroPosition.x, currentOutroPosition.y, outroView.transform.position.z);
                    outroView.Canvas.alpha = Mathf.Clamp(1 - evaluateAnimValue, 0, 1);
                }

                if (currentTime > transitionTime)
                {
                    outroView?.gameObject.SetActive(false);

                    IsTransitioning = false;
                    currentTime = 0f;
                }
            }
        }
    }

}