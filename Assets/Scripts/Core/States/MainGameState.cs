﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using LevelLogic;
using Utils;

namespace Core
{
    public class MainGameState : MachineState<MainGameState>, IBaseState<MainController>
    {
        private DataStorage storage;
        public DataStorage Storage => storage;

        private LevelsContainer levelContainer;

        [Space]
        [Header("DEV")]
        [SerializeField] private Level levelToLoadDEV;
        public enum StartState { Blueprint, MainGame };
        public StartState stateOnStart;

        public override void ChangeState(IBaseState<MainGameState> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {

        }

        #region StateImplementation
        public void Init(MainController controller)
        {
            storage = controller.Storage;
            levelContainer = storage.Levels;
            storage.ReferenceStorage.Settings.fraction.Init(storage);
            storage.ReferenceStorage.Spells.Init(storage);
            storage.ReferenceStorage.BlueprintController.Init(storage);
            storage.ReferenceStorage.EventsController.Init(storage);
            storage.ReferenceStorage.Brightness.Init(storage);
            storage.ReferenceStorage.WindController.Init(storage);
            storage.UIStorage.CostPanelUI.Init(storage);
            storage.ReferenceStorage.StockMarket.Init(storage);
            storage.ReferenceStorage.CameraMovement.Init(storage);
            storage.ReferenceStorage.MainGameCollection.Init(storage);

            if (stateOnStart == StartState.Blueprint)
                ChangeState(new BlueprintState());
            else if (stateOnStart == StartState.MainGame)
            {
                storage.ReferenceStorage.LevelManager.LoadLevel(levelToLoadDEV);
            }
        }

        public void UpdateState()
        {
            storage.ReferenceStorage.EventsController.UpdateController();
            currentState?.UpdateState();
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(MainController controller)
        {
            currentState?.Deinit(this);
            currentState = null;
            storage.ReferenceStorage.Spells.Deinit();
            storage.ReferenceStorage.Settings.fraction.Deinit();
            storage.ReferenceStorage.WindController.Deinit();
            storage.ReferenceStorage.BlueprintController.Deinit();
            storage.UIStorage.CostPanelUI.Deinit();
            storage.ReferenceStorage.StockMarket.Deinit();
            storage.ReferenceStorage.MainGameCollection.Deinit();
            storage.ReferenceStorage.CameraMovement.Deinit();
        }
        #endregion

        public void SetLevelState()
        {
            storage.ReferenceStorage.TransitionLoader.LoadByEvent(() => { ChangeState(new LevelState()); });
        }

        public void SetLevelState(Level level)
        {
            LevelManager levelController = storage.ReferenceStorage.LevelManager;
            storage.ReferenceStorage.TransitionLoader.LoadByEvent(() => { SetEmptyState(); ChangeState(new LevelState(level, levelController)); });
        }

        public void SetBlueprintState()
        {
            storage.ReferenceStorage.TransitionLoader.LoadByEvent(() => ChangeState(new BlueprintState()));
        }

        public void SetEmptyState()
        {
            ChangeState(null);
        }
    }

}