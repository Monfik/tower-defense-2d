using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class MonsterDataValueUI : MonoBehaviour
    {
        [SerializeField] private string valueName;
        public string ValueName => valueName;

        [SerializeField] private TMP_Text valueText;

        public void SetupValue(int value)
        {
            valueText.text = value.ToString();
        }
    }
}
