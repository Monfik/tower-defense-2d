using System;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using Utils;

namespace TowerDefense
{
    public class Raycaster : BaseInitable
    {
        private Monster currentRaycastedMonster;

        public Func<Monster> Monster { get; private set; }

        public void UpdateRaycaster()
        {
            RaycastForMonster();
        }

        private void RaycastForMonster()
        {
            int layer = 1 << Values.LayerMasks.MonsterLayer;
            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            Collider2D raycasted = Physics2D.OverlapPoint(mousePosition, layer);

            if(raycasted == null)
            {
                if(currentRaycastedMonster != null)
                {
                    storage.EventsStorage.UtilEvents.CallOnMonsterRaycastedStop(currentRaycastedMonster);
                    currentRaycastedMonster = null;
                }
                return;
            }

            Monster raycastedMonster = raycasted.gameObject.GetComponent<Monster>();
            if (raycastedMonster != null)
            {
                if (raycastedMonster == currentRaycastedMonster)
                    return;
                else
                {
                    if (currentRaycastedMonster != null)
                    {
                        storage.EventsStorage.UtilEvents.CallOnMonsterRaycastedStop(currentRaycastedMonster);
                        currentRaycastedMonster = null;
                        
                    }

                    storage.EventsStorage.UtilEvents.CallOnMonsterRaycasted(raycastedMonster);
                    currentRaycastedMonster = raycastedMonster;
                }
            }
        }
    }
}
