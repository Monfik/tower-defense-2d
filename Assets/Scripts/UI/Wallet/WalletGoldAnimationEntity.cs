using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CoreUI
{
    public class WalletGoldAnimationEntity : MonoBehaviour
    {
        [SerializeField] private Image image;

        [SerializeField] private Animator animator;
        private UnityAction<WalletGoldAnimationEntity> entityFinishedAnimation;

        public void StartAnimation(UnityAction<WalletGoldAnimationEntity> entityFinishedAnimation, string animationName)
        {
            this.entityFinishedAnimation = entityFinishedAnimation;
            animator.enabled = true;
            animator.Play(animationName);
        }

        public void Finish()
        {
            entityFinishedAnimation?.Invoke(this);
        }
    }
}
