using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class raycastTest : MonoBehaviour
{
    public Vector2 raycastBoxSize;

    [SerializeField]
    private Camera cam;

    void Update()
    {
        Vector3 VScreen = new Vector3();
        Vector3 mousePosition = new Vector3();

        VScreen.x = Input.mousePosition.x;
        VScreen.y = Input.mousePosition.y;
        VScreen.z = Camera.main.transform.position.z;
        mousePosition = cam.ScreenToWorldPoint(VScreen);

        RaycastHit2D hit = Physics2D.Raycast(mousePosition, transform.TransformDirection(Vector2.up), 5f);

        if (hit)
        {
            Debug.Log(hit.transform.name);
        }
    }

    private void OnDrawGizmos()
    {
        Vector3 VScreen = new Vector3();
        Vector3 mousePosition = new Vector3();

        VScreen.x = Input.mousePosition.x;
        VScreen.y = Input.mousePosition.y;
        VScreen.z = Camera.main.transform.position.z;
        mousePosition = cam.ScreenToWorldPoint(VScreen);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(mousePosition, raycastBoxSize);
    }
}
