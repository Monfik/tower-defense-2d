using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;

namespace LevelLogic
{
    public class Map : MonoBehaviour
    {
        [SerializeField]
        private List<Animator> elementsAnimators;

        [SerializeField]
        private List<PathCreator> paths;
        public List<PathCreator> Paths => paths;

        [SerializeField]
        private List<PathCreator> middlePaths;
        public List<PathCreator> MiddlePaths => middlePaths;

        private DataStorage storage;

        private MapEffectsController effectsController = new MapEffectsController();

        [Header("Data")]
        [SerializeField]
        private MapLimitsData mapLimits;
        public MapLimitsData MapLimits => mapLimits;

        public virtual void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.ReferenceStorage.PathSnapper.Init(this);
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared += effectsController.AddEffect;

            List<MapEffect> mapEffects = new List<MapEffect>();
        }

        public virtual void UpdateMap()
        {
            effectsController.UpdateController();
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared -= effectsController.AddEffect;
            FinishAllMapEffects();
        }

        private void FinishAllMapEffects()
        {
            for (int i = effectsController.MapEffects.Count - 1; i >= 0; i--)
            {
                effectsController.MapEffects[i].FinishEffect();
            }
        }

        public Vector2 GetSize()
        {
            Vector2 size = new Vector2(Mathf.Abs(mapLimits.rightBottomCorner.position.x - mapLimits.leftUpperCorner.position.x),
                Mathf.Abs(mapLimits.leftUpperCorner.position.y - mapLimits.rightBottomCorner.position.y));

            return size;
        }
    }

    public class MapEffectsController
    {
        private List<MapEffect> mapEffects = new List<MapEffect>();
        public List<MapEffect> MapEffects => mapEffects;

        public void AddEffect(MapEffect effect)
        {
            effect.AssignEvents(OnEffectFinished);
            mapEffects.Add(effect);
        }

        public void UpdateController()
        {
            for (int i = 0; i < mapEffects.Count; i++)
            {
                mapEffects[i].UpdateEffect();
            }
        }

        private void OnEffectFinished(MapEffect effect)
        {
            mapEffects.Remove(effect);
            GameObject.Destroy(effect.gameObject);
        }
    }

    [System.Serializable]
    public class MapLimitsData
    {
        public Transform leftUpperCorner;
        public Transform rightBottomCorner;

        public Vector2 GetCenter()
        {
            Vector2 center = new Vector2((leftUpperCorner.position.x + rightBottomCorner.position.x) / 2f, (leftUpperCorner.position.y + rightBottomCorner.position.y) / 2f);
            return center;
        }
    }
}
