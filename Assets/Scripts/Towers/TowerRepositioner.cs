using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Towers
{
    public class TowerRepositioner
    {
        private ITowerMovable movable;
        private DataStorage storage;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }

        public void AddToMove(ITowerMovable movable)
        {
            this.movable = movable;
            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.DEFAULT_REPOSITIONER_ACCEPT_KEY, Values.InputValues.DEFAULT_ACCEPT_KEY, null, UpdateRepositioners, MouseLeftUp);
        }

        public void UpdateRepositioners()
        {
            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        private void MouseLeftUp()
        {
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.DEFAULT_REPOSITIONER_ACCEPT_KEY);
        }
    }
}
