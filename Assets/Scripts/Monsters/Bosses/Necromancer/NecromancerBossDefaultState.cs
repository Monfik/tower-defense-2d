using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class NecromancerBossDefaultState : BaseBossState<NecromancerBoss>
    {
        [SerializeField] private List<BaseBossState<NecromancerBoss>> availableStates;
        public List<BaseBossState<NecromancerBoss>> AvailableStates => availableStates;

        private Timer checkStatesTimer;

        public override void Init(NecromancerBoss boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Walk");

            checkStatesTimer = gameObject.AddComponent<Timer>();
            checkStatesTimer.Setup(0.2f, CheckStatesAvailable, false);
        }

        public override void Deinit()
        {
            Destroy(checkStatesTimer);
        }

        public override void UpdateState()
        {
            boss.UpdateMovement();            
        }

        private void CheckStatesAvailable()
        {
            BaseBossState<NecromancerBoss> newState = availableStates.Find(c => c.IsReadyForWork());
            if (newState != null)
            {
                boss.ChangeState(newState);
            }
        }
    }
}
