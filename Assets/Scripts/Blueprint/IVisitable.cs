using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Blueprints
{
    public interface IVisitable
    {
        void SetState(LocationCheckpoint.LocationState state);
        bool CanBeUnlocked();
    }
}
