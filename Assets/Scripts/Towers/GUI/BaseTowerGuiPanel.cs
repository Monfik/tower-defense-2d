using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class BaseTowerGuiPanel : MonoBehaviour
    {
        protected Tower currentTower;
        protected DataStorage storage;
        protected TowerSelector gui;

        public virtual void Open(Tower tower, TowerSelector gui, DataStorage storage)
        {
            gameObject.SetActive(true);
            currentTower = tower;
            this.gui = gui;
            this.storage = storage;
        }

        public virtual void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
