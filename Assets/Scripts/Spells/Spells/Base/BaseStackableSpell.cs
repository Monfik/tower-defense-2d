using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class BaseStackableSpell : BaseSpell
    {
        [SerializeField] 
        protected int maxStacks;
        public int MaxStacks => maxStacks;

        [SerializeField]
        private int startStacks;
        public int StartStacks => startStacks;

        public int currentStackAmount;

        [SerializeField] 
        private BaseSpell spell;
        public BaseSpell Spell => spell;

        public override void Unlock(DataStorage storage, ISpellHoldable holder)
        {
            base.Unlock(storage, holder);
            currentStackAmount = startStacks;
        }

        public override void UpdateSpell()
        {
            
        }

        public override void PayMana()
        {
            storage.ReferenceStorage.ManaController.Substract(spell.ManaCost);
        }

        public override float GetCooldown()
        {
            return spell.Cooldown;
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Max traps", $"{maxStacks}"));

            TooltipSpellDescriptionData spellData = spell.CreateTooltipData();
            foreach(DescriptionSingleSpecifiedData specifiedData in spellData.specifiedDatas)
            {
                data.specifiedDatas.Add(specifiedData);
            }

            return data;
        }
    }
}
