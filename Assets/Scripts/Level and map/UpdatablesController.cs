using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Updatables
{
    public class UpdatablesController : BaseInitable
    {
        public UpdatablesContainer generalUpdatables;
        public UpdatablesContainer spellsUpdatables;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            generalUpdatables = new UpdatablesContainer(storage, transform);
            spellsUpdatables = new UpdatablesContainer(storage, transform);
        }

        public void UpdateUpdatables()
        {
            generalUpdatables.UpdateUpdatables();
            spellsUpdatables.UpdateUpdatables();
        }

        public void Finish()
        {
            generalUpdatables.Finish();
            spellsUpdatables.Finish();
        }
    }

    public class UpdatablesContainer
    {
        [SerializeField] private List<IUpdatable> baseUpdatables = new List<IUpdatable>();
        [SerializeField] private int updatablesCount;

        private DataStorage storage;
        private Transform container;

        public UpdatablesContainer(DataStorage storage, Transform container)
        {
            this.storage = storage;
            this.container = container;
        }

        public void AddUpdatable(IUpdatable updatable)
        {
            updatable.StartUpdate(storage, OnUpdatableFinished);
            baseUpdatables.Add(updatable);

            MonoBehaviour mono = updatable as MonoBehaviour;
            if (mono != null)
            {
                mono.transform.SetParent(container);
            }
            updatablesCount = baseUpdatables.Count;
        }

        public void UpdateUpdatables()
        {
            for (int i = 0; i < baseUpdatables.Count; i++)
            {
                baseUpdatables[i].UpdateUpdatable();
            }
        }

        public void OnUpdatableFinished(IUpdatable updatable)
        {
            baseUpdatables.Remove(updatable);

            MonoBehaviour mono = updatable as MonoBehaviour;
            if (mono != null)
            {
                GameObject.Destroy(mono.gameObject);
            }

            updatablesCount = baseUpdatables.Count;
        }

        public void Finish()
        {
            for (int i = baseUpdatables.Count - 1; i >= 0; i--)
            {
                OnUpdatableFinished(baseUpdatables[i]);
            }

            baseUpdatables.Clear();
        }
    }
}
