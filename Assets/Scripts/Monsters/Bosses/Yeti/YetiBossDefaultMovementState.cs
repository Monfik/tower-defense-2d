using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class YetiBossDefaultMovementState : BaseBossState<YetiBossBehaviour>
    {
        [SerializeField] private List<BaseBossState<YetiBossBehaviour>> availableStates;
        public List<BaseBossState<YetiBossBehaviour>> AvailableStates => availableStates;

        private Timer checkStatesTimer;

        public override void Init(YetiBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Walk");

            checkStatesTimer = gameObject.AddComponent<Timer>();
            checkStatesTimer.Setup(0.3f, CheckStatesAvailable, false);
        }

        public override void Deinit()
        {
            Destroy(checkStatesTimer);
        }

        public override void UpdateState()
        {
            boss.UpdateMovement();
        }

        private void CheckStatesAvailable()
        {
            BaseBossState<YetiBossBehaviour> newState = availableStates.Find(c => c.IsReadyForWork());
            if (newState != null)
            {
                boss.ChangeState(newState);
            }
        }
    }
}
