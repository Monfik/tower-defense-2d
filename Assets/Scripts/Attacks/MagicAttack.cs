using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Power
{
    public class MagicAttack : IPowerAttackable
    {
        private MagicAttackData data;

        public MagicAttack(MagicAttackData data)
        {
            this.data = data;
        }

        public void Attack(IDestructable dmgTakable)
        {
            int armor = dmgTakable.GetData().GetDataByType(data.magicType).currentValue;
            armor -= data.magicArmorReduction;
            int randomDmg = Random.Range(data.minDmg, data.maxDmg);
            int dmg = DamageCalculator.CalculateDamage(randomDmg, armor);
            dmgTakable.TakeDamage(dmg, DamageType.Magic);
        }

        public void Attack(IDestructable dmgTakable, float dmgFactor)
        {
            int armor = dmgTakable.GetData().GetDataByType(data.magicType).currentValue;
            armor -= data.magicArmorReduction;
            int randomDmg = Random.Range(data.minDmg, data.maxDmg);
            randomDmg = (int)(randomDmg * dmgFactor);
            int dmg = DamageCalculator.CalculateDamage(randomDmg, armor);
            dmgTakable.TakeDamage(dmg, DamageType.Magic);
        }
    }

    [System.Serializable]
    public class MagicAttackData
    {
        public int minDmg;
        public int maxDmg;
        public int magicArmorReduction;
        public MagicType magicType;

        public MagicAttackData(MagicAttackData data, float factor)
        {
            minDmg = (int)(data.minDmg / factor);
            maxDmg = (int)(data.maxDmg / factor);
            magicArmorReduction = data.magicArmorReduction;
            magicType = data.magicType;
        }
    }
}
