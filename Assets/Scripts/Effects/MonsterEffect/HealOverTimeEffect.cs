using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class HealOverTimeEffect : BaseMonsterEffect
    {
        [SerializeField] private float interval = 1f;
        private float currentIntervalTime = 0f;
        [SerializeField] private int healPerSecond;

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);
            monster.Heal(healPerSecond); //initally
        }

        public override void UpdateEffect()
        {
            currentIntervalTime += Time.deltaTime;
            if(currentIntervalTime >= interval)
            {
                monster.Heal(Mathf.RoundToInt(healPerSecond * interval));
                currentIntervalTime = 0f;
            }

            base.UpdateEffect();
        }
    }
}
