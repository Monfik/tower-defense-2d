using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.GlobalEvents;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class GenieBossSandstorm : BaseBossState<GenieBossBehaviour>
    {
        [Header("Values")]
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private float sandstormCallAnimationDuration;

        [SerializeField] private SandstormEvent sandstormEvent;

        private bool casted = false;

        public override void PreInit(GenieBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + pathInterval.intervalRandomRange.GetRandom();
        }

        public override void Init(GenieBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Sandstorm");
            storage.ReferenceStorage.Postponer.InvokePostponeAction(StartSandstorm);
        }

        public void StartSandstorm()
        {  
            float resurectAnimationDuration = boss.Animator.GetCurrentAnimatorStateInfo(0).length;
            float animatorSpeed = resurectAnimationDuration / sandstormCallAnimationDuration;
            boss.Animator.SetFloat("SandstormSpeed", animatorSpeed);
        }

        //Called from animator
        public void CallSandstormEvent()
        {
            SandstormEvent sandstorm = Instantiate(sandstormEvent);
            sandstorm.gameObject.SetActive(true);
            storage.ReferenceStorage.EventsController.BeginLevelEvent(sandstorm);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + pathInterval.intervalRandomRange.GetRandom();
            casted = true;       
        }

        //Called from animator
        public void AnimationFinished()
        {
            boss.SetDefaultMovementState();
        }

        public override void UpdateState()
        {

        }

        public override bool IsReadyForWork()
        {
            if (boss.DistanceTravelled >= pathInterval.nextValue && !casted)
            {
                return true;
            }

            return false;
        }

        public override void Deinit()
        {
            
        }
    }
}
