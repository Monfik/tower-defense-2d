using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class UpgradableStaticsData : IGUIElementable
    {
        public TowerStaticsData data;
        public List<TowerStaticsData> upgrades;

        [HideInInspector]
        public int level = 1;

        public UpgradableStaticsData(int value)
        {
            data = new TowerStaticsData(value);
            data.Reset();
            data.multiplier = 1f;
            upgrades = new List<TowerStaticsData>();

            for(int i=0; i<10; i++)
            {
                upgrades.Add(new TowerStaticsData(value));
                upgrades[i].multiplier = 1f;
            }
        }

        public void Upgrade(int level)
        {
            this.level = level;
            TowerStaticsData newData = upgrades[level - 2];
            newData.Reset();
            newData.BuffByValue(data.buffValue);
            newData.SetupMultiplier(data.multiplier);
            data = newData;
        }

        public TowerStaticsData GetNextUpgradeData()
        {
            return upgrades[level - 1];
        }

        public TowerStaticsData GetNextUpgradeDataBasedOnCurrent()
        {
            TowerStaticsData upgradedData = new TowerStaticsData(upgrades[level - 1]);
            upgradedData.BuffByValue(data.buffValue);
            upgradedData.SetupMultiplier(data.multiplier);
            return upgradedData;
        }

        public void Reset()
        {
            level = 1;
            data.Reset();
            data.multiplier = 1f;

            foreach(TowerStaticsData statics in upgrades)
            {
                statics.Reset();
                statics.multiplier = 1f;
            }
        }
    }

    [System.Serializable]
    public class UpgradableStaticsFloatData : IGUIElementable
    {
        public TowerStaticsDataFloat data;
        public List<TowerStaticsDataFloat> upgrades;

        [HideInInspector] public int level = 1;

        public UpgradableStaticsFloatData(int value)
        {
            data = new TowerStaticsDataFloat(value);
            upgrades = new List<TowerStaticsDataFloat>();

            for (int i = 0; i < 10; i++)
            {
                upgrades.Add(new TowerStaticsDataFloat(value));
            }
        }

        public void Upgrade(int level)
        {
            this.level = level;
            TowerStaticsDataFloat newData = upgrades[level - 2];
            newData.Reset();
            newData.BuffByValue(data.buffValue);
            data = newData;
        }

        public TowerStaticsDataFloat GetNextUpgradeData()
        {
            return upgrades[level - 1];
        }

        public TowerStaticsDataFloat GetNextUpgradeDataBasedOnCurrent()
        {
            TowerStaticsDataFloat upgradedData = new TowerStaticsDataFloat(upgrades[level - 1]);
            upgradedData.BuffByValue(data.buffValue);
            return upgradedData;
        }

        public void Reset()
        {
            data.Reset();

            foreach (TowerStaticsDataFloat statics in upgrades)
            {
                statics.Reset();
            }
        }
    }
}
