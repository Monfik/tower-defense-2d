using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LevelLogic
{
    public interface ILevelFinishable
    {
        void StartFinish(UnityAction<ILevelFinishable> OnFinished, Level level);
        void UpdateFinisher();
    }
}
