using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class NinjaGhostSpell : BaseSpell
    {
        [Header("Refs")]
        [SerializeField] private NinjaGhostMapEffect ghostMapEffect;
        [SerializeField] private SpriteRenderer ninjaGhostPreview;

        private PathSnapper snapper;

        [Header("Colors")]
        [SerializeField] private Color possibleCastColor;
        [SerializeField] private Color unpossibleCastColor;

        [Header("Data")]
        [SerializeField] private NinjaGhostData ghostData;

        public override void StartCast()
        {
            base.StartCast();
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            snapper = storage.ReferenceStorage.PathSnapper;
            ninjaGhostPreview.gameObject.SetActive(true);
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 snappedPosition;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);

            if (snapped)
            {
                ninjaGhostPreview.transform.position = snappedPosition;
                ninjaGhostPreview.material.SetColor("_Color", possibleCastColor);
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (Values.InputValues.POINTER_OVER_UI)
                        return;

                    SpawnGhost(snappedPosition);
                }
            }
            else
            {
                ninjaGhostPreview.material.SetColor("_Color", unpossibleCastColor);
                ninjaGhostPreview.transform.position = mousePosition;
            }
        }

        private void SpawnGhost(Vector2 position)
        {
            NinjaGhostMapEffect ghost = Instantiate(ghostMapEffect);
            ghost.gameObject.SetActive(true);
            PathCreator path = snapper.GetClosestPath(position);
            ghost.NinjaGhost.Init(path, storage.ReferenceStorage.LevelManager.CurrentLevel.Map, ghostData, position);
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(ghost);
            PayMana();
            OnSpellCasted?.Invoke(this);
            FinishSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=5>Physical hit", $"{ ghostData.dmg.minDmg} - {ghostData.dmg.maxDmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Armor reduction", $"{ghostData.dmg.armorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Duration ", $"{ghostMapEffect.Duration}"));
            return data;
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            base.FinishSpell();
        }
    }
}
