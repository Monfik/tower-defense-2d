using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public interface IStaticsUpgradable<T>
    {
        void Reset();
        void BuffByValue(T value);
        void Upgrade(int level);
    }
}
