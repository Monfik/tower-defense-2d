using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Towers
{
    public class TowerEffectPreview : MonoBehaviour
    {
        public Image image;
        [SerializeField] private TipDescription tip;
        public TipDescription Tip => tip;
    }
}
