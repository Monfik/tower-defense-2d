using Data;
using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Creatures
{
    public class Monster : MonoBehaviour, IDestructable, ITargetable, ITransparable, ISelectable, IHealable
    {
        [SerializeField] private string monsterName;
        public string Name { get { return monsterName; } set { monsterName = value; } }

        [SerializeField] private Sprite preview;
        public Sprite Preview { get { return preview; } set { preview = value; } }

        [Header("Ref")]
        [SerializeField]
        new protected SpriteRenderer renderer;
        public SpriteRenderer Renderer => this.renderer;

        [SerializeField]
        protected Animator animator;
        public Animator Animator => animator;

        [SerializeField]
        protected CanvasGroup canvasGroup;
        public CanvasGroup CanvasGroup => canvasGroup;

        [SerializeField]
        protected RectTransform barsHolder;
        public RectTransform BarsHolder => barsHolder;

        [SerializeField]
        protected SlidableBar healthBar;
        public SlidableBar HealthBar => healthBar;

        [SerializeField]
        new protected BoxCollider2D collider;
        public BoxCollider2D Collider => this.collider;

        protected VertexPath path;
        public VertexPath Path => path;
        protected DataStorage storage;

        protected float distanceTravelled = 0f;
        public float DistanceTravelled => distanceTravelled;

        [Header("Properties")]
        [SerializeField]
        protected MonsterData data;
        public MonsterData Data => data;

        public enum MonsterType { Ground, Fly };
        [SerializeField] private MonsterType type;
        public MonsterType Type => type;

        [SerializeField]
        protected EffectsController effects;
        public EffectsController Effects => effects;

        protected bool dead = false;
        public bool Dead => dead;

        private MonsterLegsCollider legs;
        public MonsterLegsCollider Legs => legs;

        [SerializeField] private HealEffect healEffect;

        public List<DamageShield> damageShields = new List<DamageShield>();

        public UnityAction<ISelectable> SelectableUnavailable { get; set; }
        public UnityAction<Monster> OnMonsterDied;

        private void Awake()
        {
            legs = GetLegsCollider();
            legs.Collider.enabled = false;
        }

        //borned
        public virtual void Init(DataStorage storage, VertexPath path, MonsterData data, float distanceTravelled = 0f)
        {
            this.storage = storage;
            this.path = path;
            this.data = data;
            data.Reset();

            this.distanceTravelled = distanceTravelled;

            if (path != null)
            {
                Vector3 position = path.GetPointAtDistance(distanceTravelled);
                transform.position = position;
            }

            healthBar.SetMaxValue(data.hp);
            effects = new EffectsController(this);
            SetSpeed(data.speed.currentValue);

            legs.Collider.enabled = true;
        }

        private MonsterLegsCollider GetLegsCollider()
        {
            MonsterLegsCollider ownLegs = transform.GetComponentInChildren<MonsterLegsCollider>();
            return ownLegs;
        }

        public void SetSpeed(float speed)
        {
            animator.SetFloat("WalkSpeed", speed / Values.GeneralValues.monsterSpeedDivider);
        }

        public virtual void UpdateMonster()
        {
            UpdateMovement();
            effects.UpdateEffects();
        }

        public virtual void Die()
        {
            dead = true;
            storage.EventsStorage.CoreEvnts.CallOnMonsterDied(this);
            OnMonsterDied?.Invoke(this);
            SelectableUnavailable?.Invoke(this);
            MonsterDeathController deathController = renderer.gameObject.AddComponent<MonsterDeathController>();
            deathController.Init(this);
            deathController.StartDeath();
        }

        public virtual void UpdateMovement()
        {
            Vector3 currentPosition = transform.position;
            distanceTravelled += data.speed.currentValue / Values.GeneralValues.monsterSpeedDivider * Time.deltaTime;
            Vector3 position = path.GetPointAtDistance(distanceTravelled);
            transform.position = position;

            if (position.x != currentPosition.x)
                renderer.transform.rotation = Quaternion.Euler(0f, position.x < currentPosition.x ? 180f : 0f, 0f);

            if (distanceTravelled >= path.length)
                EndPath();
        }

        public void TakeDamage(int dmg, DamageType damageType)
        {
            if (dead)
                return;

            int damageTaken = Mathf.RoundToInt(dmg * data.damageMultiplier);

            int damageReduced = 0;

            for (int i = damageShields.Count - 1; i >= 0; i--)
            {
                int dmgReduce = damageShields[i].Reduce(damageTaken - damageReduced, damageTaken, damageType);
                damageReduced += dmgReduce;

                if (damageShields[i].shieldData.shieldValue <= 0)
                    damageShields.RemoveAt(i);

                if (damageReduced >= damageTaken)
                    break;
            }

            damageReduced = Mathf.Clamp(damageReduced, 0, damageTaken);
            damageTaken -= damageReduced;

            data.hp -= damageTaken;
            healthBar.SetValue(data.hp);

            if (data.hp <= 0)
            {
                data.hp = 0;
                Die();
            }
        }

        private void EndPath()
        {
            SelectableUnavailable?.Invoke(this);
            storage.EventsStorage.CoreEvnts.CallOnMonsterFinishedPath(this);
        }

        public MonsterData GetData()
        {
            return data;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void MakeTransparable(float alpha)
        {
            Color rendColor = renderer.color;
            rendColor.a = alpha;
            renderer.color = rendColor;
            canvasGroup.alpha = alpha;
        }

        public Vector3 GetCenter()
        {
            Vector3 center = transform.position;
            center.y += collider.size.y / 2;
            return center;
        }

        public void OnSelected()
        {
            storage.UIStorage.MonsterDataPanel.DisplayBasicData(this);
        }

        public void OnDeselected()
        {
            storage.UIStorage.MonsterDataPanel.Hide();
        }

        public void Heal(int value)
        {
            data.hp += value;
            if (data.hp > healthBar.MaxValue)
            {
                data.hp = healthBar.MaxValue;
            }

            healthBar.SetValue(data.hp);

            HealEffect healEffectSpawned = Instantiate(healEffect, this.transform);
            healEffectSpawned.gameObject.SetActive(true);
            healEffectSpawned.transform.position = GetCenter();
            DOVirtual.DelayedCall(healEffectSpawned.EffectDuration, () => Destroy(healEffectSpawned.gameObject), false);
        }

        private void OnDestroy()
        {
            SelectableUnavailable?.Invoke(this);
        }
    }

    public class DamageShield
    {
        public DamageShieldData shieldData;
        public UnityAction<int> OnShieldValueUpdate;

        public DamageShield(DamageShieldData shieldData, UnityAction<int> OnShieldValueUpdate)
        {
            this.shieldData = shieldData;
            this.OnShieldValueUpdate = OnShieldValueUpdate;
        }

        public int Reduce(int dmg, int dmgMax, DamageType damageType)
        {
            if (damageType != shieldData.damageType)
                return 0;

            int damageReduce = Mathf.RoundToInt(dmgMax * shieldData.factor);
            damageReduce = Mathf.Clamp(damageReduce, 0, shieldData.shieldValue);

            shieldData.shieldValue -= damageReduce;
            if (shieldData.shieldValue < 0)
            {
                shieldData.shieldValue = 0;
            }

            OnShieldValueUpdate?.Invoke(shieldData.shieldValue);
            return damageReduce;
        }

        [System.Serializable]
        public class DamageShieldData
        {
            public int priority;
            public float factor;
            public int shieldValue;
            public DamageType damageType;
            public UnityAction<int> OnShieldValueUpdate;
        }
    }
}
