using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class AttackReducer : MonoBehaviour
    {
        public List<EfficientAttackDmgMultiplierReducer> reducers;
        public EfficientAttackDmgMultiplierReducer currentReducer;

        public void UpdateEfficient(float efficient)
        {
            EfficientAttackDmgMultiplierReducer reducer = reducers.Find(c => (c.range.start < efficient && c.range.end >= efficient));
            if (currentReducer != reducer)
                ChangeReducer(reducer, efficient);
        }

        private void ChangeReducer(EfficientAttackDmgMultiplierReducer reducer, float efficient)
        {
            currentReducer?.Restore();
            currentReducer = reducer;
            currentReducer?.Reduce(efficient);
        }
    }
}
