using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class WizardBossBehaviour : BossBehaviour<WizardBossBehaviour>
    {
        [Header("States")]
        [SerializeField] private WizardDefaultMovementState defaultMovementState;

        public override void Init(DataStorage storage, VertexPath path, MonsterData data, float distanceTravelled = 0f)
        {
            base.Init(storage, path, data);
            defaultMovementState.AvailableStates.ForEach(c => c.PreInit(this, storage));
            ChangeState(defaultMovementState);
        }

        public override void ChangeState(BaseBossState<WizardBossBehaviour> newState)
        {
            base.ChangeState(newState);
            newState.Init(this, storage);
        }

        public void SetDefaultMovementState()
        {
            ChangeState(defaultMovementState);
        }
    }
}
