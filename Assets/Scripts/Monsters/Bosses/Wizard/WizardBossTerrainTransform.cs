using Data;
using DG.Tweening;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefense.Enviroment;
using UnityEngine;
using Utils;

namespace TowerDefense.Creatures
{
    public class WizardBossTerrainTransform : BaseBossState<WizardBossBehaviour>
    {
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private float spawningDuration = 2f;
        [SerializeField] private List<MapDecor> mapDecors;
        [SerializeField] private List<MapDecor> spawnedDecors;

        private Vector2 nextSpawnDecorPoint;
        private MapDecor nextMapDecor;

        private TerrainRemover extraTerrainRemover;

        public override void PreInit(WizardBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);

            if(extraTerrainRemover == null)
            {
                extraTerrainRemover = new TerrainRemover();
                storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(extraTerrainRemover);
            }
        }

        public override void Init(WizardBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);

            boss.Animator.Play("CreateTerrain");
            StartSpawn(nextMapDecor, nextSpawnDecorPoint);
        }

        public void TerrainCreated()
        {
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
            boss.SetDefaultMovementState();
        }

        private bool TrySpawnDecor()
        {
            MapDecor randomedMapDecor = mapDecors[Random.Range(0, mapDecors.Count)];
            return TrySpawnDecor(randomedMapDecor);
        }

        private bool TrySpawnDecor(MapDecor decor)
        {
            Vector2 decorSize = decor.boxOverlapCollider.size;

            Map currentMap = storage.ReferenceStorage.LevelManager.CurrentLevel.Map;
            Vector2 leftTopPoint = currentMap.MapLimits.leftUpperCorner.transform.position;
            Vector2 rightBotPoint = currentMap.MapLimits.rightBottomCorner.transform.position;

            Vector2 maxLeftTopPoint = new Vector2(leftTopPoint.x + decor.boxOverlapCollider.size.x / 2, leftTopPoint.y - decor.boxOverlapCollider.size.y / 2);
            Vector2 maxRightBotPoint = new Vector2(rightBotPoint.x - decor.boxOverlapCollider.size.x / 2, rightBotPoint.y + decor.boxOverlapCollider.size.y / 2);

            float[] randomMultipliers = new float[3] { Random.Range(1.5f, 2.6f), Random.Range(1.5f, 2.6f), Random.Range(1.5f, 2.4f) };
            int multiplierIndex = 0;

            List<Vector2> positions = new List<Vector2>();

            for (float i = maxLeftTopPoint.x; i < maxRightBotPoint.x; i += decorSize.x * randomMultipliers[multiplierIndex % 3])
            {
                for (float j = maxRightBotPoint.y; j < maxLeftTopPoint.y; j += decorSize.y * randomMultipliers[multiplierIndex % 3])
                {
                    Vector2 caseSpot = new Vector2(i, j);
                    multiplierIndex++;
                    positions.Add(caseSpot);
                }
            }

            positions.Shuffle();

            for (int i = 0; i < positions.Count; i++)
            {
                Collider2D collided =  Physics2D.OverlapBox(positions[i], decorSize, 0f);
                if(collided == null)
                {
                    nextSpawnDecorPoint = positions[i];
                    nextMapDecor = decor;
                    return true;
                }
            }

            return false;
        }

        public void StartSpawn(MapDecor decor, Vector2 point)
        {
            MapDecor spawned = Instantiate(decor);
            spawned.MakeTransparable(0f);
            spawned.transform.position = point;
            spawned.gameObject.SetActive(true);

            spawnedDecors.Add(spawned);
            extraTerrainRemover.terrains.Add(spawned.gameObject);

            DOVirtual.Float(0f, 1f, spawningDuration, (f) => OnCreateTerrainFloatUpdate(f, spawned)).OnComplete(TerrainCreated).SetId("Spawn terrain transform");
        }

        private void OnCreateTerrainFloatUpdate(float alpha, ITransparable renderer)
        {
            renderer.MakeTransparable(alpha);
        }

        public override void UpdateState()
        {

        }

        public override bool IsReadyForWork()
        {
            if (boss.DistanceTravelled >= pathInterval.nextValue)
            {
                return TrySpawnDecor();
            }

            return false;
        }

        public override void Deinit()
        {

        }
    }
}
