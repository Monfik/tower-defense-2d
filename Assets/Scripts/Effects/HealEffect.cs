using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class HealEffect : MonoBehaviour
    {
        [SerializeField] private float effectDuration;
        public float EffectDuration => effectDuration;
        [SerializeField] private ParticleSystem particles;
    }
}
