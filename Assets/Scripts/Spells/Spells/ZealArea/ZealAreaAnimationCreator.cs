using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Spells
{
    public class ZealAreaAnimationCreator : MonoBehaviour
    {
        [SerializeField] 
        private Animator animator;
        public Animator Animator => animator;
        public UnityAction AnimationFinished;

        public void OnAnimationFinished()
        {
            animator.enabled = false;
            AnimationFinished?.Invoke();
        }
    }
}
