using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInitable : MonoBehaviour
{
    protected DataStorage storage;

    public virtual void Init(DataStorage storage)
    {
        this.storage = storage;
    }

    public virtual void Deinit()
    {

    }
}
