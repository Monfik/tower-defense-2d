using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class TrapPlacementSpell : BaseSpell
    {
        [SerializeField] private SpellTargetMarker targetMarker;
        [SerializeField] private HunterTrapMapEffect trap;

        public override void StartCast()
        {
            base.StartCast();
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            targetMarker.gameObject.SetActive(true);
            targetMarker.SetMark(Color.red, false);
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            targetMarker.transform.position = mousePosition;
            bool mouseOverRoad = IsMouseOverRoad(mousePosition);
            ManageMarkerWithMousePosition(mouseOverRoad);

            if (Values.InputValues.POINTER_OVER_UI)
                return;

            if (Input.GetMouseButtonUp(0) && targetMarker.MarkerEnabled)
            {
                PlaceTrap(mousePosition);
            }
        }

        private void PlaceTrap(Vector2 position)
        {
            HunterTrapMapEffect spawnedTrap = Instantiate(trap);
            spawnedTrap.PlaceTrap(position);
            spawnedTrap.gameObject.SetActive(true);
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(spawnedTrap);
            OnSpellCasted?.Invoke(this);
            PayMana();
            FinishSpell();
        }

        private bool IsMouseOverRoad(Vector2 mousePosition)
        {
            int roadMask = 1 << Values.LayerMasks.RoadLayer;
            Collider2D collider = Physics2D.OverlapPoint(mousePosition, roadMask);
            return collider != null;
        }

        private void ManageMarkerWithMousePosition(bool mouseOverRoad)
        {
            if (mouseOverRoad && !targetMarker.MarkerEnabled)
            {
                targetMarker.SetMark(Color.white, true);
            }
            else if (!mouseOverRoad && targetMarker.MarkerEnabled)
            {
                targetMarker.SetMark(Color.red, false);
            }
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=5>Enter damage", $"{trap.TrapData.dmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Bleeding damage", $"{trap.TrapData.startDmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Slow", $"{trap.TrapData.startSlowValue}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Laying duration", $"{trap.TrapData.layingTime}s"));

            return data;
        }
    }
}
