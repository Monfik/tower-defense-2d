using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowerBuildingGuiPanel : BaseTowerGuiPanel
    {
        [SerializeField]
        private TowerGuiInfoPanel infoPanel;
        public TowerGuiInfoPanel InfoPanel => infoPanel;

        [SerializeField]
        private TowerBuildingToolsPanel buildingPanel;
        public TowerBuildingToolsPanel BuildingPanel => buildingPanel;

        public override void Open(Tower tower, TowerSelector gui, DataStorage storage)
        {
            base.Open(tower, gui, storage);
            currentTower.Builder.OnFinishedUpgradingBasic += SetNormalGUI;
            currentTower.Builder.OnFinishedBuild += SetNormalGUI;
            currentTower.Builder.OnFinishedFix += SetNormalGUI;

            infoPanel.Open(tower);
        }

        public override void Close()
        {
            currentTower.Builder.OnFinishedUpgradingBasic -= SetNormalGUI;
            currentTower.Builder.OnFinishedBuild -= SetNormalGUI;
            currentTower.Builder.OnFinishedFix -= SetNormalGUI;

            currentTower.GuiManager.OnUpgradeGuiExit?.Invoke();
            base.Close();
        }

        private void SetNormalGUI()
        {
            gui.Open(gui.BasicPanel, currentTower);
        }
    }
}
