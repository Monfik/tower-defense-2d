using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class TreeBuffState : BaseBossState<TreeBossBehaviour>
    {   
        public override void Init(TreeBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);

            boss.Animator.SetTrigger("Buff");
        }

        public void Buff()
        {
            boss.Buffer.Buff(boss.Spawner.SpawnedMonsters);
            FinishBuff();
        }

        public void FinishBuff()
        {
            SetupDefaultState();
        }

        private void SetupDefaultState()
        {
            boss.SetDefaultMovementState();
        }

        public override void UpdateState()
        {
            
        }

        public override void Deinit()
        {
            
        }

        public override bool IsReadyForWork()
        {
            return boss.Buffer.CanBuff();
        }
    }
}
