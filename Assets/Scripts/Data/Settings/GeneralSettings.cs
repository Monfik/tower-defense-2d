using System.Collections;
using System.Collections.Generic;
using TowerDefense.Fractions;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "General Settings", menuName
= "Settings/General Settings")]
    public class GeneralSettings : ScriptableObject
    {
        public float startGold = 100f;
        public int difficultyLevel;
        public Fraction fraction;
    }
}
