using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Feature
{
    public class InfoMarkPool : BasePool<BaseInfoMark>
    {
        public override BaseInfoMark GetPoolObject()
        {
            var ob = base.GetPoolObject();
            ob.gameObject.SetActive(true);
            ob.transform.localScale = Vector3.one;
            return ob;
        }

        public override void ReturnToPool(BaseInfoMark ob)
        {
            ob.transform.SetParent(transform);
            ob.gameObject.SetActive(false);
            base.ReturnToPool(ob);
        }
    }
}
