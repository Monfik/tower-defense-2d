using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class NinjaDescentSpell : BaseActiveSpell
    {
        [SerializeField] private MovableTower ninjaDescentTowerPrefab;
        private MovableTower placedNinjaDescentTower;
        private UnityAction CurrentAction;

        public override void Init(DataStorage storage, ISpellHoldable holder, UnityAction<BaseSpell> OnSpellStartCasting, UnityAction<BaseSpell> OnSpellCasted, UnityAction<BaseSpell> OnSpellFinished, UnityAction<BaseSpell> OnSpellCanceled)
        {
            base.Init(storage, holder, OnSpellStartCasting, OnSpellCasted, OnSpellFinished, OnSpellCanceled);
            CurrentAction = StartPlacingNinja; 
        }

        public override void StartCast()
        {
            CurrentAction?.Invoke();
        }

        private void StartPlacingNinja()
        {
            base.StartCast();
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined += OnTowerPreviewDeclined;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;

            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            storage.EventsStorage.CoreEvnts.OnTowerPicked(ninjaDescentTowerPrefab);
        }

        public override void UpdateSpell()
        {
            base.UpdateSpell();
        }

        private void OnTowerBuilded(Tower tower)
        {
            OnSpellCasted?.Invoke(this);
            CurrentAction = FinishSpell;

            MovableTower buildedNinja = tower as MovableTower;
            if (buildedNinja != null)
            {
                placedNinjaDescentTower = buildedNinja;
                placedNinjaDescentTower.Builder.OnStartDemolishing += OnBuildedTowerDestroyed;
            }
            else
            {
                Debug.LogWarning("Builded tower isnt ninja!");
            }

            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            PayMana();          
        }

        private void OnBuildedTowerDestroyed()
        {
            FinishSpell();
        }

        private void OnTowerPreviewDeclined(Tower tower)
        {
            CancelSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();
            data.specifiedDatas = ninjaDescentTowerPrefab.CreateTooltipData(new Tips.DescriptionData()).specifiedDatas;

            return data;
        }

        public override void CancelSpell()
        {
            base.CancelSpell();
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined -= OnTowerPreviewDeclined;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
        }

        public override void FinishSpell()
        {
            placedNinjaDescentTower.Builder.OnStartDemolishing -= OnBuildedTowerDestroyed;
            placedNinjaDescentTower.Builder.Demolish();
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined -= OnTowerPreviewDeclined;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            CurrentAction = StartPlacingNinja;
            //storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            base.FinishSpell();
        }
    }
}
