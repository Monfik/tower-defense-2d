using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Tools;
using TowerDefense.Effects;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class YetiBossIceMorphState : BaseBossState<YetiBossBehaviour>
    {
        [SerializeField] private IntervalData pathInterval;

        [Header("State values")]
        [SerializeField] private float morphingDuration = 1f;
        [SerializeField] private bool iceMorph;
        [SerializeField] private bool morphing;
        [SerializeField] private RangeFloat randomIceMorphDurationRange;

        [SerializeField] private AnimatorOverrideController animatorOverider;
        private RuntimeAnimatorController originalAnimator;

        [SerializeField] private Material blendMaterial;
        [SerializeField] private Material defaultMaterial;

        [Space(15)]
        [SerializeField] private MonsterBuffEffect buff;

        public override void PreInit(YetiBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(YetiBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);

            morphing = true;
            boss.Animator.enabled = false;

            boss.Renderer.material = blendMaterial;

            DOVirtual.Float(0f, 1f, morphingDuration, UpdateMorhping).OnComplete(SetupIceMorph).SetId(10003);

            boss.OnMonsterDied += m => { PrepareDestroy(); };
        }

        public override void UpdateState()
        {
            if (morphing)
                return;

            boss.UpdateMovement();
        }

        private void SetupIceMorph()
        {
            originalAnimator = boss.Animator.runtimeAnimatorController;
            boss.Animator.runtimeAnimatorController = animatorOverider;

            boss.Renderer.material = defaultMaterial;
            iceMorph = true;
            morphing = false;
            boss.Animator.enabled = true;
            boss.Animator.Play("IcePhaseWalk");

            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);

            Timer currentMorphTimer = boss.gameObject.AddComponent<Timer>();
            float iceMorphDuration = randomIceMorphDurationRange.GetRandom();
            currentMorphTimer.Setup(iceMorphDuration, BackToDefaultState);
            Buff(boss, iceMorphDuration);
        }

        private void Buff(Monster target, float duration)
        {
            MonsterBuffEffect buffEffect = Instantiate(buff);
            buffEffect.duration = duration;
            target.Effects.AddEffect(buffEffect);
        }

        private void BackToDefaultState()
        {
            if (boss.Dead)
                return;

            morphing = true;
            boss.Renderer.material = blendMaterial;
            boss.Animator.enabled = false;
            DOVirtual.Float(1f, 0f, morphingDuration, UpdateMorhping).OnComplete(SetupDefaultMorph).SetId(10003);
        }

        private void SetupDefaultMorph()
        {
            iceMorph = false;
            morphing = false;

            boss.Renderer.material = defaultMaterial;
            boss.Animator.runtimeAnimatorController = originalAnimator;
            boss.Animator.enabled = true;
            boss.SetDefaultMovementState();
        }

        public override void Deinit()
        {
            iceMorph = false;
            morphing = false;
        }

        private void UpdateMorhping(float transition)
        {
            boss.Renderer.material.SetFloat("_Progress", transition);
        }

        public override bool IsReadyForWork()
        {
            return boss.DistanceTravelled >= pathInterval.nextValue;
        }

        public void PrepareDestroy()
        {
            DOTween.Kill(10003);
        }
    }
}
