using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public abstract class BaseMonsterEffect : MonoBehaviour, ITooltipable
    {
        [SerializeField]
        new protected string name;
        public string Name => name;

        [SerializeField]
        protected Sprite icon;    

        public UnityAction<BaseMonsterEffect> effectTimeOver;
        protected Monster monster;
        [SerializeField] protected float currentTime = 0f;
        public float duration;
        public bool durationable = true;

        public virtual void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            this.monster = monster;
            this.effectTimeOver += effectTimeOver;
            currentTime = 0f;
            gameObject.SetActive(true);
            transform.position = monster.GetCenter();
        }

        public virtual void UpdateEffect()
        {
            if (!durationable)
                return;

            currentTime += Time.deltaTime;

            if (currentTime >= duration)
                EndEffect();
        }

        public virtual void EndEffect()
        {
            effectTimeOver?.Invoke(this);
        }

        public void ResetTimer()
        {
            currentTime = 0f;
        }

        public virtual DescriptionData CreateTooltipData(DescriptionData data)
        {
            return null;
        }
    }
}
