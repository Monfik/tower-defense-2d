using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using TowerDefense.GlobalEvents;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Enviroment
{
    public class TreeMapEntity : MonoBehaviour, IWindSusceptible
    {
        [SerializeField] private float windStrengthBreakThreshold; //0 - 1

        [SerializeField] private SpriteRenderer rend;
        [SerializeField] private SpriteRenderer brokeRend;
        [SerializeField] new private Collider2D collider;

        private bool broken;
        public bool Broken => broken;

        public UnityAction<TreeMapEntity> OnBroken;

        public void Affect(WindForceData windData)
        {
            if (broken)
                return;

            if (windData.strength >= windStrengthBreakThreshold)
                Break();
        }

        public void Break()
        {
            broken = true;

            collider.enabled = false;
            rend.enabled = false;
            brokeRend.gameObject.SetActive(true);
            OnBroken?.Invoke(this);
        }

        public void ResetEntity()
        {
            broken = false;

            collider.enabled = true;
            rend.enabled = true;
            brokeRend.gameObject.SetActive(false);
        }
    }
}
