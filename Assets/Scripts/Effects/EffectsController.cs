using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class EffectsController
    {
        [SerializeField]
        private List<BaseMonsterEffect> effects;
        public List<BaseMonsterEffect> Effects => effects;

        private Monster owner;

        public EffectsController(Monster owner)
        {
            this.owner = owner;
            effects = new List<BaseMonsterEffect>();
        }

        public void AddEffect(BaseMonsterEffect effect)
        {
            effect.StartEffect(owner, OnEffectTimeExpired);
            effect.transform.SetParent(owner.transform);
            effects.Add(effect);
        }

        private void OnEffectTimeExpired(BaseMonsterEffect effect)
        {
            effects.Remove(effect);
            GameObject.Destroy(effect.gameObject);
        }

        public void FinishEffect(BaseMonsterEffect effect)
        {
            effect.EndEffect();
            effects.Remove(effect);
        }

        public void UpdateEffects()
        {
            for (int i = 0; i < effects.Count; i++)
            {
                effects[i].UpdateEffect();
            }
        }

        public BaseMonsterEffect FindByName(string name)
        {
            return effects.Find(c => c.Name.Equals(name));
        }
    }
}
