using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public abstract class MapEffect : MonoBehaviour
    {
        protected DataStorage storage;
        protected UnityAction<MapEffect> EffectExpired;
        [SerializeField] protected float duration;
        public float Duration => duration;
        protected float currentTime;

        public void InitData(DataStorage storage, float duration)
        {
            this.storage = storage;
            this.duration = duration;
        }

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }

        public void AssignEvents(UnityAction<MapEffect> timeExpired)
        {
            EffectExpired += timeExpired;
        }

        public virtual void UpdateEffect()
        {
            currentTime += Time.deltaTime;
            if(currentTime >= duration)
            {
                FinishEffect();
            }
        }

        public virtual void FinishEffect()
        {
            EffectExpired?.Invoke(this);
        }
    }
}
