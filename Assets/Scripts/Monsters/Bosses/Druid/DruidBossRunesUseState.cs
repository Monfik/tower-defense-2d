using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefense.Effects;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Creatures
{
    public class DruidBossRunesUseState : BaseBossState<DruidBossBehaviour>
    {
        [SerializeField] private IntervalData pathInterval;

        [Header("RuneMarkVisualing")]
        [SerializeField] private Transform runeMarkVisualPoint;
        [SerializeField] private SpriteRenderer runeMarkVisualRenderer;
        [SerializeField] private AnimationCurve runeMarkVisualCurve;
        [SerializeField] private RangeFloat runeMarkVisualIntensityRange;

        [Space(10)]
        [SerializeField] private bool isMarkNext = false;
        [SerializeField] private bool isTriggerNext = false;

        [SerializeField] private DruidMapActionRune nextMarkRune;
        [SerializeField] private DruidMapActionRune markedRune;

        private Queue<DruidMapActionRune> runesQueue = new Queue<DruidMapActionRune>();

        private Dictionary<string, UnityAction> runesActions = new Dictionary<string, UnityAction>();
        private UnityAction ActionAfterTrigger;

        [Header("Runes Activator References")]
        [Header("Heal over time")]
        [SerializeField] private HealOverTimeEffect healOverTimeEffect;

        [Header("Monsters spawning")]
        [SerializeField] private DruidBossSpawnBeast spawnState;

        [Header("Resistances buff")]
        [SerializeField] private MonsterBuffEffect resistancesBuff;

        public override void PreInit(DruidBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);

            isMarkNext = true;
            isTriggerNext = false;

            CreateRunesActionsDictionary();
            List<DruidMapActionRune> activeRunes = GameObject.FindObjectsOfType<DruidMapActionRune>(true).ToList();
            activeRunes.RemoveAll(c => !c.gameObject.activeSelf);

            SetupRunesActions(activeRunes, runesActions);

            CreateRunesQueue(activeRunes);
            nextMarkRune = runesQueue.Dequeue();
        }

        private void CreateRunesActionsDictionary()
        {
            runesActions = new Dictionary<string, UnityAction>();
            runesActions["HealOverTime"] = HealOverTimeACTION;
            runesActions["DestroyRandomTower"] = DestroyRandomTowerACTION;
            runesActions["SpawnMonster"] = SpawnMonsterACTION;
            runesActions["BuffResistances"] = BuffResistancesACTION;
        }

        private void SetupRunesActions(List<DruidMapActionRune> runes, Dictionary<string, UnityAction> dictionary)
        {
            foreach (KeyValuePair<string, UnityAction> entry in dictionary)
            {
                foreach(DruidMapActionRune rune in runes)
                {
                    if (rune.actionName.Equals(entry.Key))
                        rune.Action = entry.Value;
                }
            }
        }

        private void CreateRunesQueue(List<DruidMapActionRune> runes)
        {
            runes.Shuffle();
            runesQueue = new Queue<DruidMapActionRune>();
            
            foreach(DruidMapActionRune rune in runes)
            {
                runesQueue.Enqueue(rune);
            }
        }

        public override void Init(DruidBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);

            if(isMarkNext)
            {
                SetAnimator("MarkRune", "MarkingSpeed");
                storage.ReferenceStorage.Postponer.InvokePostponeAction(() => StartMarkRune(nextMarkRune));
            }
            else if(isTriggerNext)
            {
                SetAnimator("TriggerRune", "TriggeringSpeed");
                storage.ReferenceStorage.Postponer.InvokePostponeAction(() => VisualMarkRune(nextMarkRune));
            }
        }
        
        //Called from animator
        private void TriggerMarkedRune()
        {
            TriggerRune(markedRune);

            isMarkNext = true;
            isTriggerNext = false;

            if (runesQueue.Count > 0)
                nextMarkRune = runesQueue.Dequeue();
            else
                nextMarkRune = null;
        }

        //Called from animator
        private void FinishTriggeringRune()
        {
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
            ActionAfterTrigger?.Invoke();

            if(ActionAfterTrigger == null) 
                boss.SetDefaultMovementState();
        }

        //Called from animator
        private void Mark()
        {
            MarkRune(nextMarkRune);

            isMarkNext = false;
            isTriggerNext = true;
        }

        //Called from animator
        private void FinishMark()
        {
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
            boss.SetDefaultMovementState();
        }

        private void SetAnimator(string animationName, string animationFloatNameReset)
        {
            boss.Animator.Play(animationName);
            boss.Animator.SetFloat(animationFloatNameReset, 1f);
        }

        private void StartMarkRune(DruidMapActionRune rune)
        {
            rune.VisualMark();
            VisualMarkRune(rune);
        }

        private void VisualMarkRune(DruidMapActionRune rune)
        {
            runeMarkVisualRenderer.SetSpriteColorAlpha(0f);
            runeMarkVisualRenderer.gameObject.SetActive(true);
            runeMarkVisualRenderer.sprite = rune.MarkSprite;
            runeMarkVisualRenderer.material.SetColor("_EmissionColor", rune.color);
            runeMarkVisualRenderer.transform.position = runeMarkVisualPoint.transform.position;

            float runeMarkAnimationDuration = boss.Animator.GetCurrentAnimatorStateInfo(0).length;
            DOVirtual.Float(0f, 1f, runeMarkAnimationDuration, RuneMarkVisualUpdate).SetEase(runeMarkVisualCurve).OnComplete(() =>
            {
                runeMarkVisualRenderer.gameObject.SetActive(false);
            }).SetId(10001);
        }

        private void RuneMarkVisualUpdate(float process)
        {
            float markAlpha = process;
            float markIntensity = Mathf.Lerp(runeMarkVisualIntensityRange.a, runeMarkVisualIntensityRange.b, process);
            runeMarkVisualRenderer.SetSpriteColorAlpha(markAlpha);
            runeMarkVisualRenderer.material.SetFloat("_Intensity", markIntensity);
        }

        private void MarkRune(DruidMapActionRune rune)
        {
            markedRune = rune;
        }

        private void TriggerRune(DruidMapActionRune rune)
        {
            rune.Trigger();
        }

        public override void UpdateState()
        {

        }

        public override bool IsReadyForWork()
        {
            return (boss.DistanceTravelled >= pathInterval.nextValue && nextMarkRune != null);
        }

        public override void Deinit()
        {
            
        }

        //--------------RUNES ACTIVATE METHODS-----------------
        private void HealOverTimeACTION()
        {
            foreach(Monster monster in storage.ReferenceStorage.WaveController.AllMonsters)
            {
                HealOverTimeEffect healEffect = Instantiate(healOverTimeEffect);
                monster.Effects.AddEffect(healEffect);
            }

            ActionAfterTrigger = boss.SetDefaultMovementState;
        }

        private void DestroyRandomTowerACTION()
        {
            List<Tower> potentialTowers = new List<Tower>(storage.ReferenceStorage.TowersController.CurrentTowers);
            potentialTowers.RemoveAll(c => /*c.Builder.upgrading ||*/ c.Builder.demolishing || c.Builder.StaticBuild);

            if(potentialTowers.Count > 1)
            {
                Tower target = potentialTowers[Random.Range(0, potentialTowers.Count)];
                target.Builder.Demolish();
            }

            ActionAfterTrigger = boss.SetDefaultMovementState;
        }

        private void SpawnMonsterACTION()
        {
            ActionAfterTrigger = () =>
            {
                boss.ChangeState(spawnState);
            };
        }

        private void BuffResistancesACTION()
        {
            foreach (Monster monster in storage.ReferenceStorage.WaveController.AllMonsters)
            {
                MonsterBuffEffect buff = Instantiate(resistancesBuff);
                monster.Effects.AddEffect(buff);
            }

            ActionAfterTrigger = boss.SetDefaultMovementState;
        }

        private void OnDestroy()
        {
            DOTween.Kill(10001);
        }
    }
}
