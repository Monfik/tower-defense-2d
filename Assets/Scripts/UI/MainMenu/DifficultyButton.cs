using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CoreUI
{
    public class DifficultyButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;
        public Button Button => button;

        [SerializeField] private Image hover;
        [SerializeField] private Image selected;

        public void ShowHoverFrame(bool enabled)
        {
            hover.gameObject.SetActive(enabled);
        }

        public void ShowSelectedFrame(bool enabled)
        {
            selected.gameObject.SetActive(enabled);
        }

        public void EnableButton(bool enabled)
        {
            button.interactable = enabled;
            ShowSelectedFrame(!enabled);
        }
    }
}
