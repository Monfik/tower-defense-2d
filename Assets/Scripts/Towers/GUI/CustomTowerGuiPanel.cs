using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class CustomTowerGuiPanel : BaseTowerGuiPanel
    {
        [SerializeField]
        private TowerGuiInfoPanel infoPanel;
        public TowerGuiInfoPanel InfoPanel => infoPanel;

        public override void Open(Tower tower, TowerSelector gui, DataStorage storage)
        {
            base.Open(tower, gui, storage);
            tower.GuiManager.ComposeBasicGUI(infoPanel);
            infoPanel.Open(tower);
        }

        public override void Close()
        {
            currentTower.GuiManager.OnBasicGuiExit?.Invoke();
            base.Close();
        }
    }
}
