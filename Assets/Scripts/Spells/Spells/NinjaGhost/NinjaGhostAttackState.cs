using Core;
using System.Collections;
using System.Collections.Generic;
using Tools;
using TowerDefense.Creatures;
using TowerDefense.Power;
using UnityEngine;

namespace TowerDefense.Spells
{
    [System.Serializable]
    public class NinjaGhostAttackState : IBaseState<NinjaGhost>
    {
        protected NinjaGhost controller;
        private ITargetable targetable;

        [Header("Daggers")]
        [SerializeField] private NinjaDagger leftDagger;
        [SerializeField] private NinjaDagger rightDagger;

        [Space(15)]
        [SerializeField] private AnimationEventCallback attackFinished;

        public void Init(NinjaGhost controller)
        {
            this.controller = controller;

            controller.Animator.SetTrigger("Attack");
            leftDagger.Init(OnMonsterHitted);
            rightDagger.Init(OnMonsterHitted);
            leftDagger.gameObject.SetActive(true);
            rightDagger.gameObject.SetActive(true);

            controller.Collider.enabled = false;
            
            if(targetable != null)
            {
                controller.GhostRenderer.transform.rotation = Quaternion.Euler(0f, targetable.GetPosition().x < controller.GhostRenderer.transform.transform.position.x ? 180f : 0f, 0f);
            }

            attackFinished.AnimationCallback += OnAttackFinished;
        }

        public void UpdateState()
        {
            
        }

        public void FixedUpdateState()
        {
            
        }

        public void Deinit(NinjaGhost controller)
        {
            attackFinished.AnimationCallback -= OnAttackFinished;
        }

        public void SetTarget(ITargetable targetable)
        {
            this.targetable = targetable;
        }

        private void OnMonsterHitted(IDestructable monster)
        {
            PhysicalAttack physicalAttack = new PhysicalAttack(controller.NinjaData.dmg);
            physicalAttack.Attack(monster);
        }

        private void OnAttackFinished()
        {
            controller.StartWalk();
        }
    }
}
