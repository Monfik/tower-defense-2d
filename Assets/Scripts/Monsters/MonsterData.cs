using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Creatures
{
    [System.Serializable]
    public class MonsterData : ICloneable
    {
        public int hp;
        public int towerDmg;
        public MonsterStaticsData speed;
        public MonsterStaticsData armor;
        public MonsterStaticsData lightingResist;
        public MonsterStaticsData fireResist;
        public MonsterStaticsData iceResist;
        public MonsterStaticsData poisonResist;
        public MonsterStaticsData waterResist;
        public float damageMultiplier = 1f;
        public int goldWorth = 1;

        public MonsterData(MonsterData monsterData)
        {
            hp = monsterData.hp;
            towerDmg = monsterData.towerDmg;
            speed = new MonsterStaticsData(monsterData.speed);
            armor = new MonsterStaticsData(monsterData.armor);
            lightingResist = new MonsterStaticsData(monsterData.lightingResist);
            fireResist = new MonsterStaticsData(monsterData.fireResist);
            iceResist = new MonsterStaticsData(monsterData.iceResist);
            poisonResist = new MonsterStaticsData(monsterData.poisonResist);
            waterResist = new MonsterStaticsData(monsterData.waterResist);
            damageMultiplier = monsterData.damageMultiplier;
            goldWorth = monsterData.goldWorth;
        }

        public void Reset()
        {
            speed.Reset();
            armor.Reset();
            lightingResist.Reset();
            fireResist.Reset();
            iceResist.Reset();
            poisonResist.Reset();

            damageMultiplier = 1f;
        }

        public MonsterStaticsData GetDataByType(MagicType type)
        {
            MonsterStaticsData magicStatics = new MonsterStaticsData();

            switch(type)
            {
                case MagicType.Lighting:
                    magicStatics = lightingResist;
                    break;
                case MagicType.Fire:
                    magicStatics = fireResist;
                    break;
                case MagicType.Ice:
                    magicStatics = iceResist;
                    break;
                case MagicType.Poison:
                    magicStatics = poisonResist;
                    break;
                case MagicType.Water:
                    magicStatics = waterResist;
                    break;
            }

            return magicStatics;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    [System.Serializable]
    public class MonsterStaticsData
    {
        public int coreValue;
        [HideInInspector] public int currentValue;
        [HideInInspector] public int buffValue;
        public int minValue;
        public int maxValue;
        public UnityAction<int> ValueChanged;

        public MonsterStaticsData(MonsterStaticsData data)
        {
            coreValue = data.coreValue;
            currentValue = data.currentValue;
            buffValue = data.buffValue;
            minValue = data.minValue;
            maxValue = data.maxValue;
        }

        public MonsterStaticsData()
        {
            
        }

        public void AddValue(int value)
        {
            if (value == 0)
                return;

            buffValue += value;
            currentValue = coreValue + buffValue;
            currentValue = Mathf.Clamp(currentValue, minValue, maxValue);
            ValueChanged?.Invoke(currentValue);
        }

        public void SetupMinMax(int minValue, int maxValue)
        {
            SetupMin(minValue);
            SetupMax(maxValue);
        }

        public void SetupMin(int minValue)
        {
            this.minValue = minValue;
        }

        public void SetupMax(int maxValue)
        {
            this.maxValue = maxValue;
        }

        public void Reset()
        {
            currentValue = coreValue;
            buffValue = 0;
        }
    }
}
