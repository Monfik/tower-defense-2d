using DG.Tweening;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.UI;

namespace LevelLogic
{
    public class Castle : MonoBehaviour
    {
        [SerializeField] private int maxHp;
        private int currentHp;

        [SerializeField] new private SpriteRenderer renderer;
        [SerializeField] private Slider hpSlider;
        [SerializeField] private TMP_Text hpText;

        [SerializeField] private CastleDestroyer destroyer;

        private EventsStorage events;

        [SerializeField] private bool invulnerable = false;
        public bool Invulnerable
        {
            get
            { return invulnerable; }
            set
            {
                invulnerable = value;
            }
        }

        [Header("Hit animation")]
        [SerializeField] private Color hitColor = Color.red;
        [SerializeField] private float hitColorTime = 0.4f;
        public AnimationCurve hitColorEase;

        public void Init(EventsStorage events)
        {
            this.events = events;
            destroyer.SetDefault();
            currentHp = maxHp;
            invulnerable = false;
            hpSlider.maxValue = maxHp;
            hpSlider.minValue = 0;
            UpdateHp(maxHp);
        }

        private void UpdateHp(int hp)
        {
            hpSlider.value = hp;
            hpText.text = currentHp.ToString() + " / " + maxHp.ToString();
        }

        public int TakeHit(int hp)
        {
            currentHp -= hp;        

            if (currentHp <= 0)
            {
                currentHp = 0;
            }
            UpdateHp(currentHp);
            DOVirtual.Color(Color.white, hitColor, hitColorTime, HitColorUpdate).SetEase(hitColorEase).SetId("Castle hit tween");
            return currentHp;
        }

        private void HitColorUpdate(Color color)
        {
            renderer.color = color;
        }

        public void Destroy()
        {
            invulnerable = true;
            StartDestroyAnimation();
            events.CoreEvnts.CallOnBaseDestroyed();
        }

        private void StartDestroyAnimation()
        {
            destroyer.Destroy();
        }

        [ContextMenu("Take hit")]
        public void TakeTestHit()
        {
            int hp  = TakeHit(3);
            if (hp <= 0)
                events.CoreEvnts.CallOnBaseDestroyed();
        }
    }
}

