using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Animations;
using TowerDefense.Towers;
using TowerDefense.Updatables;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class YetiBossThrowSnowballState : BaseBossState<YetiBossBehaviour>
    {
        [Header("Values")]
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private float horizontalUpAdmissibleAngle;
        [SerializeField] private float horizontalDownAdmissibleAngle;
        [SerializeField] private RangeFloat randomScaleRangeFactor;
        [SerializeField] private RangeFloat snowballMovementSpeedRange;
        [SerializeField] private RangeFloat snowballMovementDurationRnage;
        [SerializeField] private RangeFloat snowballParabolaHeightRange;
        [SerializeField] private float range;

        [Header("Times")]
        [SerializeField] private RangeFloat throwAnimationSpeedRange; //far away = maxRange; close bottom = minRange;

        [Header("Snowball prefab")]
        [SerializeField] private Snowball snowballPrefab;

        [Header("References")]
        [SerializeField] private Transform handTransform;
        
        /////// references
        private List<Tower> currentTowers;
        ///////

        [SerializeField] private SnowballActionHandler lastSnowballHandler;

        private ISnowballable potentialTarget;


        public override void PreInit(YetiBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            currentTowers = storage.ReferenceStorage.TowersController.CurrentTowers;
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(YetiBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);        
            ThrowToTarget(potentialTarget);
        }

        public override void UpdateState()
        {
            
        }

        public override void Deinit()
        {
            
        }

        private void ThrowToTarget(ISnowballable target)
        {
            if (target == null)
            {
                Debug.LogError("Target is null");
                boss.SetDefaultMovementState();
                return;
            }

            lastSnowballHandler = new SnowballActionHandler(target);
            lastSnowballHandler.durationable = false;
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);

            boss.Renderer.transform.rotation = Quaternion.Euler(0f, boss.GetCenter().x > target.GetCenter().x ? 180f : 0f, 0f);
            boss.Animator.Play("ThrowSnawball");
        }

        //Called by animator
        private void SpawnSnowball()
        {
            Snowball snowball = Instantiate(snowballPrefab);
            snowball.MakeTransparable(0f);
            SpriteAlphaAnimation spriteAlphaAnimation = snowball.gameObject.AddComponent<SpriteAlphaAnimation>();
            spriteAlphaAnimation.Init(0f, 1f, 0.4f, snowball);
            SetupSnowballHandler(lastSnowballHandler, snowball);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(lastSnowballHandler);
            boss.SetDefaultMovementState();
        }

        private void SetupSnowballHandler(SnowballActionHandler handler, Snowball snowball)
        {
            handler.snowball = snowball;
            handler.startPosition = handTransform.position;
            handler.targetPosition = handler.snowballable.GetCenter();

            //Strength
            float snowballStrength = CalculateStrength(handTransform.position, handler.targetPosition, range);
            snowball.strength = snowballStrength;

            //Parabola height
            snowballParabolaHeightRange.a = 1f;
            snowballParabolaHeightRange.b = Mathf.Abs(handler.targetPosition.y - handler.startPosition.y);
            if (snowballParabolaHeightRange.b < snowballParabolaHeightRange.a)
                snowballParabolaHeightRange.b = snowballParabolaHeightRange.a + 0.5f;

            handler.parabolaHeight = Mathf.Lerp(snowballParabolaHeightRange.a, snowballParabolaHeightRange.b, snowballStrength);

            //Speed
            handler.snowballSpeed = Mathf.Lerp(snowballMovementSpeedRange.a, snowballMovementSpeedRange.b, snowballStrength);

            //Movement duration
            handler.flyDuration = Mathf.Lerp(snowballMovementDurationRnage.a, snowballMovementDurationRnage.b, snowballStrength);

            //Animation speed
            float animationSpeed = Mathf.Lerp(throwAnimationSpeedRange.a, throwAnimationSpeedRange.b, snowballStrength);
            boss.Animator.SetFloat("ThrowSpeed", animationSpeed);
            //Scale
            //float randomSnowballSize = Random.Range(randomScaleRangeFactor.a, randomScaleRangeFactor.b);
            //Vector2 snowballSize = snowball.transform.localScale * randomSnowballSize;
            //snowball.transform.localScale = snowballSize;
        }

        private float CalculateStrength(Vector3 source, Vector3 target, float range) //return 0-1
        {
            if (source.x > target.x) //we move to easier calculate angles
            {
                target.x += (source.x - target.x) * 2;
            }

            float aAzimuth = 0;
            float aAltitudeUp = (horizontalUpAdmissibleAngle) * Mathf.Deg2Rad;
            float aAltitudeDown = (horizontalDownAdmissibleAngle) * Mathf.Deg2Rad;
            float cUp = Mathf.Cos(aAltitudeUp);
            float cDown = Mathf.Cos(aAltitudeDown);
            Vector3 rightdirectionUp = new Vector3(Mathf.Cos(aAzimuth) * cUp, Mathf.Sin(aAltitudeUp), Mathf.Sin(aAzimuth) * cUp);
            Vector3 rightdirectionDown = new Vector3(Mathf.Cos(aAzimuth) * cDown, -Mathf.Sin(aAltitudeDown), Mathf.Sin(aAzimuth) * cDown);
            Vector3 maxUpPosition = source + rightdirectionUp * range;
            Vector3 maxDownPosition = source + rightdirectionDown * range;

            float maXPosition = maxUpPosition.x > maxDownPosition.x ? maxUpPosition.x : maxDownPosition.x;
            RangeFloat latitudeRange = new RangeFloat(source.x, maXPosition);
            RangeFloat altitudeRange = new RangeFloat(maxDownPosition.y, maxUpPosition.y);
            float latitudeStrength = Mathf.InverseLerp(latitudeRange.a, latitudeRange.b, target.x);
            float altitudeStrength = Mathf.InverseLerp(altitudeRange.a, altitudeRange.b, target.y);
            float strength = latitudeStrength * altitudeStrength;

            return strength;
        }

        public override bool IsReadyForWork()
        {
            if (boss.DistanceTravelled >= pathInterval.nextValue)
            {
                List<ISnowballable> potentialTargets = new List<ISnowballable>();

                foreach(Tower tower in currentTowers)
                {                
                    if (Vector3.Distance(boss.GetCenter(), tower.GetCenter()) <= range && tower.Builder.Builded && tower.Builder.Repairer.Efficient > 0 && IsAngleAdmissible(boss.GetCenter(), tower.GetCenter()))
                        potentialTargets.Add(tower);
                }
                
                if(potentialTargets.Count > 0)
                {
                    potentialTarget = potentialTargets[Random.Range(0, potentialTargets.Count - 1)];
                    return true;
                }
            }

            return false;
        }

        private bool IsAngleAdmissible(Vector3 source, Vector3 target)
        {
            if(source.x > target.x) //we move to easier calculate angles
            {
                target.x += (source.x - target.x) * 2;
            }

            Vector2 direction = target - source;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            return (angle >= -horizontalDownAdmissibleAngle && angle <= horizontalUpAdmissibleAngle);          
        }

        private void OnDrawGizmosSelected()
        {
            //float range = this.range;
            //float angle = horizontalAdmissibleAngle;
            //float sideHeight = range * Mathf.Sin(angle * Mathf.Deg2Rad);

            float aAzimuth = 0;
            float aAltitudeUp = (horizontalUpAdmissibleAngle) * Mathf.Deg2Rad;
            float aAltitudeDown = (horizontalDownAdmissibleAngle) * Mathf.Deg2Rad;
            float cUp = Mathf.Cos(aAltitudeUp);
            float cDown = Mathf.Cos(aAltitudeDown);
            Vector3 rightdirectionUp = new Vector3(Mathf.Cos(aAzimuth) * cUp, Mathf.Sin(aAltitudeUp), Mathf.Sin(aAzimuth) * cUp );
            Vector3 leftdirectionUp = new Vector3(-Mathf.Cos(aAzimuth) * cUp, Mathf.Sin(aAltitudeUp), Mathf.Sin(aAzimuth) * cUp );
            Vector3 rightdirectionDown = new Vector3(Mathf.Cos(aAzimuth) * cDown, -Mathf.Sin(aAltitudeDown), Mathf.Sin(aAzimuth) * cDown);
            Vector3 leftdirectionDown = new Vector3(-Mathf.Cos(aAzimuth) * cDown, -Mathf.Sin(aAltitudeDown), Mathf.Sin(aAzimuth) * cDown);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + rightdirectionUp * range);
            Gizmos.DrawLine(transform.position, transform.position + leftdirectionUp * range);
            Gizmos.DrawLine(transform.position, transform.position + rightdirectionDown * range);
            Gizmos.DrawLine(transform.position, transform.position + leftdirectionDown * range);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }

    //Handler to update snowball when yeti is dead
    [System.Serializable]
    public class SnowballActionHandler : BaseUpdatableMonoless
    {
        public ISnowballable snowballable;
        public Snowball snowball;
        public Vector3 startPosition;
        public Vector3 targetPosition;
        public float snowballSpeed;

        public float flyDuration;
        public float parabolaHeight;

        public SnowballActionHandler(ISnowballable snowballable)
        {
            this.snowballable = snowballable;
        }

        public override void UpdateUpdatable()
        {
            currentTime += Time.deltaTime * snowballSpeed;
            float t = currentTime / flyDuration;
            t = Mathf.Clamp(t, 0f, 1f);
            Vector2 newPosition = MathParabola.Parabola(startPosition, targetPosition, parabolaHeight, t);
            snowball.transform.position = newPosition;

            if (t == 1)
            {
                if(snowballable != null)
                    snowball.Hit(snowballable);

                Finish();
            }
        }

        protected override void Finish()
        {
            GameObject.Destroy(snowball.gameObject);
            base.Finish();
        }
    }
}
