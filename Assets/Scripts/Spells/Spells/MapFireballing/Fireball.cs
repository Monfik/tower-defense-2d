using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class Fireball : MapEffect
    {
        [SerializeField] private SpriteRenderer fireTrails;
        [SerializeField] private SpriteRenderer fireStone;
        [SerializeField] private AnimationCurve fallingCurve;
        [SerializeField] private float boomRadius = 0.5f;
        [SerializeField] private float fallSpeed;
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Fire firePrefab;
        private FireballData fireballData;

        public void InitFireball(FireballData fireballData)
        {
            this.fireballData = fireballData;
            transform.position = fireballData.startPosition;
        }

        public override void UpdateEffect()
        {
            Vector2 currentPosition = transform.position;
            float t = Mathf.InverseLerp(fireballData.startPosition.y, fireballData.targetPosition.y, currentPosition.y);
            float evaluatedT = fallingCurve.Evaluate(t);
            currentPosition.y -= fallSpeed * Time.deltaTime * (1 + evaluatedT);
            transform.position = currentPosition;

            Vector3 currentRotation = transform.rotation.eulerAngles;
            currentRotation.z += rotateSpeed * Time.deltaTime;
            fireStone.transform.rotation = Quaternion.Euler(currentRotation);

            if (currentPosition.y <= fireballData.targetPosition.y)
            {
                currentPosition.y = fireballData.targetPosition.y;
                transform.position = currentPosition;
                Hit(fireballData.targetPosition);
            }
        }

        private void Hit(Vector2 position)
        {
            List<Monster> hittedMonsters = new List<Monster>();
            int layerMask = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D[] collidedMonsters = Physics2D.OverlapCircleAll(position, boomRadius, layerMask);

            foreach (Collider2D collider in collidedMonsters)
            {
                Monster monster = collider.GetComponent<Monster>();

                if (monster != null)
                {
                    Hit(monster);
                }
            }

            Fire fire = Instantiate(firePrefab);
            fire.InitData(storage, fireballData.fireData);
            fire.transform.position = position;
            fire.gameObject.SetActive(true);
            storage.EventsStorage.CoreEvnts.CallOnMapEffectAppeared(fire);
            FinishEffect();
        }

        private void Hit(Monster monster)
        {
            int armorReduction = monster.Data.armor.currentValue - fireballData.armorReduction;
            if (armorReduction < 0)
                armorReduction = 0;
            int damage = DamageCalculator.CalculateDamage(fireballData.physicalDmg, armorReduction);
            monster.TakeDamage(damage, DamageType.Magic);
        }
    }

    [System.Serializable]
    public class FireballData
    {
        public FireData fireData;
        public int physicalDmg;
        public int armorReduction;
        [HideInInspector] public Vector2 startPosition;
        [HideInInspector] public Vector2 targetPosition;
    }
}
