using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Tests
{
    public class SpriteDotweenMoveTest : MonoBehaviour
    {
        public Vector3 endPos;

        private void Start()
        {
            transform.DOMove(endPos, 3f);
        }
    }
}
