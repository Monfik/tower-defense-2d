using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    [System.Serializable]
    public class RespawnPlaceAnimationPair
    {
        public List<PathCreator> paths;
        public RespawnPlaceAnimator respawnAnimator;
    }
}
