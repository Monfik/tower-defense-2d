using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace LevelLogic
{
    public class Wave : MonoBehaviour
    {
        private int waveIndex;
        public int WaveIndex => waveIndex;

        private WaveData waveData;
        public WaveData WaveData => waveData;

        [SerializeField]
        private List<Monster> monsters;
        public List<Monster> Monsters => monsters;

        public void Init(int waveIndex, WaveData waveData)
        {
            this.waveIndex = waveIndex;
            this.waveData = waveData;
        }

        public bool IsFinished()
        {
            return monsters.Count == 0 && waveData.RealeasersData.Count == 0;
        }
    }
}
