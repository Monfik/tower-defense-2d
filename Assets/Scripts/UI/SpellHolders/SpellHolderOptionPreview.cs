using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class SpellHolderOptionPreview : MonoBehaviour
    {
        [SerializeField] private TMP_Text title;
        public TMP_Text Title => title;

        [SerializeField] private TMP_Text description;
        public TMP_Text Description => description;

        private Tween deactiveTween;
        [SerializeField] private Image image;

        public void DisplayPreview(float displayTime)
        {
            deactiveTween?.Kill();

            gameObject.SetActive(true);
            Color color = Color.white;
            color.a = 1;
            title.DOColor(color, displayTime);
            description.DOColor(color, displayTime);
            image.DOColor(color, displayTime);
        }

        public void HidePreview(float hideTime)
        {
            gameObject.SetActive(true);
            Color color = Color.white;
            color.a = 0;
            title.DOColor(color, hideTime);
            description.DOColor(color, hideTime);
            image.DOColor(color, hideTime);

            deactiveTween = DOVirtual.DelayedCall(hideTime, Deactive, false);
        }

        private void Deactive()
        {
            gameObject.SetActive(false);
            deactiveTween = null;
        }
    }
}
