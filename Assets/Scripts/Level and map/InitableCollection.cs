using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InitableCollection
{
    [SerializeField] private List<BaseInitable> initables;

    public void Init(DataStorage storage)
    {
        initables.ForEach(c => c.Init(storage));
    }

    public void Deinit()
    {
        initables.ForEach(c => c.Deinit());
    }
}
