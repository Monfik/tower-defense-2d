using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Animations;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class DemonBossEvilBallAttackState : BaseBossState<DemonBossBehaviour>
    {
        private DemonAttackTarget target;
        
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private float horizontalAdmissibleAngle;
        

        [Header("Evil balls data")]
        [SerializeField] private Transform evilBallSpawnPoint;
        [SerializeField] private EvilBall evilBallPrefab;
        [SerializeField] private MonsterData evilBallData;
        [SerializeField] private List<EvilBall> currentBalls;

        public override void PreInit(DemonBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            target = GameObject.FindObjectOfType<DemonAttackTarget>();
            if(target == null)
            {
                boss.DefaultMovementState.AvailableStates.Remove(this);
                return;
            }

            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(DemonBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            storage.EventsStorage.CoreEvnts.OnMonsterDied += OnMonsterDied;
            ShotToTarget(target);
        }

        private void ShotToTarget(DemonAttackTarget target)
        {
            boss.Renderer.transform.rotation = Quaternion.Euler(0f, boss.transform.position.x > target.transform.position.x ? 180f : 0f, 0f);
            boss.Animator.Play("AttackActivator");
        }

        //Method called from animator
        public void SpawnEvilBall()
        {
            EvilBall ball = storage.ReferenceStorage.WaveSpawner.SpawnMonster(evilBallPrefab, evilBallData, boss.transform.parent, true);
            ball.transform.position = evilBallSpawnPoint.transform.position;
            ball.MakeTransparable(0f);
            SpriteAlphaAnimation alphaAnimation = ball.gameObject.AddComponent<SpriteAlphaAnimation>();
            alphaAnimation.Init(0f, 1f, 1f, ball);
            Rotate rotateAnimation = ball.gameObject.AddComponent<Rotate>();
            rotateAnimation.Init(35f, ball.Renderer.transform);
            ball.SetupTarget(target);
            ball.TargetReached += OnBallTargetReached;
            currentBalls.Add(ball);

            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
            boss.SetDefaultMovementState();
        }

        private void OnBallTargetReached(EvilBall ball)
        {
            currentBalls.Remove(ball);
        }

        private void OnMonsterDied(Monster monster)
        {
            EvilBall ball = monster as EvilBall;
            if (ball != null)
                currentBalls.Remove(ball);
        }

        public override void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnMonsterDied -= OnMonsterDied;
        }

        public override void UpdateState()
        {
            
        }

        public override bool IsReadyForWork()
        {
            if(boss.DistanceTravelled >= pathInterval.nextValue)
            {
                if (IsAngleAdmissible(evilBallSpawnPoint.position, target.GetCenter()))
                    return true;
            }

            return false;
        }

        private bool IsAngleAdmissible(Vector3 source, Vector3 target)
        {
            Vector2 direction = target - source;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            angle = Mathf.Abs(angle);
            if (angle > 90f)
                angle = 180f - angle;
            return angle <= horizontalAdmissibleAngle;
        }
    }

    [System.Serializable]
    public class IntervalData
    {
        public float baseInterval;
        public RangeFloat intervalRandomRange;
        public float nextValue;
    }
}
