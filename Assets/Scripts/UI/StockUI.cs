using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class StockUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text costText;
        public Image priceTendency;
        public float currentCost;
        public float lastCost;
        public string stockName;

        public void UpdateCost(float cost)
        {
            lastCost = currentCost;
            currentCost = cost;

            costText.text = cost.ToString();
        }
    }
}
