using Data;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Towers
{
    public class AttackableTower : Tower, IGUIComposable
    {
        [Header("Attackable Tower")]
        [SerializeField]
        protected BaseAttackSystem attackSystem;
        public BaseAttackSystem AttackSystem => attackSystem;

        public override void Init(DataStorage storage)
        {
            attackSystem.Init(this, storage);
            //FillTooltipData();
            base.Init(storage);       
        }

        protected void FillTooltipData(DescriptionData data)
        {
            attackSystem.CreateTooltipData(data);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            //FillTooltipData(data);
            data = base.CreateTooltipData(data);
            attackSystem.CreateTooltipData(data);
            return data;
        }

        public override void UpdateTower()
        {
            base.UpdateTower();
            attackSystem.UpdateAttackSystem();
        }

        public virtual void BuffRange(float range)
        {
            
        }

        public virtual void BuffAttackSpeed(float attackSpeed)
        {
            
        }

        public void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerBasicGUIText rangeModel = infoPanel.BasicTextes.GetElement();
            rangeModel.Text.text = "Range: " + range.data.GetGUILabel();
            UnityAction RangeUpdated = () => { rangeModel.Text.text = "Range: " + range.data.GetGUILabel(); };

            guiManager.guiElementableActionController.Add(new GUIElementableEventPair(RangeUpdated, range));
        }

        public void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerUpgradeGuiText rangeModel = infoPanel.UpgradeTextes.GetElement();
            rangeModel.Text.text = "Range: " + range.data.GetGUILabel();
            rangeModel.UpgradeText.text = range.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

            UnityAction RangeUpdated = () => {
                rangeModel.Text.text = "Range: " + range.data.GetGUILabel();
                rangeModel.UpgradeText.text = range.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };

            guiManager.guiElementableActionController.Add(new GUIElementableEventPair(RangeUpdated, range));
        }

        public override void OnDemolished()
        {
            base.OnDemolished();
            attackSystem.OnTowerDemolished();
        }
    }
}
