using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public abstract class TargetableBullet : Bullet
    {
        [SerializeField] protected float minDistanceToHit;
        [SerializeField] protected float speed;

        public override void UpdateBullet()
        {
            UpdateRotation();
            UpdatePosition();
            CheckForReach();
        }

        protected void UpdateRotation()
        {
            Vector3 vectorToTarget = target.GetCenter() - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);
        }

        protected void UpdatePosition()
        {
            Vector3 diff = target.GetCenter() - transform.position;
            Vector3 newPosition = transform.position + diff.normalized * speed * Time.deltaTime;
            transform.position = newPosition;
        }

        protected void CheckForReach()
        {
            float distance = Vector3.Distance(target.GetCenter(), transform.position);
            if (distance < minDistanceToHit)
            {
                ManageTargetReached();
            }
        }

        protected virtual void ManageTargetReached()
        {
            OnTargetReached?.Invoke(this);
            DealDamage(target);
        }

        public abstract void DealDamage(Monster target);
    }
}
