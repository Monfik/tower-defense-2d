﻿using CoreUI;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Creatures;
using UI;
using UnityEngine;

namespace Data
{
    public class UIStorage : MonoBehaviour
    {
        [SerializeField]
        private MainMenuView mainMenuView;
        public MainMenuView MainMenuView => mainMenuView;

        [SerializeField]
        private LevelView levelView;
        public LevelView LevelView => levelView;

        [SerializeField]
        private TextParticleDisolver textDisolver;
        public TextParticleDisolver TextDisolver => textDisolver;

        [SerializeField]
        private ResourcesCostPanel costPanelUI;
        public ResourcesCostPanel CostPanelUI => costPanelUI;

        [SerializeField]
        private MonsterDataPanel monsterDataPanel;
        public MonsterDataPanel MonsterDataPanel => monsterDataPanel;
    }
}
