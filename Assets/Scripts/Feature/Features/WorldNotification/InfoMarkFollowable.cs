using UnityEngine;
using UnityEngine.Events;

namespace Feature
{
    public class InfoMarkFollowable : BaseInfoMark
    {
        protected Transform owner;
        public Transform Owner => owner;

        public void Display(string message, Transform owner, Vector3 startPosition, float existingTime, Camera camera, UnityAction<BaseInfoMark> OnMarkFinished)
        {
            SetText(message);
            this.owner = owner;
            currentWorldPosition = startPosition;
            worldPositionDelta = startPosition - owner.transform.position;
            screenStartPosition = camera.WorldToScreenPoint(startPosition);
            screenTargetPosition = screenStartPosition + Vector2.up * (existingTime * targetHeightMultiplier);
            rect.position = screenStartPosition;
            this.existingTime = existingTime;
            this.OnMarkFinished = OnMarkFinished;
            currentTime = 0f;
        }

        public override void UpdateMark(Camera camera)
        {
            if (owner == null)
            {
                OnMarkFinished?.Invoke(this);
                return;
            }

            currentTime += Time.deltaTime;
            float timeValue = currentTime / existingTime;
            currentWorldPosition = owner.transform.position + worldPositionDelta;
            screenStartPosition = camera.WorldToScreenPoint(currentWorldPosition);
            screenTargetPosition = screenStartPosition + Vector2.up * (existingTime * targetHeightMultiplier);
            rect.position = Vector2.Lerp(screenStartPosition, screenTargetPosition, movementCurve.Evaluate(timeValue));
            float alpha = alphaCurve.Evaluate(timeValue);
            SetTransparency(alpha);

            if (timeValue >= 1)
            {
                OnMarkFinished?.Invoke(this);
            }
        }
    }
}
