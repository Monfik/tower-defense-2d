using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public List<PathCreator> paths;

    [ContextMenu("Collect paths")]
    public void CollectPaths()
    {
        paths = new List<PathCreator>();
        Collider2D polygonCollider = GetComponent<Collider2D>();

        Map map = GameObject.FindObjectOfType<Map>();
        foreach (PathCreator path in map.MiddlePaths)
        {
            foreach (Vector2 vert in path.path.localPoints)
            {
                Vector2 globalPos = path.transform.TransformPoint(vert);
                if (polygonCollider.OverlapPoint(globalPos))
                {
                    paths.Add(path);
                    break;
                }
            }
        }
    }
}
