using TowerDefense.Creatures;
using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using TowerDefense.Towers;
using UnityEngine.Events;

namespace LevelLogic
{
    public class LevelManager : BaseInitable
    {
        [SerializeField] private Level currentLevel;
        public Level CurrentLevel => currentLevel;

        private LevelFinisher levelFinisher;
        public LevelFinisher LevelFinisher => levelFinisher;

        [SerializeField] private LevelSummaryPanel summaryPanel;

        public void UpdateController()
        {
            currentLevel?.UpdateLevel();  
        }

        //public void LoadLevel(int levelIndex)
        //{
        //    currentLevel = storage.Levels.levels[levelIndex];
        //    currentLevel.gameObject.SetActive(true);
        //    currentLevel.Init(storage);
        //    currentLevel.StartLevel();
        //}

        public void LoadLevel(Level level, bool enterLevelState = true)
        {
            currentLevel = level;
            currentLevel.gameObject.SetActive(true);
            currentLevel.Init(storage, OnLevelFinished);

            if(enterLevelState)
                storage.ReferenceStorage.MainState.SetLevelState();
        }

        private void OnLevelFinished(Level level)
        {
            levelFinisher = new LevelFinisher(level, OnLevelCompleted);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(levelFinisher);
            storage.EventsStorage.CoreEvnts.CallOnlevelFinished(level);
            storage.EventsStorage.CoreEvnts.CallOnLevelUnloaded(level);
            levelFinisher.AddFinisher(summaryPanel);
            levelFinisher.StartFinishing();

            if (levelFinisher.LevelFinishers.Count == 0)
                OnLevelCompleted(currentLevel);
        }

        private void OnLevelCompleted(Level level)
        {
            Values.levelNumber++;
            storage.ReferenceStorage.BlueprintController.OnLevelFinished(level);
            storage.ReferenceStorage.MainState.SetBlueprintState();
            levelFinisher = null;
        }

        public void OrderRepeatLevel()
        {
            currentLevel.Castle.Invulnerable = true;
            storage.ReferenceStorage.PauseController.DisablePauseInput();
            
            levelFinisher = new LevelFinisher(currentLevel, storage.ReferenceStorage.MainState.SetLevelState);
            storage.EventsStorage.CoreEvnts.CallOnLevelUnloaded(currentLevel);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(levelFinisher);
            //levelFinisher.AddFinisher(summaryPanel);
            levelFinisher.StartFinishing();

            if (levelFinisher.LevelFinishers.Count == 0)
                storage.ReferenceStorage.MainState.SetLevelState(currentLevel);
        }

        private void StartFinisher()
        {

        }

        public void UnloadLevel()
        {
            currentLevel.Deinit();
            currentLevel.gameObject.SetActive(false);
            currentLevel = null;
            summaryPanel.SetupDefault();
        }      
    }
}