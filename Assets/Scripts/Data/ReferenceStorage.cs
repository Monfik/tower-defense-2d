﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomInput;
using LevelLogic;
using TowerDefense.Towers;
using Core;
using TowerDefense.Building;
using CameraHandle;
using Postpone;
using CoreUI;
using TowerDefense.Spells;
using TowerDefense;
using TowerDefense.Updatables;
using UnityEngine.UI;
using TowerDefense.Blueprints;
using UnityEngine.Rendering;
using TowerDefense.Enviroment;
using TowerDefense.GlobalEvents;
using TowerDefense.Controllers;
using Feature.Notifications;

namespace Data
{
    public class ReferenceStorage : MonoBehaviour
    {
        [SerializeField] private MainGameState mainGameState;
        public MainGameState MainState => mainGameState;

        [SerializeField]
        private InputController input;
        public InputController Input => input;

        [SerializeField]
        private GeneralSettings settings;
        public GeneralSettings Settings => settings;

        [SerializeField]
        private WaveSpawner waveSpawner;
        public WaveSpawner WaveSpawner => waveSpawner;

        [SerializeField]
        private LevelManager levelManager;
        public LevelManager LevelManager => levelManager;

        [SerializeField]
        private LevelController levelController;
        public LevelController LvlController => levelController;

        [SerializeField]
        private WaveController waveController;
        public WaveController WaveController => waveController;

        [SerializeField]
        private Shop shop;
        public Shop Shop => shop;

        [SerializeField]
        private MainController mainController;
        public MainController MainController => mainController;

        [SerializeField]
        private PreviewController previewController;
        public PreviewController PreviewController => previewController;

        [SerializeField]
        private TowersController towersController;
        public TowersController TowersController => towersController;

        [SerializeField]
        private Camera mainCamera;
        public Camera MainCamera => mainCamera;

        [SerializeField]
        private CameraMovement cameraMovement;
        public CameraMovement CameraMovement => cameraMovement;

        [SerializeField]
        private SpeedController speedController;
        public SpeedController SpeedController => speedController;

        [SerializeField]
        private TowerSelector towerGUI;
        public TowerSelector TowerGUI => towerGUI;

        [SerializeField]
        private PostponeController postponer;
        public PostponeController Postponer => postponer;

        [SerializeField]
        private Wallet wallet;
        public Wallet Wallet => wallet;

        [SerializeField]
        private SpellsController spellsController;
        public SpellsController Spells => spellsController;

        [SerializeField]
        private ManaController manaController;
        public ManaController ManaController => manaController;

        [SerializeField]
        private PathSnapper pathSnapper;
        public PathSnapper PathSnapper => pathSnapper;

        [SerializeField]
        private UpdatablesController updatableController;
        public UpdatablesController UpdatablesController => updatableController;

        [SerializeField]
        private Raycaster raycaster;
        public Raycaster Raycaster => raycaster;

        [SerializeField]
        private UIUpdatableManager UIiconsManager;
        public UIUpdatableManager UIIconsManager => UIiconsManager;

        [SerializeField] private Image avatar;
        public Image Avatar => avatar;

        [SerializeField] private BlueprintController blueprintController;
        public BlueprintController BlueprintController => blueprintController;

        [SerializeField] private SceneLoader transitionLoader;
        public SceneLoader TransitionLoader => transitionLoader;

        [SerializeField] private Volume postprocessVolume;
        public Volume PostprocessVolume => postprocessVolume;

        [SerializeField] private GlobalEventsController eventsController;
        public GlobalEventsController EventsController => eventsController;

        [SerializeField] private BrightnessController brightness;
        public BrightnessController Brightness => brightness;

        [SerializeField] private WindController windController;
        public WindController WindController => windController;

        [SerializeField] private PauseController pauseController;
        public PauseController PauseController => pauseController;

        [SerializeField] private StockMarketController stockMarket;
        public StockMarketController StockMarket => stockMarket;

        [SerializeField] private InfoMarkGenerator markGenerator;
        public InfoMarkGenerator MarkGenerator => markGenerator;

        public FinishersCollection levelFinishers;

        [SerializeField] private InitableCollection mainGameCollection;
        public InitableCollection MainGameCollection => mainGameCollection;

        [SerializeField] private Selector selector;
        public Selector Selector => selector;

        [SerializeField] private MusicPlayer musicPlayer;
        public MusicPlayer MusicPlayer => musicPlayer;
    }
}
