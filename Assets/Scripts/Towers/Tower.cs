using Data;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Affectors;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class Tower : MonoBehaviour, ITooltipable, ISnowballable
    {
        [SerializeField]
        protected TowerGraphicsSystem graphicsSystem;
        public TowerGraphicsSystem GraphicsSystem => graphicsSystem;

        [SerializeField]
        protected TowerGuiManager guiManager;
        public TowerGuiManager GuiManager => guiManager;

        [SerializeField]
        protected TowerBuilder builder;
        public TowerBuilder Builder => builder;

        [SerializeField]
        protected SpriteRenderer rangeRenderer;
        public SpriteRenderer RangeRenderer => rangeRenderer;

        [SerializeField]
        protected BoxCollider2D boxCollider;
        public BoxCollider2D BoxCollider => boxCollider;

        [SerializeField]
        new protected Collider2D collider;
        public Collider2D Collider => collider;

        [SerializeField]
        protected string towerName;
        public string TowerName => towerName;

        public UpgradableStaticsFloatData range;

        protected float realRange;
        public float RealRange => realRange;

        [SerializeField] protected TowerEffectsController effects;
        public TowerEffectsController Effects => effects;

        [SerializeField] private AffectorsContainer<Tower> affectorsContainer;

        [SerializeField] private Canvas towerCanvas;
        protected DataStorage storage;

        [Header("Colors")]
        [SerializeField] private Color highlightColor = Color.white;
        [SerializeField] private Color blockColor = Color.red;

        [SerializeField] private RangeInt snowballCoruptionRange = new RangeInt(0, 10); //0-100

        public virtual void Preinit()
        {
            range.Reset();
            UpdateRange();
        }

        public virtual void Init(DataStorage storage)
        {
            this.storage = storage;
            guiManager.Init(storage);
            graphicsSystem.Init(this);
            affectorsContainer.Init(this, storage);

            builder.Init(storage, this);
            if (!builder.StaticBuild)
                builder.Build();

            towerCanvas.worldCamera = storage.ReferenceStorage.MainCamera;
            effects.Init(storage, this);
            AssignEvents(storage.EventsStorage);
            builder.OnFinishedUpgradingBasic += UpdateRange;
        }

        protected virtual void AssignEvents(EventsStorage events)
        {

        }

        public void UpdateRange()
        {
            realRange = GetRealRange(range.data.currentValue);
            SetupRangeVisualizer(realRange);
        }

        private void SetupRangeVisualizer(float range)
        {
            rangeRenderer.transform.localScale = new Vector3(range * 2, range * 2, 1f);
        }

        public virtual void UpdateTower()
        {
            builder.UpdateSystem();
            effects.UpdateController();
        }

        public virtual void DestroyTower()
        {
            OnDemolished();
        }

        public void EnableRangeVisualizer(bool enabled)
        {
            rangeRenderer.gameObject.SetActive(enabled);
        }

        public virtual void HighlightBlockBuildColor()
        {
            foreach (SpriteRenderer renderer in graphicsSystem.GraphicsData.visualSprites)
            {
                renderer.color = blockColor;
            }
        }

        public virtual void HighlightNormalBuildColor()
        {
            foreach (SpriteRenderer renderer in graphicsSystem.GraphicsData.visualSprites)
            {
                renderer.color = highlightColor;
            }
        }

        protected float GetRealRange(float range)
        {
            return range / 10f;
        }

        public virtual void OnDemolished()
        {

        }

        public virtual DescriptionData CreateTooltipData(DescriptionData data)
        {
            data.title1 = towerName;
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Range", range.data.coreValue.ToString()));
            return data;
        }

        public void HitBySnawball(Snowball snowball)
        {
            if (snowball.strength == 0)
                return;

            storage.ReferenceStorage.MarkGenerator.Notify("Destroyed/Weakened", GetCenter() + new Vector3(0f, boxCollider.size.y / 2, 0f));
            float snowballCoruption = Mathf.Lerp(snowballCoruptionRange.a, snowballCoruptionRange.b, snowball.strength);
            builder.Repairer.Damage(snowballCoruption);
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public Vector3 GetCenter()
        {
            return boxCollider.bounds.center;
        }

        [ContextMenu("Demolish")]
        private void DEVDemolish()
        {
            builder.Demolish();
        }
    }

    [System.Serializable]
    public class TooltipTowerDescriptionData
    {
        //title and description setup from Inspector please
        public string title;
        public string description;
        public string cost;
        public List<DescriptionSingleSpecifiedData> specifiedDatas;

        public static DescriptionData CreateDescription(TooltipTowerDescriptionData data)
        {
            DescriptionData description = DescriptionData.CreateDescription(data.title, data.description, string.Empty, data.cost, data.specifiedDatas);

            return description;
        }
    }
}
