using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Tips;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowerEffectsController : MonoBehaviour
    {
        private List<BaseTowerEffect> effects;
        private Tower owner;
        protected DataStorage storage;

        [Header("Preview handlers")]
        [SerializeField] private RectTransform effectsPreviewParent;
        [SerializeField] private List<PreviewEffectPair> previews;
        [SerializeField] private TowerEffectPreview previewPrefab;

        public void Init(DataStorage storage, Tower owner)
        {
            this.storage = storage;
            this.owner = owner;
            effects = new List<BaseTowerEffect>();
        }
            
        public void AddEffect(BaseTowerEffect effect)
        {
            AddEffectStatusPreview(effect);

            effects.Add(effect);
            effect.Init(owner, OnEffectFinished);
            effect.StartEffect();
        }

        public void AddEffectStatusPreview(BaseTowerEffect effect)
        {
            if (!effect.effectPreviewable)
                return;

            bool previewExists = previews.Exists(c => c.effectId.Equals(effect.Id));
            if (previewExists)
                return;

            TowerEffectPreview preview = CreatePreview(effect.PreviewData);

            AddPreview(preview, effect.Id);
        }

        public TowerEffectPreview CreatePreview(EffectPreviewData data)
        {
            TowerEffectPreview preview = Instantiate(previewPrefab, effectsPreviewParent);
            preview.transform.localScale = Vector3.one;
            preview.Tip.SetDescription(DescriptionData.CreateDescription(data.effectDescription));
            if (data.Preview != null)
                preview.image.sprite = data.Preview;

            return preview;
        }

        public void AddPreview(TowerEffectPreview preview, string id)
        {
            PreviewEffectPair effectPair = new PreviewEffectPair(id, preview);
            previews.Add(effectPair);
        }

        public void RemovePreview(string id)
        {
            PreviewEffectPair previewPair = previews.Find(c => c.effectId.Equals(id));
            if (previewPair != null)
            {
                Destroy(previewPair.preview.gameObject);
                previews.Remove(previewPair);
            }
        }

        public void UpdateController()
        {
            for(int i=0; i<effects.Count; i++)
            {
                effects[i].UpdateEffect();
            }
        }

        private void OnEffectFinished(BaseTowerEffect effect)
        {
            effects.Remove(effect);
            RemovePreview(effect);
        }

        private void RemovePreview(BaseTowerEffect effect)
        {
            if (!effect.effectPreviewable)
                return;

            List<BaseTowerEffect> relatedEffects = effects.FindAll(c => c.Id.Equals(effect.Id));
            if(relatedEffects.Count == 0)
            {
                RemovePreview(effect.Id);
            }
        }

        public bool Exists(string id)
        {
            return effects.Exists(c => c.Id.Equals(id));
        }

        public BaseTowerEffect FindById(string id)
        {
            return effects.Find(c => c.Id.Equals(id));
        }
    }

    [System.Serializable]
    public class PreviewEffectPair
    {
        public string effectId;
        public TowerEffectPreview preview;

        public PreviewEffectPair(string id, TowerEffectPreview preview)
        {
            effectId = id;
            this.preview = preview;
        }
    }
}
