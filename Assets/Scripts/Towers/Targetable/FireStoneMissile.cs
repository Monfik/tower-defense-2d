using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Towers
{
    public class FireStoneMissile : StoneMissile
    {
        [SerializeField]
        private Fire fireEffect;

        protected FireStoneData explTowerData;

        private UnityAction<FireStoneMissile> OnFireStoneExploded;

        public void Init(Monster target, DataStorage storage, float range, FireStoneData fireTowerData, UnityAction<FireStoneMissile> OnStoneExploded)
        {
            this.target = target;
            this.storage = storage;
            this.OnFireStoneExploded = OnStoneExploded;
            explTowerData = fireTowerData;
            data.range = range;
            storage.EventsStorage.CoreEvnts.OnMonsterDied += MonsterDied;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath += MonsterDied;
        }

        public override void UpdateMissile()
        {
            if (!fly)
                return;

            currentTime += Time.deltaTime * data.flySpeed;
            float t = currentTime / flyTime;
            t = Mathf.Clamp(t, 0f, 1f);
            Vector2 newPosition = MathParabola.Parabola(startPosition, targetPosition, parabolaHeight, t);
            UpdateRotation(newPosition);
            transform.position = newPosition;

            if (t == 1)
            {
                Explode();
            }
        }

        private void UpdateRotation(Vector2 targetPos)
        {
            Vector2 position = new Vector2(transform.position.x, transform.position.y);
            Vector2 vectorToTarget = targetPos - position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);
        }

        protected override void Explode()
        {
            boomAnimation.process = true;
            boomAnimation.enabled = false;
            fly = false;

            boomCollider.enabled = true;
            Collider2D[] colliders = new Collider2D[100];
            ContactFilter2D filter = new ContactFilter2D();
            int colliderCount = Physics2D.OverlapCollider(boomCollider, filter, colliders);
            List<Monster> hittedMonsters = new List<Monster>();

            for (int i = 0; i < colliderCount; i++)
            {
                Monster hittedMonster = colliders[i].GetComponent<Monster>();
                if (hittedMonster != null)
                {
                    if(!hittedMonster.Dead)
                        hittedMonsters.Add(hittedMonster);
                }
            }

            for (int i = 0; i < hittedMonsters.Count; i++)
            {
                DealDamage(hittedMonsters[i]);
            }

            Fire fire = Instantiate(fireEffect);
            fire.InitData(storage, explTowerData.fireDamagePerSecond.data.currentValue, explTowerData.fireDuration.data.currentValue);
            fire.transform.position = targetPosition;
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(fire);
            OnFireStoneExploded?.Invoke(this);
        }

        private void MonsterDied(Monster monster)
        {
            if (monster == target && !fly)
            {
                targetPosition = target.transform.position;
                target = null;
            }
        }

        protected override void DealDamage(Monster monster)
        {
            int armorResistance = monster.Data.armor.currentValue;
            int randomDamage = Random.Range(explTowerData.minExploDamage.data.currentValue, explTowerData.maxExploDamage.data.currentValue);
            int Dmg = Utils.DamageCalculator.CalculateDamage(randomDamage, armorResistance);
            monster.TakeDamage(Dmg, DamageType.Physics);
        }
    }
}
