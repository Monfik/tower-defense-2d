using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class BaseDurationableEvent : BaseDisplayableGlobalEvent
    {
        [SerializeField] protected RangeFloat durationRange;
        protected float duration;
        private bool finishing = false;
        [SerializeField] private float currentDuration = 0f;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnEventFinished)
        {
            base.BeginEvent(storage, OnEventFinished);
            duration = durationRange.GetRandom();
        }

        public override void UpdateEvent()
        {
            base.UpdateEvent();
            if(!finishing)
                CalculateTime();
        }

        private void CalculateTime()
        {
            currentDuration += Time.deltaTime;
            if(currentDuration >= duration)
            {
                StartToFinishEvent();
            }
        }

        public override void StartToFinishEvent()
        {
            base.StartToFinishEvent();
            finishing = true;
        }
    }
}
