using Core;
using Core.Invokers;
using Data;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class MapFireballingSpellInvoker : BaseInvokerMono<MapFireballSpellCommand>
    {
        private FireballingMapData fireballsMapData;
        private UnityAction<FireballingMapData> OnInvokerReceivedData;

        public void Init(DataStorage storage, FireballingMapData fireballsMapData, UnityAction<FireballingMapData> OnInvokerFinished)
        {
            base.Init(storage, null, null);
            this.fireballsMapData = fireballsMapData;
            this.OnInvokerReceivedData = OnInvokerFinished;
        }

        public override void Execute()
        {
            if (commands.Count > 0)
            {
                currentCommandable = commands.Peek();
                currentCommandable.InitCommand(storage, OnCommandFinished, OnCommandFailed);
                MapFireballSpellCommand fireballSpellCommand = currentCommandable as MapFireballSpellCommand;
                fireballSpellCommand.InitData(fireballsMapData);
                fireballSpellCommand.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("No commands to execute");
            }
        }

        protected override void FinishInvoker()
        {
            base.FinishInvoker();
            OnInvokerReceivedData?.Invoke(fireballsMapData);
        }

        public void ManageCaster(FireballSpellCaster caster)
        {
            foreach(MapFireballSpellCommand command in commandsList)
            {
                command.ManageCaster(caster);
            }
        }
    }
}


