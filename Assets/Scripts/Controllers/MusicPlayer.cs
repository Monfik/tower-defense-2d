using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Controllers
{
    public class MusicPlayer : BaseInitable
    {
        [SerializeField] new private AudioSource audio;

        public AudioClip mainMenuClip;

        public void Play(AudioClip clip)
        {
            audio.Stop();
            audio.clip = clip;
            audio.Play();
        }

        public void Stop()
        {
            audio.Stop();
        }
    }
}
