using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Power
{
    public class PhysicalAttack : IPowerAttackable
    {
        private PhysicalAttackData data;

        public PhysicalAttack(PhysicalAttackData data)
        {
            this.data = data;
        }

        public void Attack(IDestructable dmgTakable)
        {
            int armor = dmgTakable.GetData().armor.currentValue;
            armor -= data.armorReduction;
            int randomDmg = Random.Range(data.minDmg, data.maxDmg);
            int dmg = DamageCalculator.CalculateDamage(randomDmg, armor);
            dmgTakable.TakeDamage(dmg, DamageType.Physics);
        }

        public void Attack(IDestructable dmgTakable, float dmgFactor)
        {
            int armor = dmgTakable.GetData().armor.currentValue;
            armor -= data.armorReduction;
            int randomDmg = Random.Range(data.minDmg, data.maxDmg);
            randomDmg = (int)(randomDmg * dmgFactor);
            int dmg = DamageCalculator.CalculateDamage(randomDmg, armor);
            dmgTakable.TakeDamage(dmg, DamageType.Physics);
        }
    }

    [System.Serializable]
    public class PhysicalAttackData
    {
        public int minDmg;
        public int maxDmg;
        public int armorReduction;
    }
}
