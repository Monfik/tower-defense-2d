using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace TowerDefense
{
    public class UIObjectEventHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public UnityAction OnPointerEntered;
        public UnityAction OnPointerExited;

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnPointerEntered?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnPointerExited?.Invoke();
        }
    }
}
