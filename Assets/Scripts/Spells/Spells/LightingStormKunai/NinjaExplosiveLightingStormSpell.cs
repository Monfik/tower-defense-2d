using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Power;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class NinjaExplosiveLightingStormSpell : BaseSpell
    {
        [SerializeField] private SpellTargetMarker kunaiPreview;

        private PathSnapper snapper;

        [Header("Colors")]
        [SerializeField] private Color possibleCastColor;
        [SerializeField] private Color unpossibleCastColor;

        [SerializeField] private NinjaLightingStormCaster caster;
        [SerializeField] private ExplosiveLightingStormData stormData;

        public override void StartCast()
        {
            base.StartCast();
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            snapper = storage.ReferenceStorage.PathSnapper;
            kunaiPreview.gameObject.SetActive(true);
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 snappedPosition;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);

            if (snapped)
            {
                kunaiPreview.transform.position = snappedPosition;

                if(!kunaiPreview.MarkerEnabled)
                    kunaiPreview.SetMark(possibleCastColor, true);

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (Values.InputValues.POINTER_OVER_UI)
                        return;

                    ThrowKunai(snappedPosition);
                }
            }
            else
            {
                if (kunaiPreview.MarkerEnabled)
                    kunaiPreview.SetMark(unpossibleCastColor, false);

                kunaiPreview.transform.position = mousePosition;
            }
        }

        public void ThrowKunai(Vector2 position)
        {
            stormData.position = position;
            caster.Init(stormData);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(caster);
            PayMana();
            OnSpellCasted?.Invoke(this);
            FinishSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=5>Physical", $"{stormData.boomDmg.minDmg} - {stormData.boomDmg.maxDmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Physical reduction", $"{stormData.boomDmg.armorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData($"<sprite={(int)stormData.lightingDamagePerSecond.magicType}>" +stormData.lightingDamagePerSecond.magicType.ToString() + " per second", $"{stormData.lightingDamagePerSecond.minDmg} - {stormData.lightingDamagePerSecond.maxDmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData(stormData.lightingDamagePerSecond.magicType.ToString() + " reduction", $"{stormData.lightingDamagePerSecond.magicArmorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Hit interval", $"{stormData.lightingDamageTimeInterval}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Storm duration", $"{stormData.stormDuration}"));

            return data;
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            base.FinishSpell();
        }
    }

    [System.Serializable]
    public class ExplosiveLightingStormData
    {
        public PhysicalAttackData boomDmg;
        public MagicAttackData lightingDamagePerSecond;
        public float lightingDamageTimeInterval;
        public float stormDuration;
        public Vector2 position;
    }
}
