using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    public class PlayerEvents
    {
        public UnityAction<int> OnPlayerTakenDamage;
        public void CallOnPlayerTakenDamage(int damage)
        {
            OnPlayerTakenDamage?.Invoke(damage);
        }

        public UnityAction OnPlayerDead;
        public void CallOnPlayerDead()
        {
            OnPlayerDead?.Invoke();
        }

        public UnityAction<int> OnManaChanged;
        public void CallOnManaChanged(int mana)
        {
            OnManaChanged?.Invoke(mana);
        }
    }
}
