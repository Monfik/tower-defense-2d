using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Towers;
using TowerDefense.Updatables;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class Sandball : BaseUpdatableMono
    {
        private Tower target;
        private float movementSpeed;
        [SerializeField] private float distanceReach = 0.1f;
        [SerializeField] [Range(0f, 1f)] private float sandStrength;
        [SerializeField] private SandDebuffEffect sandDebuffEffect;

        [SerializeField] private RangeFloat sinusoidalScaleRange;

        public void Setup(Tower target, float movementSpeed)
        {
            this.target = target;
            this.movementSpeed = movementSpeed;

            target.Builder.OnFinishDemolish += OnTargetDestroyed;
        }

        private void OnTargetDestroyed()
        {
            Finish();
        }

        public override void UpdateUpdatable()
        {
            base.UpdateUpdatable();
            Vector2 direction = target.transform.position - transform.position;
            Vector3 movementDelta = direction.normalized * movementSpeed * Time.deltaTime;
            transform.position = transform.position + movementDelta;

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);

            float distance = Vector3.Distance(transform.position, target.transform.position);
            if(distance <= distanceReach)
            {
                Hit(target);
            }

            float sinValue = Mathf.Sin(Time.time * Mathf.Rad2Deg);
            sinValue *= 5f;
            float sinInversed = Mathf.InverseLerp(-1f, 1f, sinValue);
            float scale = Mathf.Lerp(sinusoidalScaleRange.a, sinusoidalScaleRange.b, sinInversed);
            transform.localScale = scale * Vector3.one;
        }

        public void Hit(Tower target)
        {
            BaseTowerEffect effect = target.Effects.FindById(sandDebuffEffect.Id);
            if(effect == null)
            {
                SandDebuffEffect debuff = new SandDebuffEffect(sandDebuffEffect);
                debuff.timable = false;
                target.Effects.AddEffect(debuff);
                debuff.AddSand(sandStrength);
                return;
            }

            SandDebuffEffect sandEffect = effect as SandDebuffEffect;
            if(sandEffect == null)
            {
                Debug.LogError("Dear god, why its null...");
                return;
            }

            sandEffect.AddSand(sandStrength);
            Finish();
        }

        public override void Finish()
        {
            base.Finish();
        }
    }
}
