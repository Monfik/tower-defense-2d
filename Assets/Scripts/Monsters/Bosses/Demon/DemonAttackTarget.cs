using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Creatures
{
    public class DemonAttackTarget : MonoBehaviour, ITargetable
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] private TowersActivatorLevelModificator towersHolder;
        public UnityAction OnActivate;

        [Header("Effects")]
        [SerializeField] private DisableTowerEffect effect;
            

        public Vector3 GetCenter()
        {
            return targetTransform.position;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void Activate()
        {
            AddEffectToRandomTower(towersHolder.TowersToActivate);
            OnActivate?.Invoke();
        }

        private void AddEffectToRandomTower(List<Tower> towers)
        {
            List<Tower> enabledTowers = new List<Tower>();

            foreach(Tower tower in towers)
            {
                if (!IsDisabled(tower))
                {
                    enabledTowers.Add(tower);
                }
            }

            if (enabledTowers.Count == 0)
                return;

            Tower randomTower = enabledTowers[Random.Range(0, enabledTowers.Count - 1)];
            DisableTowerEffect disableEffect = new DisableTowerEffect(effect);
            AddEffect(randomTower, disableEffect);
        }

        private bool IsDisabled(Tower tower)
        {
            return tower.Effects.Exists(effect.Id);
        }

        private void AddEffect(Tower tower, BaseTowerEffect effect)
        {
            tower.Effects.AddEffect(effect);
        }
    }
}
