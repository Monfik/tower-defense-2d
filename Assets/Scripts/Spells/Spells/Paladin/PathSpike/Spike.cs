using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class Spike : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRend;
        [SerializeField] private SpriteAnimationv2 animator;
        public SpriteAnimationv2 Animator => animator;
        private List<IDestructable> damaged = new List<IDestructable>();

        public UnityAction<Spike> OnSpikeFinished;
        public UnityAction<IDestructable> OnTargetHitted;
        public Pool<Spike> poolOwner;

        public void Play(UnityAction<IDestructable> OnTargetHitted, UnityAction<Spike> OnSpikeFinished)
        {
            this.OnTargetHitted = OnTargetHitted;
            this.OnSpikeFinished = OnSpikeFinished;
            animator.PlayAnimation(OnAnimationFinished);
        }

        private void OnAnimationFinished(SpriteAnimationv2 animation)
        {
            OnSpikeFinished?.Invoke(this);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            IDestructable destructable = collision.GetComponent<IDestructable>();
            if(destructable != null)
            {
                if (damaged.Exists(c => c == destructable))
                    return;

                damaged.Add(destructable);
                OnTargetHitted?.Invoke(destructable);
            }
        }
    }
}
