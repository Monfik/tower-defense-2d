using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TowerDefense.Creatures
{
    public abstract class BaseBossState<T> : MonoBehaviour
    {
        protected T boss;
        protected DataStorage storage;

        public virtual void PreInit(T boss, DataStorage storage)
        {
            this.boss = boss;
            this.storage = storage;
        }

        public virtual void Init(T boss, DataStorage storage)
        {
            this.boss = boss;
            this.storage = storage;
        }

        public abstract void UpdateState();
        public abstract void Deinit();
        public virtual bool IsReadyForWork()
        {
            return false;
        }
    }
}
