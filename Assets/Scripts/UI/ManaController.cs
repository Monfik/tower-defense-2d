using Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

namespace TowerDefense
{
    public class ManaController : MonoBehaviour
    {
        [SerializeField] private int maxMana;
        public int MaxMana => maxMana;

        private int manaRegenValue;

        [SerializeField] private int currentMana;
        public int CurrentMana => currentMana;
        [SerializeField] private int additionalMana;

        [SerializeField] private SlidableBar manaBar;
        [SerializeField] private TMP_Text text;

        private float lastTime;

        [Header("Mana-level data")]
        public List<ManaHandler> manaHandlers;

        [SerializeField] private ManaHandler currentManaHandler;
        private DataStorage storage;

        public void Init(DataStorage storage, int manaHandlerId)
        {
            this.storage = storage;

            SetupManaHandler(manaHandlerId);
        }

        public void SetupManaHandler(int manaHandlerId)
        {
            currentManaHandler = manaHandlers.Find(c => c.handlerId == manaHandlerId);
        }

        public void SetupLevelMana(int levelId)
        {
            LevelManaData manaData = currentManaHandler.levelManaPairs.Find(c => c.levelId == levelId);
            if(manaData == null)
            {
                Debug.LogError("There is no mana data accorded to this level in " + currentManaHandler.fractionName);
                manaData = currentManaHandler.levelManaPairs.Find(c => c.levelId == 1);
            }

            currentManaHandler.currentData = manaData;

            int maxCalculatedMana = Mathf.RoundToInt(manaData.manaLevelOnStart * Values.DifiicultyValues.GetMaxManaMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            manaBar.SetMaxValue(maxCalculatedMana);
            maxMana = maxCalculatedMana;
            SetMana(maxCalculatedMana);

            manaRegenValue = Mathf.RoundToInt(manaData.manaRegenPerSecond * Values.DifiicultyValues.GetManaRegenMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));

            lastTime = Time.time;
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                Add(additionalMana);
            }
        }

        public void UpdateMana()
        {
            if(Time.time - lastTime >= 1f)
            {
                RegenMana();
                lastTime = Time.time;
            }
        }

        public void RegenMana()
        {
            Add(manaRegenValue);
        }

        public void SetMana(int mana)
        {       
            manaBar.SetValue(mana);
            currentMana = mana;
            text.text = currentMana.ToString() + "/" + maxMana.ToString();
        }

        public void Substract(int mana)
        {
            currentMana -= mana;

            if (currentMana < 0)
                currentMana = 0;

            storage.EventsStorage.PlayerEv.CallOnManaChanged(currentMana);
            SetMana(currentMana);
        }

        public void Add(int mana)
        {
            if (currentMana == maxMana)
                return;

            currentMana += mana;

            if (currentMana > maxMana)
                currentMana = maxMana;

            storage.EventsStorage.PlayerEv.CallOnManaChanged(currentMana);
            SetMana(currentMana);
        }

        public bool Afford(int mana)
        {
            return currentMana >= mana;
        }

        public void ResetMana()
        {
            SetMana(maxMana);
        }
    }

    [System.Serializable]
    public class ManaHandler
    {
        public string fractionName;
        public int handlerId;
        public List<LevelManaData> levelManaPairs;
        public LevelManaData currentData;
    }

    [System.Serializable]
    public class LevelManaData
    {
        public int levelId;
        public int manaLevelOnStart;
        public int manaRegenPerSecond;
    }
}
