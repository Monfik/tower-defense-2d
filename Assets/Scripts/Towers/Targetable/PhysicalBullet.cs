using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.GlobalEvents;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Towers
{
    public class PhysicalBullet : MonoBehaviour, IWindSusceptible
    {
        [Header("References")]
        [SerializeField]
        new protected SpriteRenderer renderer;
        public SpriteRenderer Renderer => this.renderer;

        protected UnityAction<PhysicalBullet> OnTargetHitted;

        [SerializeField] private List<BaseMonsterEffect> effects;
        public List<BaseMonsterEffect> Effects => effects;

        [Header("Physical and forces values")]
        [SerializeField]
        protected float bulletSpeed = 5f;
        private PhysicsBulletData bulletData;
        
        [SerializeField]
        protected float minDistanceToHit = 0.1f;
        [SerializeField][Range(0, 90f)] private float maxAngleBlow = 45f;

        [Space]
        protected Monster target;
        public Monster Target => target;

        private Vector3 lastPosition;
        [SerializeField] private BulletRotationAproximater rotAproximater;
        private WindForceData windData;

        public void Init(Monster target, PhysicsBulletData bulletData, UnityAction<PhysicalBullet> OnTargetHitted)
        {
            this.target = target;
            this.bulletData = bulletData;
            this.OnTargetHitted = OnTargetHitted;

            lastPosition = transform.position;
            rotAproximater.Init();
            //Debug.Log("MaxDistance to miss = " + maxDistanceToMiss + " and distance to target = " + targetDistance);
        }

        public void UpdateBullet()
        {
            UpdateRotation();
            UpdatePosition();
            //rotAproximater.Update(windData.strength);
        }

        private void UpdateRotation()
        {
            Vector3 vectorToTarget = transform.position - lastPosition;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);
        }

        private void UpdatePosition()
        {
            lastPosition = transform.position;
            Vector3 diff = target.GetCenter() - transform.position;
            float pushedAngleByForces = RandomBlowAngleByWind();

            diff = Quaternion.Euler(0, 0, pushedAngleByForces) * diff.normalized;
            Vector3 newPosition = transform.position + diff * bulletSpeed * Time.deltaTime;
            transform.position = newPosition;

            float distance = Vector3.Distance(target.GetCenter(), transform.position);
            if(distance < minDistanceToHit)
            {
                OnTargetHitted?.Invoke(this);
                DealDamage(target);
            }
        }

        public virtual void DealDamage(Monster monster)
        {
            int randomDamage = (int)Random.Range(bulletData.minDamage.data.currentValue, bulletData.maxDamage.data.currentValue);
            int damage = DamageCalculator.CalculateDamage(randomDamage, monster.Data.armor.currentValue);
            monster.TakeDamage(damage, DamageType.Physics);

            if(monster.Data.hp > 0)
            {
                effects.ForEach(c => monster.Effects.AddEffect(c));
            }
        }

        //private float CalculateMaxMissDistance(float distanceToTarget)
        //{
        //    distanceToTarget = Mathf.Clamp(distanceToTarget, minMissDistance, maxMissDistance);
        //    float distanceT = Mathf.InverseLerp(minMissDistance, maxMissDistance, distanceToTarget);
        //    float curveValue = maxDistanceCalculateCurve.Evaluate(distanceT);
        //    float calculatedMaxDistance = curveValue * distanceToTarget;
        //    if(calculatedMaxDistance <= distanceToTarget)
        //    {
        //        calculatedMaxDistance = distanceToTarget * 2;
        //    }

        //    return calculatedMaxDistance;
        //}

        private float RandomBlowAngleByWind()
        {
            if (windData == null || windData.strength == 0f)
                return 0f;

            float pushedAngleByForces = rotAproximater.GetAngle(maxAngleBlow);
            return pushedAngleByForces;
        }

        public void Affect(WindForceData windData)
        {
            this.windData = windData;
        }
    }

    public class BulletWindForceRandomizer
    {
        private int mark; //-1 or 1

        private float timeChangeInterval;
    }

    [System.Serializable]
    public class BulletRotationAproximater
    {
        private float windStrength;
        private float currentTime;
        [SerializeField] private float periodTime = 2f;
        [SerializeField] private AnimationCurve forceCurve;
        private float mark = 1f;

        public void Init()
        {
            mark = Random.Range(0, 2) == 0 ? -1f : 1f;
        }

        public void Update(float strength)
        {
            windStrength = strength;
            currentTime += Time.deltaTime;
            if (currentTime >= 1800f)
                currentTime -= 1800 + periodTime;
        }

        public float GetAngle(float maxAngle)
        {
            float timeT = currentTime / periodTime;
            timeT = Mathf.Clamp(timeT, 0, 1);
            float curveT = forceCurve.Evaluate(timeT);
            return curveT * maxAngle * windStrength * mark;
        }
    }
}
