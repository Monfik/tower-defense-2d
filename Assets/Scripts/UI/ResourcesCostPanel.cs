using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI
{
    public class ResourcesCostPanel : BaseInitable
    {
        [Header("UI Refs")]
        [SerializeField] private Sprite increasedPrice;

        [SerializeField] private List<StockUI> stocks;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            storage.EventsStorage.CoreEvnts.OnMaterialCostChanged += UpdateCost;
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnMaterialCostChanged -= UpdateCost;
        }

        private void UpdateCost(string resourceName, float stockCost)
        {
            StockUI stockUI = stocks.Find(c => c.stockName == resourceName);
            UpdateUIStock(stockUI, stockCost);
        }

        private void UpdateUIStock(StockUI stock, float value)
        {
            stock.UpdateCost(value);
            stock.priceTendency.enabled = true;

            if (stock.currentCost > stock.lastCost)
            {
                stock.priceTendency.sprite = increasedPrice;
                stock.priceTendency.transform.rotation = Quaternion.identity;
            }
            else if (stock.currentCost == stock.lastCost)
                stock.priceTendency.enabled = false;
            else
            {
                stock.priceTendency.sprite = increasedPrice;
                stock.priceTendency.transform.rotation = Quaternion.Euler(new Vector3(180f, 0f, 0f));
            }
        }

        public void ResetLevel(MaterialCostsData costsData)
        {
            stocks.Find(c => c.stockName == "Wood").UpdateCost(costsData.wood);
            stocks.Find(c => c.stockName == "Clay").UpdateCost(costsData.clay);
            stocks.Find(c => c.stockName == "Iron").UpdateCost(costsData.iron);

            foreach (StockUI stock in stocks)
            {
                stock.priceTendency.enabled = false;
            }
        }

        public void UpdateCosts(MaterialCostsData stockCost)
        {
            stocks.Find(c => c.stockName == "Wood").UpdateCost(stockCost.wood);
            stocks.Find(c => c.stockName == "Clay").UpdateCost(stockCost.clay);
            stocks.Find(c => c.stockName == "Iron").UpdateCost(stockCost.iron);
        }
    }
}
