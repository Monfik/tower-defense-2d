using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class LevelEventFinisher : ILevelFinishable
    {
        private UnityAction<ILevelFinishable> OnFinished;
        private EventContainer events;

        public LevelEventFinisher(EventContainer events)
        {
            this.events = events;
            events.OnEventFinished += OnEventFinished;
        }

        public void StartFinish(UnityAction<ILevelFinishable> OnFinished, Level level)
        {
            this.OnFinished = OnFinished;
        }

        private void OnEventFinished(IGlobalEvent globalEvent)
        {
            CheckForEventsFinished();
        }

        private void CheckForEventsFinished()
        {
            if(events.Events.Count == 0)
            {
                OnFinished?.Invoke(this);
                return;
            }
        }

        public void UpdateFinisher()
        {
            
        }
    }
}
