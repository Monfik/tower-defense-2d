using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class TemporaryAttackableTower : AttackableTower
    {
        [SerializeField] protected float existTime;
        public float ExistTime => existTime;
        private float currentTime = 0f;
        private UnityAction CurrentAction;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            CurrentAction = null;
            builder.OnFinishedBuild += () => { CurrentAction = CalculateTempTime; };
        }

        public override void UpdateTower()
        {
            base.UpdateTower();
            CurrentAction?.Invoke();
        }

        private void CalculateTempTime()
        {
            currentTime += Time.deltaTime;
            if (currentTime >= existTime)
            {
                DestroyTower();
            }
        }

        private void DestroyTower()
        {
            CurrentAction = null;
            builder.Demolish();
        }
    }
}
