using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalEvents
{
    public interface INightSusceptible
    {
        void Affect(float value);
    }
}
