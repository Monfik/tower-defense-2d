using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelLogic
{
    [System.Serializable]
    public class LevelData
    {
        public List<WaveData> waveDatas;
        public Map map;
        public string levelName;
        public int startGold;
        public float postProcessExposureValue = 1f;
    }
}
