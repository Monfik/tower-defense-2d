using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Power;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class ElementalPowerConfigurableSpell : DisplayableFinishableSpell
    {
        [SerializeField] private MagicAttackData attackData;
        [SerializeField] private ConfigurableSpellHolder configurableHolder;
        [SerializeField] private MagicDamageTowerBuff dmgBuff;

        private List<BuffedTowerPowerPair> pairs = new List<BuffedTowerPowerPair>();

        private MagicType currentType = MagicType.Fire;

        public override void Unlock(DataStorage storage, ISpellHoldable holder)
        {
            base.Unlock(storage, holder);

            List<SpellHolderConfigurableOption> options = configurableHolder.Holder.Options;
            options[0].AddListener(() => { PickElemental(MagicType.Fire); });
            options[1].AddListener(() => { PickElemental(MagicType.Ice); });
            options[2].AddListener(() => { PickElemental(MagicType.Water); });

            options[0].Select(); //default fire
        }

        public override void UpdateSpell()
        {
            base.UpdateSpell();
        }

        private void PickElemental(MagicType type)
        {
            currentType = type;
            attackData.magicType = type;
            spellHoldable.SetupTooltipDescription(TooltipSpellDescriptionData.CreateDescription(CreateTooltipData()));
        }

        public override void StartCast()
        {
            base.StartCast();
            AddElementalToMagicTowers();
            OnSpellCasted?.Invoke(this);

            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished += OnTowerDemolished;
        }

        private void AddElementalToMagicTowers()
        {
            foreach (Tower tower in storage.ReferenceStorage.TowersController.CurrentTowers)
            {
                AttackableTower attackable = tower as AttackableTower;
                if (attackable != null)
                {
                    BaseAttackSystem attackSystem = attackable.AttackSystem;
                    MagicDamageAttackSystem magicAttackSystem = attackSystem as MagicDamageAttackSystem;

                    if (magicAttackSystem != null)
                    {
                        MagicDamageTowerBuff additionalDamageBuff = new MagicDamageTowerBuff(dmgBuff, magicAttackSystem, attackData);
                        tower.Effects.AddEffect(additionalDamageBuff);
                        pairs.Add(new BuffedTowerPowerPair(tower, magicAttackSystem, additionalDamageBuff));
                    }
                }
            }
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= OnTowerDemolished;

            foreach(BuffedTowerPowerPair pair in pairs)
            {
                pair.buff.FinishEffect();
            }

            base.FinishSpell();
        }

        private void OnTowerBuilded(Tower tower)
        {
            AttackableTower attackable = tower as AttackableTower;
            if (attackable != null)
            {
                BaseAttackSystem attackSystem = attackable.AttackSystem;
                MagicDamageAttackSystem magicAttackSystem = attackSystem as MagicDamageAttackSystem;

                if (magicAttackSystem != null)
                {
                    MagicDamageTowerBuff additionalDamageBuff = new MagicDamageTowerBuff(dmgBuff, magicAttackSystem, attackData);
                    tower.Effects.AddEffect(additionalDamageBuff);
                    pairs.Add(new BuffedTowerPowerPair(tower, magicAttackSystem, additionalDamageBuff));
                }
            }
        }

        private void OnTowerDemolished(Tower tower)
        {
            BuffedTowerPowerPair pair = pairs.Find(c => c.tower == tower);
            if (pair != null)
                pairs.Remove(pair);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();
            data.specifiedDatas.Clear();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Current selected", "<sprite=" + (int)attackData.magicType + ">"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData($"<sprite={(int)attackData.magicType}>" + attackData.magicType.ToString(), attackData.minDmg + " - " + attackData.maxDmg));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData(attackData.magicType.ToString() + " reduction", $"{attackData.magicArmorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Buff duration", $"{dmgBuff.Duration}"));

            return data;
        }
    }

    public class BuffedTowerPowerPair
    {
        public Tower tower;
        public MagicDamageAttackSystem attackSystem;
        public MagicDamageTowerBuff buff;

        public BuffedTowerPowerPair(Tower tower, MagicDamageAttackSystem attackSystem, MagicDamageTowerBuff buff)
        {
            this.tower = tower;
            this.attackSystem = attackSystem;
            this.buff = buff;
        }
    }
}
