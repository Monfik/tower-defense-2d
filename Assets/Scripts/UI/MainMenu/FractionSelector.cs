using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CoreUI
{
    public class FractionSelector : MonoBehaviour
    {
        private MainController controller;
        [SerializeField] private SceneLoader sceneLoader;

        [Header("Difficulty buttons")]
        [SerializeField]
        private FractionSelectButton fraction1;

        [SerializeField]
        private FractionSelectButton fraction2;

        [SerializeField]
        private FractionSelectButton fraction3;

        [SerializeField]
        private FractionSelectButton fraction4;

        private FractionSelectButton currentButton;

        [Space(10)]
        [SerializeField]
        private Button startButton;

        [SerializeField] private bool preselectFraction;


        public void Init(MainController controller)
        {
            this.controller = controller;

            fraction1.EnableButton(true);
            fraction2.EnableButton(true);
            fraction3.EnableButton(true);
            fraction4.EnableButton(true);

            startButton.onClick.AddListener(StartGame);

            fraction1.Button.onClick.AddListener(() => { OnButtonClicked(fraction1); });
            fraction2.Button.onClick.AddListener(() => { OnButtonClicked(fraction2); });
            fraction3.Button.onClick.AddListener(() => { OnButtonClicked(fraction3); });
            fraction4.Button.onClick.AddListener(() => { OnButtonClicked(fraction4); });

            if (preselectFraction)
                MarkFraction();
        }

        private void MarkFraction()
        {
            List<FractionSelectButton> fractionButtons = new List<FractionSelectButton>() { fraction1, fraction2, fraction3, fraction4 };
            FractionSelectButton currentSelectedFractionButton = fractionButtons.Find(c => c.Fraction == controller.Storage.ReferenceStorage.Settings.fraction);
            if (currentSelectedFractionButton != null)
                OnButtonClicked(currentSelectedFractionButton);
        }

        private void OnButtonClicked(FractionSelectButton button)
        {
            currentButton?.EnableButton(true);
            currentButton = button;
            currentButton?.EnableButton(false);

            controller.Storage.ReferenceStorage.Settings.fraction = button.Fraction;
            startButton.interactable = true;
        }

        public void Deinit()
        {
            startButton.onClick.RemoveAllListeners();
            fraction1.Button.onClick.RemoveAllListeners();
            fraction2.Button.onClick.RemoveAllListeners();
            fraction3.Button.onClick.RemoveAllListeners();
            fraction4.Button.onClick.RemoveAllListeners();
            startButton.interactable = false;
            currentButton?.EnableButton(false);
        }

        private void StartGame()
        {
            startButton.interactable = false;
            currentButton?.EnableButton(false);

            UnityAction startGame = () => { controller.Storage.EventsStorage.CoreEvnts.CallOnMainGameStateEnter(); };
            sceneLoader.LoadByEvent(startGame);
        }
    }
}
