using Data;
using DG.Tweening;
using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Creatures
{
    [System.Serializable]
    public class TreeMonsterSpawner
    {
        private float lastSpawnDistance;
        private float nextSpawnDistance;
        public float NextSpawnDistance => nextSpawnDistance;

        [SerializeField] private float pathSpawnInterval = 100f;
        public float PathSpawnInterval => pathSpawnInterval;
        [SerializeField] [Range(0f, 1f)] private float pathSpawnIntervalDiffer = 0.3f;
        public float PathSpawnIntervalDiffer => pathSpawnInterval;

        public int spawnedTimes = 0;

        [SerializeField] private int minSpawnEntitysCount = 3;
        [SerializeField] private int maxSpawnEntitysCount = 4;
        [SerializeField] private float maxSpawnDistanceDelta = 20f;
        [SerializeField] private float spawningDuration = 1f;

        [SerializeField] private ParticleSystemRenderer spawnParticle;

        [SerializeField] private MonsterData prefabMonsterData;

        [Header("Prefabs and refs")]
        [SerializeField] private Monster monsterPrefab;
        [SerializeField] private Animator spawnPointAnimation;

        [SerializeField] private List<Monster> spawnedMonsters;
        public List<Monster> SpawnedMonsters => spawnedMonsters;

        private List<SpawningMonsterHandler> spawningHandlers;
        private UnityAction OnSpawnFinished;
        private DataStorage storage;
        private Monster owner;

        public void Init(DataStorage storage, Monster owner)
        {
            this.storage = storage;
            this.owner = owner;
            owner.OnMonsterDied += (m) => CloseDoTweenCalls();
            storage.EventsStorage.CoreEvnts.OnMonsterDied += OnMonsterNotReached;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath += OnMonsterNotReached;

            float randomIntervalDiff = Random.Range(-pathSpawnIntervalDiffer, pathSpawnIntervalDiffer);
            nextSpawnDistance = (spawnedTimes + 1) * pathSpawnInterval * (1 + randomIntervalDiff) + (pathSpawnInterval * 0.4f);
        }

        private void CloseDoTweenCalls()
        {
            DOTween.Kill(10002, true);
        }

        public bool CanSpawn(float distance)
        {
            return distance >= nextSpawnDistance;
        }

        public void SpawnNear(TreeBossBehaviour boss, UnityAction OnSpawnFinished)
        {
            spawningHandlers = new List<SpawningMonsterHandler>();
            this.OnSpawnFinished = OnSpawnFinished;
            Wave bossWave = storage.ReferenceStorage.WaveController.FindWaveByMonster(boss);
            int randomSpawnCount = Random.Range(minSpawnEntitysCount, maxSpawnEntitysCount);
            for (int i = 0; i < randomSpawnCount; i++)
            {
                float randomDistance = Random.Range(-maxSpawnDistanceDelta, maxSpawnDistanceDelta);
                float newTravelDistance = boss.DistanceTravelled + randomDistance;

                SpawnEnemie(monsterPrefab, boss.Path, newTravelDistance, bossWave, prefabMonsterData);
            }

            spawnedTimes++;
            float randomIntervalDiff = Random.Range(-pathSpawnIntervalDiffer, pathSpawnIntervalDiffer);
            nextSpawnDistance = (spawnedTimes + 1) * pathSpawnInterval * (1 + randomIntervalDiff);
        }

        private void SpawnEnemie(Monster prefab, VertexPath path, float distance, Wave wave, MonsterData monsterData)
        {
            MonsterData data = new MonsterData(monsterData);
            Monster spawned = storage.ReferenceStorage.WaveSpawner.SpawnMonster(prefab, path, data, wave.transform, false, false);
            spawned.gameObject.layer = 0; //DEFAULT
            Vector3 position = path.GetPointAtDistance(distance);
            spawned.transform.position = position;
            spawned.MakeTransparable(0f);
            spawned.SetSpeed(0f);
            data.hp = Mathf.RoundToInt(data.hp * Values.DifiicultyValues.GetHpMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            ParticleSystemRenderer spawningParticle = GameObject.Instantiate(spawnParticle, spawned.transform);
            spawningParticle.gameObject.SetActive(true);
            spawningParticle.transform.localPosition = spawned.Collider.bounds.center;
            Animator spawnPointAnimator = GameObject.Instantiate(spawnPointAnimation, spawned.transform);
            spawnPointAnimator.gameObject.SetActive(true);
            spawnPointAnimator.transform.localPosition = Vector3.zero;
            float spawnDurationAnimation = spawnPointAnimator.GetCurrentAnimatorStateInfo(0).length;
            float animSpeed = spawnDurationAnimation / spawningDuration;
            spawnPointAnimator.speed = animSpeed;
            UnityAction<SpawningMonsterHandler> SpawningFinished = OnSpawningHandlerFinished;
            SpawningFinished += (handler) => GameObject.Destroy(spawnPointAnimator.gameObject);
            SpawningMonsterHandler spawnHandler = new SpawningMonsterHandler(spawned, data, path, distance, storage, spawningParticle, SpawningFinished);
            spawnHandler.MakeSpawnEffect(spawningDuration);
            spawningHandlers.Add(spawnHandler);
        }

        private void OnSpawningHandlerFinished(SpawningMonsterHandler handler)
        {
            if (owner.Dead)
                return;

            spawnedMonsters.Add(handler.monster);
            spawningHandlers.Remove(handler);
            if (spawningHandlers.Count == 0)
                OnSpawnFinished?.Invoke();
        }

        private void OnMonsterNotReached(Monster monster)
        {
            spawnedMonsters.Remove(monster);
        }
    }

    public class SpawningMonsterHandler
    {
        public Monster monster;
        private MonsterData monsterData;
        private VertexPath path;
        private float distance;
        private DataStorage storage;
        private UnityAction<SpawningMonsterHandler> HandlerFinished;
        private ParticleSystemRenderer spawnParticle;

        public SpawningMonsterHandler(Monster monster, MonsterData monsterData, VertexPath path, float distance, DataStorage storage, ParticleSystemRenderer spawnParticle, UnityAction<SpawningMonsterHandler> HandlerFinished)
        {
            this.monster = monster;
            this.monsterData = monsterData;
            this.path = path;
            this.distance = distance;
            this.storage = storage;
            this.HandlerFinished = HandlerFinished;
            this.spawnParticle = spawnParticle;
        }

        public void MakeSpawnEffect(float duration)
        {
            DOVirtual.Float(0f, 1f, duration, OnAlphaChanged).OnComplete(InitializeMonster).SetId(10002);
        }

        public void OnAlphaChanged(float alpha)
        {
            monster.MakeTransparable(alpha);
            Color currentColor = spawnParticle.material.color;
            alpha = Mathf.Sin((Mathf.PI / 180) * Mathf.Lerp(0f, 180f, alpha));
            currentColor.a = alpha;
            spawnParticle.material.color = currentColor;
        }

        private void InitializeMonster()
        {
            OnAlphaChanged(1f);
            GameObject.Destroy(spawnParticle.gameObject);
            monster.Init(storage, path, new MonsterData(monsterData), distance);
            monster.gameObject.layer = Values.LayerMasks.MonsterLayer;
            storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(monster);
            HandlerFinished?.Invoke(this);
            GameObject.Destroy(spawnParticle.gameObject);
        }
    }
}

