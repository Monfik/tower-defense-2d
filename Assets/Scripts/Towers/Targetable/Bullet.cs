using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public abstract class Bullet : MonoBehaviour
    {
        protected Monster target;
        public Monster Target => target;

        protected UnityAction<Bullet> OnTargetReached;

        [SerializeField]
        new protected SpriteRenderer renderer;

        [SerializeField]
        protected Animator animator;
        public Animator Animator => animator;

        public virtual void Init(Monster target, string jsonedModelData, UnityAction<Bullet> OnTargetReached)
        {
            this.target = target;
            this.OnTargetReached = OnTargetReached;
        }

        public abstract void UpdateBullet();
    }
}
