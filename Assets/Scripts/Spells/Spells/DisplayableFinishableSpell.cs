using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class DisplayableFinishableSpell : BaseSpell
    {
        [SerializeField] protected float duration;
        public Sprite icon;
        public string spellDescription;
        protected UIUpdatableIcon updatableIcon;

        private float currentTime;

        public override void StartCast()
        {
            base.StartCast();
            updatableIcon = storage.ReferenceStorage.UIIconsManager.CreateIcon(icon, spellDescription);
            currentTime = 0f;
        }

        public override void UpdateSpell()
        {
            currentTime += Time.deltaTime;
            float t = currentTime / duration;
            updatableIcon.UpdateIcon(t);

            if(t >= 1f)
            {
                FinishSpell();
            }
        }

        public override void FinishSpell()
        {
            storage.ReferenceStorage.UIIconsManager.ReturnIcon(updatableIcon);
            base.FinishSpell();
        }
    }
}
