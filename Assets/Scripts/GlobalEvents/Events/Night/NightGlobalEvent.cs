using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using TowerDefense.Enviroment;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.Universal;

namespace TowerDefense.GlobalEvents
{
    public class NightGlobalEvent : BaseLevelEvent
    {
        private BrightnessController brightness;

        [Header("Night values")]
        [SerializeField] private NightData nightData;
        
        private BaseNightPhase currentPhase;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnEventFinished)
        {
            base.BeginEvent(storage, OnEventFinished);
            brightness = storage.ReferenceStorage.Brightness;
            currentPhase = new NightDarkeningPhase(this, nightData, brightness);
        }

        public override void UpdateEvent()
        {
            base.UpdateEvent();
            currentPhase?.Update();
        }

        public void SetMiddlePhase()
        {
            currentPhase = new NightMiddlePhase(this, nightData);
        }

        public override void StartToFinishEvent()
        {
            base.StartToFinishEvent();
            currentPhase = new NightBrighteningPhase(this, nightData, brightness);
        }
    }

    public abstract class BaseNightPhase
    {
        protected NightGlobalEvent nightEvent;
        protected NightData nightData;

        public BaseNightPhase(NightGlobalEvent nightEvent, NightData nightData)
        {
            this.nightEvent = nightEvent;
            this.nightData = nightData;
        }

        public abstract void Update();
    }

    public class NightDarkeningPhase : BaseNightPhase
    {
        protected BrightnessController adjustments;
        private float currentTime = 0f;
        private float exposureLastValue;

        public NightDarkeningPhase(NightGlobalEvent nightEvent, NightData nightData, BrightnessController adjustments) 
            : base(nightEvent, nightData)
        {
            this.adjustments = adjustments;
            exposureLastValue = 0f;
        }

        public override void Update()
        {
            currentTime += Time.deltaTime;
            float timeT = currentTime / nightData.nightAdjustmentDuration;
            timeT = Mathf.Clamp(timeT, 0f, 1f);
            float value = Mathf.Lerp(nightData.minNightValue, nightData.maxNightValue, timeT);
            float valueDiff = exposureLastValue - value;
            exposureLastValue = value;
            adjustments.AddValue(valueDiff);

            if(timeT >= 1f)
            {
                nightEvent.SetMiddlePhase();
            }
        }
    }

    public class NightMiddlePhase : BaseNightPhase
    {
        public NightMiddlePhase(NightGlobalEvent nightEvent, NightData nightData)
            : base(nightEvent, nightData)
        {
            
        }

        public override void Update()
        {
            
        }
    }

    public class NightBrighteningPhase : BaseNightPhase
    {
        protected BrightnessController adjustments;
        private float currentTime = 0f;
        private float exposureLastValue;

        public NightBrighteningPhase(NightGlobalEvent nightEvent, NightData nightData, BrightnessController adjustments)
            : base(nightEvent, nightData)
        {
            this.adjustments = adjustments;
            exposureLastValue = 0f;
        }

        public override void Update()
        {
            currentTime += Time.deltaTime;
            float timeT = currentTime / nightData.nightAdjustmentDuration;
            timeT = Mathf.Clamp(timeT, 0f, 1f);
            float value = Mathf.Lerp(nightData.minNightValue, nightData.maxNightValue, timeT);
            float valueDiff = value - exposureLastValue;
            exposureLastValue = value;
            adjustments.AddValue(valueDiff);

            if (timeT >= 1f)
            {
                nightEvent.FinishEvent();
            }
        }
    }

    [System.Serializable]
    public class NightData
    {
        public float minNightValue;
        public float maxNightValue;
        public float nightAdjustmentDuration;
        public float currentNightValue = 0f;
        public float defaultExposureValue = 1f;
    }
}
