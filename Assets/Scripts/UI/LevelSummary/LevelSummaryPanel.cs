using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace LevelLogic
{
    public class LevelSummaryPanel : BaseInitable, ILevelFinishable
    {
        [SerializeField] private RectTransform rect;
        [SerializeField] private TMP_Text title;

        [Space]
        [SerializeField] private float waitTimeAfterDisplay = 1f;

        [Space]
        [Header("Panel sizes data")]
        [SerializeField] private Vector2 biggerSize;
        [SerializeField] private Vector2 lowerSize;
        [SerializeField] private float duration;
        [SerializeField] private Ease ease;

        private UnityAction<ILevelFinishable> OnFinished;

        public void ShowPanel(LevelFinisher finisher)
        {
            finisher.AddFinisher(this);
        }

        #region ILevelFinishable Implementation
        public void StartFinish(UnityAction<ILevelFinishable> OnFinished, Level level)
        {
            gameObject.SetActive(true);
            this.OnFinished = OnFinished;
            AnimatePanel(level.LevelData.levelName);
        }

        public void UpdateFinisher()
        {
            
        }

        private void CallFinish()
        {
            OnFinished?.Invoke(this);
        }
        #endregion

        private void AnimatePanel(string levelName)
        {
            rect.DOSizeDelta(biggerSize, duration).SetEase(ease);
            DOVirtual.DelayedCall(duration + waitTimeAfterDisplay, CallFinish, false);
            title.text = levelName + " completed";
        }

        [ContextMenu("Test fire")]
        private void TestFire()
        {
            AnimatePanel("xxx");
        }

        [ContextMenu("SetupDefault")]
        public void SetupDefault()
        {
            gameObject.SetActive(false);
            rect.sizeDelta = lowerSize;
        }
    }
}
