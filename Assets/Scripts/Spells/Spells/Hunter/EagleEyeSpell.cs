using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class EagleEyeSpell : BaseActiveSpell
    {
        [SerializeField] private TowerRangeBuff rangeBuff; 
        private List<TowerBuffPair> buffedTowers = new List<TowerBuffPair>();

        private UnityAction CurrentAction;

        public override void Init(DataStorage storage, ISpellHoldable holder, UnityAction<BaseSpell> OnSpellStartCasting, UnityAction<BaseSpell> OnSpellCasted, UnityAction<BaseSpell> OnSpellFinished, UnityAction<BaseSpell> OnSpellCanceled)
        {
            base.Init(storage, holder, OnSpellStartCasting, OnSpellCasted, OnSpellFinished, OnSpellCanceled);
            CurrentAction = StartSpell;
        }

        public override void StartCast()
        {
            CurrentAction?.Invoke();
            
        }

        private void OnTowerBuilded(Tower tower)
        {
            TowerRangeBuff buff = new TowerRangeBuff(rangeBuff);
            tower.Effects.AddEffect(buff);
            buffedTowers.Add(new TowerBuffPair(tower, buff));
        }

        private void OnTowerDemolished(Tower tower)
        {
            TowerBuffPair buffPair = buffedTowers.Find(c => c.tower == tower);
            if(buffPair != null)
            {
                buffedTowers.Remove(buffPair);
            }
        }

        private void StartSpell()
        {
            base.StartCast();
            BuffTowers();
            OnSpellCasted?.Invoke(this);
            CurrentAction = FinishSpell;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished += OnTowerDemolished;
        }

        private void BuffTowers()
        {
            foreach(Tower tower in storage.ReferenceStorage.TowersController.CurrentTowers)
            {
                TowerRangeBuff buff = new TowerRangeBuff(rangeBuff);
                tower.Effects.AddEffect(buff);
                buffedTowers.Add(new TowerBuffPair(tower, buff));
            }
        }

        private void DebuffTowers()
        {
            foreach (TowerBuffPair pair in buffedTowers)
            {
                pair.buff.FinishEffect();
            }

            buffedTowers.Clear();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Range multiplier", (1 + rangeBuff.rangeMultiplier).ToString() + "x"));
            return data;
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= OnTowerDemolished;
            DebuffTowers();
            CurrentAction = StartSpell;
            base.FinishSpell();
        }
    }

    public class TowerBuffPair
    {
        public Tower tower;
        public TowerRangeBuff buff;

        public TowerBuffPair(Tower tower, TowerRangeBuff buff)
        {
            this.tower = tower;
            this.buff = buff;
        }
    }
}
