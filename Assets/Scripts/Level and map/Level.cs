using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.Universal;
using Utils;

namespace LevelLogic
{
    public class Level : MonoBehaviour
    {
        [SerializeField]
        private int levelId;
        public int LevelId => levelId;

        [SerializeField]
        private Map map;
        public Map Map => map;

        [SerializeField] private Castle castle;
        public Castle Castle => castle;

        [SerializeField]
        private LevelData levelData;
        public LevelData LevelData => levelData;

        [SerializeField] private List<BaseLevelModificator> levelModificators;

        [SerializeField] private List<RespawnPlaceAnimationPair> respawnPlacesAnimations;
        public List<RespawnPlaceAnimationPair> RespawnPlacesAnimations => respawnPlacesAnimations;

        private DataStorage storage;
        public DataStorage Storage => storage;

        private UnityAction<Level> OnLevelFinished;
        [SerializeField] private AudioClip musicClip;
        public AudioClip MusicClip => musicClip;
        //tools
        [SerializeField] private List<LevelData> waveDatasCache;

        public void Init(DataStorage storage, UnityAction<Level> OnLevelFinished)
        {
            this.storage = storage;
            this.OnLevelFinished = OnLevelFinished;
            map.Init(storage);
            castle.Init(storage.EventsStorage);
            if(storage.ReferenceStorage.PostprocessVolume.profile.TryGet<ColorAdjustments>(out ColorAdjustments colorAdjustment))
                colorAdjustment.postExposure.value = levelData.postProcessExposureValue;

            
            storage.EventsStorage.CoreEvnts.OnWaveFinished += OnWaveFinished;
            storage.EventsStorage.CoreEvnts.CallOnLevelLoaded(levelData);
        }

        public void UpdateLevel()
        {
            map.UpdateMap();
            storage.ReferenceStorage.WaveController.UpdateController();
            storage.ReferenceStorage.WaveSpawner.UpdateSpawner(); //TODO 
        }

        public void StartLevel()
        {
            Values.waveNumber = 1;
            storage.ReferenceStorage.MusicPlayer.Play(musicClip);
            storage.EventsStorage.CoreEvnts.CallOnLevelStarted(this);
            BeginWave(levelData.waveDatas[Values.waveNumber - 1]);
            
            foreach (BaseLevelModificator levelModificator in levelModificators)
            {
                levelModificator.Init(storage);
                levelModificator.StartLevel(this);
            }
        }

        public void BeginWave(WaveData waveData)
        {
            WaveData data = new WaveData(waveData);
            storage.ReferenceStorage.WaveController.LoadWave(data, Values.waveNumber);
        }

        private void OnWaveFinished(Wave wave)
        {
            Values.waveNumber++;
            if (wave.WaveIndex >= levelData.waveDatas.Count)
            {
                FinishLevel();
                return;
            }

            BeginWave(levelData.waveDatas[Values.waveNumber - 1]);
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnWaveFinished -= OnWaveFinished;

            foreach (BaseLevelModificator levelModificator in levelModificators)
            {
                levelModificator.FinishLevel(this);
            }

            map.Deinit();
        }

        public void FinishLevel()
        {
            foreach (BaseLevelModificator levelModificator in levelModificators)
            {
                levelModificator.FinishLevel(this);
            }

            OnLevelFinished?.Invoke(this);
        }
    }
}
