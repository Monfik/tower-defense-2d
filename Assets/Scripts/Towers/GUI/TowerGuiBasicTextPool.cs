using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace TowerDefense.Towers
{
    public class TowerGuiBasicTextPool : BasePool<TowerBasicGUIText>
    {
        public override TowerBasicGUIText GetPoolObject()
        {
            var ob = base.GetPoolObject();
            ob.gameObject.SetActive(true);
            return ob;
        }

        public override void ReturnToPool(TowerBasicGUIText ob)
        {
            ob.gameObject.SetActive(false);
            base.ReturnToPool(ob);
        }
    }
}
