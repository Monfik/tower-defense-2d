using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilsExtension : MonoBehaviour
{
    public static void MakeSpriteRendererTransparable(float alpha, SpriteRenderer renderer)
    {
        Color color = renderer.color;
        color.a = alpha;
        renderer.color = color;
    }
}
