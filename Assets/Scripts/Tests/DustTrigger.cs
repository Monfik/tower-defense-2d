using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DustTrigger : MonoBehaviour
{
    public UnityAction<Collider2D> Enter;
    public UnityAction<Collider2D> Exit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enter?.Invoke(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Exit?.Invoke(collision);
    }
}
