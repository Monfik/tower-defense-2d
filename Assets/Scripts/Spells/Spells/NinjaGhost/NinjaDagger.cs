using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class NinjaDagger : MonoBehaviour
    {
        [SerializeField] new private CapsuleCollider2D collider;
        public CapsuleCollider2D Collider => this.collider;

        [SerializeField] new private SpriteRenderer renderer;
        public SpriteRenderer Renderer => renderer;

        private List<IDestructable> takenDmgObjects = new List<IDestructable>();

        private UnityAction<IDestructable> OnDamageTakableHitted;

        public void Init(UnityAction<IDestructable> OnDamageTakableHitted)
        {
            this.OnDamageTakableHitted = OnDamageTakableHitted;
            takenDmgObjects = new List<IDestructable>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            IDestructable dmgTakable = collision.gameObject.GetComponent<IDestructable>();
            if(dmgTakable != null)
            {
                if(!takenDmgObjects.Exists(c => c == dmgTakable))
                {
                    OnDamageTakableHitted?.Invoke(dmgTakable);
                    takenDmgObjects.Add(dmgTakable);
                }
            }
        }
    }
}