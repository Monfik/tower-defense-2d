using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class SpriteAnimation : MonoBehaviour
    {
        [SerializeField] private string animationName;

        [SerializeField]
        private List<Sprite> sprites;

        [SerializeField]
        new private SpriteRenderer renderer;
        public SpriteRenderer Renderer => this.renderer;

        [SerializeField]
        private Sprite currentSprite;

        private UnityAction<SpriteAnimation> OnAnimationFinished;
        private float currentTime = 0f;
        public float speed = 10f;
        private int spritesCount;
        private int currentIndex = 0;
        private int counter = 0;
        public bool process = false;

        public void PlayAnimation(UnityAction<SpriteAnimation> OnAnimationFinished)
        {
            this.OnAnimationFinished = OnAnimationFinished;
            counter = 0;
            currentIndex = 0;

            spritesCount = sprites.Count;

            process = true;
        }

        public void PlayAnimation(UnityAction<SpriteAnimation> OnAnimationFinished, float speed)
        {
            this.OnAnimationFinished = OnAnimationFinished;
            this.speed = speed;
            counter = 0;
            currentIndex = 0;

            spritesCount = sprites.Count;

            process = true;
        }

        public void Update()
        {
            if (!process)
                return;

            currentTime += Time.deltaTime;

            if(currentTime > 1f/speed)
            {
                ChangeSprite();
                currentTime = 0f;

                if(counter >= spritesCount)
                {
                    FinishAnimation();
                }
            }
        }

        private void ChangeSprite()
        {
            renderer.sprite = sprites[currentIndex];
            currentSprite = sprites[currentIndex];

            currentIndex++;

            counter++;
        }

        private void FinishAnimation()
        {
            OnAnimationFinished?.Invoke(this);
            process = false;
        }
    }
}
