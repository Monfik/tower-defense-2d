using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Blueprints
{
    public class BlueprintController : BaseInitable
    {
        [SerializeField] private List<Blueprint> blueprints;
        [SerializeField] private Blueprint currentBlueprint;

        [Header("DEV")]
        [SerializeField] private bool autoReloading;
        public List<int> locationIndexes;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            if(blueprints.Count == 0)
            {
                Debug.LogError("Blueprints count is 0 !");
                return;
            }
        }

        private void AssignBlueprint(Blueprint blueprint)
        {
            currentBlueprint?.Deinit();
            currentBlueprint = blueprint;
            currentBlueprint.Init(storage);
            currentBlueprint.CheckForUnlockCheckpoints(Values.levelNumber);
        }

        public void ShowBlueprint()
        {
            if (currentBlueprint == null)
                AssignBlueprint(blueprints[0]);

            currentBlueprint.Display();

            if(autoReloading)
            {
                if(locationIndexes.Count > 0)
                {
                    currentBlueprint.PickRandom(locationIndexes[0]);
                    locationIndexes.RemoveAt(0);
                }
            }
        }

        public void OnLevelFinished(Level level)
        {
            currentBlueprint.FinishLevel(level);          
        }

        public void HideBlueprint()
        {
            currentBlueprint.Hide();
        }
        
        public void ResetBlueprints()
        {
            currentBlueprint = null;

            foreach(Blueprint blueprint in blueprints)
            {
                blueprint.ResetBlueprint();
            }
        }

        public void Deinit()
        {
            ResetBlueprints();
        }
    }
}
