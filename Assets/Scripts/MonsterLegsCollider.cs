using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class MonsterLegsCollider : MonoBehaviour
    {
        [SerializeField] private BoxCollider2D colider;
        public BoxCollider2D Collider => colider;

        [SerializeField]
        private Monster owner;
        public Monster Owner => owner;
        
        public void SetupOwner(Monster owner)
        {
            this.owner = owner;
        }
    }
}
