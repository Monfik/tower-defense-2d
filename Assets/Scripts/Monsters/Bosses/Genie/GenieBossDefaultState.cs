using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class GenieBossDefaultState : BaseBossState<GenieBossBehaviour>
    {
        [SerializeField] private List<BaseBossState<GenieBossBehaviour>> availableStates;
        public List<BaseBossState<GenieBossBehaviour>> AvailableStates => availableStates;

        private Timer checkStatesTimer;

        public override void Init(GenieBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Walk");

            checkStatesTimer = gameObject.AddComponent<Timer>();
            checkStatesTimer.Setup(0.3f, CheckStatesAvailable, false);
        }

        public override void Deinit()
        {
            Destroy(checkStatesTimer);
        }

        public override void UpdateState()
        {
            boss.UpdateMovement();
        }

        private void CheckStatesAvailable()
        {
            BaseBossState<GenieBossBehaviour> newState = availableStates.Find(c => c.IsReadyForWork());
            if (newState != null)
            {
                boss.ChangeState(newState);
            }
        }
    }
}
