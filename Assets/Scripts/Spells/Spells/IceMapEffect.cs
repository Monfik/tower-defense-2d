using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using Utils;

namespace TowerDefense.Effects
{
    public class IceMapEffect : MapEffect
    {
        private float currentTimeMultiplier = 0f;
        private Vector2 position;
       
        [SerializeField] private MonsterBuffEffect slowEffectPrefab;
        [SerializeField] private Animator animator;
        [SerializeField] private BoxCollider2D colider;
        private IceEffectData iceData;
        private List<SlowEffectMonsterPair> pairs = new List<SlowEffectMonsterPair>();

        public void InitIce(IceEffectData iceData, Vector2 position)
        {
            this.iceData = iceData;
            this.position = position;
            duration = iceData.iceDuration;
            transform.position = position;
            animator.enabled = true;
            gameObject.SetActive(true);
        }

        public override void UpdateEffect()
        {
            currentTime += Time.deltaTime * currentTimeMultiplier;
            if (currentTime >= duration)
            {
                animator.speed = 1f;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            MonsterLegsCollider monsterLegs = collision.gameObject.GetComponent<MonsterLegsCollider>();

            if(monsterLegs != null)
            {
                AddSlow(monsterLegs.Owner);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            MonsterLegsCollider monsterLegs = collision.gameObject.GetComponent<MonsterLegsCollider>();

            if (monsterLegs != null)
            {
                RemoveSlow(monsterLegs.Owner);
            }
        }

        private void RemoveSlow(Monster monster)
        {
            SlowEffectMonsterPair pair = pairs.Find(c => c.monster == monster);
            if(pair != null)
            {
                MonsterBuffEffect slow = pair.slow;
                monster.Effects.FinishEffect(slow);
                pairs.Remove(pair);
            }
        }

        private void AddSlow(Monster monster)
        {
            //Check For Existing Poison, if exists reset its timer
            SlowEffectMonsterPair pair = pairs.Find(c => c.monster == monster);
            if (pair != null) //exists
            {
                pair.slow.ResetTimer();
                return;
            }
            else //not exists
            {
                MonsterBuffEffect slow = Instantiate(slowEffectPrefab);
                slow.BuffData.speed = -iceData.slowData.slowValue;
                slow.duration = iceData.slowData.slowDuration;
                monster.Effects.AddEffect(slow);
                pairs.Add(new SlowEffectMonsterPair(slow, monster));
            }
        }

        private void IceAppeared()
        {
            currentTimeMultiplier = 1f;
            animator.speed = 0f;
            colider.enabled = true;
        }

        private void Melting()
        {
            currentTimeMultiplier = 0f;
            animator.speed = 1f;
            colider.enabled = false;
        }

        private void IceDissepeared()
        {
            animator.enabled = false;
            FinishEffect();
        }
    }

    [System.Serializable]
    public class IceEffectData
    {
        public float iceDuration;
        public SlowEffectData slowData;
    }

    [System.Serializable]
    public class SlowEffectData
    {
        public int slowValue;
        public int slowDuration;
    }

    public class SlowEffectMonsterPair
    {
        public MonsterBuffEffect slow;
        public Monster monster;

        public SlowEffectMonsterPair(MonsterBuffEffect slow, Monster monster)
        {
            this.slow = slow;
            this.monster = monster;
        }
    }
}
