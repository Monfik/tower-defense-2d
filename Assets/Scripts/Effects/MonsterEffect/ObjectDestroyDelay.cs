using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Effects
{
    public class ObjectDestroyDelay : MonoBehaviour
    {
        [SerializeField] private GameObject go;
        [SerializeField] private float delay;

        private float currentTime = 0f;
        //private void OnEnable()
        //{
        //    DOVirtual.DelayedCall(delay, DestroyItself, false);
        //}

        private void DestroyItself()
        {
            Destroy(go);
        }

        private void Update()
        {
            currentTime += Time.deltaTime;
            if (currentTime >= delay)
                DestroyItself();
        }
    }
}
