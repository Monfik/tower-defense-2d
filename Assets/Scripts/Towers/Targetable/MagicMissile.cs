using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Towers
{
    public class MagicMissile : HitAnimableBullet
    {
        private MagicBulletData magicMissileData;

        [SerializeField]
        private List<BaseMonsterEffect> magicEffects;
        public List<BaseMonsterEffect> MagicEffects => magicEffects;

        public override void Init(Monster target, string jsonedModelData, UnityAction<Bullet> OnTargetReached)
        {
            base.Init(target, jsonedModelData, OnTargetReached);
            MagicBulletData missileData = JsonUtility.FromJson<MagicBulletData>(jsonedModelData);
            magicMissileData = missileData;
        }

        public override void DealDamage(Monster monster)
        {
            int summaryDamage = 0;

            foreach(MagicDamageData magicBullet in magicMissileData.magicDamages)
            {
                MonsterStaticsData magicStatics = monster.Data.GetDataByType(magicBullet.magicType);

                int magicResitance = magicStatics.currentValue;
                magicResitance -= magicStatics.currentValue;
                magicResitance = Mathf.Clamp(magicResitance, magicStatics.minValue, magicStatics.maxValue);
                int randomDmg = (int)Random.Range(magicBullet.minDamage.data.currentValue, magicBullet.maxDamage.data.currentValue);
                int dmg = DamageCalculator.CalculateDamage(randomDmg, magicResitance);
                summaryDamage += dmg;
            }

            foreach (BaseMonsterEffect effect in magicEffects)
            {
                monster.Effects.AddEffect(effect);
            }

            monster.TakeDamage(summaryDamage, DamageType.Magic);       
        }
    }
}
