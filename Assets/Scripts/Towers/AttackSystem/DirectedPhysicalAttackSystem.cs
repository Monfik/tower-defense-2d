using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class DirectedPhysicalAttackSystem : BaseAttackSystem
    {
        [Space(15)]
        [SerializeField] private DirectBullet bulletPrefab;

        [SerializeField] private List<DirectBullet> bullets;

        [Header("Data")]
        public PhysicsBulletData bulletData;

        [SerializeField]
        private DirectionalAttackLine directionalAttackLine;
        public DirectionalAttackLine DirectionalAttackLine => directionalAttackLine;

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            bulletData.Reset();
        }

        public override void Attack()
        {
            
        }

        private Vector2 GetBulletTargetPosition(Vector2 attackDirection, Vector2 from) //Should be 0-1, 0-1
        {
            Vector2 position = from + (attackableTower.range.data.currentValue * attackDirection);
            return position;
        }

        public override void UpdateAttackSystem()
        {
            base.UpdateAttackSystem();

            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].UpdateBullet();
            }
        }

        private void OnBulletHitted(DirectBullet bullet)
        {
            bullets.Remove(bullet);
            GameObject.Destroy(bullet.gameObject);
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            return null;
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            
        }

        public override void OnTowerDemolished()
        {
            
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            bulletData.minDamage.data.SetupMultiplier(multiplier);
            bulletData.maxDamage.data.SetupMultiplier(multiplier);

            for (int i = 0; i < bulletData.minDamage.upgrades.Count; i++)
            {
                TowerStaticsData upgradeMinData = bulletData.minDamage.upgrades[i];
                upgradeMinData.SetupMultiplier(multiplier);

                TowerStaticsData upgradeMaxData = bulletData.maxDamage.upgrades[i];
                upgradeMaxData.SetupMultiplier(multiplier);
            }

            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.minDamage);
            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.maxDamage);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            return null; //here dont need until create new tower which uses this attack system and is in shop

        }
    }
}
