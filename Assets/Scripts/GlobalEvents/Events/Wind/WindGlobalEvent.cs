using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class WindGlobalEvent : BaseLevelEvent
    {
        [Space]
        [SerializeField] private float adjustmentDuration;
        [SerializeField] private WindForceData windData;
        private float lastValue;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnUpdatableFinished)
        {
            base.BeginEvent(storage, OnUpdatableFinished);
            DOVirtual.Float(0f, windData.strength, adjustmentDuration, WindChangeStrength);
        }

        private void WindChangeStrength(float windValue)
        {
            if(windValue != 0f)
                storage.ReferenceStorage.WindController.AddValue(-lastValue);

            storage.ReferenceStorage.WindController.AddValue(windValue);
            lastValue = windValue;
        }

        public override void StartToFinishEvent()
        {
            base.StartToFinishEvent();
            lastValue = 0f;
            DOVirtual.Float(0f, -windData.strength, adjustmentDuration, WindChangeStrength).OnComplete(FinishEvent);
        }

        public override void FinishEvent()
        {
            base.FinishEvent();
        }
    }

}