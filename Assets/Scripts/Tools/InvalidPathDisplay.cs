using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvalidPathDisplay : MonoBehaviour
{
    [ContextMenu("Show invalid path")]
    public void ShowInvalidPath()
    {
        Road [] roads = GameObject.FindObjectsOfType<Road>();

        foreach(Road road in roads)
        {
            if(road.paths.Count == 0)
            {
                SpriteRenderer roadSprite = road.GetComponent<SpriteRenderer>();
                if(roadSprite != null)
                {
                    roadSprite.color = Color.red;
                }
            }
        }
    }
}
