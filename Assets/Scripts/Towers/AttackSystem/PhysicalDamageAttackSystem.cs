using Data;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class PhysicalDamageAttackSystem : BaseAttackSystem
    {
        [Space(15)]
        [SerializeField] private PhysicalBullet bulletPrefab;

        [SerializeField] private List<PhysicalBullet> bullets;

        [Header("Data")]
        public PhysicsBulletData bulletData;

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            bulletData.Reset();
        }

        public override void Attack()
        {
            //foreach(TowerAttackablePart part in attackableParts)
            //{
            //    part.Attack();
            //}

            foreach (AttackableTowerEntity attackableEntitie in attackableTower.GraphicsSystem.GraphicsData.attackEntities)
            {
                PhysicalBullet spawnedBullet = GameObject.Instantiate(bulletPrefab);
                spawnedBullet.gameObject.SetActive(true);
                spawnedBullet.transform.position = attackableEntitie.AttackPoint.position;
                spawnedBullet.Init(target, bulletData, OnBulletHitted);
                attackableEntitie.PlayAttackAnimation();
                storage.ReferenceStorage.WindController.AddSusceptible(spawnedBullet);
                bullets.Add(spawnedBullet);

                Vector2 diff = target.transform.position - attackableEntitie.transform.position;
                attackableEntitie.transform.rotation = Quaternion.Euler(new Vector3(0, diff.x > 0 ? 0f : 180f));
            }

            foreach (AttackableTowerEntity animabeEntity in attackableTower.GraphicsSystem.GraphicsData.attackableAnimators)
            {
                animabeEntity.PlayAttackAnimation();
            }

            DamageItself();
        }

        public override void UpdateAttackSystem()
        {
            base.UpdateAttackSystem();

            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].UpdateBullet();
            }
        }

        private void OnBulletHitted(PhysicalBullet bullet)
        {
            bullets.Remove(bullet);
            storage.ReferenceStorage.WindController.RemoveSusceptible(bullet);
            GameObject.Destroy(bullet.gameObject);
        }

        public override void Upgrade(int level)
        {
            base.Upgrade(level);
            bulletData.Upgrade(level);
        }

        protected override void OnTargetNotReachable(Monster monster)
        {
            List<PhysicalBullet> bulletsToRemove = new List<PhysicalBullet>();

            for (int i = bullets.Count - 1; i >= 0; i--)
            {
                if (bullets[i].Target == monster)
                {

                    bulletsToRemove.Add(bullets[i]);
                }
            }

            foreach (PhysicalBullet bullet in bulletsToRemove)
            {
                bullets.Remove(bullet);
                Destroy(bullet.gameObject);
            }

            base.OnTargetNotReachable(monster);
        }

        public override void OnTowerDemolished()
        {
            foreach (PhysicalBullet bullet in bullets)
            {
                Destroy(bullet.gameObject);
            }

            bullets.Clear();
        }

        #region IComposable interface implementation
        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            int attackEntities = attackableTower.GraphicsSystem.GraphicsData.attackEntities.Count;
            TowerBasicGUIText damageText = infoPanel.BasicTextes.GetElement();
            damageText.Text.text = "Physical damage: " + bulletData.minDamage.data.GetGUILabel(attackEntities) + " - " + bulletData.maxDamage.data.GetGUILabel(attackEntities);
            TowerBasicGUIText armorReduction = infoPanel.BasicTextes.GetElement();
            armorReduction.Text.text = "Armor reduction: " + bulletData.armorReduction.data.GetGUILabel();
            TowerBasicGUIText attackSpeedText = infoPanel.BasicTextes.GetElement();
            attackSpeedText.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();

            UnityAction DamageUpdated = () => { damageText.Text.text = "Physical damage: " + bulletData.minDamage.data.GetGUILabel(attackEntities) + " - " + bulletData.maxDamage.data.GetGUILabel(attackEntities); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, bulletData.minDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, bulletData.maxDamage));
            UnityAction ArmorReductionUpdated = () => { armorReduction.Text.text = "Armor reduction: " + bulletData.armorReduction.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, bulletData.armorReduction));
            UnityAction AttackSpeedUpdated = () => { attackSpeedText.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            int attackEntities = attackableTower.GraphicsSystem.GraphicsData.attackEntities.Count;
            int upgradeAttackEntities = attackableTower.GraphicsSystem.GetLevelGraphicsData(attackableTower.Builder.TowerLevel + 1).attackEntities.Count;
            TowerUpgradeGuiText damageText = infoPanel.UpgradeTextes.GetElement();
            damageText.Text.text = "Physical damage: " + bulletData.minDamage.data.GetGUILabel(attackEntities) + " - " + bulletData.maxDamage.data.GetGUILabel(attackEntities);
            damageText.UpgradeText.text = bulletData.minDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities) + " - " + bulletData.maxDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities);
            TowerUpgradeGuiText armorReductionText = infoPanel.UpgradeTextes.GetElement();
            armorReductionText.Text.text = "Armor reduction: " + bulletData.armorReduction.data.GetGUILabel();
            armorReductionText.UpgradeText.text = bulletData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText attackSpeedText = infoPanel.UpgradeTextes.GetElement();
            attackSpeedText.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
            attackSpeedText.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

            UnityAction DamageUpdated = () =>
            {
                damageText.Text.text = "Physical damage: " + bulletData.minDamage.data.GetGUILabel(attackEntities) + " - " + bulletData.maxDamage.data.GetGUILabel(attackEntities);
                damageText.UpgradeText.text = bulletData.minDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities) + " - " + bulletData.maxDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities);
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, bulletData.minDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, bulletData.maxDamage));
            UnityAction ArmorReductionUpdated = () =>
            {
                armorReductionText.Text.text = "Armor reduction: " + bulletData.armorReduction.data.GetGUILabel();
                armorReductionText.UpgradeText.text = bulletData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, bulletData.armorReduction));
            UnityAction AttackSpeedUpdated = () =>
            {
                attackSpeedText.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
                attackSpeedText.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }
        #endregion

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            int minDmgBuff = (int)Mathf.Round(bulletData.minDamage.data.coreValue * multiplier);
            bulletData.minDamage.data.BuffByValue(minDmgBuff);
            int maxDmgBuff = (int)Mathf.Round(bulletData.maxDamage.data.coreValue * multiplier);
            bulletData.maxDamage.data.BuffByValue(maxDmgBuff);

            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.minDamage);
            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.maxDamage);

            UnityAction OnDebuff = () =>
            {
                bulletData.minDamage.data.BuffByValue(-minDmgBuff);
                bulletData.maxDamage.data.BuffByValue(-maxDmgBuff);

                attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.minDamage);
                attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.maxDamage);
            };

            return OnDebuff;
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            bulletData.minDamage.data.SetupMultiplier(multiplier);
            bulletData.maxDamage.data.SetupMultiplier(multiplier);

            for (int i = 0; i < bulletData.minDamage.upgrades.Count; i++)
            {
                TowerStaticsData upgradeMinData = bulletData.minDamage.upgrades[i];
                upgradeMinData.SetupMultiplier(multiplier);

                TowerStaticsData upgradeMaxData = bulletData.maxDamage.upgrades[i];
                upgradeMaxData.SetupMultiplier(multiplier);
            }

            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.minDamage);
            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.maxDamage);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            data = base.CreateTooltipData(data);

            data.title2 = "Physical targetable damage";

            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("<sprite=5>Physical", $"{bulletData.minDamage.data.coreValue} - {bulletData.maxDamage.data.coreValue}"));
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Armor reduction", bulletData.armorReduction.data.coreValue.ToString()));

            //foreach (BaseMonsterEffect monsterEffect in bulletPrefab.Effects)
            //{
            //    data = monsterEffect.CreateTooltipData(data);
            //}

            return data;
        }
    }

    [System.Serializable]
    public class PhysicsBulletData
    {
        public UpgradableStaticsData minDamage;
        public UpgradableStaticsData maxDamage;
        public UpgradableStaticsData armorReduction;

        public void Reset()
        {
            minDamage.Reset();
            maxDamage.Reset();
            armorReduction.Reset();
        }

        public void Upgrade(int level)
        {
            minDamage.Upgrade(level);
            maxDamage.Upgrade(level);
            armorReduction.Upgrade(level);
        }
    }
}
