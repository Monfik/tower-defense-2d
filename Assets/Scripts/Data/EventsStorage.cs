﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    public class EventsStorage
    {
        private CoreEvents coreEvents = new CoreEvents();
        public CoreEvents CoreEvnts => coreEvents;

        private PlayerEvents playerEvents = new PlayerEvents();
        public PlayerEvents PlayerEv => playerEvents;

        private UIEvents uiEvents = new UIEvents();
        public UIEvents UIEvents => uiEvents;

        private SpellEvents spellEvents = new SpellEvents();
        public SpellEvents SpellEvents => spellEvents;

        private UtilEvents utilEvents = new UtilEvents();
        public UtilEvents UtilEvents => utilEvents;
    }
}
