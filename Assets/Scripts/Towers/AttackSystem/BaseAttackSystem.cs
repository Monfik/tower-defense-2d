using Data;
using GameEvents;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using TowerDefense.Tips;

namespace TowerDefense.Towers
{
    public abstract class BaseAttackSystem : MonoBehaviour, IGUIComposable, IUpgradable, ITooltipable
    {
        protected Monster target;
        protected DataStorage storage;
        protected int level;

        [SerializeField] private List<Monster.MonsterType> attackableTypes = new List<Monster.MonsterType>() { Monster.MonsterType.Fly, Monster.MonsterType.Ground };

        [SerializeField] private List<float> singleAttackDamaging;

        [SerializeField]
        protected UpgradableStaticsData attackSpeed;
        public UpgradableStaticsData AttackSpeed => attackSpeed;

        protected float lastAttackTime = 1000f;
        [SerializeField] protected AttackReducer attackReducer;

        protected AttackableTower attackableTower;

        protected UnityAction UpdateState;
        private UnityAction UpdateAttackAction;

        public abstract void Attack();
        public virtual void Upgrade(int level)
        {
            attackSpeed.Upgrade(level);
        }

        public abstract UnityAction BuffDamageByMultiplier(float multiplier);

        public virtual void Init(AttackableTower attackable, DataStorage storage)
        {
            attackableTower = attackable;
            this.storage = storage;
            attackSpeed.Reset();
            level = 1;

            attackableTower.Builder.OnStartUpgradingBasic += SwitchToIdleState;
            attackableTower.Builder.OnFinishedUpgrading += OnFinishedUpgrading;
            attackableTower.Builder.OnStartBuilding += SwitchToIdleState;
            attackableTower.Builder.OnFinishedBuild += SwitchToNormalState;
            attackableTower.Builder.OnStartDemolishing += SwitchToIdleState;
            attackableTower.Builder.OnStartFixing += SwitchToIdleState;
            attackableTower.Builder.OnFinishedFix += SwitchToNormalState;
            attackableTower.Builder.OnTowerEfficientChanged += OnTowerEfficientChanged;

            //States
            SwitchToNormalState();

            AssignEvents(storage.EventsStorage);
        }

        protected virtual void DamageItself()
        {
            attackableTower.Builder.Repairer.Damage(singleAttackDamaging[level - 1]);
        }

        protected virtual void AssignEvents(EventsStorage events)
        {
            events.CoreEvnts.OnMonsterFinishedPath += OnMonsterNotReachable;
            events.CoreEvnts.OnMonsterDied += OnMonsterNotReachable;
        }

        public virtual void UpdateAttackSystem()
        {
            lastAttackTime += Time.deltaTime;
            UpdateState?.Invoke();
        }

        protected void SearchTarget()
        {
            List<Monster> targetsInRange = new List<Monster>();

            foreach (Monster monster in storage.ReferenceStorage.WaveController.Monsters)
            {
                float distance = Vector2.Distance(attackableTower.transform.position, monster.transform.position);

                if (distance < attackableTower.RealRange && !monster.Dead && attackableTypes.Contains(monster.Type))
                {
                    targetsInRange.Add(monster);
                }
            }

            if (targetsInRange.Count > 0)
            {
                Monster foundedTarget = targetsInRange[Random.Range(0, targetsInRange.Count - 1)];
                OnFoundTarget(foundedTarget);
            }
        }

        private void OnFoundTarget(Monster monster)
        {
            target = monster;
            UpdateState = UpdateAttackTarget;
        }

        protected virtual void UpdateAttackTarget()
        {
            UpdateAttackAction?.Invoke();
        }

        protected virtual void UpdateDefaultAttack()
        {
            if (IsTargetOutOfRange())
            {
                target = null;
                UpdateState = SearchTarget;
                return;
            }

            if (lastAttackTime >= 1f / (attackSpeed.data.currentValue / 4f))
            {
                Attack();
                lastAttackTime = 0f;
            }
        }

        private void OnMonsterNotReachable(Monster monster)
        {
            OnTargetNotReachable(monster);
        }

        protected virtual void OnTargetNotReachable(Monster monster)
        {
            target = null;
            UpdateState = SearchTarget;
        }

        private bool IsTargetOutOfRange()
        {
            float distance = Vector2.Distance(attackableTower.transform.position, target.transform.position);

            return distance > attackableTower.RealRange;
        }

        public void SwitchToIdleState()
        {
            UpdateState = null;
            target = null;
            UpdateAttackAction = null;
        }

        public virtual void SwitchToNormalState()
        {
            UpdateAttackAction = UpdateDefaultAttack;
            UpdateState = SearchTarget;
            lastAttackTime = 1000f;
        }

        protected virtual void OnStartUpgrading()
        {
            SwitchToIdleState();
        }

        protected virtual void OnFinishedUpgrading(int level)
        {
            this.level = level;
            Upgrade(level);
            SwitchToNormalState();
        }

        public abstract void SetupDamageMultiplier(float multiplier);

        public abstract void ComposeBasicInfo(TowerGuiInfoPanel infoPanel);
        public abstract void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel);

        public abstract void OnTowerDemolished();
        public virtual DescriptionData CreateTooltipData(DescriptionData data)
        {
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Attack speed", attackSpeed.data.coreValue.ToString()));
            return data;
        }

        protected virtual void OnTowerEfficientChanged(float efficient)
        {
            attackReducer?.UpdateEfficient(efficient);
        }
    }

    public class GUIElementableEventPair
    {
        public UnityAction Action;
        public IGUIElementable elementable;

        public GUIElementableEventPair(UnityAction Action, IGUIElementable elementable)
        {
            this.Action = Action;
            this.elementable = elementable;
        }
    }
}