using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Feature
{
    public class InfoMarkStatic : BaseInfoMark
    {
        public void Display(string message, Vector3 startPosition, float existingTime, Camera camera, UnityAction<BaseInfoMark> OnMarkFinished)
        {
            SetText(message);
            currentWorldPosition = startPosition;
            screenStartPosition = transform.position;
            screenTargetPosition = screenStartPosition + Vector2.up * (existingTime * targetHeightMultiplier);
            rect.position = screenStartPosition;
            this.existingTime = existingTime;
            this.OnMarkFinished = OnMarkFinished;
            currentTime = 0f;
        }

        public override void UpdateMark(Camera camera)
        {
            currentTime += Time.unscaledDeltaTime;
            float timeValue = currentTime / existingTime;
            rect.position = Vector2.Lerp(screenStartPosition, screenTargetPosition, movementCurve.Evaluate(timeValue));
            float alpha = alphaCurve.Evaluate(timeValue);
            SetTransparency(alpha);

            if (timeValue >= 1)
            {
                OnMarkFinished?.Invoke(this);
            }
        }
    }
}
