using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class MovableTower : AttackableTower, ITowerMovable
    {
        public void Move(Vector2 position)
        {
            transform.position = position;
        }
    }
}
