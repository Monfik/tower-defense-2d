using Core;
using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Towers
{
    public class TowerBuilder : MachineState<TowerBuilder>, IGUIComposable
    {
        [Header("Values")]
        [SerializeField]
        private float demolishTimeMultiplier = 0.2f;
        public float DemolishTimeMultiplier => demolishTimeMultiplier;

        [SerializeField]
        private float fixingTimeMultiplier = 0.2f;
        public float FixingTimeMultiplier => fixingTimeMultiplier;

        private Tower tower;
        public Tower Tower => tower;

        private DataStorage storage;
        public DataStorage Storage => storage;

        [SerializeField] protected int towerLevel = 1;
        public int TowerLevel => towerLevel;

        private int maxLevel;
        public int MaxLevel => maxLevel;

        [SerializeField]
        private TowerUpgradeData cost;
        public TowerUpgradeData Cost => cost;

        [SerializeField]
        private List<TowerUpgradeData> upgrades;
        public List<TowerUpgradeData> Upgrades => upgrades;

        private TowerRepairer repairer;
        public TowerRepairer Repairer => repairer;

        private Dictionary<string, TowerBasicGUIText> guiTextes;
        public bool upgrading = false;
        public bool demolishing = false;

        [SerializeField] private bool staticBuild = false;
        public bool StaticBuild => staticBuild;

        public bool Builded { get; set; }

        [SerializeField] private List<EfficientColorPair> efficientColors;
        [SerializeField] private EffectPreviewData destroyPreviewData;
        public EffectPreviewData PreviewPreviewData => destroyPreviewData;

        #region Events
        public UnityAction<int> OnStartUpgrading;
        public UnityAction<int> OnFinishedUpgrading;
        public UnityAction OnStartUpgradingBasic;
        public UnityAction OnFinishedUpgradingBasic;
        public UnityAction OnStartBuilding;
        public UnityAction OnFinishedBuild;
        public UnityAction OnStartDemolishing;
        public UnityAction OnFinishDemolish;
        public UnityAction OnStartFixing;
        public UnityAction OnFinishedFix;

        public UnityAction<float> OnTowerEfficientChanged;
        #endregion

        public override void ChangeState(IBaseState<TowerBuilder> newState)
        {
            currentState?.Deinit(this);
            currentState = newState;
            currentState?.Init(this);
        }

        public override void UpdateMachine()
        {
            currentState?.UpdateState();
        }

        public void Init(DataStorage storage, Tower tower)
        {
            this.storage = storage;
            this.tower = tower;
            repairer = new TowerRepairer(this);
            maxLevel = upgrades.Count + 1;
            ChangeState(null);

            tower.GuiManager.OnBasicGuiExit += OnGuiClosed;
            tower.GuiManager.OnUpgradeGuiExit += OnGuiClosed;
        }

        public virtual void Build()
        {
            ChangeState(new BuilderBuildingState());
        }

        public TowerUpgradeData GetCurrentUpgradeData()
        {
            int upgradesIndex = towerLevel - 1;
            upgradesIndex = Mathf.Clamp(upgradesIndex, 0, upgrades.Count - 1);
            return upgrades[upgradesIndex];
        }

        public TowerUpgradeData GetLastUpgradeData()
        {
            if (towerLevel == 1)
                return cost;

            return upgrades[towerLevel - 2];
        }

        public void Upgrade()
        {
            float upgradeCost = CostCalculator.CalculateCost(GetCurrentUpgradeData().stock);
            storage.ReferenceStorage.Wallet.Subtract(Mathf.RoundToInt(upgradeCost));
            ChangeState(new BuilderUpgradeState());
        }

        public void Fix()
        {
            ChangeState(new BuilderFixingState(100f));
        }

        public void Fix(float fixValue)
        {
            fixValue = Mathf.Clamp(fixValue, 0f, 100f);
            ChangeState(new BuilderFixingState(fixValue));
        }

        public virtual void Demolish()
        {
            ChangeState(new BuilderDemolishState());
        }

        public void UpdateSystem()
        {
            UpdateMachine();
        }

        public void LevelUp()
        {
            towerLevel++;
        }

        public bool IsMaxUpgraded()
        {
            return towerLevel >= maxLevel;
        }

        public float GetSellValue()
        {
            float value = CostCalculator.CalculateCost(cost.stock);
            for (int i = 0; i < towerLevel - 1; i++)
            {
                value += CostCalculator.CalculateCost(upgrades[i].stock);
            }

            if (upgrading)
                value += CostCalculator.CalculateCost(upgrades[towerLevel - 1].stock);

            value *= Values.GeneralValues.sellMultiplier;
            return value;
        }

        private void EfficientTowerChanged(float efficient)
        {
            string colorHex = ColorUtility.ToHtmlStringRGBA(GetColorByEfficient(repairer.Efficient));
            string style = efficient <= 20f ? "<b>" : "";
            guiTextes["Efficient"].Text.text = "Efficient = " + style + $"<color=#{colorHex}>" + repairer.Efficient.ToString("F2") + " % </color></b>";
        }

        private Color GetColorByEfficient(float efficient)
        {
            Color efficientColor = efficientColors.Find(c => (c.range.start < efficient && c.range.end >= efficient)).color;
            return efficientColor;
        }

        private void OnGuiClosed()
        {
            guiTextes = null;
            OnTowerEfficientChanged -= EfficientTowerChanged;
        }

        #region IGUIComposable interface implementation
        public void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            OnTowerEfficientChanged -= EfficientTowerChanged;
            OnTowerEfficientChanged += EfficientTowerChanged;

            guiTextes = new Dictionary<string, TowerBasicGUIText>();

            TowerBasicGUIText efficientText = infoPanel.BasicTextes.GetElement();
            string colorHex = ColorUtility.ToHtmlStringRGBA(GetColorByEfficient(repairer.Efficient));
            efficientText.Text.text = "Efficient = " + $"<color=#{colorHex}>" + repairer.Efficient.ToString("F2") + " % </color>";
            guiTextes["Efficient"] = efficientText;

            if (tower.Builder.IsMaxUpgraded())
            {
                TowerBasicGUIText upgradeModel = infoPanel.BasicTextes.GetElement();
                upgradeModel.Text.text = "<color=green>Upgraded max</color>";
            }
            else
            {
                TowerUpgradeData upgrade = GetCurrentUpgradeData();

                TowerUpgradeMaterialCostTemplate costTemplate = infoPanel.UpgradeCost.GetElement();
                costTemplate.SetTemplate(upgrade.stock);
            }
        }

        public void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            ComposeBasicInfo(infoPanel);
        }
        #endregion
    }

    public class BuilderUpgradeState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private float currentTime;
        private float upgradeTime;

        public void Init(TowerBuilder controller)
        {
            builder = controller;
            builder.OnStartUpgrading?.Invoke(builder.TowerLevel + 1);
            builder.OnStartUpgradingBasic?.Invoke();
            upgradeTime = builder.Upgrades[builder.TowerLevel - 1].time;
            controller.upgrading = true;
        }

        public void UpdateState()
        {
            currentTime += Time.deltaTime;
            float t = currentTime / upgradeTime;
            builder.Tower.GraphicsSystem.UpdateSystem(t);

            if (t >= 1f)
            {
                FinishUpgrade();
            }
        }

        public void FixedUpdateState()
        {

        }

        private void FinishUpgrade()
        {
            builder.LevelUp();
            builder.upgrading = false;          
            builder.Tower.range.Upgrade(builder.TowerLevel);
            builder.Repairer.Fix(20f);
            builder.OnFinishedUpgrading?.Invoke(builder.TowerLevel);
            builder.OnFinishedUpgradingBasic?.Invoke();
            builder.ChangeState(null);
            CreateUpgradedNotification(builder.TowerLevel);
        }

        private void CreateUpgradedNotification(int towerLevel)
        {
            Vector2 notificationPosition = builder.Tower.transform.position;
            notificationPosition.y += builder.Tower.BoxCollider.size.y / 2;
            builder.Storage.ReferenceStorage.MarkGenerator.Notify("Upgraded to level " + towerLevel.ToString(), notificationPosition);
        }

        public void Deinit(TowerBuilder controller)
        {

        }
    }

    public class BuilderBuildingState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private float currentTime;
        private float buildTime;

        public void Init(TowerBuilder controller)
        {
            builder = controller;
            builder.OnStartBuilding?.Invoke();
            buildTime = builder.Cost.time;
        }

        public void UpdateState()
        {
            currentTime += Time.deltaTime;
            float t = currentTime / buildTime;
            builder.Tower.GraphicsSystem.UpdateSystem(t);

            if (t >= 1f)
            {
                FinishBuild();
            }
        }

        public void FixedUpdateState()
        {

        }

        public void FinishBuild()
        {
            builder.OnFinishedBuild?.Invoke();
            builder.Builded = true;
            builder.ChangeState(null);
        }

        public void Deinit(TowerBuilder controller)
        {

        }
    }

    public class BuilderDemolishState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private float currentTime;
        private float demolishTime;

        public void Init(TowerBuilder controller)
        {
            builder = controller;
            currentTime = 0f;
            demolishTime = builder.GetLastUpgradeData().time * builder.DemolishTimeMultiplier;
            builder.Tower.gameObject.layer = Values.LayerMasks.TowerUnaccessableLayer;
            builder.OnStartDemolishing?.Invoke();
            TowerEffectPreview effectPreview = builder.Tower.Effects.CreatePreview(builder.PreviewPreviewData);
            builder.Tower.Effects.AddPreview(effectPreview, "Tower demolishing");
            controller.demolishing = true;
        }

        public void UpdateState()
        {
            currentTime += Time.deltaTime;
            float t = currentTime / demolishTime;
            float reversedT = 1f - t;
            reversedT = Mathf.Clamp(reversedT, 0, 1);
            builder.Tower.GraphicsSystem.UpdateSystem(reversedT);

            if (t >= 1f)
            {
                FinishDemolishing();
            }
        }

        public void FixedUpdateState()
        {

        }

        private void FinishDemolishing()
        {
            builder.Tower.OnDemolished();
            builder.OnFinishDemolish?.Invoke();
            builder.Storage.EventsStorage.CoreEvnts.CallOnTowerDemolished(builder.Tower);
            builder.demolishing = false;
            builder.ChangeState(null);
            builder.Tower.Effects.RemovePreview("Tower demolishing");
        }

        public void Deinit(TowerBuilder controller)
        {

        }
    }

    public class BuilderFixingState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private float currentTime;
        private float fixTime;
        private float fixValue = 100f;

        public BuilderFixingState(float fixValue)
        {
            this.fixValue = fixValue;
        }

        public void Init(TowerBuilder controller)
        {
            builder = controller;

            builder.OnStartFixing?.Invoke();

            float fixT = builder.Repairer.Efficient / 100f;
            fixTime = (1 - fixT) * builder.GetLastUpgradeData().time * builder.FixingTimeMultiplier;
            currentTime = fixT * fixTime;
            builder.Tower.GraphicsSystem.UpdateSystem(fixT);
        }

        public void UpdateState()
        {
            currentTime += Time.deltaTime;
            float t = currentTime / fixTime;
            builder.Tower.GraphicsSystem.UpdateSystem(t);

            if (t >= 1f)
            {
                FinishFixing();
            }
        }

        public void FixedUpdateState()
        {

        }

        private void FinishFixing()
        {
            builder.OnFinishedFix?.Invoke();
            builder.Repairer.Fix(100f);
            builder.ChangeState(null);
        }

        public void Deinit(TowerBuilder controller)
        {

        }
    }

    [System.Serializable]
    public class EfficientColorPair
    {
        public Color color;
        public RangeIntInterval range;
    }
}