using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class DisplayableSpell : BaseSpell
    {
        public Sprite icon;
        public string spellDescription;
        protected UIUpdatableIcon updatableIcon;

        public override void StartCast()
        {
            base.StartCast();
            updatableIcon = storage.ReferenceStorage.UIIconsManager.CreateIcon(icon, spellDescription, false);
        }

        public override void UpdateSpell()
        {
            
        }

        public override void FinishSpell()
        {
            storage.ReferenceStorage.UIIconsManager.ReturnIcon(updatableIcon);
            base.FinishSpell();
        }
    }
}
