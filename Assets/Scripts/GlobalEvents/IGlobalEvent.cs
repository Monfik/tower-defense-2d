using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public interface IGlobalEvent
    {
        string EventName { get; }
        void StartToFinishEvent();
        void UpdateEvent();
        void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnEventFinished);
    }
}
