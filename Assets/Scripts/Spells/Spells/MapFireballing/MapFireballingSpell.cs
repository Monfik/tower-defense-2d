using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class MapFireballingSpell : BaseSpell
    {
        [SerializeField] private MapFireballingSpellInvoker invoker;
        [SerializeField] private FireballSpellCaster caster;
        [SerializeField] private FireballingMapData fireballsData;

        public override void StartCast()
        {
            base.StartCast();
            invoker.Init(storage, fireballsData, InvokerFinished);
            invoker.Execute();
            Values.InputValues.POINTER_ON_ACTION = true;
        }

        public override void UpdateSpell()
        {
            invoker.UpdateInvoker();

            if(Input.GetKeyDown(KeyCode.Mouse1))
            {
                CancelSpell();
            }
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Fireballs count", $"{invoker.CommandsList.Count}"));
            data.specifiedDatas.AddRange(invoker.CommandsList[0].GetTooltipData());

            return data;
        }

        private void InvokerFinished(FireballingMapData fireballingData)
        {
            caster.gameObject.SetActive(true);
            invoker.ManageCaster(caster);
            caster.Cast();
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(caster);
            OnSpellCasted?.Invoke(this);
            OnSpellFinished?.Invoke(this);
            PayMana();
            Values.InputValues.POINTER_ON_ACTION = false;
        }
    }

    [System.Serializable]
    public class FireballingMapData
    {
        public List<Vector2> fireballsPositions = new List<Vector2>();
        public FireData fireData;
    }
}
