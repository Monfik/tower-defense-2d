using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;

namespace TowerDefense.Towers
{
    public class TowerSelector : MonoBehaviour
    {
        private DataStorage storage;
        private Tower currentTower;

        [SerializeField] private RectTransform rect;

        [Header("Refs")]
        [SerializeField]
        private TowerBasicGuiPanel basicPanel;
        public TowerBasicGuiPanel BasicPanel => basicPanel;

        [SerializeField]
        private TowerBuildingGuiPanel upgradingPanel;
        public TowerBuildingGuiPanel UpgradingPanel => upgradingPanel;

        [SerializeField]
        private CustomTowerGuiPanel customTowerPanel;
        public CustomTowerGuiPanel CustomTowerPanel => customTowerPanel;

        private BaseTowerGuiPanel currentGuiPanel;

        private TowerRepositioner repositioner = new TowerRepositioner();

        [Header("Mouse handler")]
        [SerializeField] private float mouseSelectThreshold = 10f;
        private Vector2 startDragScreenPosition;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            repositioner.Init(storage);
            storage.EventsStorage.CoreEvnts.OnTowerPicked += OnPreviewCreated;
            storage.EventsStorage.SpellEvents.OnUsableSpellStartedCast += DisableSelecting;
            storage.EventsStorage.SpellEvents.OnUsableSpellCasted += EnableSelecting;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished += OnTowerDemolished;
            storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.TOWER_SELECTOR_MOUSE_CLICKED, Values.InputValues.DEFAULT_ACCEPT_KEY, OnMouseClicked, null, OnMouseReleased);
        }

        private void OnMouseClicked()
        {
            startDragScreenPosition = Input.mousePosition;
        }

        private void OnMouseReleased()
        {
            if (Values.InputValues.POINTER_ON_ACTION || Values.InputValues.POINTER_OVER_UI)
                return;

            float dragLength = Vector2.Distance(startDragScreenPosition, Input.mousePosition);

            if (dragLength > mouseSelectThreshold)
                return;

            Camera camera = storage.ReferenceStorage.MainCamera;
            Vector2 clickedPoint = camera.ScreenToWorldPoint(Input.mousePosition);

            foreach (Tower tower in storage.ReferenceStorage.TowersController.CurrentTowers)
            {
                bool towerClicked = tower.Collider.bounds.Contains(clickedPoint);

                if (towerClicked)
                {
                    if (tower == currentTower)
                    {
                        Close();
                        return;
                    }

                    Open(tower);
                    return;
                }
            }

            if (currentTower != null)
                Close();
        }

        private void OnPreviewCreated(Tower tower)
        {
            if(currentTower != null)
                Close();
        }

        public void Open(BaseTowerGuiPanel guiPanel, Tower tower)
        {
            if (currentGuiPanel != null)
                currentGuiPanel.Close();

            currentGuiPanel = guiPanel;
            guiPanel.gameObject.SetActive(true);
            currentGuiPanel.Open(tower, this, storage);  
        }

        public void Open(Tower tower)
        {
            gameObject.SetActive(true);

            if (currentTower != tower && currentTower != null)
            {
                currentTower.GuiManager.HideTowerGUI();
            }

            this.currentTower = tower;
            currentTower.Builder.OnStartDemolishing += Close;
            currentTower.GuiManager.OpenGUI(this);

            CheckForReposition(tower);
            CalculatePosition(tower);
        }

        public void CheckForReposition(Tower tower)
        {
            ITowerMovable movableTower = tower.GetComponent<ITowerMovable>();
            if(movableTower != null)
            {
                repositioner.AddToMove(movableTower);
            }
        }

        private void CalculatePosition(Tower tower)
        {
            Bounds towerBounds = tower.BoxCollider.bounds;

            Vector2 towerUpperMidlePos = towerBounds.center;
            towerUpperMidlePos.y += towerBounds.size.y / 2;

            Vector2 guiSize = rect.sizeDelta;
            Vector2 guiPosition = new Vector2(towerUpperMidlePos.x, towerUpperMidlePos.y + guiSize.y/2);
            transform.position = guiPosition;
        }

        public void Close()
        {
            gameObject.SetActive(false);

            if (currentTower != null)
            {
                currentTower.GuiManager.CloseGUI();
                currentTower.Builder.OnStartDemolishing -= Close;
            }

            currentTower = null;
            currentGuiPanel?.Close();
        }

        private void DisableSelecting()
        {
            Values.InputValues.POINTER_ON_ACTION = true;

            if (currentTower != null)
                Close();
        }

        private void EnableSelecting()
        {
            //Debug.Log("Enable selecting in gui tower");
            Values.InputValues.POINTER_ON_ACTION = false;
        }

        private void OnTowerDemolished(Tower tower)
        {
            if(currentTower == tower)
            {
                
            }
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnTowerPicked -= OnPreviewCreated;
            storage.EventsStorage.SpellEvents.OnUsableSpellStartedCast -= DisableSelecting;
            storage.EventsStorage.SpellEvents.OnUsableSpellCasted -= EnableSelecting;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= OnTowerDemolished;
            storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.TOWER_SELECTOR_MOUSE_CLICKED);
            Close();
        }
    }

    public class TowerGUIModel
    {
        public List<TowerGUITextModel> textes = new List<TowerGUITextModel>();
    }

    public class TowerGUITextModel
    {
        public string basicText;
        public string upgradeText;
        public float fontSize;
    }

    public class TowerGUIUpgradeTextModel
    {
        public string basicText;
        public string upgradeText;
        public float fontSize;
    }
}
