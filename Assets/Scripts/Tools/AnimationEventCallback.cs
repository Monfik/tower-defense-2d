using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Tools
{
    public class AnimationEventCallback : MonoBehaviour
    {
        public UnityAction AnimationCallback;

        public void CallCallback()
        {
            AnimationCallback?.Invoke();
        }
    }
}
