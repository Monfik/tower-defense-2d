using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class HighlightAnimator //Remember use 
    {
        private HighlightData highlightData;
        private List<Renderer> spritesToHighlight;
        private Color currentColor;
        private float currentIntensity;

        public void Highlight(HighlightData highlightData, List<Renderer> spritesToHighlight)
        {
            this.highlightData = highlightData;
            this.spritesToHighlight = spritesToHighlight;
            ResetHighlight();
        }

        public void Highlight(List<Renderer> spritesToHighlight)
        {
            ResetHighlight();

            this.spritesToHighlight = spritesToHighlight;
        }

        public void Update(float t)
        {
            currentColor = Color.Lerp(highlightData.startColor, highlightData.endColor, t);
            currentIntensity = Mathf.Lerp(highlightData.startIntensity, highlightData.endIntensity, t);

            foreach(Renderer renderer in spritesToHighlight)
            {
                renderer.material.SetColor("_EmissionColor", currentColor);
                renderer.material.SetFloat("_Intensity", currentIntensity);
            }
        }

        public void FinishHighlight()
        {
            ResetHighlight();
        }

        public void ResetHighlight()
        {
            currentColor = Color.white;
            currentIntensity = 0;

            foreach (Renderer renderer in spritesToHighlight)
            {
                renderer.material.SetColor("_EmissionColor", currentColor);
                renderer.material.SetFloat("_Intensity", currentIntensity);
            }
        }
    }

    [System.Serializable]
    public class HighlightData
    {
        public Color startColor;
        public Color endColor;
        public float startIntensity;
        public float endIntensity;
    }
}
