using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Affectors
{
    public abstract class BaseAffector<T> : MonoBehaviour
    {
        protected T target;
        protected DataStorage storage;
        public Type type => target.GetType();

        public virtual void Init(T target, DataStorage storage)
        {
            this.target = target;
            this.storage = storage;
        }
    }
}
