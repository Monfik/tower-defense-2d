using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class Snowball : MonoBehaviour, ITransparable
    {
        [SerializeField] private SpriteRenderer rend;
        public SpriteRenderer Rend => rend;

        [SerializeField] private GameObject explosionAnimation;

        [SerializeField] private bool playExplosionAnimation = true;
        public float strength; //0-1 0=min; 1=max

        public void MakeTransparable(float alpha)
        {
            UtilsExtension.MakeSpriteRendererTransparable(alpha, rend);
        }

        public void Hit(ISnowballable snowballable)
        {
            if(playExplosionAnimation)
            {
                GameObject explosion = Instantiate(explosionAnimation);
                explosion.transform.position = explosionAnimation.transform.position;
                explosion.gameObject.SetActive(true);
            }

            snowballable.HitBySnawball(this);
        }
    }
}
