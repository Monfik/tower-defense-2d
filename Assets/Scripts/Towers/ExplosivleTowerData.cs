using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class ExplosivleTowerData
    {
        public int armorReduction = 0;
    }
}
