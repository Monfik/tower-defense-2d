using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Updatables
{
    public class BaseUpdatableMono : MonoBehaviour, IUpdatable
    {
        protected UnityAction<IUpdatable> OnUpdatableFinished;
        protected DataStorage storage;
        public float duration;
        protected float currentTime;
        public bool durationable = true;

        public virtual void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            this.storage = storage;
            this.OnUpdatableFinished = OnUpdatableFinished;
        }

        public virtual void UpdateUpdatable()
        {
            if (!durationable)
                return;

            currentTime += Time.deltaTime;
            if(currentTime >= duration)
                Finish();
        }

        public virtual void Finish()
        {
            OnUpdatableFinished?.Invoke(this);
        }
    }
}
