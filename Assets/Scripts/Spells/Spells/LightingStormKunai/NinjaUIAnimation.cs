using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class NinjaUIAnimation : BaseUpdatableMono
    {
        [SerializeField] private Image ninjaImage;
        [SerializeField] private RectTransform rectNinjaImage;
        [SerializeField] private Animator animator;
        [SerializeField] private RectTransform kunaiHandTransform;
        [SerializeField] private List<NinjaFlyAnimationData> animationDatas;
        [SerializeField] new private Camera camera;

        private NinjaFlyAnimationData currentAnimationData;

        private UnityAction NinjaUIAnimated;
        private UnityAction<Vector2> KunaiThrowedFromPosition;
        private bool throwed = false;

        public override void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            base.StartUpdate(storage, OnUpdatableFinished);
        }

        public void ThrowRandom(UnityAction NinjaUIAnimated, UnityAction<Vector2> KunaiThrowedFromPosition)
        {
            this.NinjaUIAnimated = NinjaUIAnimated;
            this.KunaiThrowedFromPosition = KunaiThrowedFromPosition;

            int randomDataNumber = Random.Range(0, animationDatas.Count - 1);
            NinjaFlyAnimationData animationData = animationDatas[randomDataNumber];
            Throw(animationData);
        }

        private void Throw(NinjaFlyAnimationData animationData)
        {
            currentAnimationData = animationData;
            rectNinjaImage.anchoredPosition = animationData.startPosition;
            Vector3 dir = animationData.endPosition - animationData.startPosition;
            Quaternion rot = Quaternion.LookRotation(dir);
            Vector3 imageRot = rectNinjaImage.rotation.eulerAngles;
            imageRot.z = rot.eulerAngles.x + 90f;
            ninjaImage.transform.rotation = Quaternion.Euler(imageRot);
            ninjaImage.gameObject.SetActive(true);
        }

        public override void UpdateUpdatable()
        {
            Vector2 currentPosition = rectNinjaImage.anchoredPosition;
            Vector2 dir = currentAnimationData.endPosition - currentAnimationData.startPosition;
            currentPosition += dir.normalized * currentAnimationData.animationSpeed * Time.deltaTime;
            rectNinjaImage.anchoredPosition = currentPosition;

            float inversedT = Mathf.InverseLerp(currentAnimationData.startPosition.x, currentAnimationData.endPosition.x, currentPosition.x);
            if(inversedT >= currentAnimationData.throwT && !throwed)
            {
                StartThrow();
            }
            else if(inversedT >= 1)
            {
                NinjaUIAnimated?.Invoke();
                Finish();
            }
        }

        private void StartThrow()
        {
            throwed = true;
            animator.enabled = true;
        }

        public void Throwed()
        {
            Vector3 canvasPosition = kunaiHandTransform.transform.position;
            canvasPosition.z = 10f;
            Vector3 worldPosition = camera.ScreenToWorldPoint(canvasPosition);
            KunaiThrowedFromPosition?.Invoke(worldPosition);
        }

        public void AnimationFinished()
        {
            animator.enabled = false;
        }
    }

    [System.Serializable]
    public class NinjaFlyAnimationData
    {
        public Vector2 startPosition;
        public Vector2 endPosition;
        public float throwT;
        public float animationSpeed;
    }
}
