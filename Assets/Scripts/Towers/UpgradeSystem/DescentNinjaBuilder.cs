using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TowerDefense.Towers
{
    public class DescentNinjaBuilder : TowerBuilder
    {
        [Header("Descent Ninja Builder")]
        [SerializeField]
        private SpriteRenderer descentNinjaWithParachute;
        public SpriteRenderer DescentNinjaWithParachute => descentNinjaWithParachute;

        [SerializeField]
        private SpriteAnimation puffAnimation;
        public SpriteAnimation PuffAnimation => puffAnimation;

        [Header("States")]
        [SerializeField] private BuilderBuildingDescentNinjaState buildState;
        [SerializeField] private BuilderDemolishDescentNinjaState demolishState;
        [SerializeField] private BuilderDemolishUnnbuildedDescentNinjaState demolishBuildingState;

        public override void Build()
        {
            buildState.Init(this);
            ChangeState(buildState);
        }

        public override void Demolish()
        {
            if (currentState == buildState)
            {
                demolishBuildingState.Init(this);
                ChangeState(demolishBuildingState);
            }
            else
            {
                demolishState.Init(this);
                ChangeState(demolishState);
            }
        }
    }

    [System.Serializable]
    public class BuilderBuildingDescentNinjaState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private DescentNinjaBuilder ninjaBuilder;

        [Header("Times")]
        [SerializeField] private float descentTime = 4.5f;
        [SerializeField] private float puffAnimTime = 1f;
        [SerializeField] private AnimationCurve parachuteDescentCurve;
        [SerializeField] private SpriteAnimationv2 puffAnim;

        private Sequence fallSequence;

        public void Init(TowerBuilder controller)
        {
            builder = controller;
            builder.OnStartBuilding?.Invoke();
            builder.Tower.GraphicsSystem.BuildRenderer.gameObject.SetActive(false);
        }

        public void Init(DescentNinjaBuilder ninjaBuilder)
        {
            this.ninjaBuilder = ninjaBuilder;
            ninjaBuilder.DescentNinjaWithParachute.gameObject.SetActive(true);

            float ninjaHeight = ninjaBuilder.Tower.BoxCollider.size.y;
            Vector2 descentPosition = ninjaBuilder.Tower.transform.position + new Vector3(0f, 2f * ninjaHeight, 0f);
            ninjaBuilder.DescentNinjaWithParachute.transform.position = descentPosition;

            Sequence transition = DOTween.Sequence();
            transition.Append(ninjaBuilder.DescentNinjaWithParachute.transform.DOMove(ninjaBuilder.Tower.transform.position + new Vector3(0f, 0.32f * ninjaHeight, 0f), descentTime));

            ninjaBuilder.DescentNinjaWithParachute.DOColor(Color.white, descentTime / 3);

            fallSequence = DOTween.Sequence();
            fallSequence.Append(ninjaBuilder.DescentNinjaWithParachute.transform.DORotate(new Vector3(0, 0, 18f), descentTime / 8)).SetEase(parachuteDescentCurve);
            fallSequence.Append(ninjaBuilder.DescentNinjaWithParachute.transform.DORotate(new Vector3(0, 0, -18f), descentTime / 4)).SetEase(parachuteDescentCurve);
            fallSequence.Append(ninjaBuilder.DescentNinjaWithParachute.transform.DORotate(new Vector3(0, 0, 18f), descentTime / 4)).SetEase(parachuteDescentCurve).OnComplete(PlayPuffAnimation);
            fallSequence.Append(ninjaBuilder.DescentNinjaWithParachute.transform.DORotate(new Vector3(0, 0, 0f), descentTime / 8)).SetEase(parachuteDescentCurve);
        }

        public void UpdateState()
        {

        }

        private void PlayPuffAnimation()
        {
            //Sequence puffSequence = DOTween.Sequence();
            //puffSequence.Append(ninjaBuilder.DescentNinjaWithParachute.DOColor(new Color(255, 255, 255, 0), puffAnimTime/2));
            //puffSequence.Append(ninjaBuilder.DescentNinjaWithParachute.DOColor(Color.white, puffAnimTime/2));

            puffAnim.Renderer.gameObject.SetActive(true);
            puffAnim.PlayAnimation(sa => OnDescentFinished(), puffAnimTime);
        }

        private void OnDescentFinished()
        {
            puffAnim.Renderer.gameObject.SetActive(false);
            FinishBuild();
        }

        public void FinishBuild()
        {
            ninjaBuilder.DescentNinjaWithParachute.gameObject.SetActive(false);
            builder.OnFinishedBuild?.Invoke();
            builder.Builded = true;
            builder.ChangeState(null);
        }

        public void Deinit(TowerBuilder controller)
        {
            puffAnim.Renderer.gameObject.SetActive(false);
            ninjaBuilder.DescentNinjaWithParachute.gameObject.SetActive(false);
            fallSequence.Kill();
            puffAnim.Stop();
        }

        public void FixedUpdateState()
        {

        }
    }

    [System.Serializable]
    public class BuilderDemolishDescentNinjaState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private DescentNinjaBuilder ninjaBuilder;

        [SerializeField] private float puffAnimationTime;

        public void Init(TowerBuilder controller)
        {
            builder = controller;
            builder.OnStartBuilding?.Invoke();
            builder.Tower.GraphicsSystem.BuildRenderer.gameObject.SetActive(false);
        }

        public void Init(DescentNinjaBuilder ninjaBuilder)
        {
            this.ninjaBuilder = ninjaBuilder;
            ninjaBuilder.PuffAnimation.Renderer.gameObject.SetActive(true);
            ninjaBuilder.PuffAnimation.PlayAnimation(OnPuffAnimationFinished, puffAnimationTime);
            ninjaBuilder.OnStartDemolishing?.Invoke();
        }

        public void UpdateState()
        {

        }

        private void OnPuffAnimationFinished(SpriteAnimation animation)
        {
            ninjaBuilder.PuffAnimation.Renderer.gameObject.SetActive(false);
            FinishDemolish();
        }

        public void FinishDemolish()
        {
            builder.Tower.OnDemolished();
            builder.OnFinishDemolish?.Invoke();
            builder.Storage.EventsStorage.CoreEvnts.CallOnTowerDemolished(builder.Tower);
            builder.ChangeState(null);
        }

        public void Deinit(TowerBuilder controller)
        {

        }

        public void FixedUpdateState()
        {

        }

    }
    [System.Serializable]
    public class BuilderDemolishUnnbuildedDescentNinjaState : IBaseState<TowerBuilder>
    {
        private TowerBuilder builder;
        private DescentNinjaBuilder ninjaBuilder;

        [SerializeField] private float puffAnimationSpeed;


        public void Init(TowerBuilder controller)
        {
            builder = controller;
            builder.OnStartBuilding?.Invoke();
            builder.Tower.GraphicsSystem.BuildRenderer.gameObject.SetActive(false);
        }

        public void Init(DescentNinjaBuilder ninjaBuilder)
        {
            this.ninjaBuilder = ninjaBuilder;
            ninjaBuilder.PuffAnimation.Renderer.gameObject.SetActive(true);
            ninjaBuilder.PuffAnimation.PlayAnimation(OnPuffAnimationFinished, puffAnimationSpeed);
            ninjaBuilder.PuffAnimation.Renderer.transform.position = ninjaBuilder.DescentNinjaWithParachute.transform.position;
            ninjaBuilder.OnStartDemolishing?.Invoke();
        }

        public void UpdateState()
        {

        }

        private void OnPuffAnimationFinished(SpriteAnimation animation)
        {
            ninjaBuilder.PuffAnimation.Renderer.gameObject.SetActive(false);
            FinishDemolish();
        }

        public void FinishDemolish()
        {
            builder.Tower.OnDemolished();
            builder.OnFinishDemolish?.Invoke();
            builder.Storage.EventsStorage.CoreEvnts.CallOnTowerDemolished(builder.Tower);
            builder.ChangeState(null);
        }

        public void Deinit(TowerBuilder controller)
        {

        }

        public void FixedUpdateState()
        {

        }
    }
}
