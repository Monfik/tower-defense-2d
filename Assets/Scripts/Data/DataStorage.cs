﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

namespace Data
{
    public class DataStorage : MonoBehaviour
    {
        [SerializeField]
        private ReferenceStorage referenceStorage;
        public ReferenceStorage ReferenceStorage => referenceStorage;

        [SerializeField]
        private UIStorage UIstorage;
        public UIStorage UIStorage => UIstorage;

        private EventsStorage eventsStorage = new EventsStorage();
        public EventsStorage EventsStorage => eventsStorage;

        [SerializeField]
        private LevelsContainer levels;
        public LevelsContainer Levels => levels;
    }
}
