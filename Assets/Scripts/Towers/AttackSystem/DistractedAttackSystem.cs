using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class DistractedAttackSystem : BaseAttackSystem
    {
        [SerializeField] private DirectBullet directBullet;
        [SerializeField] private int directions;
        [SerializeField] private DirectBulletData bulletData;

        private List<DirectBullet> bullets = new List<DirectBullet>();

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            bulletData.Reset();
        }

        public override void Attack()
        {
            float randomAngleRad = Random.Range(0f, 180f) * Mathf.Deg2Rad;
            for (int i = 0; i < directions; i++)
            {
                float angleRad = randomAngleRad + (i * (360f / directions)) * Mathf.Deg2Rad;
                Vector2 velocityMagnitude = new Vector2(Mathf.Sin(angleRad), Mathf.Cos(angleRad));
                DirectBullet spawnedBullet = SpawnBullet();
                spawnedBullet.Init(velocityMagnitude, bulletData, OnBulletReachedTarget);
                bullets.Add(spawnedBullet);
            }
        }

        private DirectBullet SpawnBullet()
        {
            DirectBullet bullet = Instantiate(directBullet);
            bullet.gameObject.SetActive(true);
            bullet.transform.position = attackableTower.transform.position;
            return bullet;
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            return null;
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            
        }

        public override void OnTowerDemolished()
        {
            
        }

        public override void SwitchToNormalState()
        {
            UpdateState = UpdateDefaultAttack;
            lastAttackTime = 1000f;
        }

        protected override void UpdateDefaultAttack()
        {
            if (lastAttackTime >= 1f / (attackSpeed.data.currentValue / 4f))
            {
                Attack();
                lastAttackTime = 0f;
            }
        }

        private void OnBulletReachedTarget(DirectBullet bullet)
        {
            bullets.Remove(bullet);
            Destroy(bullet.gameObject);
        }

        protected override void OnTargetNotReachable(Monster monster)
        {
            
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            bulletData.minDmg.SetupMultiplier(multiplier);
            bulletData.maxDmg.SetupMultiplier(multiplier);

            //attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.minDmg);
            //attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(bulletData.maxDamage);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            string str = "Distracted attack \n" +
                "Attack speed " + attackSpeed.data.coreValue + "\n" +
                "Damage: " + bulletData.minDmg.coreValue + " - " + bulletData.maxDmg.coreValue + "\n" +
                "Armor reduction: " + bulletData.armorReduction.coreValue + "\n" +
                "Range = " + bulletData.distance + "\n" +
                "Speed = " + bulletData.speed + "\n" +
                "Shurikens count: " + directions + "\n\n" +
                "Effects: \n" +
                directBullet.CreateTooltipData(data);

            return data;
        }
    }

    [System.Serializable]
    public class DirectBulletData
    {
        public float distance;
        public float speed;
        public TowerStaticsData minDmg;
        public TowerStaticsData maxDmg;
        public TowerStaticsData armorReduction;

        public void Reset()
        {
            minDmg.Reset();
            maxDmg.Reset();
            armorReduction.Reset();
        }
    }
}
