using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PausePanel : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] private Button backButton;
    [SerializeField] private Button repeatLevelButton;
    [SerializeField] private Button backToMainMenuButton;

    private UnityAction PauseOff;
    private DataStorage storage;

    public void Enter(UnityAction PauseOff, DataStorage storage)
    {
        this.storage = storage;
        gameObject.SetActive(true);
        this.PauseOff = PauseOff;
        backButton.onClick.AddListener(Quit);
        repeatLevelButton.onClick.AddListener(RepeatLevelButtonClicked);
        backToMainMenuButton.onClick.AddListener(BackToMainMenuButtonClicked);
    }

    private void RepeatLevelButtonClicked()
    {
        storage.ReferenceStorage.LevelManager.OrderRepeatLevel();
        Quit();
    }

    private void BackToMainMenuButtonClicked()
    {
        storage.ReferenceStorage.LevelManager.CurrentLevel.Castle.Invulnerable = true;
        storage.ReferenceStorage.MainController.BackToMainMenu();
        Quit();
    }

    public void Quit()
    {
        PauseOff?.Invoke();
        backButton.onClick.RemoveAllListeners();
        repeatLevelButton.onClick.RemoveAllListeners();
        backToMainMenuButton.onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }

    public void ClosePanel()
    {
        backButton.onClick.RemoveAllListeners();
        repeatLevelButton.onClick.RemoveAllListeners();
        backToMainMenuButton.onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }
}
