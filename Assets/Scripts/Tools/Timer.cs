using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Tools
{
    public class Timer : MonoBehaviour
    {
        private float time;
        private float currentTime = 0f;
        private UnityAction funcToExecute;
        private bool destroyAfter = true;

        public void Setup(float time, UnityAction func, bool destroyAfterTime = true)
        {
            this.time = time;
            funcToExecute = func;
            destroyAfter = destroyAfterTime;
        }

        private void Update()
        {
            currentTime += Time.deltaTime;

            if (currentTime > time)
            {
                funcToExecute?.Invoke();
                currentTime = 0f;

                if (destroyAfter)
                    Destroy(this);
            }
        }
    }
}
