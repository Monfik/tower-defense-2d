using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public abstract class BaseTowerEffect
    {
        [SerializeField] private string id;
        public string Id => id;

        protected Tower tower;
        protected UnityAction<BaseTowerEffect> OnEffectFinished;
        [SerializeField] protected float duration;
        public float Duration => duration;
        public bool timable = true;
        protected float currentTime = 0f;

        [SerializeField] protected EffectPreviewData previewData;
        public EffectPreviewData PreviewData => previewData;
        public bool effectPreviewable = true;

        public BaseTowerEffect(BaseTowerEffect effect)
        {
            this.id = effect.Id;
            duration = effect.duration;
            currentTime = effect.currentTime;
            this.previewData = effect.previewData;
            effectPreviewable = effect.effectPreviewable;
            timable = effect.timable;
        }

        public BaseTowerEffect() { }

        public void Init(Tower owner, UnityAction<BaseTowerEffect> OnEffectFinished)
        {
            tower = owner;
            this.OnEffectFinished = OnEffectFinished;
        }

        public abstract void StartEffect();
        public virtual void UpdateEffect()
        {
            if (!timable)
                return;

            currentTime += Time.deltaTime;

            if(currentTime >= duration)
            {
                FinishEffect();
            }
        }

        public virtual void FinishEffect()
        {
            OnEffectFinished?.Invoke(this);
        }
    }

    [System.Serializable]
    public class EffectPreviewData
    {
        [TextArea] public string effectDescription;
        [SerializeField] private Sprite effectPreview;
        public Sprite Preview => effectPreview;
    }
}
