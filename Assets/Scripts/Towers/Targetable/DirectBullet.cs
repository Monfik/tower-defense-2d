using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using TowerDefense.Effects;
using Utils;
using SpriteGlow;
using static TowerDefense.Creatures.IDestructable;
using TowerDefense.Tips;

namespace TowerDefense.Towers
{
    public class DirectBullet : MonoBehaviour, ITooltipable
    {
        [SerializeField]
        new protected SpriteRenderer renderer;
        protected UnityAction<DirectBullet> OnTargetHitted;
        [SerializeField]
        new private BoxCollider2D collider2D;

        private DirectBulletData bulletData;
        [SerializeField] private MonsterBuffEffect slowPrefab;
        [SerializeField] private float alphaDisplayTime;
        [SerializeField] private float hitScaleFactor;

        [Header("Effects")]
        [SerializeField] private SpriteGlowEffect glow;

        public void Init(Vector2 velocityMagnitude, DirectBulletData bulletData, UnityAction<DirectBullet> OnTargetHitted)
        {
            this.bulletData = bulletData;
            this.OnTargetHitted = OnTargetHitted;
            renderer.DOColor(Color.white, alphaDisplayTime);
            Vector2 targetPosition = (Vector2)transform.position + (velocityMagnitude * bulletData.distance);
            float duration = Vector2.Distance(transform.position, targetPosition) / bulletData.speed;
            transform.DOMove(targetPosition, duration).OnComplete(OnTargetReached);
            StartCoroutine(WaitFor(StartDissapear, duration - (alphaDisplayTime * 1.01f)));
        }

        public void UpdateBullet()
        {

        }

        private void StartDissapear()
        {
            DOVirtual.DelayedCall(alphaDisplayTime * 0.1f, () => { collider2D.enabled = false; });
            Color color = Color.white;
            color.a = 0;
            renderer.DOColor(color, alphaDisplayTime);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Monster hittedMonster = collision.gameObject.GetComponent<Monster>();
            if(hittedMonster != null)
            {
                int armor = hittedMonster.Data.armor.currentValue;
                armor -= bulletData.armorReduction.currentValue;
                int randomDamage = Random.Range(bulletData.minDmg.currentValue, bulletData.maxDmg.currentValue);
                int dmg = DamageCalculator.CalculateDamage(randomDamage, armor);
                hittedMonster.TakeDamage(dmg, DamageType.Physics);

                AddSlow(hittedMonster);
                Glow();
            }
        }

        private void AddSlow(Monster monster)
        {
            MonsterBuffEffect slow = Instantiate(slowPrefab);
            monster.Effects.AddEffect(slow);
        }

        private void Glow()
        {
            glow.enabled = true;
            StartCoroutine(WaitFor(() => { glow.enabled = false; }, 0.1f));
        }

        private void OnTargetReached()
        {
            OnTargetHitted?.Invoke(this);
        }

        IEnumerator WaitFor(UnityAction action, float time)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }

        private void OnDestroy()
        {
            DOTween.Kill(renderer);
            DOTween.Kill(this);
        }

        public DescriptionData CreateTooltipData(DescriptionData data)
        {
            return slowPrefab.CreateTooltipData(data);
        }
    }
}
