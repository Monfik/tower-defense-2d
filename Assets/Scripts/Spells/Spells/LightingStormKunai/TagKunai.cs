using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using TowerDefense.Updatables;

namespace TowerDefense.Spells
{
    public class TagKunai : BaseUpdatableMono
    {
        [SerializeField] private float speedPerUni = 2f;
        private UnityAction KunaiLanded;

        public void Init(Vector2 position, UnityAction OnUnityLanded)
        {
            float duration = Vector2.Distance(position, transform.position) / speedPerUni;
            transform.DOMove(position, duration).OnComplete(Landed).SetEase(Ease.Linear);
            KunaiLanded = OnUnityLanded;

            transform.rotation = RotateToTarget(position, transform.position);
        }

        public override void UpdateUpdatable()
        {
            
        }

        private void Landed()
        {
            KunaiLanded?.Invoke();
            Finish();
        }

        private Quaternion RotateToTarget(Vector2 target, Vector2 positionFrom)
        {
            Vector3 vectorToTarget = target - positionFrom;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle-90f, Vector3.forward);
            Quaternion rot = Quaternion.RotateTowards(transform.rotation, q, 360f);
            return rot;
        }

        private void Update()
        {
            Vector2 direction = Vector3.zero - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        }
    }
}
