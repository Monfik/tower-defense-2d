using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense
{
    public class PathSelector : MonoBehaviour
    {
        [SerializeField] private PathSelectButtonPool buttonPool;
        [SerializeField] private List<PathSelectButton> pathButtons;
        [SerializeField] private RectTransform contentRect;

        [SerializeField] private PathDisplayer displayer;
        private UnityAction<PathCreator> OnPathSelected;

        public void Show(List<PathCreator> paths, UnityAction<PathCreator> OnPathSelected)
        {
            gameObject.SetActive(true);
            this.OnPathSelected = OnPathSelected;
            int counter = 1;
            foreach(PathCreator path in paths)
            {
                PathSelectButton button = CreateButton(path);
                button.Text.text = counter.ToString();
                pathButtons.Add(button);
                counter++;
            }

            displayer.CreateVisuals(paths);
        }

        private PathSelectButton CreateButton(PathCreator path)
        {
            PathSelectButton button = buttonPool.GetPoolObject();
            button.SetElement(path);
            button.Init(PathSelected, PathHovered, PathHoveredExit);
            button.transform.SetParent(contentRect);
            button.transform.localScale = Vector3.one;
            return button;
        }

        public void UpdateSelector()
        {
            displayer.UpdateDisplayer();
        }

        private void PathHovered(PathCreator path)
        {
            displayer.Highlight(path);
        }

        private void PathHoveredExit(PathCreator path)
        {
            displayer.Unhighlight(path);
        }

        private void PathSelected(PathCreator path)
        {
            OnPathSelected?.Invoke(path);
        }

        public void Hide()
        {
            foreach(PathSelectButton button in pathButtons)
            {
                buttonPool.ReturnToPool(button);
            }

            pathButtons.Clear();
            displayer.Clear();
            gameObject.SetActive(false);
        }
    }
}
