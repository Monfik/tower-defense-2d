using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Animations
{
    public class Rotate : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Transform target;

        public void Init(float rotateSpeed, Transform target)
        {
            this.rotateSpeed = rotateSpeed;
            this.target = target;
        }

        private void Update()
        {
            float currentRotation = target.rotation.eulerAngles.z;
            currentRotation += rotateSpeed * Time.deltaTime;
            target.rotation = Quaternion.Euler(new Vector3(target.rotation.eulerAngles.x, target.rotation.eulerAngles.y, currentRotation));
        }
    }
}
