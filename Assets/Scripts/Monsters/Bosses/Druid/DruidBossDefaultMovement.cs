using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class DruidBossDefaultMovement : BaseBossState<DruidBossBehaviour>
    {
        [SerializeField] private List<BaseBossState<DruidBossBehaviour>> availableStates;
        public List<BaseBossState<DruidBossBehaviour>> AvailableStates => availableStates;

        private Timer checkStatesTimer;

        public override void Init(DruidBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Walk");

            checkStatesTimer = gameObject.AddComponent<Timer>();
            checkStatesTimer.Setup(0.3f, CheckStatesAvailable, false);
        }

        public override void Deinit()
        {
            Destroy(checkStatesTimer);
        }

        public override void UpdateState()
        {
            boss.UpdateMovement();
        }

        private void CheckStatesAvailable()
        {
            BaseBossState<DruidBossBehaviour> newState = availableStates.Find(c => c.IsReadyForWork());
            if (newState != null)
            {
                boss.ChangeState(newState);
            }
        }
    }
}
