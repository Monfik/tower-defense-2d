using System.Collections;
using System.Collections.Generic;
using TowerDefense.Spells;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Updatables
{
    public class UIUpdatableIcon : MonoBehaviour
    {
        [SerializeField] private Image artwork;
        public Image Artwork => artwork;

        [SerializeField] private Image fill;
        public Image Fill => fill;
        [SerializeField] private Image overlay;
        public Image Overlay => overlay;

        [SerializeField] private SpellHolderLoadBarsController loadBarsController;
        public SpellHolderLoadBarsController Bars => loadBarsController;

        [SerializeField] private TipDescription tip;
        public TipDescription Tip => tip;

        public void UpdateIcon(float t)
        {
            
        }
    }
}
