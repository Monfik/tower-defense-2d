using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Creatures
{
    public class DruidMapActionRune : MonoBehaviour
    {
        public float visualMarkUpDuration = 1f;
        public float visualMarkDownDuration = 0.3f;
        [SerializeField] private SpriteRenderer spriteRend;
        [SerializeField] private SpriteRenderer markRenderer;
        [SerializeField] private Sprite markSprite;
        public Sprite MarkSprite => markSprite;
        [SerializeField] private Animator pufAnimator;
        public string actionName;
        public Color color;
        [SerializeField] private float visualMarkUpIntensityValue;

        public UnityAction Action;

        public void VisualMark()
        {
            Sequence markAnimation = DOTween.Sequence();
            markAnimation.Append(DOVirtual.Float(0f, visualMarkUpIntensityValue, visualMarkUpDuration, OnVisualMarkUpdate));
            markAnimation.Append(DOVirtual.Float(visualMarkUpIntensityValue, 0f, visualMarkDownDuration, OnVisualMarkUpdate));
        }

        private void OnVisualMarkUpdate(float value)
        {
            markRenderer.material.SetFloat("_Intensity", value);
        }

        private void OnVisualMarkAlphaUpdate(float alpha)
        {
            spriteRend.SetSpriteColorAlpha(alpha);
        }

        public void UnactiveItself()
        {
            gameObject.SetActive(false);
            UnactiveAnimator();
        }

        public void ActiveItself()
        {
            UnactiveAnimator();
            gameObject.SetActive(true);
            spriteRend.SetSpriteColorAlpha(1f);
            spriteRend.material.SetFloat("_Intensity", 0f);
        }

        public void ActiveAnimator()
        {
            pufAnimator.gameObject.SetActive(true);
        }

        public void UnactiveAnimator()
        {
            pufAnimator.gameObject.SetActive(false);
        }

        public void Trigger()
        {
            Action?.Invoke();

            Sequence markAnimation = DOTween.Sequence();
            markAnimation.Append(DOVirtual.Float(0f, visualMarkUpIntensityValue * 2, visualMarkUpDuration / 3, OnVisualMarkUpdate));
            markAnimation.Append(DOVirtual.Float(visualMarkUpIntensityValue * 2, 0f, visualMarkDownDuration / 3, OnVisualMarkUpdate).OnComplete(ActiveAnimator));
            markAnimation.Append(DOVirtual.Float(1f, 0f, 0.5f, OnVisualMarkAlphaUpdate));
            markAnimation.onComplete = UnactiveItself;
            markAnimation.SetId(0);
        }

        private void OnDisable()
        {
            DOTween.Kill(0);
        }
    }
}
