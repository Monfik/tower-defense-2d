using Core;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace CoreUI
{
    public class LevelView : BaseView
    {
        [SerializeField]
        private WaveNumberVisualizer waveNumberText;
        public WaveNumberVisualizer WaveNumberText => waveNumberText;

        [SerializeField]
        private Wallet gold;

        public void Deinit()
        {
            
        }
    }
}
