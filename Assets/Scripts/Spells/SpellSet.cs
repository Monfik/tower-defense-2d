using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class SpellSet : MonoBehaviour
    {
        [SerializeField] 
        private List<SpellPair> pairs;
        public List<SpellPair> Pairs => pairs;

        public void ResetSet()
        {
            foreach(SpellPair pair in pairs)
            {
                if (pair.unlocked)
                    pair.holder.RefreshToDefaultState();
            }
        }
    }

    [System.Serializable]
    public class SpellPair
    {
        public BaseSpell caster;
        public SpellHolder holder;
        public int levelUnlock;
        public int waveUnlock;
        public bool unlocked = false;
    }
}
