using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Enviroment
{
    public class MapDecor : MonoBehaviour, ITransparable
    {
        public Collider2D mainCollider;
        public BoxCollider2D boxOverlapCollider;
        public SpriteRenderer spriteRend;

        public void MakeTransparable(float alpha)
        {
            spriteRend.SetSpriteColorAlpha(alpha);
        }

        private void Reset()
        {
            mainCollider = GetComponent<Collider2D>();
            spriteRend = GetComponent<SpriteRenderer>();
        }

        private void OnDestroy()
        {
            DOTween.Kill("Spawn terrain transform");
        }
    }
}
