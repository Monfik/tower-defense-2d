using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public abstract class HitAnimableBullet : TargetableBullet
    {
        private UnityAction UpdateBehaviour;

        [SerializeField] protected bool hasAnimableToPlay;

        public override void Init(Monster target, string jsonedModelData, UnityAction<Bullet> OnTargetReached)
        {
            base.Init(target, jsonedModelData, OnTargetReached);
            UpdateBehaviour = UpdateTargetableBehaviour;
        }

        public override void UpdateBullet()
        {
            UpdateBehaviour?.Invoke();
        }

        protected override void ManageTargetReached()
        {
            if (hasAnimableToPlay)
            {
                DealDamage(target);
                animator.SetTrigger("Hitted");
                UpdateBehaviour = UpdatePosition;
            }
            else
            {
                base.ManageTargetReached();
            }
        }

        private void UpdateTargetableBehaviour()
        {
            UpdateRotation();
            UpdatePosition();
            CheckForReach();
        }

        public void OnAnimableFinished()
        {
            OnTargetReached?.Invoke(this);
        }
    }
}
