using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace TowerDefense
{
    public interface ISnowballable : ITargetable
    {
        void HitBySnawball(Snowball snowball);
    }
}
