using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

namespace TowerDefense
{
    public class GenericHoverButton<T> : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler where T : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text text;
        public TMP_Text Text => text;

        private UnityAction<T> OnButtonHovered;
        private UnityAction<T> OnButtonClicked;
        private UnityAction<T> OnButtonHoverExit;
        public T element;

        public void Init(UnityAction<T> OnButtonClicked, UnityAction<T> OnButtonHovered, UnityAction<T> OnButtonHoverExit)
        {
            AssignEvents(OnButtonClicked,OnButtonHovered, OnButtonHoverExit);
            button.onClick.AddListener(() => this.OnButtonClicked?.Invoke(element));
        }

        public void SetElement(T element)
        {
            this.element = element;
        }

        public void AssignEvents(UnityAction<T> OnButtonClicked, UnityAction<T> OnButtonHovered, UnityAction<T> OnButtonHoverExit)
        {
            this.OnButtonHovered = OnButtonHovered;
            this.OnButtonClicked = OnButtonClicked;
            this.OnButtonHoverExit = OnButtonHoverExit;
        }

        public void Deinit()
        {
            button.onClick.RemoveAllListeners();
            UnassignEvents();
        }

        public void UnassignEvents()
        {
            OnButtonClicked = null;
            OnButtonHovered = null;
            OnButtonHoverExit = null;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnButtonHovered?.Invoke(element);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnButtonHoverExit?.Invoke(element);
        }
    }
}
