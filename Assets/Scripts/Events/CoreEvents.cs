﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Data;
using TowerDefense.Creatures;
using LevelLogic;
using TowerDefense.Towers;
using TowerDefense.Effects;
using Utils;

public class CoreEvents
{
    public UnityAction OnMainGameStateEnter;
    public void CallOnMainGameStateEnter()
    {
        OnMainGameStateEnter?.Invoke();
    }

    public UnityAction OnFreestyleStateExit;
    public void CallOnFreestyleStateExit()
    {
        OnFreestyleStateExit?.Invoke();
    }

    public UnityAction<Monster> OnMonsterSpawned;
    public void CallOnMonsterSpawned(Monster monster)
    {
        OnMonsterSpawned?.Invoke(monster);
    }

    public UnityAction<Monster> OnMonsterFinishedPath;
    public void CallOnMonsterFinishedPath(Monster monster)
    {
        OnMonsterFinishedPath?.Invoke(monster);
    }

    public UnityAction<Monster> OnMonsterDied;
    public void CallOnMonsterDied(Monster monster)
    {
        OnMonsterDied?.Invoke(monster);
    }

    public UnityAction<Wave> OnWaveFinished;
    public void CallOnWaveFinished(Wave wave)
    {
        OnWaveFinished?.Invoke(wave);
    }

    public UnityAction<Wave> OnWaveStarted;
    public void CallOnWaveStarted(Wave wave)
    {
        OnWaveStarted?.Invoke(wave);
    }

    public UnityAction<Tower> OnTowerPicked;
    public UnityAction OnTowerPickedBasic;
    public void CallOnTowerPicked(Tower tower)
    {
        OnTowerPicked?.Invoke(tower);
        OnTowerPickedBasic?.Invoke();
    }

    public UnityAction<Tower> OnTowerPreviewDeclined;
    public void CallOnTowerPreviewDeclined(Tower tower)
    {
        OnTowerPreviewDeclined?.Invoke(tower);
    }

    public UnityAction<Tower> OnTowerBuilded;
    public void CallOnTowerBuilded(Tower tower)
    {
        OnTowerBuilded?.Invoke(tower);
    }

    public UnityAction OnCurrentWavesFinished;
    public void CallOnCurrentWavesFinished()
    {
        OnCurrentWavesFinished?.Invoke();
    }

    public UnityAction<MapEffect> OnMapEffectAppeared;
    public void CallOnMapEffectAppeared(MapEffect effect)
    {
        OnMapEffectAppeared?.Invoke(effect);
    }

    public UnityAction<LevelData> OnLevelLoaded;
    public void CallOnLevelLoaded(LevelData levelData)
    {
        OnLevelLoaded?.Invoke(levelData);
    }

    public UnityAction OnLevelStartedBasic;
    public UnityAction<Level> OnLevelStarted;
    public void CallOnLevelStarted(Level level)
    {
        OnLevelStarted?.Invoke(level);
        OnLevelStartedBasic?.Invoke();
    }

    public UnityAction<float> OnGoldChanged;
    public void CallOnGoldChanged(float gold)
    {
        OnGoldChanged?.Invoke(gold);
    }

    public UnityAction<MaterialCostsData> OnMaterialsCostsChanged;
    public void CallOnMaterialsCostChanged(MaterialCostsData materialsCost)
    {
        OnMaterialsCostsChanged?.Invoke(materialsCost);
    }

    public UnityAction<string, float> OnMaterialCostChanged;
    public void CallOnMaterialCostChanged(string resourceName, float cost)
    {
        OnMaterialCostChanged?.Invoke(resourceName, cost);
    }

    public UnityAction<Tower, int> OnTowerStartUpgrade;
    public void CallOnTowerStartUpgrade(Tower tower, int level)
    {
        OnTowerStartUpgrade?.Invoke(tower, level);
    }

    public UnityAction<Tower> OnTowerDemolished;
    public void CallOnTowerDemolished(Tower tower)
    {
        OnTowerDemolished?.Invoke(tower);
    }

    public UnityAction OnBaseDestroyed;
    public void CallOnBaseDestroyed()
    {
        OnBaseDestroyed?.Invoke();
    }

    public UnityAction<LevelFinisher> BaseDestroyed;
    public void CallBaseDestroyed(LevelFinisher finisher)
    {
        BaseDestroyed?.Invoke(finisher);
    }

    public UnityAction OnLevelFinishedBasic;
    public UnityAction<Level> OnLevelFinished;
    public void CallOnlevelFinished(Level level)
    {
        OnLevelFinished?.Invoke(level);
        OnLevelFinishedBasic?.Invoke();
    }

    public UnityAction<Level> OnLevelUnloaded;
    public void CallOnLevelUnloaded(Level level)
    {
        OnLevelUnloaded?.Invoke(level);
    }

    public UnityAction OnGamePaused;
    public void CallOnGamePaused()
    {
        OnGamePaused?.Invoke();
    }

    public UnityAction OnGameUnPaused;
    public void CallOnGameUnPaused()
    {
        OnGameUnPaused?.Invoke();
    }
}
