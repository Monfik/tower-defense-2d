using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Affectors
{
    [System.Serializable]
    public class AffectorsContainer<T>
    {
        [SerializeField] private List<BaseAffector<T>> affectors;

        public void Init(T target, DataStorage storage)
        {
            affectors.ForEach(c => c.Init(target, storage));
        }
    }
}
