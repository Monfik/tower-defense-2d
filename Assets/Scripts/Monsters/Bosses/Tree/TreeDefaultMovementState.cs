using Data;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;
using Utils;

namespace TowerDefense.Creatures
{
    [System.Serializable]
    public class TreeDefaultMovementState : BaseBossState<TreeBossBehaviour>
    {
        [SerializeField] private List<BaseBossState<TreeBossBehaviour>> availableStates;
        public List<BaseBossState<TreeBossBehaviour>> AvailableStates => availableStates;

        private Timer checkStatesTimer;

        public override void Init(TreeBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("Walk");

            checkStatesTimer = gameObject.AddComponent<Timer>();
            checkStatesTimer.Setup(0.2f, CheckStatesAvailable, false);
        }

        public override void Deinit()
        {
            Destroy(checkStatesTimer);
        }

        public override void UpdateState()
        {
            boss.UpdateMovement();      
        }

        private void CheckStatesAvailable()
        {
            BaseBossState<TreeBossBehaviour> newState = availableStates.Find(c => c.IsReadyForWork());
            if (newState != null)
            {
                boss.ChangeState(newState);
            }
        }
    }
}
