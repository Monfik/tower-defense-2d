using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class ZealAreaSpell : BaseSpell
    {
        [SerializeField] private SpriteRenderer mark;
        [SerializeField] private ZealArea area; 

        public override void StartCast()
        {
            base.StartCast();
            mark.gameObject.SetActive(true);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic += CancelSpell;
            Values.InputValues.POINTER_ON_ACTION = true;
            //storage.ReferenceStorage.Input.AddNewInput(Values.InputValues.DEFAULT_ACCEPT_SPELL_CAST, Values.InputValues.DEFAULT_ACCEPT_KEY, CreateArea, null, null);
        }

        public override void UpdateSpell()
        {
            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            mark.transform.position = mousePosition;

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                CreateArea(mousePosition);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                CancelSpell();
            }
        }

        private void CreateArea(Vector2 position)
        {
            if (Values.InputValues.POINTER_OVER_UI)
            {
                return;
            }

            Values.InputValues.POINTER_ON_ACTION = false;
            OnSpellCasted?.Invoke(this);
            ZealArea mapArea = Instantiate(area);
            mapArea.gameObject.SetActive(true);
            mapArea.transform.position = position;
            mapArea.StartAreaEffect();
            PayMana();
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(mapArea);
            FinishSpell();
        }

        public override void CancelSpell()
        {
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            base.CancelSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Damage multiplier", $"{1 + area.AdditionalDmgMultiplier.BuffData.damageMultiplier}x"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Duration", $"{area.Duration}"));
            return data;
        }

        public override void FinishSpell()
        {
            //storage.ReferenceStorage.Input.RemoveInput(Values.InputValues.DEFAULT_ACCEPT_SPELL_CAST);
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            base.FinishSpell();
        }
    }
}
