﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public interface IBaseState<T>
    {
        void Init(T controller);
        void UpdateState();

        void FixedUpdateState();
        void Deinit(T controller);
    }

}