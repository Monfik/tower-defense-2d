using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TowerDefense.Towers
{
    public class TowerPreset : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private Image artwork;

        [SerializeField]
        new private TMP_Text name;

        [SerializeField]
        private Tower tower;
        public Tower Tower => tower;

        [SerializeField] private TMP_Text cost;
        [SerializeField] private TipDescription tip;

        private UnityAction<Tower> OnTowerClicked;

        public void Init(UnityAction<Tower> OnTowerClicked)
        {
            this.OnTowerClicked = OnTowerClicked;
            button.interactable = true;
            button.onClick.AddListener(PresetClicked);
            tip.SetDescription(tower.CreateTooltipData(tip.Description));
        }

        private void PresetClicked()
        {
            OnTowerClicked?.Invoke(tower);
        }

        public void UpdateCost(int cost, Color color, bool interactable)
        {
            this.cost.text = cost.ToString();
            this.cost.color = color;
            button.interactable = interactable;
            //tip.Description.footer = cost.ToString();
        }

        public void Deinit()
        {
            button.onClick.RemoveAllListeners();
            button.interactable = false;
            OnTowerClicked = null;
        }
    }
}
