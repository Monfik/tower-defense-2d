using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreUI
{
    public class BaseView : MonoBehaviour
    {
        protected MainController controller;
        [SerializeField]
        private CanvasGroup canvasGroup;

        public void EnableView(bool enabled)
        {
            gameObject.SetActive(enabled);
        }

        public virtual void Init(MainController controller)
        {
            this.controller = controller;
        }
    }
}
