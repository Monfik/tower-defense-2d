using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public interface IGUIComposable
    {
        void ComposeBasicInfo(TowerGuiInfoPanel infoPanel);
        void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel);
    }
}
