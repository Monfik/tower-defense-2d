using System.Collections;
using System.Collections.Generic;
using TowerDefense.Controllers;
using TowerDefense.GlobalEvents;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Enviroment
{
    public class WindEntity : MonoBehaviour, IWindSusceptible
    {
        [SerializeField] private ParticleSystem particle;
        public ParticleSystem Particle => particle;

        [SerializeField] private ParticleSystemRenderer particleRend;
        public ParticleSystemRenderer ParticleRend => particleRend;

        public UnityAction<WindEntity> OnParticleStopped;

        public void Affect(WindForceData windData)
        {
            
        }

        private void OnParticleSystemStopped()
        {
            OnParticleStopped?.Invoke(this);
        }
    }
}
