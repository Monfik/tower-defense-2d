using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Spells
{
    public class ActiveSpellHolder : SpellHolder
    {
        private bool active = false;

        public override void Init(SpellsController controller, DataStorage storage)
        {
            base.Init(controller, storage);
            spell.Init(storage, this, OnSpellCastStarted, OnSpellCasted, OnSpellFInished, OnSpellCanceled);
        }

        protected override void SetupCost(int cost)
        {
            this.cost.text = $"{cost} / s";
        }

        protected override void CastSpell()
        {
            spell.StartCast();
        }

        protected override void OnSpellCasted(BaseSpell spell)
        {          
            controller.Spells.Add(spell);
            Use();
        }

        protected override void OnSpellFInished(BaseSpell spell)
        {
            overlay.material.SetFloat("_OutlineWidth", 0f);
            controller.Spells.Remove(spell);
            controller.UpdatableSpellHolders.Remove(this);
            active = false;
        }

        public override void Use()
        {
            //Debug.Log("Use #1");
            ////if (Values.InputValues.POINTER_ON_ACTION)
            ////    return;

            //Debug.Log("Use #2");
            overlay.material.SetFloat("_OutlineWidth", 1f);
            controller.UpdatableSpellHolders.Add(this);
            active = true;
        }

        public override void DisableByMana()
        {
            spellArtwork.color = new Color(105f/255f, 105f/255f, 105f/255f, 1f); //light red
            button.interactable = false;
            CurrentManaCheck = CheckForEnableByMana;

            if (active)
                spell.FinishSpell();
        }

        public override void EnableByMana()
        {
            spellArtwork.color = Color.white;
            button.interactable = true;
            CurrentManaCheck = CheckForDisableByMana;
        }

        public override void UpdateHolder()
        {
            
        }

        protected override void CheckForDisableByMana(int mana)
        {
            if (mana < spell.ManaCost * 3)
            {
                DisableByMana();
            }
        }

        protected override void CheckForEnableByMana(int mana)
        {
            if (mana >= spell.ManaCost * 3)
            {
                EnableByMana();
            }
        }

        private void Awake()
        {
            overlay.material.SetFloat("_OutlineWidth", 0f);
        }
    }
}
