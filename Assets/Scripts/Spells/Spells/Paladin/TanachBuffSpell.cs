using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Spells
{
    public class TanachBuffSpell : BaseSpell
    {
        [SerializeField] private SpriteRenderer crosshair;
        private AttackableTower upgradedTower;
        private UnityAction TowerDebuff;
        private AttackableTower highlightedTower;
        [SerializeField] private float fixValue;
        [SerializeField] private float damageBuffMultiplier;
        [SerializeField] private float duration;

        [SerializeField] private float glowingSpeed = 10f;

        [SerializeField] private HighlightData highlightData;

        private bool buffed = false;
        private float currentTime = 0f;
        private float currentHiglightTime = 0f;

        public override void StartCast()
        {
            base.StartCast();
            crosshair.gameObject.SetActive(true);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic += CancelSpell;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished += TowerDemolished;
        }

        public override void UpdateSpell()
        {
            if(!buffed)
            {
                UpdateCrosshair();
            }
            else
            {
                currentTime += Time.deltaTime;
                if (currentTime >= duration)
                    FinishSpell();
            }
        }

        private void UpdateCrosshair()
        {
            UpdateHighlight();

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            crosshair.transform.position = mousePosition;
            int towerMask = 1 << Values.LayerMasks.TowerLayer;
            Collider2D collider = Physics2D.OverlapPoint(mousePosition, towerMask);

            if (collider != null)
            {
                AttackableTower crosshairedTower = collider.GetComponent<AttackableTower>();

                if (crosshairedTower != null)
                {
                    if (crosshairedTower != highlightedTower)
                    {
                        StartHighlightTower(crosshairedTower);
                    }

                    if (Input.GetMouseButtonUp(0))
                        Buff(crosshairedTower);
                }   
            }
            else
            {
                if (highlightedTower != null)
                    FinishHighlightTower(highlightedTower);
            }

            if (Input.GetMouseButtonUp(1))
                CancelSpell();
        }

        private void UpdateHighlight()
        {
            currentHiglightTime += Time.deltaTime * glowingSpeed;

            float sin = Mathf.Sin(currentHiglightTime);
            float t = Mathf.InverseLerp(-1, 1, sin);

            if(highlightedTower != null)
            {
                highlightedTower.GraphicsSystem.Highliter.Update(t);
            }

            if (currentHiglightTime >= 900f)
                currentHiglightTime -= 900f;
        }

        private void StartHighlightTower(AttackableTower tower)
        {
            if (highlightedTower != null)
                FinishHighlightTower(highlightedTower);

            highlightedTower = tower;

            tower.GraphicsSystem.Highlight(highlightData);
        }

        private void FinishHighlightTower(AttackableTower tower)
        {
            tower.GraphicsSystem.FinishHighlight();
            highlightedTower = null;
        }

        private void Buff(AttackableTower tower)
        {
            FinishHighlightTower(tower);
            upgradedTower = tower;
            TowerDebuff = tower.AttackSystem.BuffDamageByMultiplier(damageBuffMultiplier);

            buffed = true;
            OnSpellCasted?.Invoke(this);
            crosshair.gameObject.SetActive(false);
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            PayMana();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Damage multiplier", $"{1 + damageBuffMultiplier}x"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Duration", $"{duration}s"));

            return data;
        }

        public override void CancelSpell()
        {
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            if (highlightedTower != null)
                FinishHighlightTower(highlightedTower);

            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= TowerDemolished;

            base.CancelSpell();
        }

        private void TowerDemolished(Tower tower)
        {
            if(tower == highlightedTower)
            {
                FinishHighlightTower(highlightedTower);
            }
            else if(tower == upgradedTower)
            {
                FinishSpell();       
            }
        }

        public override void FinishSpell()
        {
            TowerDebuff?.Invoke();
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= TowerDemolished;
            base.FinishSpell();
        }
    }
}
