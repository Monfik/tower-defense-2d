using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Buttons;
using Core;

public class VocationSelectorController : MonoBehaviour
{
    private int currentVocationIndex;
    
    [SerializeField]
    private Button startButton;

    private VocationButton currentVocationButton;

    private void Start()
    {
        startButton.interactable = false;
        currentVocationIndex = -1;
        currentVocationButton = null;
    }

    public void SelectVocationIndex(int vocIndex)
    {
        currentVocationIndex = vocIndex;
        startButton.interactable = true;
    }

    public void SelectVocationButton(VocationButton button)
    {
        if (currentVocationButton == button)
            return;

        currentVocationButton?.Deselect();
        currentVocationButton = button;
        currentVocationButton?.Select();

        startButton.interactable = true;

        SelectVocationIndex(currentVocationButton.AssignedVocationIndex);
    }

    public void Reset()
    {
        currentVocationIndex = -1;
        currentVocationButton?.Deselect();
        currentVocationButton = null;
        startButton.interactable = false;
    }

    public void StartGame()
    {
        
    }
}
