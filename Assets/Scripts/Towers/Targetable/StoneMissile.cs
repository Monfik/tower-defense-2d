using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Towers
{
    public class StoneMissile : MonoBehaviour
    {
        [SerializeField]
        new protected SpriteRenderer renderer;

        [SerializeField]
        protected Animator animator;
        public Animator Animator => animator;

        [SerializeField]
        protected BoxCollider2D boomCollider;

        protected bool fly = false;
        protected UnityAction<StoneMissile> StoneExploded;
        protected DataStorage storage;
        private StoneData stoneData;

        public StoneMissileData data;
        [SerializeField] protected SpriteAnimation boomAnimation;
        protected float currentTime = 0f;
        protected float flyTime;
        protected Monster target;
        public Monster Target => target;
        public Vector2 targetPosition;
        protected Vector2 startPosition;
        protected bool targetDied = false;
        protected float parabolaHeight;

        public void Init(Monster target, DataStorage storage, float range, StoneData stoneData, UnityAction<StoneMissile> OnExploded)
        {                  
            this.target = target;
            this.storage = storage;
            this.StoneExploded = OnExploded;
            data.range = range;
            this.stoneData = stoneData;
            storage.EventsStorage.CoreEvnts.OnMonsterDied += MonsterDied;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath += MonsterDied;
        }

        private void MonsterDied(Monster monster)
        {
            if(monster == target && !fly)
            {
                targetPosition = target.GetCenter();
                target = null;
            }
        }

        public void ThrewCallback()
        {
            startPosition = transform.position;

            if(target != null)
                targetPosition = target.GetCenter();

            float distance = Vector2.Distance(targetPosition, startPosition);
            Vector2 diff = targetPosition - startPosition;
            float diffY = Mathf.Clamp(diff.y, 0, data.range);
            diffY /= data.range;
            diffY = Mathf.Clamp(diffY, 0f, 1f);
            parabolaHeight = Mathfx.Lerp(data.minPushHeight, data.maxPushHeight, diffY);
            flyTime = Mathfx.Lerp(data.minFlyTime, data.maxFlyTime, distance / data.range);
            fly = true;
            animator.enabled = false;
        }

        public void OverrideLastTargetPosition(Vector2 lastPosition)
        {
            targetPosition = lastPosition;
            targetDied = true;
        }

        public virtual void UpdateMissile()
        {
            if (!fly)
                return;

            currentTime += Time.deltaTime * data.flySpeed;
            float t = currentTime / flyTime;
            t = Mathf.Clamp(t, 0f, 1f);
            Vector2 newPosition = MathParabola.Parabola(startPosition, targetPosition, parabolaHeight, t);
            transform.position = newPosition;

            if (t == 1)
            {
                Explode();
            }
        }

        protected virtual void Explode()
        {
            boomAnimation.PlayAnimation(OnExploded);
            fly = false;

            boomCollider.enabled = true;
            Collider2D[] colliders = new Collider2D[100];
            ContactFilter2D filter = new ContactFilter2D();
            int colliderCount = Physics2D.OverlapCollider(boomCollider, filter, colliders);
            List<Monster> hittedMonsters = new List<Monster>();

            for(int i=0; i<colliderCount; i++)
            {
                Monster hittedMonster = colliders[i].GetComponent<Monster>();
                if (hittedMonster != null)
                {
                    if(!hittedMonster.Dead)
                    {
                        hittedMonsters.Add(hittedMonster);
                    }
                }
            }

            for(int i=0; i<hittedMonsters.Count; i++)
            {
                DealDamage(hittedMonsters[i]);
            }         
        }

        protected virtual void OnExploded(SpriteAnimation animation)
        {
            boomAnimation.process = false;
            StoneExploded.Invoke(this);
        }

        protected virtual void DealDamage(Monster monster)
        {
            int armorResistance = monster.Data.armor.currentValue;
            armorResistance -= stoneData.armorReduction.data.currentValue;
            armorResistance = Mathf.Clamp(armorResistance, monster.Data.armor.minValue, monster.Data.armor.maxValue);
            int randomDamage = Random.Range(stoneData.minExploDamage.data.currentValue, stoneData.maxExploDamage.data.currentValue);
            int Dmg = Utils.DamageCalculator.CalculateDamage(randomDamage, armorResistance);
            monster.TakeDamage(Dmg, DamageType.Physics);
        }
    }

    [System.Serializable]
    public class StoneMissileData
    {
        public float speed = 3f;
        public float mass = 1f;
        public float range;
        public float minPushHeight;
        public float maxPushHeight;
        public float minFlyTime = 1.5f;
        public float maxFlyTime = 2f;
        public float flySpeed = 1.1f;

        public void SetRange(float range)
        {
            this.range = range;
            minPushHeight = range / 2;
            maxPushHeight = range * 1.4f;
        }
    }
}
