using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    public class SpellEvents
    {
        public UnityAction OnUsableSpellStartedCast;
        public void CallOnUsableSpellStartedCast()
        {
            OnUsableSpellStartedCast?.Invoke();
        }

        public UnityAction OnUsableSpellCasted;
        public void CallOnUsableSpellCasted()
        {
            OnUsableSpellCasted?.Invoke();
        }
    }
}
