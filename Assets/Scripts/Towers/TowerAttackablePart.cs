using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowerAttackablePart : MonoBehaviour
    {
        [SerializeField]
        new private SpriteRenderer renderer;

        [SerializeField]
        private Animator animator;

        public void Attack()
        {
            animator.enabled = true;
            animator.SetTrigger("Attack");
        }
    }
}
