using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Creatures
{
    public class MonsterDeathController : MonoBehaviour
    {
        private Monster monster;

        public void Init(Monster monster)
        {
            this.monster = monster;
        }

        public void StartDeath()
        {
            monster.Collider.enabled = false;
            monster.Legs.Collider.enabled = false;
            monster.Animator.enabled = true;
            monster.Animator.Play("Die");
            monster.Animator.speed = 1f;
            monster.HealthBar.gameObject.SetActive(false);

            AnimationClip clip = monster.Animator.runtimeAnimatorController.animationClips[1];

            foreach(AnimationClip animclip in monster.Animator.runtimeAnimatorController.animationClips)
            {
                if (animclip.name == "Die")
                {
                    clip = animclip;
                    break;
                }
            }

            AnimationEvent evt = new AnimationEvent();
            evt.time = clip.length;
            evt.functionName = "Die";       
            clip.AddEvent(evt);
        }

        public void Die()
        {
            monster.Animator.enabled = false;
            Destroy(monster.gameObject);
        }
    }
}
