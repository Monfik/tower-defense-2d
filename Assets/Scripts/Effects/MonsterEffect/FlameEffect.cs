using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class FlameEffect : BaseMonsterEffect
    {
        [SerializeField] private int dmgPerSecond;
        [SerializeField] private float tickTime;
        private float currentTickTime = 0f;

        [Header("References")]
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Animator animator;

        public void SetDmg(int dmgPerSecond)
        {
            this.dmgPerSecond = dmgPerSecond;
        }

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);

            Tick();
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();

            currentTickTime += Time.deltaTime;
            if (currentTickTime >= tickTime)
            {
                Tick();
            }
        }

        private void Tick()
        {
            GiveDamage();
            currentTickTime = 0f;
            spriteRenderer.enabled = true;
            animator.enabled = true;
            animator.Play("Tick");
        }

        private void GiveDamage()
        {
            int dmg = DamageCalculator.CalculateMagicDamage(monster.Data, dmgPerSecond, MagicType.Fire);
            monster.TakeDamage(dmg, DamageType.Magic);
        }

        private void TickAnimFinished()
        {
            spriteRenderer.enabled = false;
            animator.enabled = false;
        }
    }
}
