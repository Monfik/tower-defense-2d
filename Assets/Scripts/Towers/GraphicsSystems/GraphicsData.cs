using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class GraphicsData
    {
        public List<AttackableTowerEntity> attackEntities;
        public List<AttackableTowerEntity> attackableAnimators;
        public List<SpriteRenderer> visualSprites;
        //public List<SpriteRenderer> spritesToEnable;
        //public List<SpriteRenderer> spritesToDisable;
        //public List<ReplaceSpriteData> spriteReplacers;
        public Sprite buildSprite;
    }

    [System.Serializable]
    public class ReplaceSpriteData
    {
        public SpriteRenderer renderer;
        public Sprite sprite;
    }
}
