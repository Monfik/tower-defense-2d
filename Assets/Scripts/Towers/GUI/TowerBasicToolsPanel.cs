using Data;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;
using static Utils.Values;

namespace TowerDefense.Towers
{
    public class TowerBasicToolsPanel : MonoBehaviour
    {
        private Tower currentTower;

        [Header("Buttons")]
        [SerializeField] private Button upgrade;
        [SerializeField] private Button sell;
        [SerializeField] private Button fix;

        [Header("Texts")]
        [SerializeField] private TMP_Text upgradeText;
        [SerializeField] private TMP_Text sellText;
        [SerializeField] private TMP_Text fixText;

        [SerializeField] private TowerSelector gui;
        [SerializeField] 
        private UIObjectEventHandler upgradeHoverHandler;
        public UIObjectEventHandler UpgradeHoverHandler => upgradeHoverHandler;

        [SerializeField] private CanvasGroup canvasGroup;

        private TowerUpgradeData currentUpgradeData;
        private DataStorage storage;

        public void Open(Tower tower, DataStorage storage)
        {
            Close();

            currentTower = tower;
            this.storage = storage;
            SetSellValues();

            sell.onClick.AddListener(Demolish);
            fix.onClick.AddListener(Fix);
            currentUpgradeData = tower.Builder.GetCurrentUpgradeData();
            UpdateFixInteract(tower.Builder.Repairer.Efficient, currentUpgradeData);

            if (tower.Builder.IsMaxUpgraded())
            {
                upgrade.gameObject.SetActive(false);
                upgradeText.gameObject.SetActive(false);
            }
            else
            {
                upgrade.gameObject.SetActive(true);
                upgradeText.gameObject.SetActive(true);     
                UpdateUpgradeInteract(currentUpgradeData.stock, Values.gold, upgrade);
                upgrade.onClick.AddListener(Upgrade);
                storage.EventsStorage.CoreEvnts.OnGoldChanged += OnGoldChanged;
                storage.EventsStorage.CoreEvnts.OnMaterialsCostsChanged += OnRawMaterialsChanged;
            }

            currentTower.Builder.OnTowerEfficientChanged += OnTowerEfficientChanged;
        }

        public void OnRawMaterialsChanged(MaterialCostsData materialCosts)
        {
            UpdateUpgradeInteract(currentUpgradeData.stock, Values.gold, upgrade);
            UpdateFixInteract(currentTower.Builder.Repairer.Efficient, currentUpgradeData);
        }

        private void OnGoldChanged(float gold)
        {
            UpdateUpgradeInteract(currentUpgradeData.stock, gold, upgrade);
            UpdateFixInteract(currentTower.Builder.Repairer.Efficient, currentUpgradeData);
        }

        private void UpdateUpgradeInteract(TowerStock upgradeData, float gold, Button button)
        {
            int upgradeCost = Mathf.RoundToInt(CostCalculator.CalculateCost(upgradeData));
            bool afford = gold >= upgradeCost;

            button.interactable = afford;
            upgradeText.text = upgradeCost.ToString();
        }

        private void UpdateFixInteract(float efficient, TowerUpgradeData upgradeData)
        {
            if(efficient >= 90)
            {
                fix.interactable = false;
                fixText.text = "Efficient";
                return;
            }
        
            float efficientRest = 100f - efficient;
            int fixCost = Mathf.RoundToInt(CostCalculator.CalculateCost(upgradeData.stock) * (efficientRest / 100f) * currentTower.Builder.Repairer.FixMultiplier);
            if (fixCost == 0)
                fixCost = 1;
            fixText.text = fixCost.ToString();
            bool afford = fixCost <= Values.gold;
            fix.interactable = afford;
        }

        private void SetSellValues()
        {
            float towerValue = currentTower.Builder.GetSellValue();
            sellText.text = towerValue.ToString();
        }

        private void Upgrade()
        {
            storage.EventsStorage.CoreEvnts.CallOnTowerStartUpgrade(currentTower, currentTower.Builder.TowerLevel);
            currentTower.Builder.Upgrade();
            gui.Close();
        }
        
        private void Demolish()
        {
            gui.Close();
            currentTower.Builder.Demolish();
        }

        public void Close()
        {
            if (storage == null)
                return;

            upgrade.onClick.RemoveAllListeners();
            sell.onClick.RemoveAllListeners();
            fix.onClick.RemoveAllListeners();
            storage.EventsStorage.CoreEvnts.OnGoldChanged -= OnGoldChanged;
            storage.EventsStorage.CoreEvnts.OnMaterialsCostsChanged -= OnRawMaterialsChanged;
            currentTower.Builder.OnTowerEfficientChanged -= OnTowerEfficientChanged;
        }

        private void OnTowerEfficientChanged(float efficient)
        {
            UpdateFixInteract(efficient, currentUpgradeData);
        }

        private void Fix()
        {
            gui.Close();
            currentTower.Builder.Fix();
            int fixCost = Mathf.RoundToInt(CostCalculator.CalculateCost(currentTower.Builder.GetCurrentUpgradeData().stock) * ((100 - currentTower.Builder.Repairer.Efficient) / 100f) * currentTower.Builder.Repairer.FixMultiplier);
            storage.ReferenceStorage.Wallet.Subtract(fixCost);
        }
    }
}
