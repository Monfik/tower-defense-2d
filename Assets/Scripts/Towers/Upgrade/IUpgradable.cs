using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public interface IUpgradable
    {
        void Upgrade(int level);
    }
}
