using TowerDefense.Creatures;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

namespace Data
{
    [System.Serializable]
    public class MonsterReleaseData
    {
        public Monster monster;
        public float delay;
        public PathCreator path;
        public MonsterData monsterData;

        public MonsterReleaseData(Monster monster, float delay, PathCreator path, MonsterData data)
        {
            this.monster = monster;
            this.delay = delay;
            this.path = path;
            this.monsterData = data;
        }
    }
}
