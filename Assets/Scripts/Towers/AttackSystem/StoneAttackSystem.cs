using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Towers
{
    public class StoneAttackSystem : BaseAttackSystem
    {
        [SerializeField]
        private List<StoneMissile> stoneMissiles;

        [SerializeField]
        private StoneMissile missilePrefab;

        [Header("Data")]
        [SerializeField]
        private StoneData stoneData;

        public override void Attack()
        {
            StoneMissile spawnedStone = GameObject.Instantiate(missilePrefab, attackableTower.transform);
            spawnedStone.Init(target, storage, attackableTower.RealRange, stoneData, OnExploded);
            spawnedStone.gameObject.SetActive(true);
            spawnedStone.transform.position = attackableTower.transform.position;
            stoneMissiles.Add(spawnedStone);
            DamageItself();

            foreach (AttackableTowerEntity animabeEntity in attackableTower.GraphicsSystem.GraphicsData.attackableAnimators)
            {
                animabeEntity.PlayAttackAnimation();
            }
        }

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            stoneData.Reset();
        }

        public override void Upgrade(int level)
        {
            base.Upgrade(level);
            stoneData.minExploDamage.Upgrade(level);
            stoneData.maxExploDamage.Upgrade(level);
            stoneData.armorReduction.Upgrade(level);
        }

        private void OnExploded(StoneMissile stoneMissile)
        {
            stoneMissiles.Remove(stoneMissile);
            Destroy(stoneMissile.gameObject);
        }

        public override void UpdateAttackSystem()
        {
            base.UpdateAttackSystem();

            for (int i = 0; i < stoneMissiles.Count; i++)
            {
                stoneMissiles[i].UpdateMissile();
            }
        }

        protected override void OnTargetNotReachable(Monster monster)
        {
            //base.OnTargetNotReachable(monster);
            UpdateState = SearchTarget;
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerBasicGUIText damageModel = infoPanel.BasicTextes.GetElement();
            damageModel.Text.text = "Physical damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
            TowerBasicGUIText armorReductionModel = infoPanel.BasicTextes.GetElement();
            armorReductionModel.Text.text = "Armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
            TowerBasicGUIText attackSpeedModel = infoPanel.BasicTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();

            UnityAction DamageUpdated = () => { damageModel.Text.text = "Physical damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.minExploDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.maxExploDamage));
            UnityAction ArmorReductionUpdated = () => { armorReductionModel.Text.text = "Armor reduction: " + stoneData.armorReduction.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, stoneData.armorReduction));
            UnityAction AttackSpeedUpdated = () => { attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerUpgradeGuiText damageModel = infoPanel.UpgradeTextes.GetElement();
            damageModel.Text.text = "Physical damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
            damageModel.UpgradeText.text = stoneData.minExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel() + " - " + stoneData.maxExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText armorReductionModel = infoPanel.UpgradeTextes.GetElement();
            armorReductionModel.Text.text = "Armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
            armorReductionModel.UpgradeText.text = stoneData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText attackSpeedModel = infoPanel.UpgradeTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
            attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

            UnityAction DamageUpdated = () => {
                damageModel.Text.text = "Physical damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
                damageModel.UpgradeText.text = stoneData.minExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel() + " - " + stoneData.maxExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.minExploDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.maxExploDamage));
            UnityAction ArmorReductionUpdated = () => {
                armorReductionModel.Text.text = "Armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
                armorReductionModel.UpgradeText.text = stoneData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, stoneData.armorReduction));
            UnityAction AttackSpeedUpdated = () => {
                attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
                attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }

        public override void OnTowerDemolished()
        {
            
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            int minDmgBuff = (int)Mathf.Round(stoneData.minExploDamage.data.coreValue * multiplier);
            stoneData.minExploDamage.data.BuffByValue(minDmgBuff);
            int maxDmgBuff = (int)Mathf.Round(stoneData.maxExploDamage.data.coreValue * multiplier);
            stoneData.maxExploDamage.data.BuffByValue(maxDmgBuff);

            UnityAction OnDebuff = () => {
                stoneData.minExploDamage.data.BuffByValue(-minDmgBuff);
                stoneData.maxExploDamage.data.BuffByValue(-maxDmgBuff);
            };

            return OnDebuff;
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            stoneData.minExploDamage.data.SetupMultiplier(multiplier);
            stoneData.maxExploDamage.data.SetupMultiplier(multiplier);

            for (int i = 0; i < stoneData.minExploDamage.upgrades.Count; i++)
            {
                TowerStaticsData upgradeMinData = stoneData.minExploDamage.upgrades[i];
                upgradeMinData.SetupMultiplier(multiplier);

                TowerStaticsData upgradeMaxData = stoneData.maxExploDamage.upgrades[i];
                upgradeMaxData.SetupMultiplier(multiplier);
            }

            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(stoneData.minExploDamage);
            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(stoneData.maxExploDamage);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            data = base.CreateTooltipData(data);

            data.title2 = "Physical AOE damage";

            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("<sprite=5>Physical AOE", $"{stoneData.minExploDamage.data.coreValue} - {stoneData.maxExploDamage.data.coreValue}"));
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Armor reduction", stoneData.armorReduction.data.coreValue.ToString()));

            return data;
        }
    }

    [System.Serializable]
    public class StoneData
    {
        public UpgradableStaticsData minExploDamage;
        public UpgradableStaticsData maxExploDamage;
        public UpgradableStaticsData armorReduction;

        public void Reset()
        {
            minExploDamage.Reset();
            maxExploDamage.Reset();
            armorReduction.Reset();
        }
    }
}
