using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class ArmorReductionSpell : BaseSpell
    {
        [SerializeField] private int armorReduction;
        [SerializeField] private int magicArmorReduction;
        [SerializeField] private float duration;
        private List<Monster> reducedMonsters;
        private float currentTime = 0f;
        
        public override void StartCast()
        {
            base.StartCast();
            Reduce();
            OnSpellCasted?.Invoke(this);  
        }

        public override void UpdateSpell()
        {
            currentTime += Time.deltaTime;

            if (currentTime >= duration)
                FinishSpell();
        }

        private void Reduce(Monster monster)
        {
            reducedMonsters.Add(monster);

            monster.Data.armor.AddValue(-armorReduction);
            monster.Data.fireResist.AddValue(-magicArmorReduction);
            monster.Data.iceResist.AddValue(-magicArmorReduction);
            monster.Data.poisonResist.AddValue(-magicArmorReduction);
            monster.Data.lightingResist.AddValue(-magicArmorReduction);
            monster.Data.waterResist.AddValue(-magicArmorReduction);
        }

        private void Reduce()
        {
            reducedMonsters = new List<Monster>();

            foreach (Monster monster in storage.ReferenceStorage.WaveController.AllMonsters)
            {
                Reduce(monster);
            }

            PayMana();
        }

        private void UndoReduce()
        {
            foreach (Monster monster in reducedMonsters)
            {
                if(monster != null)
                {
                    if(!monster.Dead)
                    {
                        UndoReduce(monster);
                    }
                }
            }

            reducedMonsters.Clear();
        }

        private void UndoReduce(Monster monster)
        {
            monster.Data.armor.AddValue(armorReduction);
            monster.Data.fireResist.AddValue(magicArmorReduction);
            monster.Data.iceResist.AddValue(magicArmorReduction);
            monster.Data.poisonResist.AddValue(magicArmorReduction);
            monster.Data.lightingResist.AddValue(magicArmorReduction);
            monster.Data.waterResist.AddValue(magicArmorReduction);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Armor reduction", $"{armorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Magic armor reduction", $"{magicArmorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Duration", $"{duration}"));

            return data;
        }

        public override void FinishSpell()
        {
            UndoReduce();
            base.FinishSpell();
        }
    }
}
