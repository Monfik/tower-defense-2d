using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishersCollection : MonoBehaviour
{
    [SerializeField] private List<BaseInitable> finishers;
    public List<ILevelFinishable> finishables = new List<ILevelFinishable>();

    private void Start()
    {
        foreach(BaseInitable initable in finishers)
        {
            ILevelFinishable finishable = initable.GetComponent<ILevelFinishable>();
            if (finishable != null)
                finishables.Add(finishable);
        }
    }
}
