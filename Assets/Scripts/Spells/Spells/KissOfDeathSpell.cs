using SpriteGlow;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class KissOfDeathSpell : BaseSpell
    {
        [SerializeField] private SpriteRenderer crosshair;
        private SpriteGlowEffect glowEffect;
        private Monster raycastedMonster;
        [SerializeField] private MonsterBuffEffect additionalDamageBuff;

        [Header("Glow propertis")]
        [SerializeField] private float brightness = 2f;
        [SerializeField] private int outlineWidth = 2;

        public override void StartCast()
        {
            base.StartCast();
            crosshair.gameObject.SetActive(true);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic += CancelSpell;
            storage.EventsStorage.UtilEvents.OnMonsterRaycasted += MonsterRaycasted;
            storage.EventsStorage.UtilEvents.OnMonsterRaycastedStop += MonsterStopRaycasted;
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            storage.EventsStorage.UtilEvents.OnMonsterRaycasted -= MonsterRaycasted;
            storage.EventsStorage.UtilEvents.OnMonsterRaycastedStop -= MonsterStopRaycasted;
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            base.FinishSpell();        
        }

        public override void UpdateSpell()
        {
            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            crosshair.transform.position = mousePosition;

            if(Input.GetMouseButtonDown(1))
            {
                CancelSpell();
            }
            else if(Input.GetMouseButtonDown(0))
            {
                MouseLeftClicked();
            }
        }

        private void MonsterRaycasted(Monster monster)
        {
            raycastedMonster = monster;
            glowEffect =  monster.Renderer.gameObject.AddComponent<SpriteGlowEffect>();
            glowEffect.GlowBrightness = brightness;
            glowEffect.OutlineWidth = outlineWidth;
        }

        private void MonsterStopRaycasted(Monster monster)
        {
            Destroy(glowEffect);
            raycastedMonster = null;
        }

        private void MouseLeftClicked()
        {
            if(raycastedMonster != null)
            {
                MonsterBuffEffect addDmgBuff = Instantiate(additionalDamageBuff);
                raycastedMonster.Effects.AddEffect(addDmgBuff);
                MonsterStopRaycasted(raycastedMonster);
                PayMana();
                OnSpellCasted?.Invoke(this);
                FinishSpell();
            }
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Damage multiplier", $"{1 + additionalDamageBuff.BuffData.damageMultiplier}x"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Debuff duration", $"{additionalDamageBuff.duration}s"));

            return data;
        }

        public override void CancelSpell()
        {
            storage.EventsStorage.CoreEvnts.OnTowerPickedBasic -= CancelSpell;
            storage.EventsStorage.UtilEvents.OnMonsterRaycasted -= MonsterRaycasted;
            storage.EventsStorage.UtilEvents.OnMonsterRaycastedStop -= MonsterStopRaycasted;
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            base.CancelSpell();
        }
    }
}