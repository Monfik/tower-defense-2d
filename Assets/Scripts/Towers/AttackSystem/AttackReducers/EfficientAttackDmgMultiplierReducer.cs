using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class EfficientAttackDmgMultiplierReducer : BaseEfficientAttackReducer
    {
        [SerializeField] protected BaseAttackSystem attackSystem;
        [SerializeField] protected float damageReduceMultiplier;

        [SerializeField] protected EffectPreviewData previewData;
        private TowerEffectPreview effectPreview;
        [SerializeField] private bool addPreview = false;

        public override void Reduce(float efficient)
        {
            attackSystem.SetupDamageMultiplier(damageReduceMultiplier);

            if(addPreview)
            {
                effectPreview = tower.Effects.CreatePreview(previewData);
                tower.Effects.AddPreview(effectPreview, "Attack Reduce");
            }
        }

        public override void Restore()
        {
            if (addPreview)
            {
                tower.Effects.RemovePreview("Attack Reduce");
            }           
        }

        public void Refresh()
        {
            attackSystem.SetupDamageMultiplier(damageReduceMultiplier);
        }
    }
}
