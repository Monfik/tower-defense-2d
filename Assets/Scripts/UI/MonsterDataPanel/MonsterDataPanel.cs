using System.Collections;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Creatures
{
    public class MonsterDataPanel : MonoBehaviour
    {
        [SerializeField] private RectTransform rect;
        [SerializeField] private TMP_Text monsterName;
        [SerializeField] private Slider hpSlider; //filled type hp to setup fill 0-1
        [SerializeField] private TMP_Text hpText;
        [SerializeField] private Image monsterPreview;

        [SerializeField] private TipDescription hpTip;

        [SerializeField] private MonsterDataValueUI speedValueUI;
        [SerializeField] private List<MonsterDataValueUI> valuesUI;
        [Header("Movement")]
        [SerializeField] private Vector2 hidedPosition;
        [SerializeField] private Vector2 displayedPosition;

        [SerializeField] private float animationTime;
        [SerializeField] private float currentTime = 0f;

        private Vector2 targetPosition;
        private Vector2 startPosition;

        private Monster currentMonster;

        private void Start()
        {
            startPosition = hidedPosition;
            targetPosition = hidedPosition;
            currentTime = animationTime;
        }

        public void DisplayBasicData(Monster monster)
        {
            currentMonster = monster;

            monsterName.text = monster.Name;
            
            SetupHealthSlider(monster.HealthBar.CurrentValue, monster.HealthBar.MaxValue);
            monster.HealthBar.OnValueChangedMax += OnHealthChanged;

            monsterPreview.sprite = monster.Preview;

            SetupUIValue(speedValueUI, monster.Data.speed);
            SetupUIValue(GetValueUIByName("fireResist"), monster.Data.fireResist);
            SetupUIValue(GetValueUIByName("lightingResist"), monster.Data.lightingResist);
            SetupUIValue(GetValueUIByName("iceResist"), monster.Data.iceResist);
            SetupUIValue(GetValueUIByName("poisonResist"), monster.Data.poisonResist);
            SetupUIValue(GetValueUIByName("waterResist"), monster.Data.waterResist);

            DisplayPanel();
        }

        public void SetupHealthSlider(int currentHp, int maxHp)
        {
            hpSlider.minValue = 0f;
            hpSlider.maxValue = maxHp;
            hpSlider.value = currentHp;
            string hpDesctiption = currentHp.ToString() + " / " + maxHp.ToString();
            hpTip.SetDescription(CreateDescription(hpDesctiption));
            hpText.text = $"{currentHp}/{maxHp}";
        }

        private void OnHealthChanged(int currentHp, int maxHp)
        {
            hpSlider.value = currentHp;
            string hpDesctiption = currentHp.ToString() + " / " + maxHp.ToString();
            hpTip.SetDescription(CreateDescription(hpDesctiption));
            hpText.text = $"{currentHp}/{maxHp}";
        }

        private DescriptionData CreateDescription(string description)
        {
            DescriptionData data = new DescriptionData();
            data.simpleText = true;
            data.description = description;

            return data;
        }


        private void SetupUIValue(MonsterDataValueUI uiValue, MonsterStaticsData staticsData)
        {
            uiValue.SetupValue(staticsData.currentValue);
            staticsData.ValueChanged += uiValue.SetupValue;
        }

        private void DesetupUIValue(MonsterDataValueUI uiValue, MonsterStaticsData staticsData)
        {
            staticsData.ValueChanged -= uiValue.SetupValue;
        }

        private void CalculateCurrentTime()
        {
            float movementT = Mathf.InverseLerp(startPosition.x, targetPosition.x, rect.anchoredPosition.x);
            movementT = Mathfx.Lerp(0f, 1f, movementT);
            currentTime = Mathfx.Lerp(0f, animationTime, movementT);
        }

        private MonsterDataValueUI GetValueUIByName(string name)
        {
            MonsterDataValueUI valueUI = valuesUI.Find(c => c.ValueName.Equals(name));
            return valueUI;
        }

        public void UpdatePanel()
        {
            if (targetPosition == rect.anchoredPosition)
                return;

            currentTime += Time.unscaledDeltaTime;
            currentTime = Mathf.Clamp(currentTime, 0f, animationTime);
            float timeT = currentTime / animationTime;
            //timeT = Mathfx.Lerp(0f, 1f, timeT);
            Vector2 currentPosition = Vector2.Lerp(startPosition, targetPosition, timeT);
            rect.anchoredPosition = currentPosition;
        }


        [ContextMenu("Display panel")]
        public void DisplayPanel()
        {
            if (targetPosition == displayedPosition)
                return;

            startPosition = hidedPosition;
            targetPosition = displayedPosition;

            currentTime = animationTime - currentTime;

            //rect.anchoredPosition = displayedPosition;
        }

        [ContextMenu("Hide panel")]
        public void Hide()
        {
            if (targetPosition == hidedPosition)
                return;

            startPosition = displayedPosition;
            targetPosition = hidedPosition;

            currentTime = animationTime - currentTime;

            //rect.anchoredPosition = hidedPosition;

            currentMonster.HealthBar.OnValueChangedMax -= OnHealthChanged;
            DesetupUIValue(speedValueUI, currentMonster.Data.speed);
            DesetupUIValue(GetValueUIByName("fireResist"), currentMonster.Data.fireResist);
            DesetupUIValue(GetValueUIByName("lightingResist"), currentMonster.Data.lightingResist);
            DesetupUIValue(GetValueUIByName("iceResist"), currentMonster.Data.iceResist);
            DesetupUIValue(GetValueUIByName("poisonResist"), currentMonster.Data.poisonResist);
            DesetupUIValue(GetValueUIByName("waterResist"), currentMonster.Data.waterResist);

            currentMonster = null;
        }
    }
}
