using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public class AnimatorOverrider : MonoBehaviour
    {
        private Animator animator;
        public AnimatorOverrideController overrideCOntroller;

        private void Start()
        {
            animator = GetComponentInChildren<Animator>();

            if (!animator)
            {
                Debug.LogError("There is no animator in childrens");
                return;
            }

            animator.runtimeAnimatorController = overrideCOntroller;
        }
    }
}
