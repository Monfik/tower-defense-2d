using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace TowerDefense.Tips
{
    public class SpecifiedTipDataRepresentation : MonoBehaviour
    {
        [SerializeField] private TMP_Text title;
        [SerializeField] private TMP_Text value;

        public void Setup(DescriptionSingleSpecifiedData data)
        {
            title.text = data.description;
            value.text = data.value;
        }
    }
}
