using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

namespace TowerDefense.Blueprints
{
    public class Blueprint : BaseInitable, IVisitable
    {
        [SerializeField] private List<LocationCheckpoint> checkpoints;
        public List<LocationCheckpoint> Checkpoints => checkpoints;
        private Level currentLevel;

        private LocationCheckpoint checkpointToFinish;
        List<LocationCheckpoint> locationsToUnlock = new List<LocationCheckpoint>();

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
        }

        public void CheckForUnlockCheckpoints(int level)
        {
            locationsToUnlock = checkpoints.FindAll(c => c.CanBeUnlocked() == true);
        }

        public void FinishLevel(Level level)
        {
            checkpointToFinish = checkpoints.Find(c => c.Level == level);
            CheckForUnlockCheckpoints(Values.levelNumber);
        }

        #region IUnlockable
        public void SetState(LocationCheckpoint.LocationState state)
        {
            
        }

        public bool CanBeUnlocked()
        {
            throw new System.NotImplementedException();
        }
        #endregion

        public void OnCheckpointSelected(LocationCheckpoint checkpoint)
        {
            currentLevel = checkpoint.Level;
            checkpoint.Button.interactable = false;
            storage.ReferenceStorage.LevelManager.LoadLevel(currentLevel);
        }

        public void Deinit()
        {

        }

        public void Display()
        {
            gameObject.SetActive(true);

            if(checkpointToFinish != null)
            {
                checkpointToFinish.SetState(LocationCheckpoint.LocationState.Finished);
                checkpointToFinish = null;
            }

            if(locationsToUnlock.Count > 0)
            {
                locationsToUnlock.ForEach(c => c.SetState(LocationCheckpoint.LocationState.Open));
                locationsToUnlock.ForEach(c => c.AssignSelectCallback(OnCheckpointSelected));
                locationsToUnlock.Clear();
            }
        }

        public void PickRandom(int index)
        {
            OnCheckpointSelected(checkpoints[index]);
        }

        public void ResetBlueprint()
        {
            foreach(LocationCheckpoint checkpoint in checkpoints)
            {
                checkpoint.SetState(LocationCheckpoint.LocationState.Closed);
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        [ContextMenu("Collect checkpoints in children")]
        private void CollectCheckpointsInChildren()
        {
            checkpoints = gameObject.GetComponentsInChildren<LocationCheckpoint>().ToList();
        }
    }
}
