using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Feature
{
    public abstract class BaseInfoMark : MonoBehaviour
    {
        [SerializeField] protected CanvasGroup group;
        [SerializeField] protected TextMeshProUGUI messageText;
        [SerializeField] protected RectTransform rect;

        [SerializeField] protected AnimationCurve alphaCurve;
        [SerializeField] protected AnimationCurve movementCurve;

        [SerializeField] protected float targetHeightMultiplier;

        protected Vector3 currentWorldPosition;
        protected Vector3 worldPositionDelta;
        protected Vector3 worldTargetPosition;

        protected Vector2 screenStartPosition;
        protected Vector2 screenTargetPosition;
        protected UnityAction<BaseInfoMark> OnMarkFinished;

        protected float existingTime;
        protected float currentTime;

        public abstract void UpdateMark(Camera camera);

        public void Clear()
        {
            rect.position = Vector2.zero;
        }

        public void SetText(string text)
        {
            messageText.text = text;
        }

        public void SetTransparency(float a)
        {
            group.alpha = a;
        }

        public float GetTransparency()
        {
            return group.alpha;
        }
    }
}
