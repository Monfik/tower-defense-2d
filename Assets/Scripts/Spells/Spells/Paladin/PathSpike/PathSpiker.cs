using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Spells
{
    [System.Serializable]
    public class PathSpiker : BaseUpdatableMonoless
    {
        private SpikesData spikesData;
        private PathCreator path;
        [SerializeField] private Pool<Spike> spikePool1;
        [SerializeField] private Pool<Spike> spikePool2;

        private Queue<Pool<Spike>> spikePoolsQueue = new Queue<Pool<Spike>>();

        private Pool<Spike> currentPool;
        private List<Spike> currentSpikes = new List<Spike>();

        private float currentPathDistance = 0f;

        public void Init(SpikesData spikesData, PathCreator path)
        {
            this.spikesData = spikesData;
            this.path = path;
            spikePoolsQueue.Enqueue(spikePool1);
            spikePoolsQueue.Enqueue(spikePool2);
        }

        public override void UpdateUpdatable()
        {
            UpdateSpawnInterval();
            UpdatePathDistance(); 
        }

        private void UpdateSpawnInterval()
        {
            currentTime += Time.deltaTime;
            if(currentTime >= spikesData.spikeSpawnInterval)
            {
                Vector2 pathPosition = path.path.GetPointAtDistance(currentPathDistance);
                SpawnSpike(pathPosition);
                currentTime = 0f;
            }
        }

        private void UpdatePathDistance()
        {
            currentPathDistance += Time.deltaTime * spikesData.speed;
            if (currentPathDistance >= path.path.length) 
            {
                Finish();
            }
        }

        public void SpawnSpike(Vector2 position)
        {
            Pool<Spike> currentPool = spikePoolsQueue.Dequeue();
            Spike spike = currentPool.GetElement();
            spike.Play(DealDamage, sa => OnSpikeHitFinished(spike));
            spike.poolOwner = currentPool;
            spike.transform.position = position;
            currentSpikes.Add(spike);
            spikePoolsQueue.Enqueue(currentPool);
        }

        private void OnSpikeHitFinished(Spike spike)
        {
            currentSpikes.Remove(spike);
            GameObject.Destroy(spike.gameObject);
        }

        private void DealDamage(IDestructable destructable)
        {
            int damage = DamageCalculator.CalculateDamage(spikesData.attackData, destructable.GetData().armor.currentValue);
            destructable.TakeDamage(damage, DamageType.Physics);
        }
    }
}
