using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LevelLogic.Respawns
{
    public class GlowRespawnerAnimator : RespawnPlaceAnimator
    {
        [SerializeField] private float duration;
        [SerializeField] private AnimationCurve intensityCurve;
        [SerializeField] [Range(0, 10)] private float minIntensity;
        [SerializeField] [Range(0, 10)] private float maxIntensity;
        [SerializeField] [Range(0, 10)] private float speed = 1f;
        [SerializeField] private List<SpriteRenderer> animatedRenderers;

        private float currentTime = 0f;
        [SerializeField] private bool updating = false;

        private float startIntensity;

        [ContextMenu("Animate")]
        public override void Animate()
        {
            currentTime = 0f;
            updating = true;
        }

        private void Update()
        {
            if (!updating)
                return;

            currentTime += Time.deltaTime * speed;
            float timeT = currentTime / duration;
            float intensityT = intensityCurve.Evaluate(timeT);
            float intensity = Mathf.Lerp(minIntensity, maxIntensity, intensityT);
            animatedRenderers.ForEach(c => c.material.SetFloat("_Intensity", intensity));

            if (currentTime >= duration)
            {
                Finish();
            }
        }

        private void Finish()
        {
            animatedRenderers.ForEach(c => c.material.SetFloat("_Intensity", startIntensity));
            updating = false;
        }
    }
}
