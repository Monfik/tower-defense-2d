using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Power;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Towers
{
    public class MagicDamageAttackSystem : BaseAttackSystem, IGUIComposable
    {
        [Space(15)]
        [SerializeField] private Bullet bulletPrefab;

        [SerializeField] private List<Bullet> bullets;

        [Header("Data")]
        [SerializeField]
        private MagicBulletData missileData;
        public MagicBulletData MissileData => missileData;

        [SerializeField] private float delayBulletTime = 0f;

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            missileData.Reset();
        }

        public override void Attack()
        {
            foreach (AttackableTowerEntity entity in attackableTower.GraphicsSystem.GraphicsData.attackEntities)
            {
                DOVirtual.DelayedCall(delayBulletTime, () => SpawnBullet(entity.AttackPoint.position), false);
                //SpawnBullet(entity.AttackPoint.position);
                entity.PlayAttackAnimation();
            }

            foreach (AttackableTowerEntity animableEntity in attackableTower.GraphicsSystem.GraphicsData.attackableAnimators)
            {
                animableEntity.PlayAttackAnimation();
            }

            DamageItself();
        }

        private void SpawnBullet(Vector3 position)
        {
            if (target == null)
                return;

            Bullet spawnedBullet = Instantiate(bulletPrefab);
            spawnedBullet.Init(target, JsonUtility.ToJson(missileData), OnBulletHitted);
            spawnedBullet.gameObject.SetActive(true);
            spawnedBullet.transform.position = position;
            bullets.Add(spawnedBullet);
        }

        public override void UpdateAttackSystem()
        {
            base.UpdateAttackSystem();

            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].UpdateBullet();
            }
        }

        private void OnBulletHitted(Bullet bullet)
        {
            bullets.Remove(bullet);
            GameObject.Destroy(bullet.gameObject);
        }

        public override void Upgrade(int level)
        {
            base.Upgrade(level);
            missileData.Upgrade(level);
        }

        protected override void OnTargetNotReachable(Monster monster)
        {
            for (int i = bullets.Count - 1; i >= 0; i--)
            {
                if (bullets[i].Target == monster)
                { 
                    Destroy(bullets[i].gameObject);
                    bullets.RemoveAt(i);
                }
            }

            base.OnTargetNotReachable(monster);
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            int attackEntities = attackableTower.GraphicsSystem.GraphicsData.attackEntities.Count;

            foreach (MagicDamageData magicData in missileData.magicDamages)
            {
                TowerBasicGUIText damageModel = infoPanel.BasicTextes.GetElement();
                damageModel.Text.text = magicData.magicType.ToString() + " damage: " + magicData.minDamage.data.GetGUILabel(attackEntities) + " - " + magicData.maxDamage.data.GetGUILabel(attackEntities);

                TowerBasicGUIText magicResistanceReductionModel = infoPanel.BasicTextes.GetElement();
                magicResistanceReductionModel.Text.text = magicData.magicType.ToString() + " resist reduction: " + magicData.magicArmorReduction.data.GetGUILabel();

                UnityAction DamageUpdated = () => { damageModel.Text.text = magicData.magicType.ToString() + " damage: " + magicData.minDamage.data.GetGUILabel(attackEntities) + " - " + magicData.maxDamage.data.GetGUILabel(attackEntities); };
                UnityAction ResistanceReductionUpdated = () => { magicResistanceReductionModel.Text.text = magicData.magicType.ToString() + " resist reduction: " + magicData.magicArmorReduction.data.GetGUILabel(); };
                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, magicData.minDamage));
                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, magicData.maxDamage));
                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ResistanceReductionUpdated, magicData.magicArmorReduction));
            }

            TowerBasicGUIText attackSpeedModel = infoPanel.BasicTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();

            UnityAction AttackSpeedUpdated = () => { attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            int attackEntities = attackableTower.GraphicsSystem.GraphicsData.attackEntities.Count;
            int upgradeAttackEntities = attackableTower.GraphicsSystem.GetLevelGraphicsData(attackableTower.Builder.TowerLevel + 1).attackEntities.Count;

            foreach (MagicDamageData magicData in missileData.magicDamages)
            {
                TowerUpgradeGuiText damageModel = infoPanel.UpgradeTextes.GetElement();
                damageModel.Text.text = magicData.magicType.ToString() + " damage: " + magicData.minDamage.data.GetGUILabel(attackEntities) + " - " + magicData.maxDamage.data.GetGUILabel(attackEntities);
                damageModel.UpgradeText.text = magicData.minDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities) + " - " +
                    magicData.maxDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities);

                TowerUpgradeGuiText magicResistanceReductionModel = infoPanel.UpgradeTextes.GetElement();
                magicResistanceReductionModel.Text.text = magicData.magicType.ToString() + " resist reduction: " + magicData.magicArmorReduction.data.GetGUILabel();
                magicResistanceReductionModel.UpgradeText.text = magicData.magicArmorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

                UnityAction DamageUpdated = () => {
                    damageModel.Text.text = magicData.magicType.ToString() + " damage: " + magicData.minDamage.data.GetGUILabel(attackEntities) + " - " + magicData.maxDamage.data.GetGUILabel(attackEntities);
                    damageModel.UpgradeText.text = magicData.minDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities) + " - " +
                    magicData.maxDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel(upgradeAttackEntities);
                    };
                UnityAction ResistanceReductionUpdated = () => {
                    magicResistanceReductionModel.Text.text = magicData.magicType.ToString() + " resist reduction: " + magicData.magicArmorReduction.data.GetGUILabel();
                    magicResistanceReductionModel.UpgradeText.text = magicData.magicArmorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
                };

                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, magicData.minDamage));
                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, magicData.maxDamage));
                attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ResistanceReductionUpdated, magicData.magicArmorReduction));
            }

            TowerUpgradeGuiText attackSpeedModel = infoPanel.UpgradeTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
            attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

            UnityAction AttackSpeedUpdated = () => {
                attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
                attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
        }

        public override void OnTowerDemolished()
        {
            foreach (TargetableBullet bullet in bullets)
            {
                Destroy(bullet.gameObject);
            }

            bullets.Clear();
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            foreach (MagicDamageData bulletData in missileData.magicDamages)
            {
                int minDmgBuff = (int)Mathf.Round(bulletData.minDamage.data.coreValue * multiplier);
                bulletData.minDamage.data.BuffByValue(minDmgBuff);
                int maxDmgBuff = (int)Mathf.Round(bulletData.maxDamage.data.coreValue * multiplier);
                bulletData.maxDamage.data.BuffByValue(maxDmgBuff);
            }

            UnityAction OnDebuff = () =>
            {
                foreach (MagicDamageData bulletData in missileData.magicDamages)
                {
                    int buffValue = (int)Mathf.Round(bulletData.minDamage.data.coreValue * multiplier);
                    bulletData.minDamage.data.BuffByValue(-buffValue);
                    buffValue = (int)Mathf.Round(bulletData.maxDamage.data.coreValue * multiplier);
                    bulletData.maxDamage.data.BuffByValue(-buffValue);

                }
            };

            return OnDebuff;
        }

        public void AddMagicDamage(MagicDamageData damage)
        {
            missileData.magicDamages.Add(damage);
            attackReducer?.currentReducer.Refresh();
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            foreach (MagicDamageData magicAttackData in missileData.magicDamages)
            {
                magicAttackData.minDamage.data.SetupMultiplier(multiplier);
                magicAttackData.maxDamage.data.SetupMultiplier(multiplier);
            }

            foreach (MagicDamageData magicAttackData in missileData.magicDamages)
            {
                for (int i = 0; i < magicAttackData.minDamage.upgrades.Count; i++)
                {
                    TowerStaticsData upgradeMinData = magicAttackData.minDamage.upgrades[i];
                    upgradeMinData.SetupMultiplier(multiplier);

                    TowerStaticsData upgradeMaxData = magicAttackData.maxDamage.upgrades[i];
                    upgradeMaxData.SetupMultiplier(multiplier);
                }
            }

            foreach (MagicDamageData magicAttackData in missileData.magicDamages)
            {
                attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(magicAttackData.minDamage);
                attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(magicAttackData.maxDamage);
            }
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            data = base.CreateTooltipData(data);

            data.title2 = "Magic targetable damage";

            foreach (MagicDamageData damageData in missileData.magicDamages)
            {
                string type = damageData.magicType.ToString();
                string damage = damageData.minDamage.data.coreValue.ToString() + " - " + damageData.maxDamage.data.coreValue.ToString();
                data.title2 = "Magic targetable damage";
                
                data.specifiedDatas.Add(new DescriptionSingleSpecifiedData($"<sprite={(int)damageData.magicType}>{type}", damage));
                data.specifiedDatas.Add(new DescriptionSingleSpecifiedData($"{type} reduction", damageData.magicArmorReduction.data.coreValue.ToString()));
            }

            //MagicMissile missile = bulletPrefab as MagicMissile;
            //if(missile != null)
            //{
            //    foreach (BaseMonsterEffect monsterEffect in missile.MagicEffects)
            //    {
            //        str += monsterEffect.CreateTooltipData(data) + "\n";
            //    }
            //}

            return data;
        }
    }

    [System.Serializable]
    public class MagicBulletData
    {
        public List<MagicDamageData> magicDamages;

        public void Reset()
        {
            foreach (MagicDamageData data in magicDamages)
            {
                data.minDamage.Reset();
                data.maxDamage.Reset();
                data.magicArmorReduction.Reset();
            }
        }

        public void Upgrade(int level)
        {
            foreach (MagicDamageData data in magicDamages)
            {
                data.minDamage.Upgrade(level);
                data.maxDamage.Upgrade(level);
                data.magicArmorReduction.Upgrade(level);
            }
        }
    }

    [System.Serializable]
    public class MagicDamageData
    {
        public MagicType magicType;
        public UpgradableStaticsData minDamage;
        public UpgradableStaticsData maxDamage;
        public UpgradableStaticsData magicArmorReduction;

        public MagicDamageData(MagicAttackData otherData)
        {
            magicType = otherData.magicType;
            minDamage = new UpgradableStaticsData(otherData.minDmg);
            maxDamage = new UpgradableStaticsData(otherData.maxDmg);
            magicArmorReduction = new UpgradableStaticsData(otherData.magicArmorReduction);
        }
    }
}
