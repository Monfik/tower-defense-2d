using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class LevelsContainer
    {
        public List<Level> levels;
    }
}
