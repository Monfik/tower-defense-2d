using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public class SpriteMerger : MonoBehaviour
    {
        [SerializeField] private List<Sprite> spritesToMerge;
        [SerializeField] private SpriteRenderer finalSpriteRenderer;

        [ContextMenu("Merge")]
        public void Merge()
        {
            Resources.UnloadUnusedAssets();

            var newTex = new Texture2D(600, 600);
            for(int i=0; i<newTex.width; i++)
            {
                for(int j=0; j<newTex.height; j++)
                {
                    newTex.SetPixel(i, j, new Color(1, 1, 1, 0));
                }
            }

            for(int i=0; i<spritesToMerge.Count; i++)
            {
                for(int x=0; x<spritesToMerge[i].texture.width; x++)
                {
                    for(int y=0; y<spritesToMerge[i].texture.width; y++)
                    {
                        var color = spritesToMerge[i].texture.GetPixel(x, y).a == 0 ?
                            newTex.GetPixel(x, y) : spritesToMerge[i].texture.GetPixel(x, y);

                        newTex.SetPixel(x, y, color);                        
                    }
                }
            }

            newTex.Apply();
            var finalSprite = Sprite.Create(newTex, new Rect(0, 0, newTex.width, newTex.height), new Vector2(0.5f, 0.5f));
            finalSprite.name = "New kurwa";
            finalSpriteRenderer.sprite = finalSprite;
        }
    }
}
