using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class TowerPlacementSpell : BaseSpell
    {
        [SerializeField] private Tower towerToPlace;

        public override void StartCast()
        {
            OnSpellStartCasting?.Invoke(this);
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined += OnTowerPreviewDeclined;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.CallOnTowerPicked(towerToPlace);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
        }

        public override void UpdateSpell()
        {

        }

        private void OnTowerBuilded(Tower tower)
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            OnSpellCasted?.Invoke(this);
            PayMana();
            FinishSpell();
        }

        private void OnTowerPreviewDeclined(Tower tower)
        {
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined -= OnTowerPreviewDeclined;
            CancelSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TemporaryAttackableTower attackTower = towerToPlace as TemporaryAttackableTower;
            MagicDamageAttackSystem attackSystem = attackTower.AttackSystem as MagicDamageAttackSystem;
            MagicDamageData damageData = attackSystem.MissileData.magicDamages[0];

            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Tower range", $"{attackTower.range.data.coreValue}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData($"<sprite={(int)damageData.magicType}>" + damageData.magicType.ToString(), $"{damageData.minDamage.data.coreValue} - {damageData.maxDamage.data.coreValue}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData(damageData.magicType.ToString() + " reduction", $"{damageData.magicArmorReduction.data.coreValue}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Tower exist duration", $"{attackTower.ExistTime}s"));

            return data;
        }

        public override void CancelSpell()
        {
            storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted();
            base.CancelSpell();
        }

        public override void FinishSpell()
        {
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerPreviewDeclined -= OnTowerPreviewDeclined;
            base.FinishSpell();
        }
    }
}
