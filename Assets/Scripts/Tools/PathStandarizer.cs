using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathStandarizer : MonoBehaviour
{
    [Range(0, 10)] public float maxError;
    [Range(0, 1)] public float minSpacing;

    [ContextMenu("Standarize")]
    public void Standarize()
    {
        PathCreator[] pathes = GameObject.FindObjectsOfType<PathCreator>(true);

        foreach(PathCreator path in pathes)
        {
            path.EditorData.vertexPathMaxAngleError = maxError;
            path.EditorData.vertexPathMinVertexSpacing = minSpacing;
            path.EditorData.VertexPathSettingsChanged();
        }
    }
}
