using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public enum MagicType { Lighting, Fire, Ice, Poison, Water, All, Physics};
}
