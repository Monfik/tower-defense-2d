using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class AttackSystemDisableReducer : EfficientAttackDmgMultiplierReducer
    {
        public override void Reduce(float efficient)
        {
            base.Reduce(efficient);
            attackSystem.SwitchToIdleState();
        }

        public override void Restore()
        {
            base.Restore();
            attackSystem.SwitchToNormalState();
        }
    }
}
