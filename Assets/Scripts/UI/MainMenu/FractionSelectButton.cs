using System.Collections;
using System.Collections.Generic;
using TowerDefense.Fractions;
using UnityEngine;
using UnityEngine.UI;

namespace CoreUI
{
    public class FractionSelectButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;
        public Button Button => button;

        [SerializeField] private Fraction fraction;
        public Fraction Fraction => fraction;

        [SerializeField] private GameObject selectedButtonFrame;

        public void EnableButton(bool enabled)
        {
            button.interactable = enabled;
            selectedButtonFrame.SetActive(!enabled);
        }
    }
}
