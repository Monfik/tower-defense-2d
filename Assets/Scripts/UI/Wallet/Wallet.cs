using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace CoreUI
{
    public class Wallet : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text text;

        [SerializeField]
        private RectTransform rect;

        [Header("Animation gold change")]
        [SerializeField] private RectTransform animationsParent;
        [SerializeField] private WalletGoldAnimationEntity goldAnimEntityPrefab;
        [SerializeField] private List<WalletGoldAnimationEntity> goldAnimEntities;

        public void SetGold(int gold)
        {
            Values.gold = gold;
            text.text = gold.ToString();
        }

        public void Subtract(int gold)
        {
            int goldValue = Values.gold - gold;
            if (goldValue < 0)
                goldValue = 0;

            SetGold(goldValue);
            CreateGoldChangeAnimation("Down");
        }
        
        public void Add(int gold)
        {
            SetGold(Values.gold + gold);
            CreateGoldChangeAnimation("Up");
        }

        private void CreateGoldChangeAnimation(string animationName)
        {
            WalletGoldAnimationEntity goldAnimation = Instantiate(goldAnimEntityPrefab, animationsParent);
            goldAnimation.gameObject.SetActive(true);
            goldAnimation.transform.localScale = Vector3.one;
            goldAnimEntities.Add(goldAnimation);
            goldAnimation.StartAnimation(GoldChangeAnimationFinished, animationName);
        }

        private void GoldChangeAnimationFinished(WalletGoldAnimationEntity goldAnimation)
        {
            goldAnimEntities.Remove(goldAnimation);
            Destroy(goldAnimation.gameObject);
        }

        [ContextMenu("Add some gold")]
        private void AddTestGold()
        {
            Add(5);
        }

        [ContextMenu("Remove some gold")]
        private void RemoveTestGold()
        {
            Subtract(6);
        }
    }
}
