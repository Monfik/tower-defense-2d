using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class WizardSpeedBuff : BaseBossState<WizardBossBehaviour>
    {
        [SerializeField] private IntervalData pathInterval;
        
        [Header("Speed Buff")]
        [SerializeField] private MonsterBuffEffect speedBuff;
        [SerializeField] private Transform buffEffectPoint; //spawnedOnRodPoint
        [SerializeField] private ParticleSystem buffParticles;

        public override void PreInit(WizardBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(WizardBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("SpeedBuff");
        }

        private void Buff(List<Monster> monsters, MonsterBuffEffect buff)
        {
            foreach(Monster monster in monsters)
            {
                Buff(monster, buff);
            }
        }

        private void Buff(Monster monster, MonsterBuffEffect buff)
        {
            MonsterBuffEffect speedBuff = Instantiate(buff, monster.transform);
            monster.Effects.AddEffect(speedBuff);
        }

        //Called from animator
        public void BuffFinished()
        {
            List<Monster> currentMonsters = storage.ReferenceStorage.WaveController.AllMonsters;
            currentMonsters.Remove(boss);
            Buff(currentMonsters, speedBuff);

            CreateBuffParticleEffect();

            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
            boss.SetDefaultMovementState();
        }

        private void CreateBuffParticleEffect()
        {
            ParticleSystem buffParticlesEffect = Instantiate(buffParticles, boss.transform);
            buffParticlesEffect.gameObject.SetActive(true);
            buffParticlesEffect.transform.position = buffEffectPoint.position;
        }

        public override void Deinit()
        {
            
        }

        public override void UpdateState()
        {
            
        }

        public override bool IsReadyForWork()
        {
            return (boss.DistanceTravelled >= pathInterval.nextValue && storage.ReferenceStorage.WaveController.AllMonsters.Count > 1);
        }
    }
}
