using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Invokers
{
    public interface ICommandable
    {
        void InitCommand(DataStorage storage, UnityAction OnCommandFinished, UnityAction OnCommandFailed);
        void UpdateCommand();
        void FinishCommand();
    }
}
