using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

public class TerrainRemover : IUpdatable
{
    public List<GameObject> terrains = new List<GameObject>();

    public void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
    {
        storage.EventsStorage.CoreEvnts.OnLevelUnloaded += lev => { Destroy(); } ;
    }

    public void UpdateUpdatable()
    {

    }

    ~TerrainRemover()
    {
        
    }

    public void Destroy()
    {
        for (int i = terrains.Count - 1; i >= 0; i--)
        {
            GameObject.Destroy(terrains[i]);
        }

    }
}
