using Data;
using GlobalEvents;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class GlobalEventsController : BaseInitable
    {
        private EventContainer currentEvents;
        public EventContainer CurrentEvents => currentEvents;

        private EventContainer levelEvents;
        public EventContainer LevelEvents => levelEvents;

        private EventContainer blueprintEvents;
        public EventContainer BlueprintEvents => blueprintEvents;

        [Space]
        [Header("DEV")]
        [SerializeField] private BaseGlobalEvent testEvent;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            currentEvents = new EventContainer();
            levelEvents = new EventContainer();
            blueprintEvents = new EventContainer();
            storage.EventsStorage.CoreEvnts.OnLevelUnloaded += OnLevelFinished;
        }

        public void UpdateController()
        {
            for(int i=0; i< currentEvents.Events.Count; i++)
            {
                currentEvents.Events[i].UpdateEvent();
            }
        }

        public void BeginLevelEvent(IGlobalEvent globalEvent)
        {
            levelEvents.Events.Add(globalEvent);
            currentEvents.Events.Add(globalEvent);
            globalEvent.BeginEvent(storage, OnEventFinished);
        }

        private void OnEventFinished(IGlobalEvent globalEvent)
        {
            currentEvents.Events.Remove(globalEvent);
            //yee thats shit
            bool existsInLevel = levelEvents.Events.Exists(c => c == globalEvent);
            if(existsInLevel)
            {
                levelEvents.Events.Remove(globalEvent);
                levelEvents.OnEventFinished?.Invoke(globalEvent);
            }
            bool existsInBlueprint = blueprintEvents.Events.Exists(c => c == globalEvent);
            if(existsInBlueprint)
            {
                blueprintEvents.Events.Remove(globalEvent);
                blueprintEvents.OnEventFinished?.Invoke(globalEvent);
            }

            MonoBehaviour monoEvent = globalEvent as MonoBehaviour;
            if(monoEvent != null)
            {
                Destroy(monoEvent.gameObject);
            }
        }

        private void OnLevelFinished(Level level)
        {
            if(levelEvents.Events.Count > 0)
            {
                LevelEventFinisher finisher = new LevelEventFinisher(levelEvents);
                storage.ReferenceStorage.LevelManager.LevelFinisher.AddFinisher(finisher);
                FinishAllLevelEvents();
            }
        }

        public void FinishAllLevelEvents()
        {
            levelEvents.Events.ForEach(c => c.StartToFinishEvent());
        }


        [ContextMenu("Start dev event")]
        private void StartDevEvent()
        {
            BeginLevelEvent(testEvent);
        }

        public override void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnLevelUnloaded -= OnLevelFinished;
        }
    }

    public class EventContainer
    {
        private List<IGlobalEvent> events = new List<IGlobalEvent>();
        public List<IGlobalEvent> Events => events;

        public UnityAction<IGlobalEvent> OnEventFinished;
    }
}