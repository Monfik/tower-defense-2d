using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Spells
{
    public class PoisonRainSpell : BaseSpell
    {
        [SerializeField] private SpriteRenderer rainPreview;
        //[SerializeField] private Highlight rainHighlight;
        [SerializeField] private PoisonRain rainPrefab;
        [SerializeField] private PoisonRainData rainData;

        [Header("Colors")]
        [SerializeField] private Color possibleCastColor;
        [SerializeField] private Color unpossibleCastColor;
        private PathSnapper snapper;

        public override void StartCast()
        {
            base.StartCast();
            rainPreview.gameObject.SetActive(true);
            storage.EventsStorage.SpellEvents.CallOnUsableSpellStartedCast();
            snapper = storage.ReferenceStorage.PathSnapper;
        }

        public override void UpdateSpell()
        {
            if (Input.GetMouseButtonUp(1))
            {
                CancelSpell();
                return;
            }

            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);

            Vector2 snappedPosition;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);

            if (snapped)
            {
                rainPreview.transform.position = snappedPosition;
                rainPreview.material.SetColor("_Color", possibleCastColor);
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (Values.InputValues.POINTER_OVER_UI)
                        return;

                    SpawnRain(snappedPosition);
                }
            }
            else
            {
                rainPreview.material.SetColor("_Color", unpossibleCastColor);
                rainPreview.transform.position = mousePosition;
            }
        }

        private void SpawnRain(Vector2 position)
        {
            PoisonRain rain = Instantiate(rainPrefab);
            storage.EventsStorage.CoreEvnts.OnMapEffectAppeared(rain);
            rain.Rain(rainData, position);
            OnSpellCasted?.Invoke(this);
            PayMana();
            FinishSpell();
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=3>Poison per second", $"{rainData.poisonData.damagePerSecond}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Poison reduction", $"{rainData.poisonData.poisonArmorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Rain duration", $"{rainData.duration }"));

            return data;
        }

        public override void CancelSpell()
        {
            base.CancelSpell();
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
        }

        public override void FinishSpell()
        {
            storage.ReferenceStorage.Postponer.InvokePostponeAction(storage.EventsStorage.SpellEvents.CallOnUsableSpellCasted);
            base.FinishSpell();
        }
    }

    [System.Serializable]
    public class PoisonRainData
    {
        public float poisonHitInterval;
        public float hitRadius;
        public float duration;
        public PoisonData poisonData;
    }

    [System.Serializable]
    public class PoisonData
    {
        public int damagePerSecond;
        public int poisonArmorReduction;
        public int duration;
    }

}

