using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Dynamic
{
    public class DestroyOnAnimationFinish : MonoBehaviour
    {
        [SerializeField] private SpriteAnimationv2 animationComponent;

        private void Start()
        {
            animationComponent = GetComponent<SpriteAnimationv2>();
            animationComponent.PlayAnimation(OnFinish);
        }

        private void OnFinish(SpriteAnimationv2 anim)
        {
            Destroy(gameObject);
        }
    }
}
