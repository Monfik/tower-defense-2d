using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace TowerDefense.GlobalEvents
{
    [CustomEditor(typeof(GlobalEventsTrigger))]
    public class GlobalEventsTriggerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GlobalEventsTrigger trigger = target as GlobalEventsTrigger;

            EditorGUILayout.Space(40);
            if (GUILayout.Button("Fire night"))
            {
                trigger.TriggerEvent(trigger.night);
            }

            if (GUILayout.Button("Fire wind"))
            {
                trigger.TriggerEvent(trigger.wind);
            }
        }
    }
}
#endif
