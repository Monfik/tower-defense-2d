using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Towers;
using UnityEngine;
using static UnityEngine.ParticleSystem;

namespace TowerDefense.GlobalEvents
{
    public class Sandstorm : MonoBehaviour
    {
        public ParticleSystem sand;
        public ParticleSystem dust;

        [SerializeField] private RangeFloat sandStrengthRange;
        [SerializeField] private float sandCoverDurationThreshold;

        [SerializeField] new private BoxCollider2D collider2D;
        [SerializeField] private List<TowerSandstormHandler> towerHandlers;

        [SerializeField] private SandDebuffEffect sandDebuffEffect;

        public void SetSize(Vector2 size)
        {
            SetupSandstormParticles(sand, size);
            SetupSandstormParticles(dust, size);

            collider2D.size = size;
        }

        public void SetupSandstormParticles(ParticleSystem particles, Vector2 mapSize)
        {
            ShapeModule shapeModule = particles.shape;
            shapeModule.scale = mapSize;
        }

        public void UpdateSandstorm()
        {
            foreach(TowerSandstormHandler handler in towerHandlers)
            {
                handler.currentTime += Time.deltaTime;
                if(handler.currentTime >= sandCoverDurationThreshold)
                {
                    float sandRandomStrength = sandStrengthRange.GetRandom();

                    BaseTowerEffect effect = handler.tower.Effects.FindById(sandDebuffEffect.Id);
                    if (effect == null)
                    {
                        SandDebuffEffect debuff = new SandDebuffEffect(sandDebuffEffect);
                        debuff.timable = false;
                        handler.tower.Effects.AddEffect(debuff);
                        debuff.AddSand(sandRandomStrength);
                        return;
                    }
                    else
                    {
                        SandDebuffEffect sandEffect = effect as SandDebuffEffect;
                        if (sandEffect == null)
                        {
                            Debug.LogError("Dear god, why its null...");
                            return;
                        }
                        else
                        {
                            sandEffect.AddSand(sandRandomStrength);
                        }
                    }

                    handler.currentTime = 0f;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Tower tower = collision.GetComponent<Tower>();
            if(tower != null)
            {
                TowerSandstormHandler newHandler = new TowerSandstormHandler();
                newHandler.tower = tower;
                towerHandlers.Add(newHandler);

                SandDebuffEffect debuff = new SandDebuffEffect(sandDebuffEffect);
                debuff.timable = false;
                tower.Effects.AddEffect(debuff);
                debuff.AddSand(sandStrengthRange.GetRandom() / 2f);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            Tower tower = collision.GetComponent<Tower>();
            if (tower != null)
            {
                TowerSandstormHandler handler = towerHandlers.Find(c => c.tower == tower);
                if (handler != null)
                    towerHandlers.Remove(handler);
            }
        }
    }

    [System.Serializable]
    public class TowerSandstormHandler
    {
        public Tower tower;
        public float currentTime = 0f;
    }
}
