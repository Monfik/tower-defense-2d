using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace TowerDefense.Creatures
{
    public class DruidBossSpawnBeast : BaseBossState<DruidBossBehaviour>
    {
        [SerializeField] private float spawningDuration = 2f;

        [SerializeField] private List<MonsterSpawnData> monstersToSpawn;

        [SerializeField] private RangeFloat distanceLengthDiffRange;
        [SerializeField] private SpriteRenderer spawnPointAnimation;

        private MonsterSpawnData currentSpawning = new MonsterSpawnData();

        public override void Init(DruidBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("MonsterSpawn");
            StartSpawnMonster();
        }

        private void StartSpawnMonster()
        {
            MonsterSpawnData randomSpawnMonster = monstersToSpawn[Random.Range(0, monstersToSpawn.Count)];
            SpawnMonster(randomSpawnMonster.prefab, randomSpawnMonster.monsterData);
        }

        private void SpawnMonster(Monster monster, MonsterData data)
        {
            float pathDistance = boss.DistanceTravelled + distanceLengthDiffRange.GetRandom();
            Vector3 pathPoint = boss.Path.GetPointAtDistance(pathDistance);
            spawnPointAnimation.gameObject.SetActive(true);
            spawnPointAnimation.transform.position = pathPoint;
            spawnPointAnimation.SetSpriteColorAlpha(0f);

            Monster spawned = storage.ReferenceStorage.WaveSpawner.SpawnMonster(monster, boss.Path, data, boss.transform.parent, false, false);
            spawned.gameObject.layer = 0; //DEFAULT
            spawned.transform.position = pathPoint;
            spawned.MakeTransparable(0f);
            spawned.SetSpeed(0f);
            currentSpawning.prefab = spawned;
            currentSpawning.monsterData = data;
            DOVirtual.Float(0f, 1f, boss.Animator.GetCurrentAnimatorStateInfo(0).length - 0.01f, UpdateSpawningProgress).OnComplete(() => FinishMonsterSpawn(pathDistance)).SetId(1);
        }

        private void UpdateSpawningProgress(float progress) // progress is 0-1
        {
            currentSpawning.prefab.MakeTransparable(progress);
            spawnPointAnimation.SetSpriteColorAlpha(progress);
        }

        private void FinishMonsterSpawn(float distance)
        {
            spawnPointAnimation.SetSpriteColorAlpha(0f);
            spawnPointAnimation.gameObject.SetActive(false);

            currentSpawning.prefab.Init(storage, boss.Path, new MonsterData(currentSpawning.monsterData), distance);
            currentSpawning.prefab.gameObject.layer = Values.LayerMasks.MonsterLayer;
            storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(currentSpawning.prefab);

            currentSpawning = new MonsterSpawnData();
            boss.SetDefaultMovementState();
        }

        public override void Deinit()
        {
            
        }

        public override void UpdateState()
        {
            
        }
    }

    [System.Serializable]
    public class MonsterSpawnData
    {
        public Monster prefab;
        public MonsterData monsterData;
    }
}
