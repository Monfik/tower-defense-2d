using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    public class UtilEvents
    {
        public UnityAction<Monster> OnMonsterRaycasted;
        public void CallOnMonsterRaycasted(Monster monster)
        {
            OnMonsterRaycasted?.Invoke(monster);
        }

        public UnityAction<Monster> OnMonsterRaycastedStop;
        public void CallOnMonsterRaycastedStop(Monster monster)
        {
            OnMonsterRaycastedStop?.Invoke(monster);
        }
    }
}
