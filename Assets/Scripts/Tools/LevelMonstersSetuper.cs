using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

public class LevelMonstersSetuper : MonoBehaviour
{
    public List<Level> levels;

    public List<MonsterDataSetupData> setups;
    public float hpMultiplier = 1f;

    [ContextMenu("Setup")]
    public void Setup()
    {
        foreach(Level level in levels)
        {
            foreach(WaveData waveData in level.LevelData.waveDatas)
            {
                foreach(MonsterDataSetupData setupData in setups)
                {
                    List<MonsterReleaseData> releases = waveData.RealeasersData.FindAll(c => c.monster == setupData.prefab);
                    if(releases != null || releases.Count > 0)
                    {
                        foreach(MonsterReleaseData relData in releases)
                        {
                            relData.monsterData = setupData.monsterData;
                        }
                    }
                } 
            }
        }
    }

    [ContextMenu("Modify HP based on multiplier")]
    public void MultipleModifierHP()
    {
        foreach (MonsterDataSetupData setupData in setups)
        {
            int multipledHp = Mathf.RoundToInt(setupData.monsterData.hp * hpMultiplier);
            setupData.monsterData.hp = multipledHp;
        }
    }    
}

[System.Serializable]
public class MonsterDataSetupData
{
    public Monster prefab;
    public MonsterData monsterData;
}
