using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Postpone
{
    public class PostponeController : MonoBehaviour
    {
        public void InvokePostponeAction(UnityAction action, float delay = 0f)
        {
            StartCoroutine(PostPoneAction(action, delay == 0f ? null : new WaitForSeconds(delay)));
        }

        private IEnumerator PostPoneAction(UnityAction action, WaitForSeconds delay)
        {
            yield return delay;
            action?.Invoke();
        }
    }
}
