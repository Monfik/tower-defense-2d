using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    [System.Serializable]
    public class BossStateMachine<T>
    {
        public BaseBossState<T> currentState;

        public void UpdateMachine()
        {
            currentState?.UpdateState();
        }
    }
}
