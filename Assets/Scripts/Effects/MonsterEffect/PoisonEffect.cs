using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using TowerDefense.Spells;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class PoisonEffect : BaseMonsterEffect
    {
        [SerializeField] private PoisonData poisonData;
        [SerializeField] private float tickTime = 0.25f;
        private float currentTickTime = 0f;


        public void Init(PoisonData poisonData)
        {
            this.poisonData = poisonData;
        }

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);

            Tick();
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();

            currentTickTime += Time.deltaTime;
            if (currentTickTime >= tickTime)
            {
                Tick();
            }
        }

        private void Tick()
        {
            GiveDamage();
            currentTickTime = 0f;
        }

        private void GiveDamage()
        {
            int poisonArmorReduction = monster.Data.poisonResist.currentValue - poisonData.poisonArmorReduction;
            if (poisonArmorReduction < 0)
                poisonArmorReduction = 0;

            int dmg = DamageCalculator.CalculateDamage(poisonData.damagePerSecond, poisonArmorReduction);
            dmg = Mathf.RoundToInt(dmg * tickTime);
            monster.TakeDamage(dmg, DamageType.Magic);
        }
    }
}
