using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using Utils;

namespace Tools
{
    public class RoadPrefabCreator : ScriptableWizard
    {
        public List<Sprite> roadSprites;
        public int mapIndex;

        public enum ColliderType { Box, Sprite, Circle};
        public ColliderType colliderType;
        
        [Space(15)]
        public List<Sprite> backgroundSprites;

        [MenuItem("Tools/Create Road Prefabs")]
        public static void CreateWizard()
        {
            DisplayWizard<RoadPrefabCreator>("Create Road Prefabs", "Create");
        }

        private void OnWizardUpdate()
        {
            
        }

        private void OnWizardCreate()
        {
            CreatePrefabs(roadSprites);
            CreateBackgroundPrefabs(backgroundSprites);
        }

        private void CreateBackgroundPrefabs(List<Sprite> sprites)
        {
            foreach (Sprite sprite in sprites)
            {
                CreateBackgroundPrefab(sprite);
            }
        }

        private void CreateBackgroundPrefab(Sprite sprite)
        {
            GameObject spriteGO = new GameObject(sprite.name);
            AddSpriteRenderer(sprite, spriteGO);
            AddCollider(spriteGO, ColliderType.Box);
            SavePrefab(spriteGO, "");
        }

        private void CreatePrefabs(List<Sprite> sprites)
        {
            foreach(Sprite sprite in sprites)
            {
                CreatePrefab(sprite);
            }
        }

        private void CreatePrefab(Sprite sprite)
        {
            GameObject spriteGO = new GameObject(sprite.name);
            AddSpriteRenderer(sprite, spriteGO);
            AddCollider(spriteGO, colliderType);
            AddMask(spriteGO, sprite);
            SavePrefab(spriteGO, "/Road");
        }

        private void AddSpriteRenderer(Sprite sprite, GameObject obj)
        {
            SpriteRenderer renderer = obj.AddComponent<SpriteRenderer>();
            renderer.sprite = sprite;
            renderer.gameObject.layer = Values.LayerMasks.RoadLayer;
            renderer.sortingOrder = 0;
        }

        private void AddCollider(GameObject obj, ColliderType type)
        {
            if (type == ColliderType.Box)
                obj.AddComponent<BoxCollider2D>();
            else if (type == ColliderType.Sprite)
                obj.AddComponent<PolygonCollider2D>();
            else if (type == ColliderType.Circle)
                obj.AddComponent<CircleCollider2D>();
        }

        private void AddMask(GameObject obj, Sprite sprite)
        {
            SpriteMask mask = obj.AddComponent<SpriteMask>();
            mask.sprite = sprite;
            mask.alphaCutoff = 0.2f;
            mask.frontSortingLayerID = 1;
            mask.frontSortingOrder = 1;
            mask.backSortingLayerID = 1;
            mask.backSortingOrder = 0;
        }

        private void SavePrefab(GameObject go, string postfix)
        {
            string pathIndex = "Assets/Prefabs/Map/Map" + mapIndex.ToString();
            string path = pathIndex + postfix;

            if (!Directory.Exists(pathIndex))
            {
                AssetDatabase.CreateFolder("Assets/Prefabs/Map", "Map" + mapIndex.ToString());

                if(!string.IsNullOrEmpty(postfix))
                    AssetDatabase.CreateFolder("Assets/Prefabs/Map/Map" + mapIndex.ToString(), "Road");
            }
           

            string localPath = path + "/" + go.name + ".prefab";
            // Make sure the file name is unique, in case an existing Prefab has the same name.
            localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

            // Create the new Prefab and log whether Prefab was saved successfully.
            bool prefabSuccess;
            PrefabUtility.SaveAsPrefabAsset(go, localPath, out prefabSuccess);
            if (prefabSuccess == true)
                Debug.Log("Prefab was saved successfully");
            else
                Debug.Log("Prefab failed to save" + prefabSuccess);
        }
    }
}

#endif
