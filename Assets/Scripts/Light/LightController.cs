using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace LightControllers
{
    public class LightController : MonoBehaviour
    {
        // Start is called before the first frame update
        private DataStorage storage;

        [SerializeField]
        private Animator globalLightAnimator;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }
    }
}
