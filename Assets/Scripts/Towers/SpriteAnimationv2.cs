using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class SpriteAnimationv2 : MonoBehaviour
    {
        [SerializeField] private string animationName;

        [SerializeField]
        private List<Sprite> sprites;

        [SerializeField]
        new private SpriteRenderer renderer;
        public SpriteRenderer Renderer => this.renderer;

        [SerializeField]
        private Sprite currentSprite;

        private UnityAction<SpriteAnimationv2> OnAnimationFinished;
        private float currentTime = 0f;
        public float duration = 10f;
        private int spritesCount;
        private int currentIndex = 0;
        private int counter = 0;
        public bool process = false;

        public void PlayAnimation(UnityAction<SpriteAnimationv2> OnAnimationFinished)
        {
            this.OnAnimationFinished = OnAnimationFinished;
            counter = 0;
            currentIndex = 0;

            this.enabled = true;
            spritesCount = sprites.Count;
            renderer.sprite = currentSprite;
            process = true;
            renderer.gameObject.SetActive(true);
        }

        public void PlayAnimation(UnityAction<SpriteAnimationv2> OnAnimationFinished, float duration)
        {
            this.duration = duration;
            PlayAnimation(OnAnimationFinished);
        }

        public void Update()
        {
            if (!process)
                return;

            currentTime += Time.deltaTime;

            if (currentTime > duration / spritesCount)
            {
                ChangeSprite();
                currentTime = 0f;

                if (counter >= spritesCount)
                {
                    FinishAnimation();
                }
            }
        }

        private void ChangeSprite()
        {
            renderer.sprite = sprites[currentIndex];
            currentSprite = sprites[currentIndex];

            currentIndex++;

            counter++;
        }

        private void FinishAnimation()
        {
            this.enabled = false;
            OnAnimationFinished?.Invoke(this);
            process = false;
        }

        public void Stop()
        {
            process = false;
        }

        [ContextMenu("Test go")]
        public void TestGo()
        {
            PlayAnimation(null);
        }
    }
}
