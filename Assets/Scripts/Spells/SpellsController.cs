using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace TowerDefense.Spells
{
    public class SpellsController : MonoBehaviour
    {
        private SpellSet spellSet;
        private List<SpellPair> unlockedSpells = new List<SpellPair>();

        [Header("Available sets")]
        [SerializeField] private List<SpellSet> availableSets;

        private int startInputIndex = 10000;
        private DataStorage storage;

        private List<SpellHolder> spellHolders = new List<SpellHolder>();
        public List<SpellHolder> UpdatableSpellHolders => spellHolders;

        private List<BaseSpell> spells = new List<BaseSpell>();
        public List<BaseSpell> Spells => spells;

        [SerializeField] private BaseSpell castingSpell;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.ReferenceStorage.Settings.fraction.SetSpellsSet(this);
            LockAllSpells();
            InitSpells(spellSet);
        }

        public void SetSpellsSet(SpellSet set)
        {
            spellSet = set;
        }

        public void SetSpellsSet(int set)
        {
            if (set > availableSets.Count)
            {
                Debug.LogError("No available set founded");
                return;
            }

            spellSet = availableSets[set];
            spellSet.gameObject.SetActive(true);
        }

        private void InitSpells(SpellSet set)
        {
            CheckForUnlock(Values.levelNumber);
        }

        private void LevelStarted(int level)
        {
            CheckForUnlock(level);
        }

        private void CheckForUnlock(int level)
        {
            foreach (SpellPair set in spellSet.Pairs)
            {
                if (set.levelUnlock >= level)
                {
                    if (unlockedSpells.Contains(set))
                        continue;

                    UnlockSpell(set);
                }
            }
        }

        private void UnlockSpell(SpellPair set)
        {
            set.holder.Init(this, storage);
            set.holder.Unlock();
            set.unlocked = true;
            unlockedSpells.Add(set);
            int spellsCount = unlockedSpells.Count;
            //storage.ReferenceStorage.Input.AddNewInput(startInputIndex + spellsCount, KeyCode.Alpha1 + spellsCount, null, null, set.caster.StartCast);
        }

        private void LockAllSpells()
        {
            foreach (SpellPair pair in spellSet.Pairs)
            {
                pair.holder.Lock();
            }
        }

        public void UpdateController()
        {
            for (int i = 0; i < spellHolders.Count; i++)
            {
                spellHolders[i].UpdateHolder();
            }

            for (int i = 0; i < spells.Count; i++)
            {
                spells[i].UpdateSpell();
            }

            castingSpell?.UpdateSpell();
        }

        public void OnSpellFinished(BaseSpell spell)
        {
            spells.Remove(spell);
            Destroy(spell.gameObject);
        }

        public void OnSpellCanceled(BaseSpell spell)
        {
            if (castingSpell != null)
            {
                Destroy(castingSpell.gameObject);
                castingSpell = null;
            }
        }

        public BaseSpell CastSpell(BaseSpell spell)
        {
            if (castingSpell != null)
            {
                castingSpell.CancelSpell();
            }

            BaseSpell spellToCast = Instantiate(spell, this.transform);
            castingSpell = spellToCast;
            return spellToCast;
        }

        public void OnSpellCasted(BaseSpell spell)
        {
            if (castingSpell != null)
            {
                spells.Add(spell);
                castingSpell = null;
            }
        }

        public void RefreshHolders()
        {
            spellSet.ResetSet();
        }

        public void ClearCastingSpells()
        {
            if (castingSpell != null)
            {
                castingSpell.CancelSpell();
                castingSpell = null;
            }

            for (int i = spells.Count - 1; i >= 0; i--)
            {
                spells[i].FinishSpell();
            }
        }

        public void Deinit()
        {
            spellSet.gameObject.SetActive(false);
            spellSet = null;
        }
    }
}
