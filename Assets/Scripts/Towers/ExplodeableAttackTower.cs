using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class ExplodeableAttackTower : AttackableTower
    {
        [SerializeField]
        protected Animator animator;

        

        [SerializeField]
        protected List<StoneMissile> stones;

        public override void UpdateTower()
        {
            base.UpdateTower();

            for (int i = 0; i < stones.Count; i++)
            {
                stones[i].UpdateMissile();
            }
        }

        protected void OnTargetHitted(Monster target, StoneMissile stone)
        {

        }

        protected void OnStoneExploded(StoneMissile stone)
        {
            stones.Remove(stone);
            Destroy(stone.gameObject);
        }
    }
}
