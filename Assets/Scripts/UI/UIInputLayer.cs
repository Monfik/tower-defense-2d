using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInputLayer : MonoBehaviour, IDragHandler, IScrollHandler, IPointerClickHandler
{
    [SerializeField]
    private DataStorage storage;

    public void OnDrag(PointerEventData eventData)
    {
        storage.EventsStorage.UIEvents.CallOnDrag(eventData);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        storage.EventsStorage.UIEvents.CallOnClick(eventData);
    }

    public void OnScroll(PointerEventData eventData)
    {
        storage.EventsStorage.UIEvents.CallOnScroll(eventData);
    }
}
