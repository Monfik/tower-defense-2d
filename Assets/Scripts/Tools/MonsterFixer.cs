using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
public class MonsterFixer : MonoBehaviour
{
    [SerializeField] private List<Monster> monsters;

    [ContextMenu("Fix")]
    public void Fix()
    {
        foreach(Monster monster in monsters)
        {
            MonsterLegsCollider legs = monster.GetComponentInChildren<MonsterLegsCollider>();
            if(monster != null)
            legs.SetupOwner(monster);
        }
    }

    [ContextMenu("Setup basic data")]
    public void SetupBasicData()
    {
        foreach (Monster monster in monsters)
        {
            monster.Name = monster.name;
            monster.Preview = monster.Renderer.sprite;
            PrefabUtility.SavePrefabAsset(monster.gameObject);
        }

        AssetDatabase.SaveAssets();  
    }
}
#endif