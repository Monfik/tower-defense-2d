# About
New version of mine [C++ and SFML game](https://youtu.be/kSyqhpq9W7w) done in <b>Unity 2020.3.8f</b>.
Almost 2-years journey to implement everything on my own. You can find it on WebGL [here](https://gamemonfdev.pl/projects/project-tower_defense)

# What can we find?
Starting from menu here is vocation selector and unique spells on each of it and difficulty level.
To win the game u have to complete 7 levels where we can find some enemies and each boss per level with unique abilites.

### What unique?
There is tower efficient and repair system. Each attacking tower is damaging itself. After some time towers are damaged enough to be weaker and in result even stop work. We need to repair by clicking repair in its own Selector GUI.
Towers are not based on gold cost. I added Stock Market to game. Each tower has cost marked as raw materials. In game there are 3 raw materials (wood, clay and steel). Stock says to us how much cost each material and then by calculating materials on gold we can build tower. By example tower costs 5 wood, 3 clay and 4 steel, then all those are calculated for gold based on stock market's prices.

### UI Designer
[Olga Paluszkiewicz](google.pl)

![Alt text](https://gamemonfdev.pl/projects/project-tower_defense/storage/OverviewLight.gif)

![Your Link Text](https://gamemonfdev.pl/projects/project-tower_defense/storage/Light/Menu3Light.jpg)
![Your Link Text](https://gamemonfdev.pl/projects/project-tower_defense/storage/Light/Game1Light.jpg)
![Your Link Text](https://gamemonfdev.pl/projects/project-tower_defense/storage/Light/Game3Light.jpg)
