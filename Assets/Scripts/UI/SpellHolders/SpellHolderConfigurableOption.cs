using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class SpellHolderConfigurableOption : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Button button;

        [SerializeField] private Image artwork;
        public Image Artwork => artwork;

        [SerializeField] private SpellHolderOptionPreview optionPreview;

        [Header("Preview Tweening Times")]
        [SerializeField] private float displayTime;
        [SerializeField] private float hideTime;

        private UnityAction selectAction;
        private UnityAction<SpellHolderConfigurableOption> OnOptionSelected;

        public void Init(UnityAction<SpellHolderConfigurableOption> OnOptionSelected)
        {
            this.OnOptionSelected = OnOptionSelected;
            button.onClick.AddListener(Select);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            optionPreview.DisplayPreview(displayTime);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            optionPreview.HidePreview(hideTime);
        }

        public void Select()
        {
            selectAction?.Invoke();
            OnOptionSelected?.Invoke(this);
        }

        public void AddListener(UnityAction Action)
        {
            selectAction += Action;
        }

        public void MakeSelected(Color color)
        {
            artwork.color = color;
            button.interactable = false;
        }

        public void MakeDeselected(Color color)
        {
            artwork.color = color;
            button.interactable = true;
        }

        //public void DisplayOption(float diplayTime)
        //{
            
        //}

        //public void HideOption(float hideTime)
        //{
            
        //} 
    }
}
