using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Test
{
    public class Draw : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer brush;
        [SerializeField] private Camera mainCam;

        public Color drawColor;
        public int brushSize = 10;

        public void Update()
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            brush.transform.position = mousePosition;

            List<SpriteRenderer> renderers = new List<SpriteRenderer>();
            int layerMask = 1 << Values.LayerMasks.RoadLayer;
            Collider2D [] hits = Physics2D.OverlapCircleAll(mousePosition, brush.transform.localScale.x/2, layerMask);
            foreach(Collider2D hit in hits)
            {
                SpriteRenderer renderer = hit.GetComponent<SpriteRenderer>();
                if (renderer != null)
                    renderers.Add(renderer);
            }

            DrawOnSprites(renderers, mousePosition);
        }

        private void DrawOnSprites(List<SpriteRenderer> sprites, Vector2 worldPoint)
        {
            foreach(SpriteRenderer sprite in sprites)
            {
                DrawOnSprite(sprite, worldPoint);
            }
        }

        private void DrawOnSprite(SpriteRenderer sprite, Vector2 worldPoint)
        {
            Color32[] pixels = sprite.sprite.texture.GetPixels32();

            Vector2 pixelPos = WorldToPixelCoordinates(worldPoint, sprite);
            int centerX = (int)pixelPos.x;
            int centerY = (int)pixelPos.y;
            int pixelWidth = (int)sprite.sprite.rect.width;
            int pixelHeight = (int)sprite.sprite.rect.height;

            for(int i=centerX - brushSize; i<centerX+brushSize; i++)
            {
                if (i > pixelWidth || i < 0)
                    continue;

                for (int j=centerY - brushSize; j<centerY + brushSize; j++)
                {
                    if (j > pixelHeight || j < 0)
                        continue;

                    int pixelIndex = GetPixelIndex(i, j, pixelWidth);
                    pixels[pixelIndex] = drawColor;
                }
            }

            sprite.sprite.texture.SetPixels32(pixels);
            sprite.sprite.texture.Apply();
        }

        private int GetPixelIndex(int x, int y, int rectWidth)
        {
            int array_pos = y * rectWidth + x;
            return array_pos;
        }

        public Vector2 WorldToPixelCoordinates(Vector2 world_position, SpriteRenderer sprite)
        {
            // Change coordinates to local coordinates of this image
            Vector3 local_pos = sprite.transform.InverseTransformPoint(world_position);

            // Change these to coordinates of pixels
            float pixelWidth = sprite.sprite.rect.width;
            float pixelHeight = sprite.sprite.rect.height;
            float unitsToPixels = pixelWidth / sprite.sprite.bounds.size.x * transform.localScale.x;

            // Need to center our coordinates
            float centered_x = local_pos.x * unitsToPixels + pixelWidth / 2;
            float centered_y = local_pos.y * unitsToPixels + pixelHeight / 2;

            // Round current mouse position to nearest pixel
            Vector2 pixel_pos = new Vector2(Mathf.RoundToInt(centered_x), Mathf.RoundToInt(centered_y));

            return pixel_pos;
        }
    }
}
