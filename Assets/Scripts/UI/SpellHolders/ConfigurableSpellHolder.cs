using Data;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TowerDefense.Spells
{
    public class ConfigurableSpellHolder : SpellHolder, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private SpellHolderOptionsHolder holder;
        public SpellHolderOptionsHolder Holder => holder;

        public override void Init(SpellsController controller, DataStorage storage)
        {
            base.Init(controller, storage);
            holder.Init();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            holder.Display();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            holder.Hide();
        }
    }
}
