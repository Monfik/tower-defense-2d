using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class UpgradeCraftItemTemplate : MonoBehaviour
    {
        [SerializeField] private TMP_Text text;

        public void SetText(string text)
        {
            this.text.text = text;
        }
    }
}
