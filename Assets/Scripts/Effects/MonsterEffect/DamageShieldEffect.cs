using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Effects
{
    public class DamageShieldEffect : BaseMonsterEffect, ITransparable
    {
        [SerializeField] private SlidableBar magicShieldBar;
        public SlidableBar MagicShieldBar => magicShieldBar;
        [SerializeField] private DamageShield.DamageShieldData shieldData;
        private DamageShield givenShield;

        [SerializeField] private SpriteRenderer shield;

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);          

            magicShieldBar.transform.SetParent(monster.BarsHolder);
            magicShieldBar.gameObject.SetActive(true);
            magicShieldBar.SetMaxValue(shieldData.shieldValue);
            magicShieldBar.SetValue(shieldData.shieldValue);
        }

        public void AddShield()
        {
            DamageShield magicShield = new DamageShield(shieldData, OnShieldValueUpdate);
            givenShield = magicShield;
            monster.damageShields.Add(magicShield);
        }

        private void OnShieldValueUpdate(int shieldValue)
        {
            magicShieldBar.SetValue(shieldValue);

            if (shieldValue <= 0)
                EndEffect();
        }

        public override void EndEffect()
        {
            magicShieldBar.gameObject.SetActive(false);
            magicShieldBar.transform.SetParent(transform);
            base.EndEffect();
        }

        public void MakeTransparable(float alpha)
        {
            shield.SetSpriteColorAlpha(alpha);
            magicShieldBar.MakeTransparable(alpha);
        }
    }
}
