using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Controllers
{
    public class StockMarketController : BaseInitable
    {
        [SerializeField] private MaterialCostsData startLevelCosts;
        [SerializeField] private MaterialCostsData currentCost;

        [Header("Resources stocks")]
        [SerializeField] private ResourceStocker wood;
        [SerializeField] private ResourceStocker clay;
        [SerializeField] private ResourceStocker iron;

        public override void Init(DataStorage storage)
        {
            base.Init(storage);

            storage.EventsStorage.CoreEvnts.OnLevelStarted += LevelStarted;
            storage.EventsStorage.CoreEvnts.OnLevelFinished += LevelFinished;
            wood.Init(StockValueUpdate);
            clay.Init(StockValueUpdate);
            iron.Init(StockValueUpdate);

            Values.materialCosts = currentCost = startLevelCosts;
        }

        private void LevelStarted(Level level)
        {
            StopStockers();

            storage.UIStorage.CostPanelUI.ResetLevel(startLevelCosts);
            wood.SetValue(startLevelCosts.wood);
            clay.SetValue(startLevelCosts.clay);
            iron.SetValue(startLevelCosts.iron);
            UpdateCosts();

            wood.CountdownPeriod();
            clay.CountdownPeriod();
            iron.CountdownPeriod();
        }

        private void LevelFinished(Level level)
        {
            StopStockers();
        }

        private void StopStockers()
        {
            wood.Stop();
            clay.Stop();
            iron.Stop();
            //Debug.Log("Stop stockers");
        }

        public void UpdateCosts()
        {
            MaterialCostsData costs = new MaterialCostsData(wood.CurrentValue, clay.CurrentValue, iron.CurrentValue);
            Values.materialCosts = currentCost = costs;
            storage.EventsStorage.CoreEvnts.CallOnMaterialsCostChanged(new MaterialCostsData(wood.CurrentValue, clay.CurrentValue, iron.CurrentValue));
        }

        private void StockValueUpdate(ResourceStocker stocker)
        {
            storage.EventsStorage.CoreEvnts.CallOnMaterialCostChanged(stocker.StockerName, stocker.CurrentValue);
            UpdateCosts();
        }

        public void Deinit()
        {
            StopStockers();
            storage.EventsStorage.CoreEvnts.OnLevelStarted -= LevelStarted;
            storage.EventsStorage.CoreEvnts.OnLevelFinished -= LevelFinished;
        }

        [ContextMenu("Increase")]
        public void Increase()
        {
            wood.AddValue(0.5f);
            clay.AddValue(0.6f);
            iron.AddValue(0.8f);
            storage.EventsStorage.CoreEvnts.CallOnMaterialsCostChanged(new MaterialCostsData(wood.CurrentValue, clay.CurrentValue, iron.CurrentValue));
        }

        [ContextMenu("Decrease")]
        public void Decrease()
        {
            wood.AddValue(-0.2f);
            clay.AddValue(-0.4f);
            iron.AddValue(-0.6f);
            storage.EventsStorage.CoreEvnts.CallOnMaterialsCostChanged(new MaterialCostsData(wood.CurrentValue, clay.CurrentValue, iron.CurrentValue));
        }
    }
}
