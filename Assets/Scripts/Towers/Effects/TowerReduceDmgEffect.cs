using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class TowerReduceDmgEffect : BaseTowerEffect
    {
        [SerializeField] protected float damageReduceMultiplier;

        public override void StartEffect()
        {
            AttackableTower attckTower = tower as AttackableTower;
            if(attckTower != null)
            {
                attckTower.AttackSystem.SetupDamageMultiplier(damageReduceMultiplier);
            }
        }

        public override void FinishEffect()
        {
            base.FinishEffect();
        }
    }
}
