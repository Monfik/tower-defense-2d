using System.Collections;
using System.Collections.Generic;
using Tools;
using TowerDefense.Creatures;
using TowerDefense.Spells;
using UnityEngine;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class StoneFallingBomb : MapEffect
    {
        private FallingBombData bombData;

        [SerializeField] private AnimationCurve fallCurve;
        [SerializeField] private float fallSpeed;
        [SerializeField] private float rotateSpeed;

        [SerializeField] private Animator animator;
        [SerializeField] private CircleCollider2D circleCollider;
        [SerializeField] private SpriteRenderer stoneRenderer;

        [SerializeField] private AnimationEventCallback animFinishCallback;
        [SerializeField] private Vector3 explodedBombScale;
        private bool falling = true;

        public void Init(FallingBombData bombData)
        {
            this.bombData = bombData;

            transform.position = bombData.startPosition;
        }

        public override void UpdateEffect()
        {
            if(falling)
            {
                Vector2 currentPosition = transform.position;
                float t = Mathf.InverseLerp(bombData.startPosition.y, bombData.targetPosition.y, currentPosition.y);
                float evaluatedT = fallCurve.Evaluate(t);
                currentPosition.y -= fallSpeed * Time.deltaTime * (1 + evaluatedT);
                transform.position = currentPosition;

                Vector3 currentRotation = transform.rotation.eulerAngles;
                currentRotation.z += rotateSpeed * Time.deltaTime;
                transform.rotation = Quaternion.Euler(currentRotation);

                if(currentPosition.y <= bombData.targetPosition.y)
                {
                    currentPosition.y = bombData.targetPosition.y;
                    transform.position = currentPosition;
                    Hit(bombData.targetPosition);
                }
            }
        }

        private void Hit(Vector2 hitPoint)
        {
            List<Monster> hittedMonsters = new List<Monster>();
            int layerMask = 1 << Values.LayerMasks.MonsterLayer;
            Collider2D [] collidedMonsters =  Physics2D.OverlapCircleAll(hitPoint, circleCollider.radius, layerMask);

            foreach(Collider2D collider in collidedMonsters)
            {
                Monster monster = collider.GetComponent<Monster>();

                if(monster != null)
                {
                    DealDamage(monster);
                }
            }

            animFinishCallback.AnimationCallback += Exploded;
            falling = false;
            animator.enabled = true;
            stoneRenderer.transform.localScale = explodedBombScale;
        }

        private void DealDamage(Monster monster)
        {
            int armorReduction = monster.Data.armor.currentValue - bombData.armorReduction;
            if (armorReduction < 0)
                armorReduction = 0;
            int damage = DamageCalculator.CalculateDamage(bombData.physicalDmg, armorReduction);
            monster.TakeDamage(damage, DamageType.Physics);
        }

        public void Exploded()
        {
            FinishEffect();
        }

        Color gizmosColor = Color.white;

        private void OnDrawGizmos()
        {
            //if (storage == null)
            //    return;

            //Gizmos.color = gizmosColor;
            //Gizmos.DrawSphere(bombData.targetPosition, circleCollider.radius);
        }
    }
}
