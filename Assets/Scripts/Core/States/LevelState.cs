using Core;
using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Building;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace Core
{
    public class LevelState : IBaseState<MainGameState>
    {
        private DataStorage storage;
        public DataStorage Storage => storage;

        [SerializeField]
        private LevelManager levelManager;

        private PreviewController previewController;
        private TowersController towersController;

        #region StateImplementation

        public LevelState(Level level, LevelManager levelController)
        {
            levelController.LoadLevel(level, false);
        }

        public LevelState() { }

        public void Init(MainGameState controller)
        {
            storage = controller.Storage;
            previewController = storage.ReferenceStorage.PreviewController;
            towersController = storage.ReferenceStorage.TowersController;
            levelManager = storage.ReferenceStorage.LevelManager;

            if (levelManager.CurrentLevel.LevelId == 0)
                Debug.LogError("Level id = 0");

            storage.UIStorage.LevelView.EnableView(true);
            storage.UIStorage.LevelView.Init(storage.ReferenceStorage.MainController);
            storage.ReferenceStorage.Shop.Init(storage);     
            previewController.Init(storage);        
            towersController.Init(storage);
            storage.ReferenceStorage.MarkGenerator.Init(storage);
            storage.ReferenceStorage.WaveController.Init(storage);
            storage.ReferenceStorage.SpeedController.Init(storage);
            storage.ReferenceStorage.TowerGUI.Init(storage);
            storage.ReferenceStorage.Selector.Init(storage);
            storage.ReferenceStorage.UpdatablesController.Init(storage);
            storage.ReferenceStorage.Raycaster.Init(storage);
            storage.ReferenceStorage.PauseController.Init(storage);
            storage.ReferenceStorage.LvlController.Init(storage);
            storage.ReferenceStorage.ManaController.SetupLevelMana(levelManager.CurrentLevel.LevelId);
            storage.ReferenceStorage.ManaController.ResetMana();
            storage.ReferenceStorage.Spells.RefreshHolders();
            storage.ReferenceStorage.Wallet.SetGold(levelManager.CurrentLevel.LevelData.startGold);
            levelManager.CurrentLevel.StartLevel(); 

            storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic += storage.ReferenceStorage.SpeedController.Deinit;
        }

        public void UpdateState()
        {
            levelManager.UpdateController();
            previewController.UpdatePreview();
            towersController.UpdateController();
            storage.ReferenceStorage.Spells.UpdateController();
            storage.ReferenceStorage.UpdatablesController.UpdateUpdatables();
            storage.ReferenceStorage.CameraMovement.UpdateCamera();
            storage.ReferenceStorage.Raycaster.UpdateRaycaster();
            storage.ReferenceStorage.PauseController.UpdateController();
            storage.ReferenceStorage.MarkGenerator.UpdateMarks();
            storage.UIStorage.MonsterDataPanel.UpdatePanel();
            storage.ReferenceStorage.ManaController.UpdateMana();
        }

        public void FixedUpdateState()
        {

        }

        public void Deinit(MainGameState controller)
        {
            storage.UIStorage.LevelView.EnableView(false);
            storage.UIStorage.LevelView.Deinit();
            storage.ReferenceStorage.Shop.Deinit();
            storage.ReferenceStorage.WaveController.Deinit();
            storage.ReferenceStorage.TowerGUI.Deinit();
            storage.ReferenceStorage.Selector.Deinit();
            storage.ReferenceStorage.PauseController.Deinit();
            storage.ReferenceStorage.MarkGenerator.Deinit();
            storage.ReferenceStorage.LvlController.Deinit();
            storage.ReferenceStorage.Spells.ClearCastingSpells();
            levelManager.UnloadLevel();
            towersController.Deinit();
            previewController.Deinit();
            storage.ReferenceStorage.UpdatablesController.Finish();
            storage.UIStorage.LevelView.WaveNumberText.ResetVisualizer();
            storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic -= storage.ReferenceStorage.SpeedController.Deinit;
        }
        #endregion
    }
}
