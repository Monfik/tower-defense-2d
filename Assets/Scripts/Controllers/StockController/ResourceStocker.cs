using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Controllers
{
    [System.Serializable]
    public class ResourceStocker
    {
        [SerializeField] private string stockerName;
        public string StockerName => stockerName;
        [SerializeField] private Vector2 updateCheckTimePeriod;
        [SerializeField] private Vector2 randomUpdateValue;
        [SerializeField] private float maxValue;
        [SerializeField] private float minValue;
        [SerializeField] private float currentValue;
        public float CurrentValue => currentValue;

        private UnityAction<ResourceStocker> StockValueChanged;

        private float currentTime = 0f;
        private float lastTime;
        [SerializeField] private float cycleDuration = 10f;

        [SerializeField] private List<AnimationCurve> curves;
        [SerializeField] private AnimationCurve currentCurve;

        private Tween HandleCostTween;

        [Header("Debug values")]
        public float process; //0-1

        public ResourceStocker(float currentValue)
        {
            this.currentValue = currentValue;
        }

        public void Init(UnityAction<ResourceStocker> StockValueChanged)
        {
            this.StockValueChanged = StockValueChanged;
            currentCurve = RandomAnimationCurve();
        }

        public void AddValue(float value)
        {
            currentValue += value;
            currentValue = Mathf.Clamp(currentValue, minValue, maxValue);
        }

        public void SetValue(float value)
        {
            currentValue = value;
            currentValue = Mathf.Clamp(currentValue, minValue, maxValue);
            StockValueChanged?.Invoke(this);
        }

        public void CountdownPeriod()
        {
            float delay = RandomBetween(updateCheckTimePeriod.x, updateCheckTimePeriod.y);
            lastTime = Time.time;
            HandleCostTween = DOVirtual.DelayedCall(delay, HandleUpdateResourceCost, false);
        }

        private void HandleUpdateResourceCost()
        {
            currentTime += Time.time - lastTime;
            if (currentTime >= cycleDuration)
            {
                currentTime = 0f;
                currentCurve = RandomAnimationCurve();
            }

            currentValue = RandomNewValue();
            StockValueChanged?.Invoke(this);
            CountdownPeriod();
        }

        private float RandomNewValue()
        {
            float timeT = process = Mathf.InverseLerp(0, cycleDuration, currentTime);
            float curveValue = currentCurve.Evaluate(timeT);
            curveValue = Mathf.Clamp(curveValue, 0f, 1f);
            float value = Mathf.Lerp(minValue, maxValue, curveValue);
            float random = Random.Range(randomUpdateValue.x, randomUpdateValue.y);
            value += random;
            value = Mathf.Clamp(value, minValue, maxValue);
            value = Mathf.Round(value * 100f) / 100f;
            return value;
        }

        private AnimationCurve RandomAnimationCurve()
        {
            int curvesCount = curves.Count;
            int randomNumber = Random.Range(0, curvesCount);
            AnimationCurve randomedCurve = curves[randomNumber];
            if (currentCurve == randomedCurve)
            {
                if(Random.Range(0f, 1f) > 0.5f) //50% to roll again
                {
                    randomNumber = Random.Range(0, curvesCount);
                    randomedCurve = curves[randomNumber];
                }
            }

            return randomedCurve;
        }

        private float RandomBetween(float a, float b)
        {
            float random = Random.Range(a, b);
            return random;
        }

        public void Stop()
        {
            HandleCostTween?.Kill();
            currentTime = process = 0f;
        }
    }
}
