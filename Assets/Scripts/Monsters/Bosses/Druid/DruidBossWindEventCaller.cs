using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.GlobalEvents;
using TowerDefense.Towers;
using UnityEngine;
using Utils;

namespace TowerDefense.Creatures
{
    public class DruidBossWindEventCaller : BaseBossState<DruidBossBehaviour>
    {
        [SerializeField] private RangeFloat distanceReachCallRange;
        private float distanceReachCall;
        [SerializeField] private float windCallAnimationDuration;

        [SerializeField] private SpriteRenderer windMarkRenderer;
        [SerializeField] private Animator windMarkAnimator;
        [SerializeField] private AnimationCurve windMarkAlphaCurve;

        [SerializeField] private WindGlobalEvent windEvent;
        private bool called = false;

        public override void PreInit(DruidBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            distanceReachCall = distanceReachCallRange.GetRandom();
        }

        public override void Init(DruidBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("WindCall");
            windMarkAnimator.gameObject.SetActive(true);

            storage.ReferenceStorage.Postponer.InvokePostponeAction(CallWindEvent);
        }

        public override void UpdateState()
        {
            
        }

        private void CallWindEvent()
        {
            float windCallAnimation = boss.Animator.GetCurrentAnimatorStateInfo(0).length;
            float animatorSpeed = windCallAnimation / windCallAnimationDuration;
            boss.Animator.SetFloat("WindCallSpeed", animatorSpeed);

            DOVirtual.Float(0f, 1f, windCallAnimationDuration, OnWindMarkVisualUpdate).SetEase(windMarkAlphaCurve).SetId(10000);
        }

        private void OnWindMarkVisualUpdate(float process)
        {
            windMarkRenderer.SetSpriteColorAlpha(process);
        }

        //Called from animator
        private void WindEventAnimationFinished()
        {
            called = true;
            windMarkAnimator.gameObject.SetActive(false);
            WindGlobalEvent windEvent = Instantiate(this.windEvent);
            windEvent.gameObject.SetActive(true);
            storage.ReferenceStorage.EventsController.BeginLevelEvent(windEvent);
            boss.SetDefaultMovementState();
        }

        public override void Deinit()
        {
            boss.DefaultMovementState.AvailableStates.Remove(this);
        }

        public override bool IsReadyForWork()
        {
            return boss.DistanceTravelled >= distanceReachCall && !called;
        }

        private void OnDestroy()
        {
            DOTween.Kill(10000);
        }
    }
}