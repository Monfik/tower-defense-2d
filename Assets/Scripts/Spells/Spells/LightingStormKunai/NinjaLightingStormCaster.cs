using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    [System.Serializable]
    public class NinjaLightingStormCaster : BaseUpdatableMonoless
    {
        private ExplosiveLightingStormData stormData;
        [SerializeField] private NinjaUIAnimation animationPrefab;
        private NinjaUIAnimation spawnedAnimation;
        [SerializeField] private TagKunai kunaiPrefab;
        [SerializeField] private LightingStormMapEffect stormPrefab;

        public void Init(ExplosiveLightingStormData stormData)
        {
            this.stormData = stormData;
        }

        public override void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            base.StartUpdate(storage, OnUpdatableFinished);
            spawnedAnimation = GameObject.Instantiate(animationPrefab, animationPrefab.transform.parent.transform);
            spawnedAnimation.gameObject.SetActive(true);
            spawnedAnimation.ThrowRandom(OnNinjaAnimated, KunaiThrowedFromPosition);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(spawnedAnimation);
            spawnedAnimation.transform.SetParent(animationPrefab.transform.parent);
        }

        public override void UpdateUpdatable()
        {
            
        }

        private void OnNinjaAnimated()
        {
            Finish();
        }

        private void KunaiThrowedFromPosition(Vector2 position)
        {
            TagKunai kunai = GameObject.Instantiate(kunaiPrefab);
            kunai.transform.position = position;
            kunai.Init(stormData.position, MakeBoom);
            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(kunai);
        }

        private void MakeBoom()
        {
            LightingStormMapEffect storm = GameObject.Instantiate(stormPrefab);
            storm.transform.position = stormData.position;
            storm.Init(storage);
            storm.Init(stormData);
            storage.EventsStorage.CoreEvnts.CallOnMapEffectAppeared(storm);
        }
    }
}
