using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TowerDefense
{
    public class SlidableBar : MonoBehaviour, ITransparable
    {
        [SerializeField]
        private Image fill;

        [SerializeField]
        private Image border;

        [SerializeField]
        private Slider slider;

        [SerializeField]
        private Gradient gradient;

        public UnityAction<int> OnValueChanged;
        public UnityAction<int, int> OnValueChangedMax;

        public int MaxValue => (int)slider.maxValue;
        public int CurrentValue => (int)slider.value;

        public void SetMaxValue(int value)
        {
            slider.maxValue = value;
            slider.value = value;
            fill.color = gradient.Evaluate(slider.normalizedValue);
        }

        public void SetValue(int value)
        {
            slider.value = value;
            fill.color = gradient.Evaluate(slider.normalizedValue);
            OnValueChanged?.Invoke(value);
            OnValueChangedMax?.Invoke(value, (int)slider.maxValue);
        }

        public void MakeTransparable(float alpha)
        {
            Color fillColor = fill.color;
            fillColor.a = alpha;
            fill.color = fillColor;

            Color borderColor = border.color;
            borderColor.a = alpha;
            border.color = borderColor;
        }
    }
}
