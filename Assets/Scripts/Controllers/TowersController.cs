using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public class TowersController : MonoBehaviour
    {
        private DataStorage storage;

        [SerializeField]
        private List<Tower> currentTowers;
        public List<Tower> CurrentTowers => currentTowers;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished += OnTowerDemolished;
        }

        public void UpdateController()
        {
            for(int i=0; i<currentTowers.Count; i++)
            {
                currentTowers[i].UpdateTower();
            }
        }

        private void OnTowerBuilded(Tower tower)
        {
            tower.Init(storage);
            currentTowers.Add(tower);
        }

        private void OnTowerDemolished(Tower tower)
        {
            storage.ReferenceStorage.Wallet.Add(Mathf.RoundToInt(tower.Builder.GetSellValue()));
            currentTowers.Remove(tower);
            Destroy(tower.gameObject);
        }

        public void DestroyTowers()
        {
            for (int i = currentTowers.Count - 1; i >= 0; i--)
            {
                currentTowers[i].DestroyTower();
                Destroy(currentTowers[i].gameObject);
            }

            currentTowers.Clear();
        }

        public void Deinit()
        {
            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnTowerDemolished -= OnTowerDemolished;
            DestroyTowers();
        }
    }
}
