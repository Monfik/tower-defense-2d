using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.GlobalEvents
{
    public class BaseLevelEvent : BaseDisplayableGlobalEvent
    {
        [Header("Event values")]
        [SerializeField] private int minWaves;
        [SerializeField] private int maxWaves;

        [SerializeField] private int waveLived = 0;

        //private Wave currentWave;
        private Wave stopableWave;
        private float currentStopableTime = 0f;
        private float randomedStopTime;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnUpdatableFinished)
        {
            base.BeginEvent(storage, OnUpdatableFinished);
            storage.EventsStorage.CoreEvnts.OnWaveFinished += OnWaveFinished;
            storage.EventsStorage.CoreEvnts.OnWaveStarted += OnWaveStarted;
        }

        private void OnWaveFinished(Wave wave)
        {
            if(wave == stopableWave)
            {
                StartToFinishEvent();
            }
        }

        public override void UpdateEvent()
        {
            base.UpdateEvent();
            if (stopableWave == null)
                return;

            currentStopableTime += Time.deltaTime;
            if (randomedStopTime <= currentStopableTime)
            {
                StartToFinishEvent();
                return;
            }
        }

        private void OnWaveStarted(Wave wave)
        {
            waveLived++;
            //currentWave = wave;

            bool finishThisWave = CheckIfFinishInThisWave(waveLived);
            if(finishThisWave)
            {
                stopableWave = wave;
                randomedStopTime = Random.Range(wave.WaveData.duration / 3, wave.WaveData.duration);
            }
        }

        protected bool CheckIfFinishInThisWave(int waveLived)
        {
            if (waveLived < minWaves)
                return false;
            else if (waveLived >= maxWaves)
                return true;
            else 
                return FinishByRandomize(waveLived, minWaves, maxWaves);
        }

        protected bool FinishByRandomize(int waveLived, int minWaves, int maxWaves)
        {
            int random = Random.Range(minWaves, maxWaves);
            return waveLived >= random;
        }

        public override void StartToFinishEvent()
        {
            base.StartToFinishEvent();
            stopableWave = null;
            storage.EventsStorage.CoreEvnts.OnWaveFinished -= OnWaveFinished;
            storage.EventsStorage.CoreEvnts.OnWaveStarted -= OnWaveStarted;
        }
    }
}
