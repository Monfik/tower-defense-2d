using System.Collections;
using System.Collections.Generic;
using TowerDefense.Power;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Effects
{
    [System.Serializable]
    public class MagicDamageTowerBuff : BuffableTowerEffect
    {
        protected MagicDamageAttackSystem attackSystem;
        protected MagicAttackData attackData;
        private MagicAttackData multipledData;
        private MagicDamageData additionalDamage;

        public MagicDamageTowerBuff(MagicDamageAttackSystem attackSystem, MagicAttackData data, MagicDamageTowerBuff copy, bool isPositiveBuff) : base(copy)
        {
            this.attackSystem = attackSystem;
            attackData = data;
        }

        public MagicDamageTowerBuff(MagicDamageAttackSystem attackSystem, MagicAttackData data, bool isPositiveBuff)
        {
            this.attackSystem = attackSystem;
            attackData = data;
        }

        public MagicDamageTowerBuff(MagicDamageTowerBuff buff) :base(buff)
        {
            this.attackSystem = buff.attackSystem;
            this.attackData = buff.attackData;
        }

        public MagicDamageTowerBuff(MagicDamageTowerBuff buff, MagicDamageAttackSystem attackSystem, MagicAttackData data) : base(buff)
        {
            this.attackSystem = attackSystem;
            this.attackData = data;
        }

        public override void StartEffect()
        {
            base.StartEffect();
            float attackSpeed = attackSystem.AttackSpeed.data.currentValue;
            multipledData = new MagicAttackData(attackData, attackSpeed);
            additionalDamage = new MagicDamageData(multipledData);

            attackSystem.AddMagicDamage(additionalDamage);

            if (tower.GuiManager.Opened)
                tower.GuiManager.Refresh();
        }

        public override void UpdateEffect()
        {
            
        }

        public override void FinishEffect()
        {
            attackSystem.MissileData.magicDamages.Remove(additionalDamage);

            if (tower.GuiManager.Opened)
                tower.GuiManager.Refresh();

            base.FinishEffect();
        }
    }

}