using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.Towers
{
    public class Shop : MonoBehaviour
    {
        [SerializeField]
        private List<TowerPreset> towerPresets;

        [SerializeField] private Color notAffordCost;
        private DataStorage storage;

        public void Init(DataStorage storage)
        {
            this.storage = storage;

            foreach(TowerPreset preset in towerPresets)
            {
                preset.Init(OnTowerPicked);
            }

            storage.EventsStorage.CoreEvnts.OnTowerBuilded += OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnMaterialsCostsChanged += UpdateCosts;

            SetupCosts(Values.materialCosts);
        }

        private void UpdateCosts(MaterialCostsData costs)
        {
            SetupCosts(costs);
        }

        private void SetupCosts(MaterialCostsData costs)
        {
            foreach(TowerPreset preset in towerPresets)
            {
                int cost = CalculateTowerCost(preset.Tower.Builder.Cost.stock, costs);
                Color color = cost <= Values.gold ? Color.white : notAffordCost;
                preset.UpdateCost(cost, color, cost <= Values.gold);
            }
        }

        public int CalculateTowerCost(TowerStock stock, MaterialCostsData costs)
        {
            float towerCost = stock.wood * costs.wood + stock.iron * costs.iron + stock.clay * costs.clay;
            int towerCostGold = Mathf.RoundToInt(towerCost);
            return towerCostGold;
        }

        private void OnTowerPicked(Tower tower)
        {
            storage.EventsStorage.CoreEvnts.CallOnTowerPicked(tower);
        }

        private void OnTowerBuilded(Tower tower)
        {
            int cost = CalculateTowerCost(tower.Builder.Cost.stock, Values.materialCosts);
            storage.ReferenceStorage.Wallet.Subtract(cost);

            SetupCosts(Values.materialCosts);
        }

        public void Deinit()
        {
            foreach (TowerPreset preset in towerPresets)
            {
                preset.Deinit();
            }

            storage.EventsStorage.CoreEvnts.OnTowerBuilded -= OnTowerBuilded;
            storage.EventsStorage.CoreEvnts.OnMaterialsCostsChanged -= UpdateCosts;
        }
    }
}
