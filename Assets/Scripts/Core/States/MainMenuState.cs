﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

namespace Core
{
    public class MainMenuState : IBaseState<MainController>
    {
        private MainController controller;
        private DataStorage storage;

        #region StateImplementation
        public void Init(MainController controller)
        {
            this.controller = controller;
            this.storage = controller.Storage;

            storage.UIStorage.MainMenuView.EnableView(true);
            storage.UIStorage.MainMenuView.Init(controller);
            storage.ReferenceStorage.MusicPlayer.Play(storage.ReferenceStorage.MusicPlayer.mainMenuClip);
        }

        public void UpdateState()
        {

        }

        public void Deinit(MainController controller)
        {
            storage.ReferenceStorage.MusicPlayer.Stop();
            storage.UIStorage.MainMenuView.EnableView(false);
            storage.UIStorage.MainMenuView.Deinit();
        }

        public void FixedUpdateState()
        {
            
        }
        #endregion

    }
}