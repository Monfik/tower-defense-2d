using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    [System.Serializable]
    public class TowerBlueprint
    {
        public int cost;
        [SerializeField] private Tower prefab;

    }
}
