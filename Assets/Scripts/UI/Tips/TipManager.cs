using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Tips
{
    public class TipManager : MonoBehaviour
    {
        public static TipManager instance;

        [SerializeField] private TipPanel panel;
        [SerializeField] private Canvas canvas;

        [SerializeField] private Vector2 extraOffset;
        private bool displayed = false;

        [SerializeField] private DescriptionData currentData;

        public enum TooltipType { Default, Spell};

        private void Start()
        {
            instance = this;
        }

        private void Update()
        {
            if (!displayed)
                return;

            Vector2 screenPosition = GetScreenPosition(panel.Rect.sizeDelta, extraOffset, Input.mousePosition);
            panel.Rect.anchoredPosition = screenPosition;
        }

        public static void Display(RectTransform source, DescriptionData description, Vector2 extraOffset, Sprite iconeSprite = null)
        {
            instance.DisplayPanel(source, description, extraOffset, iconeSprite);
        }

        public static void UpdateTip(RectTransform source, DescriptionData description, Vector2 extraOffset, Sprite iconeSprite = null)
        {
            instance.DisplayPanel(source, description, extraOffset, iconeSprite);
        }

        public static void Hide()
        {
            instance.HidePanel();
        }

        public void DisplayPanel(RectTransform source, DescriptionData data, Vector2 extraOffset, Sprite iconeSprite)
        {
            if (iconeSprite != null)
            {
                panel.TooltipIcone.sprite = iconeSprite;
                panel.TooltipIcone.gameObject.SetActive(true);
            }
            else
                panel.TooltipIcone.gameObject.SetActive(false);

            currentData = data;
            this.extraOffset = extraOffset;

            if (data.simpleText)
            {
                panel.SetTooltip(data.description);
            }
            else
            {
                panel.SetTooltip(data);
            }

            StartCoroutine(AdjustPanelToText());
        }

        private IEnumerator AdjustPanelToText()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            Vector2 size = panel.ParentRootRect.rect.size;
            size.x += panel.ParentRootRect.offsetMin.x * 2;
            panel.Rect.sizeDelta = size;

            Vector2 screenPosition = GetScreenPosition(size, extraOffset, Input.mousePosition);
            panel.Rect.anchoredPosition = screenPosition;

            displayed = true;
        }

        public Vector2 GetScreenPosition(Vector2 tipSize, Vector2 extraOffset, Vector2 mousePosition)
        {
            mousePosition += extraOffset;
            Vector2 screenInput = new Vector2(Mathf.Clamp(mousePosition.x, 0f, Screen.width - tipSize.x), Mathf.Clamp(mousePosition.y, 0f, Screen.height - tipSize.y));
            screenInput /= canvas.transform.localScale.x;

            return screenInput;
        }

        public void HidePanel()
        {
            displayed = false;
            panel.Rect.anchoredPosition = new Vector2(5000f, 5000f);
        }
    }
}
