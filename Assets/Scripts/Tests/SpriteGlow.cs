using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    [ExecuteInEditMode]
    public class SpriteGlow : MonoBehaviour
    {
        [SerializeField] private Material material;
        [SerializeField] private SpriteRenderer spriteRend;

        [Range(0, 10)]
        public float glowIntensity;

        private void Update()
        {
            material.SetFloat("_Intensity", glowIntensity);
        }
    }
}
