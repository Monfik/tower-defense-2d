using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.GlobalEvents;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Controllers
{
    public class WindController : BaseInitable, IUpdatable
    {
        [SerializeField] private WindForceData windForceData;
        public WindForceData WindForceData => windForceData;

        private List<IWindSusceptible> susceptibles;
        public List<IWindSusceptible> Susceptibles => susceptibles;

        [SerializeField] private WindEntitiesManager windEntitiesManager;

        private void Start()
        {
            susceptibles = new List<IWindSusceptible>();
        }

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            susceptibles = new List<IWindSusceptible>();
            windEntitiesManager.Init(storage);
        }

        public void AddValue(float value)
        {
            windForceData.strength += value;
            windForceData.strength = Mathf.Clamp(windForceData.strength, 0f, 1f);
            PerformCallbacks();
            windEntitiesManager.HandleWindEntities(windForceData.strength);
        }

        private void PerformCallbacks()
        {
            susceptibles.ForEach(c => c.Affect(windForceData));
        }

        public void AddSusceptible(IWindSusceptible susceptible)
        {
            susceptibles.Add(susceptible);
            susceptible.Affect(windForceData);
        }

        public void RemoveSusceptible(IWindSusceptible susceptible)
        {
            susceptibles.Remove(susceptible);
        }

        public void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {

        }

        public void UpdateUpdatable()
        {

        }

        public void Deinit()
        {
            windEntitiesManager.Deinit(storage);
        }

        #region Internal
        [SerializeField] private float additionalTestValue = 0.1f;
        
        [ContextMenu("Add test value")]
        private void AddTestValue()
        {
            AddValue(additionalTestValue);
        }
        #endregion
    }

    [System.Serializable]
    public class WindForceData
    {
        public float strength; //0 - 1
    }
}
