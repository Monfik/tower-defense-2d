using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using DG.Tweening;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Effects
{
    public class HunterTrapEffect : BaseMonsterEffect
    {
        private HunterTrapEffectData effectData;
        private float startTime;
        private float lastSlowValue = 0f;
        //Should be inited before StartEffect!
        public void InitByData(HunterMapTrapData data)
        {
            effectData = data;
            duration = data.effectTime;
        }

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);
            startTime = Time.time;
            DealDamage(effectData.dmg, monster.Data.armor.currentValue);
            SlowMonster(effectData.startSlowValue);
            DOVirtual.Int(effectData.startDmg, 0, effectData.effectTime, OnDmgTweened).SetEase(Ease.Linear); //dmg
            DOVirtual.Float(effectData.startSlowValue, 0f, effectData.effectTime, OnSlowTweened);
        }

        public void OnDmgTweened(int dmg)
        {
            if(Time.time - startTime >= effectData.dmgInterval)
            {
                DealDamage(dmg, monster.Data.armor.currentValue);
                startTime = Time.time;
            }      
        }

        private void DealDamage(int dmg, int armor)
        {
            monster.TakeDamage(DamageCalculator.CalculateDamage(dmg, armor), DamageType.Physics);
        }

        private void OnSlowTweened(float value)
        {
            SlowMonster(value);
        }

        private void SlowMonster(float slowValue)
        {
            monster.Data.speed.AddValue(Mathf.RoundToInt(lastSlowValue));
            monster.Data.speed.AddValue(-Mathf.RoundToInt(slowValue));
            lastSlowValue = slowValue;
        }

        public override void EndEffect()
        {
            monster.Data.speed.AddValue(Mathf.RoundToInt(lastSlowValue));
            base.EndEffect();
        }
    }

    public class HunterTrapEffectData
    {
        public float effectTime;
        public int dmg;
        public int startDmg;
        public float dmgInterval;
        public float startSlowValue;
    }
}
