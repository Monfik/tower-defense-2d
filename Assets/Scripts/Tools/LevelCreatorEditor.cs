using Data;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefense.Creatures;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
namespace Tools
{
    [CustomEditor(typeof(LevelCreator))]
    public class LevelCreatorEditor : Editor
    {
        private LevelCreator levelCreator;
        public SerializedProperty monstersListProperty;
        public SerializedProperty levelProperty;
        public SerializedProperty delayProperty;
        public SerializedProperty linePathProperty;

        private MonsterDataSetupData currentSelectedMonster;
        private PathCreator currentSelectedPath;

        private void OnEnable()
        {
            levelCreator = (LevelCreator)target;

            monstersListProperty = serializedObject.FindProperty("monsterSetupers");
            levelProperty = serializedObject.FindProperty("level");
            delayProperty = serializedObject.FindProperty("delay");
            linePathProperty = serializedObject.FindProperty("pathRepresentation");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(levelProperty);
            EditorGUILayout.PropertyField(monstersListProperty);
            EditorGUILayout.PropertyField(linePathProperty);

            DrawMonsters(levelCreator.monsterSetupers.Select(c => c.prefab).ToList());


            if (levelCreator.level != null)
            {
                DrawPaths(levelCreator.level.LevelData.map.Paths);
            }

            GUILayout.Space(20);
            EditorGUILayout.PropertyField(delayProperty);

            GUILayout.Space(40);
            DrawCurrentData(currentSelectedMonster);

            if (currentSelectedMonster != null && currentSelectedPath != null && levelCreator.delay != 0f && levelCreator.level != null)
            {
                if (GUILayout.Button("Dodaj"))
                {
                    AddEnemy(currentSelectedMonster, currentSelectedPath, levelCreator.level.LevelData.waveDatas[levelCreator.level.LevelData.waveDatas.Count - 1]);
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawCurrentData(MonsterDataSetupData setup)
        {
            if(setup != null && setup.prefab != null)
            {
                string monsterTexturePath = AssetDatabase.GetAssetPath(setup.prefab.Renderer.sprite);
                Texture banner = (Texture)AssetDatabase.LoadAssetAtPath(monsterTexturePath, typeof(Texture));
                GUILayout.Box(banner, GUILayout.Width(150f), GUILayout.Height(150f));
            }
        }

        private void DrawMonsters(List<Monster> monsters)
        {
            monsters.RemoveAll(c => c == null);

            for (int i = 0; i < monsters.Count; i++)
            {
                if (i % 10 == 0)
                    GUILayout.BeginHorizontal();

                string monsterTexturePath = AssetDatabase.GetAssetPath(monsters[i].Renderer.sprite);
                Texture banner = (Texture)AssetDatabase.LoadAssetAtPath(monsterTexturePath, typeof(Texture));
                if (GUILayout.Button(banner, GUILayout.Width(80f), GUILayout.Height(80f)))
                {
                    SelectMonster(i);
                }

                if (i % 10 == 9 || i == monsters.Count - 1)
                    GUILayout.EndHorizontal();
            }
        }

        private void AddEnemy(MonsterDataSetupData data, PathCreator path, WaveData wave)
        {
            wave.RealeasersData.Add(new MonsterReleaseData(data.prefab, levelCreator.delay, path, data.monsterData));
        }

        private void DrawPaths(List<PathCreator> paths)
        {
            GUILayout.BeginVertical();
            for (int i = 0; i < paths.Count; i++)
            {
                if (GUILayout.Button("�cie�ka nr " + (i + 1)))
                {
                    SelectPath(paths[i]);
                }
            }
            GUILayout.EndVertical();
        }

        private void SelectPath(PathCreator path)
        {
            currentSelectedPath = path;

            if (currentSelectedPath != null)
            {
                levelCreator.pathRepresentation.positionCount = currentSelectedPath.path.NumPoints;
                for (int i = 0; i < currentSelectedPath.path.NumPoints; i++)
                {
                    levelCreator.pathRepresentation.SetPosition(i, currentSelectedPath.path.GetPoint(i));
                }
            }
        }

        private void SelectMonster(int index)
        {
            currentSelectedMonster = levelCreator.monsterSetupers[index];
        }
    }
}

#endif
