using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class TowerGuiManager : MonoBehaviour
    {
        private DataStorage storage;
        [SerializeField] private Tower tower;

        [SerializeField] 
        private List<GameObject> guiComposables;
        public List<GameObject> GuiComposables => guiComposables;

        [SerializeField] private bool customTower = false;

        private UnityAction<TowerSelector> OpenGuiAction;

        public UnityAction OnBasicGuiExit;
        public UnityAction OnUpgradeGuiExit;

        private UnityAction<TowerSelector> ComposeAction;

        private bool opened;
        public bool Opened => opened;

        public GUIElementsEventsController guiElementableActionController = new GUIElementsEventsController();

        private void Awake()
        {
            this.enabled = false;
        }

        public void Init(DataStorage storage)
        {
            this.storage = storage;
            this.enabled = true;
            SwitchToNormalGui();
            tower.Builder.OnStartUpgradingBasic += SwitchToBuildingGui;
            tower.Builder.OnFinishedUpgradingBasic += SwitchToNormalGui;
            tower.Builder.OnStartBuilding += SwitchToBuildingGui;
            tower.Builder.OnFinishedBuild += SwitchToNormalGui;
            tower.Builder.OnStartDemolishing += SwitchToDemolishingGui;
            tower.Builder.OnStartFixing += SwitchToBuildingGui;
            tower.Builder.OnFinishedFix += SwitchToNormalGui;

            OnBasicGuiExit += ClearGUIElementsRelations;
            OnUpgradeGuiExit += ClearGUIElementsRelations;
        }  

        public void OpenGUI(TowerSelector gui)
        {           
            OpenGuiAction?.Invoke(gui);
            opened = true;
        }

        private void OpenNormalGUI(TowerSelector gui)
        {
            DisplayTowerGUI();
            BaseTowerGuiPanel guiPanel;
            if (customTower) 
                guiPanel = gui.CustomTowerPanel; 
            else 
                guiPanel = gui.BasicPanel;

            gui.Open(guiPanel, tower);
        }

        private void OpenUpgradingGUI(TowerSelector gui)
        {
            DisplayTowerGUI();
            BaseTowerGuiPanel guiPanel;
            if (customTower)
                guiPanel = gui.CustomTowerPanel;
            else
                guiPanel = gui.UpgradingPanel;

            gui.Open(guiPanel, tower);
        }

        private void CloseGUI(TowerSelector gui)
        {
            gui.Close();
            opened = false;
        }

        public void SwitchToNormalGui()
        {
            OpenGuiAction = OpenNormalGUI;
        }

        public void SwitchToBuildingGui()
        {
            OpenGuiAction = OpenUpgradingGUI;
        }

        private void SwitchToDemolishingGui()
        {
            OpenGuiAction = CloseGUI;
        }

        public void ComposeBasicGUI(TowerGuiInfoPanel infoPanel)
        {
            ComposeAction = OpenNormalGUI;
            infoPanel.Close();

            foreach (GameObject composableGO in guiComposables)
            {
                IGUIComposable composable = composableGO.GetComponent<IGUIComposable>();

                if (composable == null)
                {
                    Debug.LogError("Given go isnt GUI composable !!!");
                    continue;
                }

                composable.ComposeBasicInfo(infoPanel);
            }

            infoPanel.AdjustPanelSize();
        }

        public void ComposeUpgradeGUI(TowerGuiInfoPanel infoPanel)
        {
            ComposeAction = OpenUpgradingGUI;

            foreach (GameObject composableGO in guiComposables)
            {
                IGUIComposable composable = composableGO.GetComponent<IGUIComposable>();
                if (composable == null)
                {
                    Debug.LogError("Given go isnt GUI composable !!!");
                    continue;
                }

                composable.ComposeUpgradeInfo(infoPanel);
            }

            infoPanel.AdjustPanelSize();
        }

        public void CloseGUI()
        {
            opened = false;
            DisplayRange(false);
        }

        public void DisplayRange(bool enable)
        {
            tower.RangeRenderer.gameObject.SetActive(enable);
        }
        
        public void DisplayTowerGUI()
        {
            DisplayRange(true);
        }

        public void HideTowerGUI()
        {
            opened = false;
            DisplayRange(false);
        }

        public void ComposeGuiInfoPanel()
        {

        }

        public void ClearGUIElementsRelations()
        {
            guiElementableActionController.Clear();
        }

        public void Refresh()
        {
            ComposeAction?.Invoke(storage.ReferenceStorage.TowerGUI);
        }
    }

    public class GUIElementsEventsController
    {
        private List<GUIElementableEventPair> pairs = new List<GUIElementableEventPair>();

        public void Add(GUIElementableEventPair pair)
        {
            GUIElementableEventPair existedPair = pairs.Find(c => c.elementable == pair.elementable);
            if(existedPair != null)
            {
                pairs.Remove(existedPair);
            }

            pairs.Add(pair);
        }

        public void CallActionIfExists(IGUIElementable elementable)
        {
            GUIElementableEventPair pair = pairs.Find(c => c.elementable == elementable);

            if (pair != null)
            {
                pair.Action?.Invoke();
            }
        }

        public void Clear()
        {
            pairs.Clear();
        }
    }
}
