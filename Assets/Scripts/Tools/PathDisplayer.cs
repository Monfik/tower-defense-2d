using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense
{
    [System.Serializable]
    public class PathDisplayer
    {
        [Header("Animation Data")]
        public PathVisualAnimationData hoverData;
        public PathVisualAnimationData nonHoverData;

        [SerializeField] private List<PathVisual> pathVisuals = new List<PathVisual>();
        [SerializeField] private LineRenderer linePrefab;
        [SerializeField] private Transform linesContent;

        public void UpdateDisplayer()
        {
            foreach(PathVisual visual in pathVisuals)
            {
                visual.UpdateVisual();
            }
        }

        public void CreateVisuals(List<PathCreator> paths)
        {
            foreach (PathCreator path in paths)
            {
                PathVisual visual = CreateVisual(path);
                pathVisuals.Add(visual);
            }
        }

        public PathVisual CreateVisual(PathCreator path)
        {
            LineRenderer line = GameObject.Instantiate(linePrefab);
            line.positionCount = path.path.localPoints.Length;
            if(path.transform.position != Vector3.zero)
            {
                line.SetPositions(ConvertLocalPathToGlobal(path.path.localPoints, path.transform));
            }
            else
            {
                line.SetPositions(path.path.localPoints);
            }

            PathVisual pathVisual = new PathVisual(path, line, nonHoverData);
            return pathVisual;
        }

        private Vector3[] ConvertLocalPathToGlobal(Vector3[] localpath, Transform pathTransform)
        {
            Vector3[] path = new Vector3[localpath.Length];
            for(int i=0; i<localpath.Length; i++)
            {
                path[i] = pathTransform.TransformPoint(localpath[i]);
            }

            return path;
        }

        public void Highlight(PathVisual path)
        {
            path.SetAnimationData(hoverData);
        }

        public void Highlight(PathCreator path)
        {
            PathVisual pathVisual = pathVisuals.Find(c => c.path == path);
            Highlight(pathVisual);
        }

        public void Unhighlight(PathCreator path)
        {
            PathVisual pathVisual = pathVisuals.Find(c => c.path == path);
            pathVisual.SetAnimationData(nonHoverData);
        }

        public void Clear()
        {
            foreach(PathVisual pathVisual in pathVisuals)
            {
                GameObject.Destroy(pathVisual.line.gameObject);
            }

            pathVisuals.Clear();
        }
    }

    [System.Serializable]
    public class PathVisual
    {
        public PathCreator path;
        public LineRenderer line;
        public PathVisualAnimationData animationData;
        private float currentTime = 0f;
        public HighlightAnimator animator;

        public PathVisual(PathCreator path, LineRenderer line, PathVisualAnimationData animationData)
        {
            this.path = path;
            this.line = line;
            animator = new HighlightAnimator();
            SetAnimationData(animationData);
        }

        public void SetAnimationData(PathVisualAnimationData animationData)
        {
            this.animationData = animationData;
            animator.Highlight(animationData.data, new List<Renderer>() { line });
        }

        public void UpdateVisual()
        {
            currentTime += Time.deltaTime * animationData.animationSpeed;
            float sin = Mathf.Sin(currentTime);
            float sinT = Mathf.Lerp(-1, 1, sin);
            float inverseSin = Mathf.InverseLerp(-1, 1, sin);
            animator.Update(inverseSin);
        }
    }

    [System.Serializable]
    public class PathVisualAnimationData
    {
        public HighlightData data;
        public float animationSpeed;
    }
}
