using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Updatables
{
    public interface IUpdatable
    {
        void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished);
        void UpdateUpdatable();
    }
}
