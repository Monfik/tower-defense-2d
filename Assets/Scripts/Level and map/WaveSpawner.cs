using TowerDefense.Creatures;
using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using PathCreation;
using System.Linq;

namespace LevelLogic
{
    public class WaveSpawner : MonoBehaviour
    {
        private DataStorage storage;
        private WaveData spawningWaveData;
        private Wave spawningWave;
        private bool spawning = false;
        private float currentTime = 0f;

        [SerializeField]
        private float spawnDelay = 2f;
        private float indexatorDelay = 1f;
        private float currentCountDownTime;

        private UnityAction UpdateSpawnerState;

        public void Init(DataStorage storage)
        {
            this.storage = storage;
        }

        public void Spawn(WaveData waveData, Wave wave)
        {
            storage.EventsStorage.CoreEvnts.CallOnWaveStarted(wave);
            spawningWaveData = waveData;
            spawningWave = wave;

            currentTime = 0f;
            spawning = true;

            currentCountDownTime = 0f;
            UpdateSpawnerState = UpdateCountDown;
        }

        public void UpdateSpawner()
        {
            if (!spawning)
                return;

            UpdateSpawnerState?.Invoke();
        }

        private void UpdateCountDown()
        {
            currentCountDownTime += Time.deltaTime;

            if(currentCountDownTime >= spawnDelay)
            {
                storage.UIStorage.LevelView.WaveNumberText.ShowWave(Values.waveNumber);
                UpdateSpawnerState = UpdateSpawning;
            }
        }

        private void UpdateSpawning()
        {
            currentTime += Time.deltaTime;

            List<MonsterReleaseData> releaseDatas = spawningWaveData.RealeasersData.FindAll(c => c.delay <= currentTime);

            foreach (MonsterReleaseData data in releaseDatas)
            {
                Spawn(data.monster, data.path.path, data.monsterData);
            }

            foreach (MonsterReleaseData data in releaseDatas)
            {
                spawningWaveData.RealeasersData.Remove(data);
            }

            if (spawningWaveData.RealeasersData.Count == 0)
                FinishSpawn();
        }


        private void Spawn(Monster monster, VertexPath path, MonsterData monsterData)
        {
            SpawnMonster(monster, path, monsterData, spawningWave.transform, true, true);
        }

        public Monster SpawnMonster(Monster prefab, VertexPath path, MonsterData monsterData, Transform parent, bool playSpawnAnimation, bool callEvent)
        {
            Monster spawnedMonster = Instantiate(prefab, parent);           
            MonsterData clonedMonsterData = new MonsterData(monsterData);
            clonedMonsterData.hp = Mathf.RoundToInt(clonedMonsterData.hp * Values.DifiicultyValues.GetHpMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            spawnedMonster.Init(storage, path, clonedMonsterData);
            if (playSpawnAnimation)
                PlayRespawnAnimationIfExists(path);
            if (callEvent)
                storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(spawnedMonster);

            return spawnedMonster;
        }

        public Monster SpawnMonster(Monster prefab, VertexPath path, MonsterData monsterData, Transform parent, bool playSpawnAnimation, bool callEvent, float distanceTravelled)
        {
            Monster spawnedMonster = Instantiate(prefab, parent);
            MonsterData clonedMonsterData = new MonsterData(monsterData);
            clonedMonsterData.hp = Mathf.RoundToInt(clonedMonsterData.hp * Values.DifiicultyValues.GetHpMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            spawnedMonster.Init(storage, path, clonedMonsterData, distanceTravelled);
            if (playSpawnAnimation)
                PlayRespawnAnimationIfExists(path);
            if (callEvent)
                storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(spawnedMonster);

            return spawnedMonster;
        }

        public Monster SpawnMonster(Monster prefab, MonsterData monsterData, Transform parent, bool callEvent)
        {
            Monster spawnedMonster = Instantiate(prefab, parent);
            MonsterData clonedMonsterData = new MonsterData(monsterData);
            clonedMonsterData.hp = Mathf.RoundToInt(clonedMonsterData.hp * Values.DifiicultyValues.GetHpMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            spawnedMonster.Init(storage, null, clonedMonsterData);
            if (callEvent)
                storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(spawnedMonster);

            return spawnedMonster;
        }

        public T SpawnMonster<T>(T prefab, MonsterData monsterData, Transform parent, bool callEvent) where T : Monster
        {
            T spawnedMonster = Instantiate(prefab, parent);
            MonsterData clonedMonsterData = new MonsterData(monsterData);
            clonedMonsterData.hp = Mathf.RoundToInt(clonedMonsterData.hp * Values.DifiicultyValues.GetHpMultiplier(storage.ReferenceStorage.Settings.difficultyLevel));
            spawnedMonster.Init(storage, null, clonedMonsterData);
            if (callEvent)
                storage.EventsStorage.CoreEvnts.CallOnMonsterSpawned(spawnedMonster);

            return spawnedMonster;
        }

            private void FinishSpawn()
        {
            spawning = false;
            spawningWaveData = null;
        }

        public void PlayRespawnAnimationIfExists(VertexPath path)
        {
            Level currentLevel = storage.ReferenceStorage.LevelManager.CurrentLevel;
            RespawnPlaceAnimationPair respawnAnimationPair = currentLevel.RespawnPlacesAnimations.Find(c => c.paths.Find(v => (v.path == path)) != null);
            if(respawnAnimationPair != null)
            {
                respawnAnimationPair.respawnAnimator.Animate();
            }
        }
    }
}
