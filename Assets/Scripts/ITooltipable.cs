using System.Collections;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine;

public interface ITooltipable
{
    DescriptionData CreateTooltipData(DescriptionData data);
}
