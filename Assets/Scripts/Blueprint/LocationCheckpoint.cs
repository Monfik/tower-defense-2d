using DG.Tweening;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Blueprints
{
    public class LocationCheckpoint : MonoBehaviour, IVisitable
    {
        [Header("Sprites")]
        [SerializeField] private Sprite unlockedFlag;
        [SerializeField] private Sprite lockedFlag;

        [Header("References")]
        [SerializeField] private Button button;
        public Button Button => button;
        [SerializeField] private Image flag;
        [SerializeField] private RectTransform flagRect;
        [SerializeField] private Image locationArtwork;
        [SerializeField] private Image skullMark;
        [SerializeField] private Level level;

        [Header("Values")]
        [Header("Unlock shake values")]
        [SerializeField] private Vector2 flagBounceSizeDelta;
        [SerializeField] private float bounceTime = 1f;
        [Space(10)]
        [SerializeField] Ease unlockEase;

        [Space(20)]
        [Header("Finish shake values")]
        [SerializeField][Range(0,90)] private int strength;
        [SerializeField][Range(0, 10)] private int vibro;
        [SerializeField][Range(0, 90)] private int randomness;

        [Space(20)]
        //[Header("Close values")]
        //[SerializeField] [Range(0, 90)] private int strength;
        private Vector2 startRectSize;
        public Level Level => level;

        [Space(15)]
        [SerializeField] private LocationState currentState;
        public LocationState CurrentState => currentState;
        public enum LocationState { Closed, Open, Finished }

        [Header("Values")]
        [SerializeField] private int unlockLevel;
        public int UnlockLevel => unlockLevel;

        private void Start()
        {
            
        }

        public void SetState(LocationState state)
        {
            bool opened = state == LocationState.Open;
            locationArtwork.gameObject.SetActive(state != LocationState.Closed);
            if (opened)
                Unlock();
            else if (state == LocationState.Closed)
            {
                Lock();
            }
            else //Finished
            {             
                Finish();
            }
            currentState = state;
        }

        private void Finish()
        {
            flag.sprite = unlockedFlag;
            flagRect.DOShakeRotation(1f, strength, vibro, randomness);
            skullMark.gameObject.SetActive(true);
            button.interactable = false;
            locationArtwork.color = new Color(255, 255, 255, 0.5f);
        }

        private void Unlock()
        {
            startRectSize = flagRect.sizeDelta;
            flag.sprite = unlockedFlag;
            Sequence shakeSequence = DOTween.Sequence();
            shakeSequence.Append(flagRect.DOSizeDelta(flagBounceSizeDelta, bounceTime).SetEase(unlockEase)).
                Append(flagRect.DOSizeDelta(startRectSize, bounceTime).SetEase(unlockEase)).OnComplete(() => button.interactable = true);           
        }

        private void Lock()
        {
            flag.sprite = lockedFlag;
            skullMark.gameObject.SetActive(false);
            button.interactable = false;
            button.onClick.RemoveAllListeners();
        }

        public bool CanBeUnlocked()
        {
            return currentState == LocationState.Closed && Values.levelNumber >= unlockLevel;
        }

        public void AssignSelectCallback(UnityAction<LocationCheckpoint> Callback)
        {
            button.onClick.AddListener(() => Callback?.Invoke(this));
        }

        [ContextMenu("Close")]
        public void LockTest()
        {
            SetState(LocationState.Closed);
        }

        [ContextMenu("Unlock")]
        public void UnlockTest()
        {
            SetState(LocationState.Open);
        }

        [ContextMenu("Finish")]
        public void FinishTest()
        {
            SetState(LocationState.Finished);
        }
    }
}
