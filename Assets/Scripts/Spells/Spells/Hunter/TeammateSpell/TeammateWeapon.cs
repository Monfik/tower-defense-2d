using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Towers;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class TeammateWeapon : BaseInitable
    {
        [SerializeField] protected Image image;
        [SerializeField] protected Animator animator;
        [SerializeField] private Transform arrowPivot;
        [SerializeField] protected TeammateWeaponMagazine magazine;

        [Header("Bullets data")]
        [SerializeField] private PhysicalBullet prefabBullet;
        [SerializeField] private PhysicsBulletData bulletData;
        private List<PhysicalBullet> bullets = new List<PhysicalBullet>();

        private Monster target;

        private UnityAction OnMagazineEmpty;

        public void Init(UnityAction OnMagazineEmpty)
        {
            this.OnMagazineEmpty = OnMagazineEmpty;
            bulletData.Reset();
        }

        public override void Init(DataStorage storage)
        {
            base.Init(storage);
            storage.EventsStorage.CoreEvnts.OnMonsterDied += OnMonsterNotReached;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath += OnMonsterNotReached;
        }

        private void OnMonsterNotReached(Monster monster)
        {
            List<PhysicalBullet> bulletToDestroy = bullets.FindAll(c => c.Target == monster);

            for(int i=bulletToDestroy.Count - 1; i >= 0; i--)
            {
                bullets.Remove(bulletToDestroy[i]);
                Destroy(bulletToDestroy[i].gameObject);
            }

            if (target == null)
                animator.enabled = false;
        }

        public void Attack(Monster target)
        {
            magazine.amount--;
            this.target = target;
            animator.enabled = true;
        }

        public void UpdateWeapon()
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].UpdateBullet();
            }

            if (target == null)
                return;

            Vector3 vectorToTarget = target.GetCenter() - GetImageWorldPosiion(transform.position, storage.ReferenceStorage.MainCamera);
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);
        }

        private Vector3 GetImageWorldPosiion(Vector2 position, Camera cam)
        {
            Vector3 pos = new Vector3(position.x, position.y, 10f);
            return cam.ScreenToWorldPoint(pos);
        }

        public void AnimationShotCallback()
        {
            animator.enabled = false;
            Vector3 canvasPosition3D = new Vector3(arrowPivot.transform.position.x, arrowPivot.transform.position.y, 10f);
            Vector2 worldPosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(canvasPosition3D);

            if (target == null || target.Dead)
                return;

            Shot(target, worldPosition);
        }

        private void Shot(Monster monster, Vector2 positionFrom)
        {
            PhysicalBullet newBullet = Instantiate(prefabBullet);
            newBullet.gameObject.SetActive(true);
            newBullet.transform.position = positionFrom;
            newBullet.Init(monster, bulletData, OnBulletHitted);
            bullets.Add(newBullet);
        }

        private void OnBulletHitted(PhysicalBullet bullet)
        {
            bullets.Remove(bullet);
            Destroy(bullet.gameObject);

            if (magazine.amount <= 0)
                CallFinish();
        }

        public void DestroyBullets()
        {
            for (int i = bullets.Count - 1; i >= 0; i--)
            {
                Destroy(bullets[i].gameObject);
            }

            bullets.Clear();
        }

        private void CallFinish()
        {
            DestroyBullets();
            storage.EventsStorage.CoreEvnts.OnMonsterDied -= OnMonsterNotReached;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath -= OnMonsterNotReached;
            OnMagazineEmpty?.Invoke();
        }

        public override void Deinit()
        {
            base.Deinit();
            storage.EventsStorage.CoreEvnts.OnMonsterDied -= OnMonsterNotReached;
            storage.EventsStorage.CoreEvnts.OnMonsterFinishedPath -= OnMonsterNotReached;
        }
    }
}