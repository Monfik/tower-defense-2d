using Data;
using DG.Tweening;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;
using static UnityEngine.ParticleSystem;

namespace TowerDefense.GlobalEvents
{
    public class SandstormEvent : BaseDurationableEvent
    {
        private Vector3 currentDirection;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float diseappringDuration = 1f;
        [SerializeField] private Sandstorm sandstorm;

        private ParticleSystemRenderer dustRenderer;
        private ParticleSystemRenderer sandRenderer;

        public override void BeginEvent(DataStorage storage, UnityAction<IGlobalEvent> OnEventFinished)
        {
            base.BeginEvent(storage, OnEventFinished);

            //currentDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            //currentDirection = currentDirection.normalized;
            Vector2[] directions = new Vector2[4]
            {
                new Vector2(1f, 0f),
                new Vector2(-1f, 0f),
                new Vector2(0f, 1f),
                new Vector2(0f, -1f)
            };

            currentDirection = directions[Random.Range(0, 4)];
            sandstorm.gameObject.SetActive(true);
            Vector2 mapSize = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.GetSize();
            MapLimitsData mapLimits = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MapLimits;
            float mapHeight = mapSize.y;
            sandstorm.transform.position = CalculatePosition(mapSize, currentDirection, mapLimits);
            sandstorm.SetSize(mapSize);

            movementSpeed = (mapSize.y / mapSize.x) * movementSpeed;
            duration = CalculateDuration(mapSize, movementSpeed, currentDirection);
        }

        private Vector2 CalculatePosition(Vector2 mapSize, Vector2 direction, MapLimitsData mapLimits)
        {
            Vector2 mapCenter = mapLimits.GetCenter();
            float posX = mapCenter.x + -direction.x * mapSize.x;
            float posY = mapCenter.y + -direction.y * mapSize.y;
            Vector2 sandstormPosition = new Vector2(posX, posY);

            return sandstormPosition;
        }

        public float CalculateDuration(Vector2 mapSize, float movementSpeed, Vector2 direction)
        {
            float duration = 0f;

            if(direction.x != 0f) //horizontal movement
            {
                float distance = mapSize.x * 2;
                duration = distance / movementSpeed;
            }
            else if(direction.y != 0f)
            {
                float distance = mapSize.y * 2;
                duration = distance / movementSpeed;
            }

            return duration;
        }

        public override void UpdateEvent()
        {
            base.UpdateEvent();
            sandstorm.transform.position = sandstorm.transform.position + currentDirection * movementSpeed * Time.deltaTime;
            sandstorm.UpdateSandstorm();
        }

        public override void StartToFinishEvent()
        {
            base.StartToFinishEvent();
            ParticleSystemRenderer renderer = sandstorm.dust.GetComponent<ParticleSystemRenderer>();
            dustRenderer = sandstorm.dust.GetComponent<ParticleSystemRenderer>();
            sandRenderer = sandstorm.sand.GetComponent<ParticleSystemRenderer>();
            DOVirtual.Float(renderer.material.color.a, 0f, diseappringDuration, SetupSandstormAlpha).OnComplete(FinishEvent);
        }

        private void SetupSandstormAlpha(float alpha)
        {
            Color color = dustRenderer.material.color;
            Color colorSand = sandRenderer.material.color;
            color.a = alpha;
            colorSand.a = alpha;
            dustRenderer.material.color = color;
            sandRenderer.material.color = colorSand;
        }
    }
}
