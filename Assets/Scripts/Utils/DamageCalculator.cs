using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Power;
using UnityEngine;

namespace Utils
{
    public static class DamageCalculator
    {
        public static int CalculateDamage(int damage, int armor)
        {
            float x = armor / 50f;
            int result = Mathf.RoundToInt(damage * (1f / Mathf.Pow(2, x)));

            return result;
        }

        public static int CalculateMagicDamage(MonsterData data, int dmg, MagicType magicType)
        {
            MonsterStaticsData magicStatics = data.GetDataByType(magicType);

            int magicResitance = magicStatics.currentValue;
            magicResitance -= magicStatics.currentValue;
            magicResitance = Mathf.Clamp(magicResitance, magicStatics.minValue, magicStatics.maxValue);
            return CalculateDamage(dmg, magicResitance);
        }

        public static int CalculateDamage(int damage, int armor, int armorReduction)
        {
            armor -= armorReduction;
            float x = armor / 50f;
            int result = Mathf.RoundToInt(damage * (1f / Mathf.Pow(2, x)));

            return result;
        }

        public static int CalculateDamage(PhysicalAttackData attackData, int armor)
        {
            armor -= attackData.armorReduction;
            float x = armor / 50f;
            int randomDamage = Random.Range(attackData.minDmg, attackData.maxDmg);
            int result = Mathf.RoundToInt(randomDamage * (1f / Mathf.Pow(2, x)));

            return result;
        }
    }
}
