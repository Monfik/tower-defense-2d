using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TowerDefense.Effects
{
    public class LightingStormEntity : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private SpriteRenderer lightingRenderer;
        private float minX;
        private float maxX;

        [Range(0.8f, 1.2f)]
        [SerializeField] private float animatorSpeedA;
        [Range(0.8f, 1.2f)]
        [SerializeField] private float animatorSpeedB;

        [SerializeField] private float randomAlphaA;
        [SerializeField] private float randomAlphaB;

        private Vector3 startScale;

        public void StartStorm(float minX, float maxX)
        {
            this.minX = minX;
            this.maxX = maxX;

            gameObject.SetActive(true);
            animator.enabled = true;
            startScale = transform.localScale;

            Color targetColor = lightingRenderer.color;
            targetColor.a = 0f;
            lightingRenderer.color = targetColor;

            AnimationFinished();
        }

        public void StopStorm()
        {
            animator.SetTrigger("Finish");
        }

        public void AnimationFinished()
        {
            RandomLightingPosition();
            RandomAnimatorSpeed();
        }

        private void RandomLightingPosition()
        {
            const int iterations = 1;
            float sum = 0;
            for (int i = 0; i < iterations; i++)
            {
                sum += Random.Range(minX, maxX);
            }

            sum /= iterations;
            transform.position = new Vector3(sum, transform.position.y, 0f);
        }

        private void RandomAnimatorSpeed()
        {
            float animatorSpeed = Random.Range(animatorSpeedA, animatorSpeedB);
            animator.speed = animatorSpeed;

            float scaleFactor = animatorSpeed;
            Vector3 targetScale = startScale * scaleFactor;
            transform.DOScale(targetScale, 0.2f);

            float randomAlphaTarget = Random.Range(randomAlphaA, randomAlphaB) / 255f;
            Color targetColor = Color.white;
            targetColor.a = randomAlphaTarget;
            lightingRenderer.DOColor(targetColor, 0.5f);
        }

        public void DoColor(Color color, float time)
        {
            lightingRenderer.DOColor(color, time);        
        }

        public void KillAllTweens()
        {
            DOTween.Kill(transform);
            DOTween.Kill(lightingRenderer);
        }
    }
}
