using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class Values
    {
        public static int waveNumber = 1;
        public static int levelNumber = 1;
        public static int gold = 100;

        public static MaterialCostsData materialCosts;

        public static class GeneralValues
        {
            public const float checkpointDiffValue = 0.1f;
            public const float minBulletDistValue = 0.1f;
            public const float monsterSpeedDivider = 7f;
            public const float monsterMinRunSpeed = 15f;
            public static float sellMultiplier = 1f;
        }

        public static class InputValues
        {
            public static bool POINTER_OVER_UI = false;
            public static bool POINTER_ON_ACTION = false;
            public static bool BUILDING_POINTER = false;
            public const float DOUBLE_CLICK_THRESHOLD = 0.25f;

            public const KeyCode DEFAULT_ACCEPT_KEY = KeyCode.Mouse0;
            public const KeyCode DEFAULT_REJECT_KEY = KeyCode.Mouse1;

            public const int PREVIEW_ACCEPT_ID = 10;
            public const int PREVIEW_REJECT_ID = 11;

            public const int DEFAULT_ACCEPT_SPELL_CAST = 20;
            public const int DEFAULT_REJECT_SPELL_CAST = 21;

            public const int DEFAULT_REPOSITIONER_ACCEPT_KEY = 30;
            public const int DEFAULT_REPOSITIONER_REJECT_KEY = 31;

            public const int TOWER_SELECTOR_MOUSE_CLICKED = 41;
            public const int SELECTOR_MOUSE_CLICKED = 42;

            public const int PAUSE_TRIGGER_ID = 40;
            public const KeyCode PAUSE_TRIGGER_KEY = KeyCode.Escape;
        }

        public static class TowerValues
        {
            public const float minAttackSpeed = 1f;
            public const float minDamage = 1f;
        }

        public static class LayerMasks
        {
            public const int MonsterLayer = 7;
            public const int RoadLayer = 8;
            public const int TowerLayer = 10;
            public const int TowerUnaccessableLayer = 11;
            public const int MonsterLegsLayer = 12;
            public const int BaseLayer = 16;
        }

        public static class DifiicultyValues
        {
            public const float easy = 0.9f;
            public const float medium = 1f;
            public const float hard = 1.1f;

            public static float GetHpMultiplier(int difficulty)
            {
                float multiplier = 1f;

                switch(difficulty)
                {
                    case 0:
                        multiplier = easy;
                        break;
                    case 1:
                        multiplier = medium;
                        break;
                    case 2:
                        multiplier = hard;
                        break;
                }

                return multiplier;
            }

            public static float GetMaxManaMultiplier(int difficulty)
            {
                float []multipliers = new float[3] { 1.1f, 1f, 0.9f };

                return multipliers[difficulty];
            }

            public static float GetManaRegenMultiplier(int difficulty)
            {
                float[] multipliers = new float[3] { 1.1f, 1f, 0.9f };

                return multipliers[difficulty];
            }
        }
    }

    [System.Serializable]
    public class MaterialCostsData
    {
        public float wood = 1f;
        public float clay = 2f;
        public float iron = 3f;

        public MaterialCostsData(float wood, float clay, float iron)
        {
            this.wood = wood;
            this.clay = clay;
            this.iron = iron;
        }
    }
}
