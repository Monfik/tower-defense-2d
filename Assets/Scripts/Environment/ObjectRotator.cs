using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Enviroment
{
    public class ObjectRotator : MonoBehaviour
    {
        [SerializeField] private Transform objToRotate;

        [SerializeField] private float speed = 1f;
        [SerializeField] private Vector3 startRotation;
        [SerializeField] private Vector3 targetRotation;
        [SerializeField] private bool slowStart = true;
        [SerializeField] private bool middleStart = true;
        [SerializeField] private float speedIncValue = 0.5f; //per second
        [SerializeField] private float currentSpeed;

        private float currentTime = 0f;

        private void OnEnable()
        {
            if (slowStart)
                currentSpeed = 0f;
            else
                currentSpeed = speed;

            currentSpeed = 0f;
        }

        private void Update()
        {
            if(slowStart)
            {
                currentSpeed += speedIncValue * Time.deltaTime;
                currentSpeed = Mathf.Clamp(currentSpeed, 0f, speed);
            }

            currentTime += Time.deltaTime * speed;
            if (currentTime > 360f )
                currentSpeed -= 360f;

            float sin = Mathf.Sin(Mathf.PI / 180f * currentTime);
            float inversedSin = Mathf.InverseLerp(-1f, 1f, sin);
            Vector3 rotation = Vector3.Lerp(startRotation, targetRotation, inversedSin);
            objToRotate.rotation = Quaternion.Euler(rotation);
        }

        private void OnDisable()
        {
            if (slowStart)
                currentSpeed = 0f;
            else
                currentSpeed = speed;

            currentSpeed = 0f;
        }
    }
}
