using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Towers
{
    public interface ITowerMovable
    {
        void Move(Vector2 position);
    }
}
