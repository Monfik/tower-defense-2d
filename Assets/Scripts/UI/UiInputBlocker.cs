﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using CustomInput;
using Utils;

public class UiInputBlocker : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{ 

    public void OnPointerEnter(PointerEventData eventData)
    {
        Values.InputValues.POINTER_OVER_UI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Values.InputValues.POINTER_OVER_UI = false;
    }
}
