using Data;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;
using Utils;

namespace TowerDefense.Creatures
{
    public class WizardBossMagicShieldSpell : BaseBossState<WizardBossBehaviour>
    {
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private DamageShieldEffect magicShieldEffect;
        [SerializeField] private SpriteRenderer energyBlast;

        [SerializeField] private float shieldingDuration = 2f;
        [SerializeField] private float elementsAppearingDuration = 0.5f;

        public override void PreInit(WizardBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(WizardBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            boss.Animator.Play("ShieldBuff");
            boss.Animator.SetFloat("ShieldingSpellSpeed", 1f);
            storage.ReferenceStorage.Postponer.InvokePostponeAction(StartCreateShield);

            boss.OnMonsterDied += KillAllTweens;
        }

        private void KillAllTweens(Monster boss)
        {
            int killed = DOTween.Kill(1);
        }

        public void StartCreateShield()
        {
            float shieldingAnimationDuration = boss.Animator.GetCurrentAnimatorStateInfo(0).length;
            float animatorSpeed = shieldingAnimationDuration / shieldingDuration;
            boss.Animator.SetFloat("ShieldingSpellSpeed", animatorSpeed);

            DamageShieldEffect effect = Instantiate(magicShieldEffect);
            effect.MakeTransparable(0f);
            effect.gameObject.SetActive(true);
            effect.effectTimeOver += OnShieldDestroyed;
            boss.Effects.AddEffect(effect);

            energyBlast.SetSpriteColorAlpha(0f);
            energyBlast.gameObject.SetActive(true);

            energyBlast.transform.DOShakeRotation(elementsAppearingDuration, 20f, 2, 50f).SetId(1);
            energyBlast.transform.DOShakeScale(elementsAppearingDuration, 0.2f, 4, 20f).SetId(1);
            DOVirtual.Float(0f, 1f, elementsAppearingDuration, (f) => OnAppearingElementsFloatUpdate(f, energyBlast, effect)).OnComplete(effect.AddShield).SetId(1);
        }

        private void OnAppearingElementsFloatUpdate(float value, SpriteRenderer blast, ITransparable transparable) // 0 - 1 <- alpha
        {
            blast.SetSpriteColorAlpha(value);
            transparable.MakeTransparable(value);
        }

        public void OnShieldDestroyed(BaseMonsterEffect effect)
        {
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval * 0.5f + pathInterval.intervalRandomRange.GetRandom(2f);
        }

        //Called by animator
        public void FinishCreatingShield()
        {
            energyBlast.gameObject.SetActive(false);
            
            boss.SetDefaultMovementState();
        }

        public override void UpdateState()
        {
            
        }

        public override void Deinit()
        {
            energyBlast.gameObject.SetActive(false);
            boss.OnMonsterDied -= KillAllTweens;
        }

        public override bool IsReadyForWork()
        {
            return (boss.DistanceTravelled >= pathInterval.nextValue && !IsMonsterShielded(boss));
        }

        private bool IsMonsterShielded(Monster monster)
        {
            return monster.Effects.FindByName(magicShieldEffect.Name) != null;
        }

        private void OnDestroy()
        {
            KillAllTweens(boss);
        }
    }
}
