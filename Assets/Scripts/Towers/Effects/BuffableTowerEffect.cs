using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Effects
{
    public class BuffableTowerEffect : BaseTowerEffect
    {
        protected bool isPositiveBuff;

        public BuffableTowerEffect(BuffableTowerEffect effect) :base(effect)
        {
            isPositiveBuff = effect.isPositiveBuff;
        }

        public BuffableTowerEffect() { }

        public override void StartEffect()
        {

        }
    }
}
