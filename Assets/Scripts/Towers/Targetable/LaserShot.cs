using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using static TowerDefense.Creatures.IDestructable;

namespace TowerDefense.Towers
{
    public class LaserShot : Bullet
    {
        private MagicBulletData magicBulletData;

        [SerializeField] private float existTime = 0.2f;
        private float currentExistTime = 0f;

        [SerializeField]
        private List<BaseMonsterEffect> magicEffects;
        public List<BaseMonsterEffect> MagicEffects => magicEffects;

        [SerializeField] private float rotatedSpriteAngle = 0f;

        public override void Init(Monster target, string jsonedModelData, UnityAction<Bullet> OnTargetReached)
        {
            base.Init(target, jsonedModelData, OnTargetReached);
            MagicBulletData missileData = JsonUtility.FromJson<MagicBulletData>(jsonedModelData);
            magicBulletData = missileData;
            animator.speed = 1 / existTime;

            UpdateRotationAndScale();
        }

        public void DealDamage(Monster target)
        {
            int summaryDamage = 0;

            foreach (MagicDamageData magicBullet in magicBulletData.magicDamages)
            {
                MonsterStaticsData magicStatics = target.Data.GetDataByType(magicBullet.magicType);

                int magicResitance = magicStatics.currentValue;
                magicResitance -= magicStatics.currentValue;
                magicResitance = Mathf.Clamp(magicResitance, magicStatics.minValue, magicStatics.maxValue);
                int randomDmg = (int)Random.Range(magicBullet.minDamage.data.currentValue, magicBullet.maxDamage.data.currentValue);
                int dmg = DamageCalculator.CalculateDamage(randomDmg, magicResitance);
                summaryDamage += dmg;
            }

            foreach (BaseMonsterEffect effect in magicEffects)
            {
                target.Effects.AddEffect(effect);
            }

            target.TakeDamage(summaryDamage, DamageType.Magic);
        }

        private Vector3 AdjustScaleToTarget(Vector3 startPosition, Vector3 targetPosition)
        {
            float distance = Vector3.Distance(startPosition, targetPosition);
            Vector3 size = new Vector3(transform.localScale.x, distance, transform.localScale.z);
            return size;
        }

        private Quaternion AdjustRotationToTarget(Vector3 startPosition, Vector3 targetPosition)
        {
            Vector3 vectorToTarget = targetPosition - startPosition;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            angle += rotatedSpriteAngle;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            Quaternion rotation = Quaternion.RotateTowards(transform.rotation, q, 360f);
            return rotation;
        }

        public override void UpdateBullet()
        {
            UpdateRotationAndScale();

            currentExistTime += Time.deltaTime;
            if(currentExistTime >= existTime)
            { 
                OnTargetReached?.Invoke(this);
            }
        }

        private void UpdateRotationAndScale()
        {
            transform.localScale = AdjustScaleToTarget(transform.position, target.GetCenter());
            transform.rotation = AdjustRotationToTarget(transform.position, target.GetCenter());
        }

        //Dont remove, animator depends on this method!
        private void ShotExecuted()
        {
            DealDamage(target);
        }
    }
}
