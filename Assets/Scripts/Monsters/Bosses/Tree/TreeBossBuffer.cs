using System.Collections;
using System.Collections.Generic;
using Tools;
using TowerDefense.Effects;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class TreeBossBuffer : MonoBehaviour
    {
        private float lastTimeBuff;
        private float nextTimeBuff;

        [Header("Values")]
        [SerializeField] private float minNecesseryDeltaTime;
        [SerializeField] private float maxNecesseryDeltaTime;

        [Header("Buffs")]
        [SerializeField] private List<MonsterBuffEffect> buffs;

        public void Init()
        {
            nextTimeBuff = RandNextTime();
        }

        public void Buff(List<Monster> monsters)
        {
            foreach(Monster monster in monsters)
            {
                Buff(monster);
            }

            nextTimeBuff = RandNextTime();
        }

        private void Buff(Monster monster)
        {
            foreach(MonsterBuffEffect effect in buffs)
            {
                MonsterBuffEffect buff = Instantiate(effect, monster.transform);
                monster.Effects.AddEffect(buff);
            }
        }

        public bool CanBuff()
        {
            return Time.time >= nextTimeBuff;
        }

        private float RandNextTime()
        {
            float random = Random.Range(minNecesseryDeltaTime, maxNecesseryDeltaTime);
            random += Time.time;
            return random;
        }
    }
}
