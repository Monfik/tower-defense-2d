using Core.Invokers;
using Data;
using System.Collections.Generic;
using TowerDefense.Tips;
using UnityEngine.Events;

namespace TowerDefense.Spells
{
    public class MapFireballSpellCommand : BaseCommandMono
    {
        protected FireballingMapData fireballsMapData;

        public void InitData(FireballingMapData fireballsMapData)
        {
            this.fireballsMapData = fireballsMapData;
        }

        public virtual void ManageCaster(FireballSpellCaster caster)
        {

        }

        public virtual List<DescriptionSingleSpecifiedData> GetTooltipData()
        {
            return null;
        }
    }
}
