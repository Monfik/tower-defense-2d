using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Towers;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TowerDefense.Spells
{
    public class BaseTeammate : MonoBehaviour
    {
        [SerializeField] protected string mateName;
        public string MateName => mateName;

        [SerializeField] protected Image avatar;
        public Image Avatar => avatar;
        [SerializeField] protected TeammateWeapon weapon;

        protected float currentTime = 0f;
        [SerializeField] protected float attackIntervalTime;

        private UnityAction<BaseTeammate> OnTeammateFinished;
        private DataStorage storage;
        private Monster currentTarget;

        public void Init(UnityAction<BaseTeammate> OnTeammateFinished, DataStorage storage)
        {
            this.OnTeammateFinished = OnTeammateFinished;
            this.storage = storage;
            currentTime = 0f;
            weapon.Init(OnFinished);
            weapon.Init(storage);
            currentTime = attackIntervalTime;
        }

        public void UpdateMate()
        {
            currentTime += Time.deltaTime;
            if(currentTime >= attackIntervalTime)
            {
                currentTarget = GetRandomMonster();
                if(currentTarget != null)
                {
                    Attack();
                    currentTime = 0f;
                }
            }

            weapon.UpdateWeapon();
        }

        public void Attack()
        {
            weapon.Attack(currentTarget);
        }

        private void OnFinished()
        {
            OnTeammateFinished?.Invoke(this);
        }

        protected virtual Monster GetRandomMonster()
        {
            List<Monster> potentialMonsters = storage.ReferenceStorage.WaveController.AllMonsters.FindAll(c => !c.Dead);
            int monstersCount = potentialMonsters.Count;
            if (monstersCount == 0)
                return null;

            int random = Random.Range(0, monstersCount - 1);
            Monster randomMonster = potentialMonsters[random];
            return randomMonster;
        }

        private void OnDestroy()
        {
            weapon.DestroyBullets();
            weapon.Deinit();
        }
    }
}
