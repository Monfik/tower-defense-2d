using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Effects;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class SlowMonsterEffect : BaseMonsterEffect
    {
        [SerializeField] private SlowEffectData slowData;

        [Header("Slow curve values")]
        [SerializeField] private float slowGrowDuration;
        [SerializeField] private float slowDownDuration;
        [SerializeField] private AnimationCurve slowCurve;

        private UnityAction CurrentHandleTimeAction;
        private int lastSlowedValue;

        public void Init(SlowEffectData iceData)
        {
            this.slowData = iceData;
            duration = iceData.slowDuration;
        }

        public override void StartEffect(Monster monster, UnityAction<BaseMonsterEffect> effectTimeOver)
        {
            base.StartEffect(monster, effectTimeOver);
            CurrentHandleTimeAction = HandleGrowTime;
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();
            //CurrentHandleTimeAction?.Invoke();

            float t = currentTime / duration;
            float evaluatedT = slowCurve.Evaluate(t);
            int slowValue = Mathf.RoundToInt(evaluatedT * slowData.slowValue);
            int slowValueDiff = lastSlowedValue - slowValue;

            if (lastSlowedValue != slowValue)
                monster.Data.speed.AddValue(slowValueDiff);

            lastSlowedValue = slowValue;
        }

        public override void EndEffect()
        {
            base.EndEffect();
        }

        private void HandleGrowTime()
        {
            float t = currentTime / duration;
            float evaluatedT = slowCurve.Evaluate(t);
            int slowValue = Mathf.RoundToInt(evaluatedT * slowData.slowValue);
            int slowValueDiff = slowValue - lastSlowedValue;
            lastSlowedValue = slowValue;

            if (lastSlowedValue != slowValue)
                monster.Data.speed.AddValue(-slowValueDiff);

            if (currentTime >= slowGrowDuration)
                CurrentHandleTimeAction = HandleRawTime;
        }

        private void HandleRawTime()
        {
            if (currentTime >= duration - slowDownDuration)
                CurrentHandleTimeAction = HandleDownTime;
        }

        private void HandleDownTime()
        {
            float t = currentTime / duration;
            float evaluatedT = slowCurve.Evaluate(t);
            int slowValue = Mathf.RoundToInt(evaluatedT * slowData.slowValue);
            int slowValueDiff = lastSlowedValue - slowValue;
            lastSlowedValue = slowValue;

            if (lastSlowedValue != slowValue)
                monster.Data.speed.AddValue(slowValueDiff);
        }
    }
    
    
}