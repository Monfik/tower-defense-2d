using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public interface ITargetable
    {
        Vector3 GetPosition();
        Vector3 GetCenter();
    }
}
