using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Updatables
{
    public class BaseUpdatableMonoless : IUpdatable
    {
        protected UnityAction<IUpdatable> OnUpdatableFinished;
        protected DataStorage storage;
        public float duration;
        protected float currentTime;
        public Sprite icone;
        public bool durationable = true;

        public virtual void StartUpdate(DataStorage storage, UnityAction<IUpdatable> OnUpdatableFinished)
        {
            this.storage = storage;
            this.OnUpdatableFinished = OnUpdatableFinished;
        }

        public virtual void UpdateUpdatable()
        {
            if(durationable)
            {
                currentTime += Time.deltaTime;
                if (currentTime >= duration)
                    Finish();
            }
        }

        protected virtual void Finish()
        {
            OnUpdatableFinished?.Invoke(this);
        }

        public Sprite GetIcon()
        {
            return icone;
        }
    }
}
