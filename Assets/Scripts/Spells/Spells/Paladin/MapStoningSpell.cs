using PathCreation;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class MapStoningSpell : BaseSpell
    {
        [SerializeField] private float minTimeInterval = 0.1f;
        [SerializeField] private float maxTimeInterval = 0.3f;
        [SerializeField] private float stoningTime = 4f;
        [SerializeField] private FallingBombData bombData;
        [SerializeField] private StoneFallingBomb bombPrefab;

        private float randomBombTime;
        private float currentBombTime = 0f;
        private float currentTime = 0f;

        public override void StartCast()
        {
            base.StartCast();
            OnSpellCasted?.Invoke(this);
            PayMana();
            randomBombTime = Random.Range(minTimeInterval, maxTimeInterval);
        }

        public override void UpdateSpell()
        {
            currentTime += Time.deltaTime;
            currentBombTime += Time.deltaTime;

            if(currentBombTime >= randomBombTime)
            {
                Bomb();
                randomBombTime = Random.Range(minTimeInterval, maxTimeInterval);
                currentBombTime = 0f;
            }

            if(currentTime >= stoningTime)
            {
                FinishSpell();
            }
        }

        Vector3 targetBombPosition;

        private void Bomb()
        {
            int pathCount = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MiddlePaths.Count;
            int randomPathIndex = Random.Range(0, pathCount - 1);
            PathCreator randomPath = storage.ReferenceStorage.LevelManager.CurrentLevel.Map.MiddlePaths[randomPathIndex];
            float randomPathPlace = Random.Range(0.1f, 0.9f);
            targetBombPosition = randomPath.path.GetPointAtTime(randomPathPlace);
            StoneFallingBomb bomb = Instantiate(bombPrefab);
            bombData.startPosition = new Vector2(targetBombPosition.x, 20f);
            bombData.targetPosition = targetBombPosition;
            FallingBombData newData = new FallingBombData(bombData);
            bomb.Init(newData);
            bomb.InitData(storage, 10f);
            bomb.gameObject.SetActive(true);
            storage.EventsStorage.CoreEvnts.CallOnMapEffectAppeared(bomb);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();

            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("<sprite=5>Physical", $"{bombData.physicalDmg}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Armor reduction", $"{bombData.armorReduction}"));
            data.specifiedDatas.Add(new Tips.DescriptionSingleSpecifiedData("Duration", $"{stoningTime}"));
            return data;
        }

        private void OnDrawGizmos()
        {
            //if (storage == null)
            //    return;

            //Vector2 mapSize = storage.ReferenceStorage.LevelController.CurrentLevel.Map.GetSize();
            //Vector3 pointOverMap = new Vector3(0f, mapSize.y / 2 + 1f, 0f);
            //Vector3 pointBelowMap = new Vector3(0f, -mapSize.y / 2 - 1f, 0f);
            //Gizmos.DrawSphere(targetBombPosition, 1f);
        }

        public override void FinishSpell()
        {
            currentTime = 0f;
            currentBombTime = 0f;
            base.FinishSpell();         
        }
    }

    [System.Serializable]
    public class FallingBombData
    {
        public int physicalDmg;
        public int armorReduction;
        [HideInInspector] public Vector2 startPosition;
        [HideInInspector] public Vector2 targetPosition;

        public FallingBombData(FallingBombData data)
        {
            physicalDmg = data.physicalDmg;
            armorReduction = data.armorReduction;
            startPosition = data.startPosition;
            targetPosition = data.targetPosition;
        }
    }
}

