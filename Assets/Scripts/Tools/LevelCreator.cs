using LevelLogic;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public class LevelCreator : MonoBehaviour
    {
        public LineRenderer pathRepresentation;
        public List<MonsterDataSetupData> monsterSetupers;
        public Level level;
        public float delay;
    }
}
