using Core.Invokers;
using Data;
using LevelLogic;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Effects;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.Spells
{
    public class SelectFireballTargetPlace : MapFireballSpellCommand
    {
        private PathSnapper snapper;
        [SerializeField] private FireballPreview fireballPreview;
        [SerializeField] private FireballMarker mark;
        private Vector2 markedPosition;

        [SerializeField] private FireballData fireballData;

        public override void InitCommand(DataStorage storage, UnityAction OnCommandFinished, UnityAction OnCommandFailed)
        {
            base.InitCommand(storage, OnCommandFinished, OnCommandFailed);
            snapper = storage.ReferenceStorage.PathSnapper;
            fireballPreview.gameObject.SetActive(true);
        }

        public override void UpdateCommand()
        {
            base.UpdateCommand();
            Vector2 mousePosition = storage.ReferenceStorage.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 snappedPosition = Vector2.zero;
            bool snapped = snapper.Snap(mousePosition, out snappedPosition);
            fireballPreview.UpdatePosition(snappedPosition);
            if (snapped)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0) && !Values.InputValues.POINTER_OVER_UI)
                {
                    MarkPosition(snappedPosition);
                }
            }
        }

        private void MarkPosition(Vector2 position)
        {
            markedPosition = position;
            mark.transform.position = position;
            mark.gameObject.SetActive(true);
            fireballsMapData.fireballsPositions.Add(position);
            fireballPreview.gameObject.SetActive(false);
            OnCommandFinished?.Invoke();
        }

        public override void ManageCaster(FireballSpellCaster caster)
        {
            base.ManageCaster(caster);
            mark.transform.SetParent(caster.transform);
            caster.MarkDataPairs.Add(new MarkerFireballDataPair(mark, fireballData));
        }

        public override List<DescriptionSingleSpecifiedData> GetTooltipData()
        {
            List<DescriptionSingleSpecifiedData> datas = new List<DescriptionSingleSpecifiedData>();
            datas.Add(new DescriptionSingleSpecifiedData("<sprite=5>Physical", $"{fireballData.physicalDmg}"));
            datas.Add(new DescriptionSingleSpecifiedData("Armor reduction", $"{fireballData.armorReduction}"));
            datas.Add(new DescriptionSingleSpecifiedData("<sprite=1>Fire per second", $"{fireballData.fireData.dmgPerSecond}"));
            datas.Add(new DescriptionSingleSpecifiedData("Fire reduction", $"{fireballData.fireData.fireArmorReduction}"));
            datas.Add(new DescriptionSingleSpecifiedData("Fire duration", $"{fireballData.fireData.duration}"));

            return datas;
        }

    }

    public class MarkerFireballDataPair
    {
        public FireballMarker marker;
        public FireballData fireballData;

        public MarkerFireballDataPair(FireballMarker marker, FireballData fireData)
        {
            this.marker = marker;
            this.fireballData = fireData;
        }
    }
}
