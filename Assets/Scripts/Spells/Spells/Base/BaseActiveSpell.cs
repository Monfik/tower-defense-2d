using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Spells
{
    public class BaseActiveSpell : BaseSpell
    {
        protected float currentIntervalManaCostTime = 0;
        [SerializeField] protected float manaCostInterval = 0.1f;

        public override void UpdateSpell()
        {
            currentIntervalManaCostTime += Time.deltaTime;

            if(currentIntervalManaCostTime >= manaCostInterval)
            {
                PayMana();
                currentIntervalManaCostTime = 0f;
            }
        }

        public override void PayMana()
        {
            int pay = Mathf.RoundToInt(manaCost * manaCostInterval);
            storage.ReferenceStorage.ManaController.Substract(pay);
        }

        public override TooltipSpellDescriptionData CreateTooltipData()
        {
            TooltipSpellDescriptionData data = base.CreateTooltipData();
            data.cooldown = "No cooldown";
            data.cost = "Mana cost: " + manaCostInterval + " per second";
            return data;
        }
    }
}
