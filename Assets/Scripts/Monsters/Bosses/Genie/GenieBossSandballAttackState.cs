using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Animations;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Creatures
{
    public class GenieBossSandballAttackState : BaseBossState<GenieBossBehaviour>
    {
        [Header("Values")]
        [SerializeField] private IntervalData pathInterval;
        [SerializeField] private float horizontalUpAdmissibleAngle;
        [SerializeField] private float horizontalDownAdmissibleAngle;
        [SerializeField] private RangeFloat sandballMovementSpeedRange;
        [SerializeField] private float range;

        [Header("Times")]
        [SerializeField] private float animationTime;

        [Header("Snowball prefab")]
        [SerializeField] private Sandball sandballPrefab;

        [Header("References")]
        [SerializeField] private Transform sandballSpawnPoint;

        /////// references
        private List<Tower> currentTowers;
        ///////

        private Tower potentialTarget;

        public override void PreInit(GenieBossBehaviour boss, DataStorage storage)
        {
            base.PreInit(boss, storage);
            currentTowers = storage.ReferenceStorage.TowersController.CurrentTowers;
            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);
        }

        public override void Init(GenieBossBehaviour boss, DataStorage storage)
        {
            base.Init(boss, storage);
            ThrowToTarget(potentialTarget);
        }

        public override void UpdateState()
        {

        }

        public override void Deinit()
        {

        }

        private void ThrowToTarget(ITargetable target)
        {
            if (target == null)
            {
                Debug.LogError("Target is null");
                boss.SetDefaultMovementState();
                return;
            }

            pathInterval.nextValue = boss.DistanceTravelled + pathInterval.baseInterval + Random.Range(pathInterval.intervalRandomRange.a, pathInterval.intervalRandomRange.b);

            boss.Renderer.transform.rotation = Quaternion.Euler(0f, boss.GetCenter().x > target.GetCenter().x ? 180f : 0f, 0f);
            boss.Animator.Play("ThrowSandball");
        }

        //Called by animator
        private void SpawnSandball()
        {
            Sandball sandball = Instantiate(sandballPrefab);
            sandball.gameObject.SetActive(true);
            sandball.transform.position = sandballSpawnPoint.position;
            float sandballMovementSpeed = Mathf.Lerp(sandballMovementSpeedRange.a, sandballMovementSpeedRange.b, Mathf.InverseLerp(0f, range, Vector3.Distance(sandballSpawnPoint.position, potentialTarget.GetCenter())));
            sandball.Setup(potentialTarget, sandballMovementSpeed);

            storage.ReferenceStorage.UpdatablesController.generalUpdatables.AddUpdatable(sandball);
            boss.SetDefaultMovementState();
        }

        public override bool IsReadyForWork()
        {
            if (boss.DistanceTravelled >= pathInterval.nextValue)
            {
                List<Tower> potentialTargets = new List<Tower>();

                foreach (Tower tower in currentTowers)
                {
                    if (Vector3.Distance(boss.GetCenter(), tower.GetCenter()) <= range && tower.Builder.Builded && tower.Builder.Repairer.Efficient > 0 && IsAngleAdmissible(boss.GetCenter(), tower.GetCenter()))
                        potentialTargets.Add(tower);
                }

                if (potentialTargets.Count > 0)
                {
                    potentialTarget = potentialTargets[Random.Range(0, potentialTargets.Count - 1)];
                    return true;
                }
            }

            return false;
        }

        private bool IsAngleAdmissible(Vector3 source, Vector3 target)
        {
            if (source.x > target.x) //we move to easier calculate angles
            {
                target.x += (source.x - target.x) * 2;
            }

            Vector2 direction = target - source;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            return (angle >= -horizontalDownAdmissibleAngle && angle <= horizontalUpAdmissibleAngle);
        }

        private void OnDrawGizmosSelected()
        {
            //float range = this.range;
            //float angle = horizontalAdmissibleAngle;
            //float sideHeight = range * Mathf.Sin(angle * Mathf.Deg2Rad);

            float aAzimuth = 0;
            float aAltitudeUp = (horizontalUpAdmissibleAngle) * Mathf.Deg2Rad;
            float aAltitudeDown = (horizontalDownAdmissibleAngle) * Mathf.Deg2Rad;
            float cUp = Mathf.Cos(aAltitudeUp);
            float cDown = Mathf.Cos(aAltitudeDown);
            Vector3 rightdirectionUp = new Vector3(Mathf.Cos(aAzimuth) * cUp, Mathf.Sin(aAltitudeUp), Mathf.Sin(aAzimuth) * cUp);
            Vector3 leftdirectionUp = new Vector3(-Mathf.Cos(aAzimuth) * cUp, Mathf.Sin(aAltitudeUp), Mathf.Sin(aAzimuth) * cUp);
            Vector3 rightdirectionDown = new Vector3(Mathf.Cos(aAzimuth) * cDown, -Mathf.Sin(aAltitudeDown), Mathf.Sin(aAzimuth) * cDown);
            Vector3 leftdirectionDown = new Vector3(-Mathf.Cos(aAzimuth) * cDown, -Mathf.Sin(aAltitudeDown), Mathf.Sin(aAzimuth) * cDown);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + rightdirectionUp * range);
            Gizmos.DrawLine(transform.position, transform.position + leftdirectionUp * range);
            Gizmos.DrawLine(transform.position, transform.position + rightdirectionDown * range);
            Gizmos.DrawLine(transform.position, transform.position + leftdirectionDown * range);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
}
