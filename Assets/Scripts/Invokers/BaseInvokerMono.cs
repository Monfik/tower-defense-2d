using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Invokers
{
    public class BaseInvokerMono<T> : BaseInitable where T : ICommandable
    {
        [SerializeField] protected List<T> commandsList;
        public List<T> CommandsList => commandsList;
        protected Queue<T> commands = new Queue<T>();
        protected ICommandable currentCommandable;

        protected UnityAction OnInvokerFinished;
        protected UnityAction OnInvokerFailed;

        protected bool executing = false;

        public void Init(DataStorage storage, UnityAction OnInvokerFinished, UnityAction OnInvokerFailed)
        {
            base.Init(storage);
            this.OnInvokerFinished = OnInvokerFinished;
            this.OnInvokerFailed = OnInvokerFailed;
            CreateQueue();
        }

        private void CreateQueue()
        {
            foreach(T command in commandsList)
            {
                commands.Enqueue(command);
            }
        }

        public virtual void Execute()
        {
            if (commands.Count > 0)
            {
                currentCommandable = commands.Peek();
                currentCommandable.InitCommand(storage, OnCommandFinished, OnCommandFailed);
                executing = true;
            }
            else
            {
                Debug.LogError("No commands to execute");
            }
        }

        public void UpdateInvoker()
        {
             currentCommandable?.UpdateCommand();
        }

        protected virtual void OnCommandFinished()
        {
            commands.Dequeue();
            if(commands.Count > 0)
            {
                Execute();
            }
            else
            {
                currentCommandable = null;
                FinishInvoker();
            }
        }

        protected void OnCommandFailed()
        {

        }

        protected virtual void FinishInvoker()
        {
            executing = false;
            OnInvokerFinished?.Invoke();
        }
    }
}
