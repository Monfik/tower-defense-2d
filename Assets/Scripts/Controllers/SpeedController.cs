using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedController : MonoBehaviour
{
    [SerializeField] private float timeSpeed = 1f;
    public float TimeSpeed => timeSpeed;

    [Header("Buttons")]
    [SerializeField] private Button slow;
    [SerializeField] private Button stop;
    [SerializeField] private Button fastx1;
    [SerializeField] private Button fastx2;
    [SerializeField] private Button fastx3;

    [SerializeField] private List<Button> buttons;

    [Header("Sprites")]
    [SerializeField] private Sprite stopSprite;
    [SerializeField] private Sprite playSprite;


    [Space]
    [SerializeField] private CanvasGroup canvasGroup;

    private bool timeStopped = false;
    private DataStorage storage;

    public void Init(DataStorage storage)
    {
        this.storage = storage;
        slow.onClick.AddListener(() => { SetTimeScale(0.5f); HandleClickedButton(slow); });
        stop.onClick.AddListener(() => { Stop(); HandleClickedButton(stop); });
        fastx1.onClick.AddListener(() => { SetTimeScale(1f); HandleClickedButton(fastx1); });
        fastx2.onClick.AddListener(() => { SetTimeScale(2f); HandleClickedButton(fastx2); });
        fastx3.onClick.AddListener(() => { SetTimeScale(4f); HandleClickedButton(fastx3); });
        canvasGroup.interactable = true;
        SetTimeScale(1f);

        HandleClickedButton(fastx1);

        storage.EventsStorage.CoreEvnts.OnGamePaused += Pause;
        storage.EventsStorage.CoreEvnts.OnGameUnPaused += Unpause;
        storage.EventsStorage.CoreEvnts.OnLevelStartedBasic += SetupDefault;
        storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic += SetupDefault;
        storage.EventsStorage.CoreEvnts.OnBaseDestroyed += SetupDefault;
    }

    private void SetTimeScale(float scale)
    {
        timeSpeed = scale;
        Time.timeScale = scale;
    }

    private void Stop()
    {
        timeStopped = !timeStopped;

        SetTimeScale(0f);
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Unpause()
    {
        Time.timeScale = timeSpeed;
    }

    private void HandleClickedButton(Button button)
    {
        foreach(Button b in buttons)
        {
            b.interactable = true;
        }

        button.interactable = false;
    }

    public void SetupDefault()
    {
        SetTimeScale(1f);
    }

    public void Deinit()
    {
        timeStopped = false;
        SetTimeScale(1f);
        canvasGroup.interactable = false;
        slow.onClick.RemoveAllListeners();
        stop.onClick.RemoveAllListeners();
        fastx1.onClick.RemoveAllListeners();
        fastx2.onClick.RemoveAllListeners();
        fastx3.onClick.RemoveAllListeners();

        storage.EventsStorage.CoreEvnts.OnGamePaused -= Pause;
        storage.EventsStorage.CoreEvnts.OnGameUnPaused -= Unpause;
        storage.EventsStorage.CoreEvnts.OnLevelStartedBasic -= SetupDefault;
        storage.EventsStorage.CoreEvnts.OnLevelFinishedBasic -= SetupDefault;
        storage.EventsStorage.CoreEvnts.OnBaseDestroyed -= SetupDefault;
    }
}
