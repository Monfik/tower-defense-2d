using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Effects
{
    public class Fire : MapEffect
    {
        private FireData fireData;

        [SerializeField]
        protected Animator animator;

        private bool goingOut = false;

        [SerializeField]
        private FlameEffect flame;

        [SerializeField]
        private MonsterFlameDictionary pairs;

        private List<Monster> firedMonsters = new List<Monster>();

        public void InitData(DataStorage storage, int fireDamagePerSecond, float duration)
        {
            this.storage = storage;
            this.duration = duration;
            fireData = new FireData();
            fireData.dmgPerSecond = fireDamagePerSecond;
            fireData.duration = duration;
        }

        public void InitData(DataStorage storage, FireData fireData)
        {
            this.storage = storage;
            this.fireData = fireData;
            this.duration = fireData.duration;
        }

        public override void UpdateEffect()
        {
            base.UpdateEffect();

            if(IsGoingOut(currentTime, duration-1))
            {
                PutOutFire();
            }
        }

        private bool IsGoingOut(float currentTime, float goingOutTime)
        {
            return currentTime >= goingOutTime && !goingOut;
        }

        private void PutOutFire()
        {
            animator.SetTrigger("GoingDown");
            goingOut = true;
        }

        public void OnTriggerStay2D(Collider2D collision)
        {
            MonsterLegsCollider monsterLegs = collision.gameObject.GetComponent<MonsterLegsCollider>();          

            if (monsterLegs != null)
            {
                Monster monsterInFlame = monsterLegs.Owner;

                if (monsterInFlame.Dead)
                    return;

                bool exists = pairs.ContainsKey(monsterInFlame);
                if (!exists)
                {
                    FlameEffect spawnedFlame = Instantiate(flame);
                    spawnedFlame.transform.SetParent(monsterInFlame.transform);
                    spawnedFlame.transform.localPosition = Vector3.zero;
                    spawnedFlame.gameObject.SetActive(true);
                    spawnedFlame.duration = fireData.duration;
                    spawnedFlame.SetDmg(fireData.dmgPerSecond);
                    monsterInFlame.Effects.AddEffect(spawnedFlame);
                    pairs[monsterInFlame] = spawnedFlame;
                }
                else //exist
                {
                    FlameEffect flame = pairs[monsterInFlame];
                    flame.ResetTimer();
                }
            }
        }
    }

    [System.Serializable]
    public class FireData
    {
        public float duration;
        public int dmgPerSecond;
        public int fireArmorReduction;
    }

    [System.Serializable]
    public class MonsterFlameDictionary : SerializableDictionary<Monster, FlameEffect> { }
}
