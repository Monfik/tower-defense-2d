using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Updatables;
using UnityEngine;
using UnityEngine.Events;

namespace LevelLogic
{
    public class LevelFinisher : BaseUpdatableMonoless
    {
        private List<ILevelFinishable> levelFinishers = new List<ILevelFinishable>();
        public List<ILevelFinishable> LevelFinishers => levelFinishers;

        private Level level;
        private UnityAction<Level> OnFinishedCompleted;

        public LevelFinisher(Level level, UnityAction<Level> OnFinishedCompleted)
        {
            this.level = level;
            this.OnFinishedCompleted = OnFinishedCompleted;
        }

        public void AddFinisher(ILevelFinishable finishable)
        {
            levelFinishers.Add(finishable);
        }

        public void StartFinishing()
        {
            if(levelFinishers.Count == 0)
            {
                Finish();
            }
            levelFinishers.ForEach(c => c.StartFinish(OnFinisherFinished, level));
        }

        private void OnFinisherFinished(ILevelFinishable finishable)
        {
            levelFinishers.Remove(finishable);
            CheckForFinish();
        }

        private void CheckForFinish()
        {
            if (levelFinishers.Count == 0)
                Finish();
        }

        public override void UpdateUpdatable()
        {
            levelFinishers.ForEach(c => c.UpdateFinisher());
        }

        protected override void Finish()
        {
            OnFinishedCompleted?.Invoke(level);
            base.Finish();
        }
    }
}
