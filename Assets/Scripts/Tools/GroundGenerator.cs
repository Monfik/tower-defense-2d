using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject prrefab;

    public Vector2 gridSize;

    [ContextMenu("Generate")]
    public void Generate()
    {
        Vector2 gridColliderSize = prrefab.GetComponent<BoxCollider2D>().size;

        for (int i = 0; i < gridSize.x; i++)
        {
            for (int j = 0; j < gridSize.y; j++)
            {
                Vector3 elementPosition;
                elementPosition.x = gridColliderSize.x * i;
                elementPosition.y = gridColliderSize.y * j;
                elementPosition.z = 0f;
                GameObject ob = Instantiate(prrefab);
                ob.transform.position = elementPosition;
            }
        }
    }

}
