using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace LevelLogic
{
    public class PathSnapper : MonoBehaviour
    {
        [SerializeField] private float snapDistance;

        private Map map;
        private bool snapped = false;
        private Vector2 currentMousePosition;
        public float pathHoverDetectThreshold = 0.1f;

        public void Init(Map map)
        {
            this.map = map;
        }

        public bool Snap(Vector2 mousePosition, out Vector2 snapPosition)
        {
            snapped = false;
            Vector2 closestPoint = mousePosition;

            int mask = 1 << Values.LayerMasks.RoadLayer;
            Collider2D coll = Physics2D.OverlapPoint(mousePosition, mask);

            if (coll != null)
            {
                Road road = coll.GetComponent<Road>();
                if (road != null)
                {
                    if (road.paths.Count > 0)
                    {
                        closestPoint = road.paths[0].path.GetClosestPointOnPath(mousePosition);
                        snapped = true;
                    }
                }
            }

            snapPosition = closestPoint;
            return snapped;
        }

        //private void OnDrawGizmos()
        //{
        //    if (map == null)
        //        return;

        //    if (!snapped)
        //        return;

        //    foreach (PathCreator path in map.MiddlePaths)
        //    {
        //        Gizmos.DrawSphere(path.path.GetClosestPointOnPath(currentMousePosition), 1f);
        //    }
        //}

        public PathCreator GetClosestPath(Vector2 position)
        {
            PathCreator closest = null;
            float minDistance = 1000f;

            foreach (PathCreator path in map.MiddlePaths)
            {
                Vector2 pathClosestPoint = path.path.GetClosestPointOnPath(position);
                float distance = Vector2.Distance(position, pathClosestPoint);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closest = path;
                }
            }

            return closest;
        }

        public PathCreator GetPathOverMouse(Vector2 mousePosition)
        {
            PathCreator closestPath = null;

            int mask = 1 << Values.LayerMasks.RoadLayer;
            Collider2D coll = Physics2D.OverlapPoint(mousePosition, mask);

            if (coll != null)
            {
                Road road = coll.GetComponent<Road>();
                if (road != null)
                {
                    if (road.paths.Count == 0)
                        return null;

                    float currentMinDistance = 1000f;

                    foreach(PathCreator path in road.paths)
                    {
                        Vector2 closestPoint = path.path.GetClosestPointOnPath(mousePosition);
                        float distance = Vector2.Distance(mousePosition, closestPoint);
                        if (distance < currentMinDistance)
                        {
                            closestPath = path;
                            currentMinDistance = distance;
                        }
                    }
                }
            }

            return closestPath;
        }

        public List<PathCreator> GetPathsOverMouse(Vector2 mousePosition)
        {
            List<PathCreator> hoveredPaths = new List<PathCreator>();

            int mask = 1 << Values.LayerMasks.RoadLayer;
            Collider2D coll = Physics2D.OverlapPoint(mousePosition, mask);
            
            if(coll != null)
            {
                Road road = coll.GetComponent<Road>();
                if(road != null)
                {
                    foreach(PathCreator path in road.paths)
                    {
                        Vector2 closestPoint = path.path.GetClosestPointOnPath(mousePosition);
                        float distance = Vector2.Distance(mousePosition, closestPoint);
                        if(distance < pathHoverDetectThreshold)
                        {
                            hoveredPaths.Add(path); //question is to sort it?
                        }
                    }

                    if (hoveredPaths.Count == 0)
                        hoveredPaths.AddRange(road.paths);
                }
            }

            return hoveredPaths;
        }
    }

    public class PathDistanceHandler
    {
        public PathCreator path;
        public float distance;

        public PathDistanceHandler(PathCreator path, float distance)
        {
            this.path = path;
            this.distance = distance;
        }
    }
}
