using Data;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Creatures;
using TowerDefense.Tips;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.Towers
{
    public class FireStoneAttackSystem : BaseAttackSystem
    {
        [SerializeField]
        private List<FireStoneMissile> fireStoneMissiles;

        [SerializeField]
        private FireStoneMissile missilePrefab;

        [Header("Data")]
        [SerializeField]
        private FireStoneData stoneData;

        public override void Attack()
        {
            FireStoneMissile spawnedStone = GameObject.Instantiate(missilePrefab, attackableTower.transform);
            spawnedStone.Init(target, storage, attackableTower.RealRange, stoneData, OnExploded);
            spawnedStone.gameObject.SetActive(true);
            spawnedStone.transform.position = attackableTower.transform.position;
            fireStoneMissiles.Add(spawnedStone);

            foreach (AttackableTowerEntity animabeEntity in attackableTower.GraphicsSystem.GraphicsData.attackableAnimators)
            {
                animabeEntity.PlayAttackAnimation();
            }

            DamageItself();
        }

        public override void Init(AttackableTower attackable, DataStorage storage)
        {
            base.Init(attackable, storage);
            stoneData.Reset();
        }

        public override void Upgrade(int level)
        {
            base.Upgrade(level);
            stoneData.minExploDamage.Upgrade(level);
            stoneData.maxExploDamage.Upgrade(level);
            stoneData.fireDamagePerSecond.Upgrade(level);
            stoneData.fireDuration.Upgrade(level);
            stoneData.armorReduction.Upgrade(level);
        }

        private void OnExploded(FireStoneMissile stoneMissile)
        {
            fireStoneMissiles.Remove(stoneMissile);
            Destroy(stoneMissile.gameObject);
        }

        public override void UpdateAttackSystem()
        {
            base.UpdateAttackSystem();

            for (int i = 0; i < fireStoneMissiles.Count; i++)
            {
                fireStoneMissiles[i].UpdateMissile();
            }
        }

        protected override void OnTargetNotReachable(Monster monster)
        {
            //base.OnTargetNotReachable(monster);
            UpdateState = SearchTarget;
        }

        public override void ComposeBasicInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerBasicGUIText damageModel = infoPanel.BasicTextes.GetElement();
            damageModel.Text.text = "Explosion damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
            TowerBasicGUIText armorReductionModel = infoPanel.BasicTextes.GetElement();
            armorReductionModel.Text.text = "Explosion armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
            TowerBasicGUIText fireDmgModel = infoPanel.BasicTextes.GetElement();
            fireDmgModel.Text.text = "Fire damage per second: " + stoneData.fireDamagePerSecond.data.GetGUILabel();
            TowerBasicGUIText fireDurationModel = infoPanel.BasicTextes.GetElement();
            fireDurationModel.Text.text = "Fire duration: " + stoneData.fireDuration.data.GetGUILabel();
            TowerBasicGUIText attackSpeedModel = infoPanel.BasicTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();

            UnityAction DamageUpdated = () => { damageModel.Text.text = "Explosion damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.minExploDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.maxExploDamage));
            UnityAction ArmorReductionUpdated = () => { armorReductionModel.Text.text = "Explosion armor reduction: " + stoneData.armorReduction.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, stoneData.armorReduction));
            UnityAction AttackSpeedUpdated = () => { attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
            UnityAction FireDamageUpdated = () => { fireDmgModel.Text.text = "Fire damage per second: " + stoneData.fireDamagePerSecond.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(FireDamageUpdated, stoneData.fireDamagePerSecond));
            UnityAction FireDurationUpdated = () => { fireDurationModel.Text.text = "Fire duration: " + stoneData.fireDuration.data.GetGUILabel(); };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(FireDurationUpdated, stoneData.fireDuration));
        }

        public override void ComposeUpgradeInfo(TowerGuiInfoPanel infoPanel)
        {
            TowerUpgradeGuiText damageModel = infoPanel.UpgradeTextes.GetElement();
            damageModel.Text.text = "Explosion damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
            damageModel.UpgradeText.text = stoneData.minExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel() + " - " + stoneData.maxExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText armorReductionModel = infoPanel.UpgradeTextes.GetElement();
            armorReductionModel.Text.text = "Explosion armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
            armorReductionModel.UpgradeText.text = stoneData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText fireDmgModel = infoPanel.UpgradeTextes.GetElement();
            fireDmgModel.Text.text = "Fire damage per second: " + stoneData.fireDamagePerSecond.data.GetGUILabel();
            fireDmgModel.UpgradeText.text = stoneData.fireDamagePerSecond.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText fireDurationModel = infoPanel.UpgradeTextes.GetElement();
            fireDurationModel.Text.text = "Fire duration: " + stoneData.fireDuration.data.GetGUILabel();
            fireDurationModel.UpgradeText.text = stoneData.fireDuration.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            TowerUpgradeGuiText attackSpeedModel = infoPanel.UpgradeTextes.GetElement();
            attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
            attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();

            UnityAction DamageUpdated = () => {
                damageModel.Text.text = "Explosion damage: " + stoneData.minExploDamage.data.GetGUILabel() + " - " + stoneData.maxExploDamage.data.GetGUILabel();
                damageModel.UpgradeText.text = stoneData.minExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel() + " - " + stoneData.maxExploDamage.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.minExploDamage));
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(DamageUpdated, stoneData.maxExploDamage));
            UnityAction ArmorReductionUpdated = () => {
                armorReductionModel.Text.text = "Explosion armor reduction: " + stoneData.armorReduction.data.GetGUILabel();
                armorReductionModel.UpgradeText.text = stoneData.armorReduction.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(ArmorReductionUpdated, stoneData.armorReduction));
            UnityAction AttackSpeedUpdated = () => {
                attackSpeedModel.Text.text = "Attack speed: " + attackSpeed.data.GetGUILabel();
                attackSpeedModel.UpgradeText.text = attackSpeed.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(AttackSpeedUpdated, attackSpeed));
            UnityAction FireDamageUpdated = () => {
                fireDmgModel.Text.text = "Fire damage per second: " + stoneData.fireDamagePerSecond.data.GetGUILabel();
                fireDmgModel.UpgradeText.text = stoneData.fireDamagePerSecond.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(FireDamageUpdated, stoneData.fireDamagePerSecond));
            UnityAction FireDurationUpdated = () => {
                fireDurationModel.Text.text = "Fire duration: " + stoneData.fireDuration.data.GetGUILabel();
                fireDurationModel.UpgradeText.text = stoneData.fireDuration.GetNextUpgradeDataBasedOnCurrent().GetGUILabel();
            };
            attackableTower.GuiManager.guiElementableActionController.Add(new GUIElementableEventPair(FireDurationUpdated, stoneData.fireDuration));
        }

        public override void OnTowerDemolished()
        {
            
        }

        public override UnityAction BuffDamageByMultiplier(float multiplier)
        {
            int minDamageBuff = (int)Mathf.Round(stoneData.minExploDamage.data.coreValue * multiplier);
            stoneData.minExploDamage.data.BuffByValue(minDamageBuff);
            int maxDamageBuff = (int)Mathf.Round(stoneData.maxExploDamage.data.coreValue * multiplier);
            stoneData.maxExploDamage.data.BuffByValue(maxDamageBuff);
            int fireDamageBuff = (int)Mathf.Round(stoneData.fireDamagePerSecond.data.coreValue * multiplier);
            stoneData.fireDamagePerSecond.data.BuffByValue(fireDamageBuff);

            UnityAction OnDebuff = () => {
                stoneData.minExploDamage.data.BuffByValue(-minDamageBuff);
                stoneData.maxExploDamage.data.BuffByValue(-maxDamageBuff);
                stoneData.fireDamagePerSecond.data.BuffByValue(-fireDamageBuff);
            };

            return OnDebuff;
        }

        public override void SetupDamageMultiplier(float multiplier)
        {
            stoneData.minExploDamage.data.SetupMultiplier(multiplier);
            stoneData.maxExploDamage.data.SetupMultiplier(multiplier);

            for (int i = 0; i < stoneData.minExploDamage.upgrades.Count; i++)
            {
                TowerStaticsData upgradeMinData = stoneData.minExploDamage.upgrades[i];
                upgradeMinData.SetupMultiplier(multiplier);

                TowerStaticsData upgradeMaxData = stoneData.maxExploDamage.upgrades[i];
                upgradeMaxData.SetupMultiplier(multiplier);
            }

            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(stoneData.minExploDamage);
            attackableTower.GuiManager.guiElementableActionController.CallActionIfExists(stoneData.maxExploDamage);
        }

        public override DescriptionData CreateTooltipData(DescriptionData data)
        {
            data = base.CreateTooltipData(data);

            data.title2 = "Physical and Fire AOE damage";

            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("<sprite=5>Physical AOE", $"{stoneData.minExploDamage.data.coreValue} - {stoneData.maxExploDamage.data.coreValue}"));
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("Armor reduction", stoneData.armorReduction.data.coreValue.ToString()));
            data.specifiedDatas.Add(new DescriptionSingleSpecifiedData("<sprite=1>Fire per second", $"{stoneData.fireDamagePerSecond.data.coreValue}"));

            return data;
        }
    }

    [System.Serializable]
    public class FireStoneData
    {
        public UpgradableStaticsData minExploDamage;
        public UpgradableStaticsData maxExploDamage;
        public UpgradableStaticsData fireDamagePerSecond;
        public UpgradableStaticsData armorReduction;
        public UpgradableStaticsData fireDuration;

        public void Reset()
        {
            minExploDamage.Reset();
            maxExploDamage.Reset();
            fireDamagePerSecond.Reset();
            armorReduction.Reset();
            fireDuration.Reset();
        }
    }
}
